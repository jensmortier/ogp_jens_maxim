package hillbillies.exceptions;

/**
 * Exception used by Cube class
 *
 * @version 1.0
 * @author  Maxim Verbiest
 */
public class CubeException extends Exception {

        public static final int NO_CUBES_FOUND = 2;
        public static final int NULL_VALUE_AS_PARAM = NO_CUBES_FOUND*2;
        public static final int COULDNT_ADD_GAME_OBJECT = NULL_VALUE_AS_PARAM *2;


        private String error_message;
        private int error_code;

        public CubeException(int error_code,String message) {
            super();
            error_message = message;
            this.error_code = error_code;
        }

        @Override
        public String getMessage() {
            return error_message;
        }

        public int getErrorCode() {
            return error_code;
        }
    }
