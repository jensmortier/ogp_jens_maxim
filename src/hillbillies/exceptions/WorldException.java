package hillbillies.exceptions;


/**
 * Exception class used by the World class
 *
 * @version 1.0
 * @author Jens Mortier
 */
public class WorldException extends Exception {
    public static final int INVALID_TERRAIN_TYPE = 2;
    public static final int FACTION_REQUESTED_NOT_FOUND = 4;
    public static final int CANNOT_ADD_GAME_OBJECT = 8;
    public static final int FILTER_CONFLICT = 2*CANNOT_ADD_GAME_OBJECT;
    public static final int TYPE_MATCHING_ERROR  = 2*FILTER_CONFLICT;
    public static final int NULL_VALUE_AS_PARAMETER = 64;
    public static final int ADVANCE_TIME_ERROR = 128;
    public static final int CONFLICT_IN_GENERATING_RANDOM_VALID_POS = 2*ADVANCE_TIME_ERROR;
    public static final int CONFLICT_WHILE_GENERATING_UNIT = 2*CONFLICT_IN_GENERATING_RANDOM_VALID_POS;

    int code;

    public WorldException(int code,String message) {
        super(message);
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
