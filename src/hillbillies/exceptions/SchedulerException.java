package hillbillies.exceptions;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Raw;

/**
 * Exception class used by a Scheduler
 * @version 1.0
 * @author Maxim Verbiest
 */
public class SchedulerException extends Exception{

    public static final int FACTION_ASSIGNMENT_ERROR = 2;
    public static final int EQUAL_ORIGINAL_AND_NEW = 2*FACTION_ASSIGNMENT_ERROR;
    public static final int NULL_VALUE_PARAM = 2*EQUAL_ORIGINAL_AND_NEW;
    public static final int NO_FREE_TASKS_AVAILABLE = 2*NULL_VALUE_PARAM;
    public static final int SCHEDULE_CONFLICT = 2*NO_FREE_TASKS_AVAILABLE;
    public static final int UNMARK_CONFLICT = 2*SCHEDULE_CONFLICT;
    public static final int REPLACEMENT_CONFLICT = 2*UNMARK_CONFLICT;
    public static final int ITERATOR_EMPTY_EXCEPTION = 2 * REPLACEMENT_CONFLICT;

    private int errorCode;

    /**
     * Constructor
     * @param code  The error code
     * @param message   The message of this error
     */
    public SchedulerException(int code, String message){
        super(message);
        this.errorCode = code;
    }

    @Basic @Raw
    public int getErrorCode(){
        return this.errorCode;
    }

}
