package hillbillies.exceptions;

/**
 * Exceptions used by Task class
 * @version 1.0
 * @author Maxim Verbiest
 */
public class TaskException extends Exception {

    public static final int NULL_VALUE_AS_PARAMETER = 2;
    public static final int TASK_ALREADY_ASSIGNED = 2*NULL_VALUE_AS_PARAMETER;
    public static final int TASK_NOT_ASSIGNED = 2*TASK_ALREADY_ASSIGNED;
    public static final int INVALID_UNIT = 2*TASK_NOT_ASSIGNED;
    public static final int INVALID_STATUS_MANIPULAITON = 2*INVALID_UNIT;
    public static final int INVALID_ASSIGNMENT = 2*INVALID_STATUS_MANIPULAITON;
    public static final int DEASSIGNMENT_ERROR = 2*INVALID_ASSIGNMENT;
    public static final int INVALID_ACTIVITY = 2 * DEASSIGNMENT_ERROR;
    public static final int ALREADY_IN_SCHEDULER = 2*INVALID_ACTIVITY;
    public static final int NO_CUBE_AT_COORDINATE = 2*ALREADY_IN_SCHEDULER;
    public static final int REPLACEMENT_CONFLICT = 2*NO_CUBE_AT_COORDINATE;

    int code;

    public TaskException(int code, String message){
        super(message);
        this.code = code;
    }

    public int getCode(){
        return code;
    }

}
