package hillbillies.exceptions;

/**
 * Exception class used by position class
 *
 * @version 1.0
 * @author Jens Mortier
 */
public class PositionException extends Exception {
    public static final int POSITION_OUT_OF_BOUNDS = 2;



    private String error_message;
    private int error_code;

    public PositionException(int error_code,String message) {
        super();
        error_message = message;
        this.error_code = error_code;
    }

    @Override
    public String getMessage() {
        return error_message;
    }

    public int getErrorCode() {
        return error_code;
    }
}
