package hillbillies.exceptions;

/**
 * Exception used by game object class
 *
 * @version 1.0
 * @author  Jens Mortier
 */
public class GameObjectException extends Exception {

    public GameObjectException(String message) {
        super(message);
    }
}
