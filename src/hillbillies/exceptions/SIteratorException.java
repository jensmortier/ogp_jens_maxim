package hillbillies.exceptions;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Raw;

/**
 * Exception class used by SIterator
 * @version 1.0
 * @author Maxim Verbiest
 */
public class SIteratorException extends Exception {

    public static final int NULL_VALUE_PARAM = 2;

    private int errorCode;

    public SIteratorException(int errorCode, String message){
        super(message);
        this.errorCode = errorCode;
    }

    @Basic @Raw
    public int getCode(){
        return errorCode;
    }
}
