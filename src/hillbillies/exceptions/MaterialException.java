package hillbillies.exceptions;

import hillbillies.gameworld.Material;

/**
 * Exception used by material class
 *
 * @version 1.0
 * @author Maxim Verbiest
 */
public class MaterialException extends GameObjectException {
    public static final int NULL_VALUE_AS_PARAMETER = 2;
    public static final int IMPOSSIBLE_METHOD_INVOCATION = 4;
    public static final int INVALID_POSITION = 8;
    public static final int MATERIAL_MANIPULATION_FAILED = 16;
    public static final int FALLING_CONFLICT = 32;
    public static final int DROP_CONFLICT = 64;


    private String error_message;
    private int error_code;

    public MaterialException(int error_code, String message) {
        super(message);
        this.error_code = error_code;
    }

    public int getErrorCode() {
        return error_code;
    }
}
