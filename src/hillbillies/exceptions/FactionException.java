package hillbillies.exceptions;


/**
 * Exception used by faction class
 *
 * @version 1.0
 * @author Jens Mortier
 */
public class FactionException extends Exception{
    public final static int UNIT_CANT_BE_ADDED = 2;
    public final static int CANNOT_REMOVE_UNIT_FROM_FACTION = 4;
    public final static int INDEX_OUT_OF_RANGE = 8;
    public final static int SCHEDULER_ASSIGNEMENT_ERROR = 2*INDEX_OUT_OF_RANGE;

    private int code;

    public FactionException(int code,String message) {
        super(message);
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
