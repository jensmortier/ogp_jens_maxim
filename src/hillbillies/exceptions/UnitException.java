package hillbillies.exceptions;

/**
 * Exception class used by the unit class
 *
 * @version 1.0
 * @author Jens Mortier
 */
public class UnitException extends GameObjectException {
	public static final int POSITION_OUT_OF_BOUNDS = 2;
	public static final int INVALID_NAME_INPUT = 4;
	public static final int INVALID_POSITION = 8;
	public static final int INVALID_ORIENTATION = 16;
	public static final int ALREADY_FOLLOWING = 2*INVALID_ORIENTATION;
	public static final int CANNOT_WORK_AT_TARGET = 64;
	public static final int ATTACKING_CONFLICT = 128;
	public static final int INVALID_SECONDS_INPUT = 256;
	public static final int NOT_ALLOWED_TO_CHANGE_STATUS = 512;
	public static final int INVALID_XP_INPUT = 1024;
	public static final int CANNOT_LEVEL_UP = 2048;
	public static final int CANNOT_HAVE_AS_FACTION = 2 * CANNOT_LEVEL_UP;
	public static final int CANNOT_MOVE_TO_FACTION = 2 * CANNOT_HAVE_AS_FACTION;
	public static final int NO_MATERIAL_CARRYING = 2 * CANNOT_MOVE_TO_FACTION;
	public static final int INVALID_MATERIAL_INPUT = 2 * NO_MATERIAL_CARRYING;
	public static final int ALREADY_CARRYING_THIS_MATERIAL = 2 * INVALID_MATERIAL_INPUT;
	public static final int RAISE_METHOD_CONFLICT = 2 * ALREADY_CARRYING_THIS_MATERIAL;
	public static final int ATTACKING_TEAMMATE = 2*RAISE_METHOD_CONFLICT;
	public static final int WORK_METHOD_CONFLICT = 2*ATTACKING_TEAMMATE;
	public static final int NO_SUCH_METHOD = 2*WORK_METHOD_CONFLICT;
	public static final int DEFAULT_BEHAVIOUR_CONFLICT = 2*NO_SUCH_METHOD;
	public static final int FALLING_CONFLICT = 2*DEFAULT_BEHAVIOUR_CONFLICT;
	public static final int DODGE_CONFLICT = 2*FALLING_CONFLICT;
	public static final int NULL_VALUE_AS_PARAMETER	= 2*DODGE_CONFLICT;
	public static final int HAS_ALREADY_A_TASK_ASSIGNED = 2*NULL_VALUE_AS_PARAMETER;
	public static final int TASK_RELATIONSHIP_CONFLICT = 2*HAS_ALREADY_A_TASK_ASSIGNED;
	public static final int SCHEDULER_INTERRUPTION_CONFLICT = 2*TASK_RELATIONSHIP_CONFLICT;
	public static final int TASK_EXECUTION_ERROR = 2*SCHEDULER_INTERRUPTION_CONFLICT;
	public static final int CANNOT_DODGE = 2 * TASK_EXECUTION_ERROR;

	private int error_code;
	
	public UnitException(int error_code,String message) {
		super(message);
		this.error_code = error_code;
	}
	
	public int getErrorCode() {
		return error_code;
	}
}
