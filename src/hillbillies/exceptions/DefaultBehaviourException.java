package hillbillies.exceptions;

/**
 * Exception used by Default Behaviour package
 *
 * @version 1.0
 * @author Maxim Verbiest
 */
public class DefaultBehaviourException extends Exception {

    public static final int INSTANCE_COULD_NOT_BE_CREATED = 2;
    public static final int NULL_VALUE_AS_PARAMETER = 4;
    public static final int WRONG_PARAMETER = 8;
    public static final int CONFLICT_OF_EXECUTION_IN_DB = 2*WRONG_PARAMETER;
    public static final int POSITION_GENERATOR_CONFLICT = 2*CONFLICT_OF_EXECUTION_IN_DB;
    public static final int LOAD_CONFLICT = 2*POSITION_GENERATOR_CONFLICT;

    private String error_message;
    private int error_code;

    public DefaultBehaviourException(int error_code,String message) {
        super();
        error_message = message;
        this.error_code = error_code;
    }

    @Override
    public String getMessage() {
        return error_message;
    }

    public int getErrorCode() {
        return error_code;
    }
}
