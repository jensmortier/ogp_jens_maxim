package hillbillies.model;

import be.kuleuven.cs.som.annotate.Value;

/**
 * The type of work a unit can perform
 *
 * @version 1.0
 * @author Maxim Verbiest
 */
@Value
public enum WorkType {
    DROP_LOG,
    DROP_BOULDER,
    CONSUME_BOULDER_AND_LOG,
    PICK_UP_BOULDER,
    PICK_UP_LOG,
    COLLAPSE_WOOD_CUBE,
    COLLAPSE_ROCK_CUBE,
    NOTHING,

}
