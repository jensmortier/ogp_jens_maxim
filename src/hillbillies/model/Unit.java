package hillbillies.model;
import hillbillies.exceptions.*;
import hillbillies.gameworld.*;
import hillbillies.scheduler.execution.components.Task;
import hillbillies.scheduler.execution.components.TaskStatus;
import hillbillies.scheduler.execution.runtime.ExecutionException;
import hillbillies.scheduler.execution.runtime.Executor;
import hillbillies.util.*;
import ogp.framework.util.*;
import be.kuleuven.cs.som.annotate.*;

import java.util.LinkedList;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * A class of units which represent entities that can move, work, rest or fight on a game map. Involving staminapoints,
 * hitpoints, a name, weight, strength, agility and toughness.
 * 
 * @invar	The positions, including current position, target position and the position of the next cube have to be 
 * 			valid for every unit, i.e. within the bounds of the game world
 * 			| isValidPosition(getPositionX(),getPositionY(),getPositionZ())
 * 			| isValidPosition(getTargetPositionX(),getTargetPositionY(),getTargetPositionZ())
 * 			| isValidPosition(getCubePositionX(),getCubePositionY(),getCubePositionZ())
 * @invar 	The Unit's cube must be in line with the Unit's current position
 * 			|World.getInstance().getCubeByVector(getCurrentPosition().toCube().toVector) == getCurrentCube()
 * 
 * @invar	The orientation for every unit has to be a valid orientation, i.e. a angle in radians between 0 and 2 pi
 * 			| isValidOrientation(getOrientation())
 * 
 * @invar	The Weight for every unit has to be a valid weight
 * 			| isValidWeight(getWeight())
 * 
 * @invar	Each unit can have its stamina points as its stamina points
 * 			| canHaveAsPoints(getStaminaPoints())
 * 
 * @invar	Each unit can have its hitpoints as its hitpoints
 *			| canHaveAsPoints(getHitpoints())
 * 
 * @invar	The name of each unit has to be a valid name
 * 			| isValidName(getName())
 * 
 * @invar	Each unit has to have a valid strength
 * 			| isValidStrength(getStrength())
 * 
 * @invar	Each unit has to have a valid agility
 * 			| isValidAgility(getAgility())
 * 
 * @invar	Each unit has to have a valid toughness
 * 			| isValidToughness(getToughness())
 * 
 * @invar	Each unit can have it's movement status as its movement status
 * 			| canHaveAsMovementStatus(getMovementStatus())
 *
 * @author  Jens Mortier, HIRB Fase 3
 * 				jens.mortier@student.kuleuven.be
 * 			Maxim Verbiest, HIRB Fase 3
 * 				maxim.verbiest@student.kuleuven.be
 * 
 * @Repository https://bitbucket.org/hirbdevs/ogp_jens_maxim
 * 
 * @version 1.0
 */

public class Unit extends GameObject {


	/**
	 * Constant containing the minimal initial value of the Unit's attributes
	 */
	private static final int MIN_INITIAL_VALUE = 25;

	/**
	 * Constant containing the maximal initial value of the Unit's attributes
	 */
	private static final int MAX_INITIAL_VALUE = 100;

	/**
	 * The Unit's current position
	 */
	private Position<Double> current_position;

	/**
	 * Next cube we are walking to
	 */
	private Cube next_cube;

	/**
	 * The position to which the unit is moving
	 */
	private Position<Double> target_position;
	
	/**	
	 * Contains the orientation of the unit's face
	 */
	private double orientation = Math.PI / 2;
	
	/**
	 * Weight of the Unit
	 */
	private int Weight;
	
	/**
	 * Strength of the Unit
	 */
	private int Strength;
	
	/**
	 * Agility of the Unit
	 */
	private int Agility;
	
	/**
	 * Toughness of the Unit
	 */
	private int Toughness; 
	
	/**
	 * The Name of the Unit
	 */
	private String Name;

	/**
	 * the Hitpoints of the Unit
	 */
	private int Hitpoints;
	
	/**
	 * Number of Hitpoints the Unit has recovered
	 */
	private int recovered_hitpoints;
	
	/**
	 * The time the unit is currently sprinting
	 */
	private double time_sprinting = 0;
	
	/**
	 * The Stamina points of the Unit
	 */
	private int StaminaPoints;
	
	/**
	 * The minimal points a Unit can have
	 */
	private static int minPoints = 0 ;
	
	/**
	 * To let the Unit know it must stop walking at the next cube
	 */
	private boolean stopAtNextCube = false;

	/**
	 * Contains current movement status
	 */
	private MovementType MovementStatus;
	
	/**
	 * Contains current type of work to be done
	 */
	private WorkType WorkToBeDone = WorkType.NOTHING;
	
	/**
	 * Contains the duration of the current work task
	 */
	private double durationOfWork;

	/**
	 * Contains the duration of the current fight
	 */
	private double secondsFighting = 0;
	
	/**
	 * Contains the game time passed after stamina points or hitpoints are increased
	 */
	private double secondsRestingAfterUpdate = 0;
	
	/**
	 * True if Unit is currently in default behaviour
	 */
	private boolean DefaultBehaviourStatus;
		
	/**
	 * How long it has been a Unit has rested
	 */
	private double lastRest = 0;
	
	/**
	 * The attackers of this Unit
	 */
	private ReverseStack<Unit> Attacker = null;
	
	/**
	 * The Action stack of this Unit
	 */
	ActionStack<MovementType> actionStack;

	/**
	 * List containing path to walk over
	 */
	private LinkedList<Pair<Integer,Cube>> cubePath;

	/**
	 * List containing the cubes already checked in pathing algorithm
	 */
	private LinkedList<Cube> checkedCubes;

	/**
	 * The seconds a Unit is defending itself in one fight
	 */
	private double secondsDefending = 0;

	/**
	 * True if the Unit has started defending itself.
	 */
	private boolean startedDefending = false;

	/**
	 * The Unit this Unit is attacking
	 */
	private Unit Defender = null ;

	/**
	 * true if the Unit has started attacking another Unit.
	 */
	private boolean startedAttacking = false;

	/**
	 * Unit's experience points
	 */
	private int experiencePoints = 0;

	/**
	 * The new XP earned after a Unit finished a task
	 */
	private int newXPEarned = 0;

	/**
	 * The boulder a Unit is carrying
	 */
	private Boulder boulder = null;

	/**
	 * The log a Unit is carrying
	 */
	private Log log = null;

	/**
	 * Faction to which unit belongs
	 */
	private Faction faction;

	/**
	 * The cube where a Unit has to work on
	 */
	private Cube workCube = null;

	/**
	 * Whether the Unit has started falling or not
	 */
	private boolean startedFalling = false;

	/**
	 * A Task this Unit should perform
	 */
	private Task assignedTask = null;

	/**
	 * Whether unit is following other unit
	 */
	private boolean isFollowing = false;

	/**
	 * Contains follow refresh rate
	 */
	private final double FOLLOW_REFRESH_RATE = 5;

	/**
	 * Contains time since last following update in seconds
	 */
	private double timeSinceFollowUpdate = FOLLOW_REFRESH_RATE;

	/**
	 * The target this Unit must follow
	 */
	private Unit followedTarget = null;

	/**
	 * The target position of the Target before an advanceTime
	 */
	private Position<Integer> followPosition = null;

	/**
	 * Constructor
	 * @param name The Unit's name
	 * @param initXPos	The Unit's x position
	 * @param initYPos 	The Unit's y position
	 * @param initZPos 	The Unit's z position
	 * @param initWeight 	The Unit's weight
	 * @param initAgility 	The Unit's agility
	 * @param initStrength 	The Unit's strength
	 * @param initToughness	The Unit's toughness
	 * @param enableDefaultBehavior	True if the Unit must start in its default behaviour
	 * @effect super()
	 * 			|super(World.getInstance().getCubeByKey()
	 * @post If an error should occur during super() method, this initialisation finished right away
	 * 			|if(isTerminated())
	 * @effect setName()
	 * 			|setName(name)
	 * @effect setCurrentPosition()
	 * 			|setCurrentPosition(initXPos,initYPos,initZPos)
	 * @effect setInitialAgility()
	 * 			|setInitialAgility(initAgility)
	 * @effect setInitialStrength()
	 * 			|setInitialStrength(initStrength)
	 * @effect setInitialToughness()
	 * 			|setInitialToughness(initToughness)
	 * @effect setInitialWeight()
	 * 			|setInitialWeight(initWeight)
	 * @effect setHitpoints()
	 * 			|setHitpoints(getMaxPoints())
	 * @effect setStaminaPoints()
	 *			|setStaminaPoints(getMaxPoints())
	 * @post The action stack will be instantiated
	 * 			|new.getActionStack() == new ActionStack<MovementType>()
	 * @post The reverse stack of Attacker will be instantiated
	 * 			|new.getAttacker() == new ReverseStack()
	 * @effect setMovementStatus()
	 * 			|if(enableDefaultBehavior)
	 * 			| then setMovementStatus(MovementType.DEFAULT_BEHAVIOUR)
	 * 			|else setMovementStatus(MovementType.DOING_NOTHING)
	 * @effect setDefaultBehaviourStatus()
	 * 			|setDefaultBehaviourStatus(enableDefaultBehavior)
	 *
     * @throws UnitException
	 * 			|setName()
	 * 			|setCurrentPosition()
	 * 			|setMovementStatus()
     */
	public Unit(String name,double initXPos, double initYPos, double initZPos, int initWeight, int initAgility, int initStrength,
				int initToughness,boolean enableDefaultBehavior) throws UnitException {
		//Call game object
		super(World.getInstance().getCubeByKey((int)Math.floor(initXPos),(int)Math.floor(initYPos),(int)Math.floor(initZPos)));

		//If already terminated, stop constructor
		if(isTerminated()) return;

		this.setName(name);
		//Set current position
		try {
			this.setCurrentPosition(new Position<Double>(initXPos,initYPos,initZPos));
		}catch(PositionException e) {
			throw new UnitException(UnitException.INVALID_ORIENTATION,e.getMessage());
		}
		//Set initial properties
		this.setInitialAgility(initAgility);
		this.setInitialStrength(initStrength);
		this.setInitialToughness(initToughness);
		this.setInitialWeight(initWeight);
		this.setHitPoints(this.getMaxPoints());
		this.setStaminaPoints(this.getMaxPoints());

		//Initialize action stack
		actionStack = new ActionStack<MovementType>();
		//Initialize attackers
		Attacker = new ReverseStack<>();

		//Set default behaviour
		setMovementStatus(enableDefaultBehavior? MovementType.DEFAULT_BEHAVIOUR : MovementType.DOING_NOTHING);
		setDefaultBehaviourStatus(enableDefaultBehavior);

	}

	/**
	 * Destructor of Unit object
	 * @effect super.terminate()
	 * @effect getLog().Drop()
	 * @effect getBoulder.Drop()
	 * @effect getFaction().removeUnit()
	 * 			|if(getFaction() != null)
	 * 			| then getFaction().removeUnit(this)
	 * @effect interruptTask()
	 */
	public void terminate()  {

		super.terminate();

		try{
			//Drop what it is carrying
			getLog().Drop();
			getBoulder().Drop();

			//Remove Unit from faction
			if(getFaction() != null) faction.removeUnit(this);

			//If Unit was executing some task, let the scheduler know this must be interrupted
			interruptTask();

		} catch (MaterialException |UnitException | FactionException e){
			//Nothing has to be done, wasn't carrying it or Unit was already removed from its Faction
		}
	}

	@Basic @Raw
	public Faction getFaction() {
		return faction;
	}

	@Basic @Raw
	public boolean isFollowing(){return isFollowing;}

	/**
	 * Sets the faction to the given faction
	 * @param faction	The given faction
	 * @post The faction will be set to the given faction
	 * 			|new.getFaction() == faction
	 * @throws UnitException
	 * 			|if(!canHaveAsFaction(faction))
	 */
	void setFaction(Faction faction) throws UnitException {
		if(!canHaveAsFaction(faction)) throw new UnitException(UnitException.CANNOT_HAVE_AS_FACTION,"Faction inappropriate");
		this.faction = faction;
	}

	/**
	 * Transfers this Unit to the given faction
	 * @param faction	The given faction
	 * @effect getFaction.removeUnit()
	 * 			|getFaction.removeUnit(this)
	 * @effect setFaction()
	 * 			|setFaction(faction)
	 * @effect faction.addUnit()
	 * 			|faction.addUnit(this)
	 * @throws UnitException
	 * 			thrown by getFaction().removeUnit()
	 * 			thrown by faction.addUnit()
     */
	public void transferToFaction(Faction faction) throws UnitException {
		try {
			//Set new faction
			Faction formerFaction = getFaction();
			setFaction(faction);

			//Delete from former faction
			formerFaction.removeUnit(this);

			//Add to new faction
			faction.addUnit(this);
		}catch(FactionException e) {
			throw new UnitException(UnitException.CANNOT_MOVE_TO_FACTION,e.getMessage());
		}
	}

	/**
	 * Checks whether the Unit can have a given faction as its faction
	 * @param faction	The given faction
	 * @return true if the given faction has no null reference, is not terminated and the Unit itself is still alive
	 * 			|result == faction != null && !faction.isTerminated() && !isTerminated()
     */
	public boolean canHaveAsFaction(Faction faction) {
		return faction != null && !faction.isTerminated() && !isTerminated();
	}

	/**
	 * Sets the default behaviour status to the given boolean value
	 * @param bool	The given boolean value
	 * @post The DB status will be set to the given value
	 * 			|new.getDefaultBehaviourStatus == bool
     */
	public void setDefaultBehaviourStatus(boolean bool){
		DefaultBehaviourStatus = bool;
	}
	
	@Basic @Raw
	public boolean getDefaultBehaviourStatus(){
		boolean result = DefaultBehaviourStatus;
		//this.setDefaultBehaviourStatus(false);//So we don't go in default behaviour endlessly
		return result;
	}
	
	@Basic @Raw
	public int getRecovered_hitpoints() {
		return recovered_hitpoints;
	}

	/**
	 * Advances the game time for this Unit
	 * @param gameTime	The time advanced
	 * @effect Raise()
	 * @effect Rest()
	 * @effect checkStability()
	 * @effect updatesWhileFollowing()
	 * @effect setMovementStatus()
	 * @effect Work()
	 * @effect Rest()
	 * @effect Walk()
	 * @effect Sprint()
	 * @effect StartDefaultBehaviour()
	 * @effect Attack()
	 * @effect Defend()
	 * @effect fall()
	 * @throws UnitException
     */
	@Override
	public void advanceTime(double gameTime) throws UnitException {

		//Check if an Attributed can be raised by new XP earned
		Raise();
		
		//Automatically start resting every 3 minutes of game time
		if(lastRest >= 60 * 3) {
			Rest();
		}

		//Check if the Unit must start falling
		checkStability();

		//Check if this Unit is following some target and if so, update the target position
		if(isFollowing && Util.fuzzyGreaterThanOrEqualTo(timeSinceFollowUpdate,FOLLOW_REFRESH_RATE)) {
			updatesWhileFollowing();
			timeSinceFollowUpdate = 0;
		}else if(isFollowing) {
			timeSinceFollowUpdate ++;
		}else {
			timeSinceFollowUpdate = FOLLOW_REFRESH_RATE;
		}
		
		if(this.MovementStatus != MovementType.RESTING) lastRest += gameTime;

		//Execute current action
		switch(this.MovementStatus) {
			case WORKING:
				this.Work(gameTime);
				break;
			case RESTING:
				this.Rest(gameTime);
				break;
			case WALKING:
				this.Walk(gameTime);
				break;
			case SPRINTING:
				if(!this.Sprint(gameTime)) {
					if(MovementStatus == MovementType.WALKING) {
						Walk(gameTime);
					}
				}
				break;
			case DEFAULT_BEHAVIOUR:
				this.StartDefaultBehaviour(gameTime);
				break;
			case FIGHTING:
				this.Attack(gameTime);
				break;
			case DEFENDING:
				this.Defend(gameTime);
				break;
			case DOING_NOTHING:
				//Default behaviour is disabled
				break;
			case FALLING:
				Fall(gameTime);
				break;
				
		}
	}

	/**
	 * Sets the Unit's current position to a given position
	 * @param position The given position
	 * @post The Unit's current position will be set to the given position
     */
	public void setCurrentPosition(Position<Double> position) {
		current_position = position;
	}

	/**
	 * Sets the target position to a given position
	 * @param position The given position
	 * @post The target position will be set to the given position
	 * 			|new.getTargetPosition() == position
     */
	public void setTargetPosition(Position<Double> position) {
		target_position = position;
	}

	@Basic
	public Position<Double> getTargetPosition() {
		return target_position;
	}
	
	@Basic
	public Position<Double> getCurrentPosition() {
		return current_position;
	}
	

	@Basic @Raw
	public double getOrientation() {
		return orientation;
	}
	
	/**
	 * Checks whether a given orientation is valid
	 * @param orientation	Orientation to check
	 * @return True if orientation is between 0 and 2*pi
	 * 			|result == orientation >= 0 && orientation <= 2 * Math.PI
	 */
	@Raw
	public static boolean isValidOrientation(double orientation) {
		return orientation >= 0 && orientation <= 2 * Math.PI;
	}
	
	/**
	 * Sets orientation if valid
	 * @param orientation	Orientation to set
	 * @post	Makes a valid angle (between 0 and 2 * pi) and sets it in the orientation attribute
	 * 			| orientation = orientation < 0 ? 2 * PI + orientation : orientation
	 * 			| while !isValidOrientation(orientation)
	 * 			| do orientation -= 2 * PI
	 * 			| this.getOrientation() == orientation
	 */
	@Raw
	public void setOrientation(double orientation) {
		//First make sure we have a positive angle
		double pos_angle = orientation < 0 ? 2 * Math.PI + orientation : orientation;
		
		//Now check if valid
		while(!isValidOrientation(pos_angle)) {
			pos_angle -= 2 * Math.PI;
		}
		
		//Now set this angle
		this.orientation = pos_angle;
	}

	/**
	 * Sets the movement status to a given status
	 * @param status	The given status
	 * @throws UnitException
     */
	public void setMovementStatus(MovementType status) throws UnitException {

		//Check if action stack is not empty and no significant job has to be executed
		boolean isPriority = false;
		if (!actionStack.isEmpty() && (status == MovementType.DEFAULT_BEHAVIOUR || status == MovementType.DOING_NOTHING)){
			//Apparently it is! So give stack priority
			status = actionStack.getNextAction();
			
			//Get next action has priority over canHaveMovementStatus
			isPriority = true;
		}
		
		//Now execute regular setter
		if(canHaveMovementStatus(status) || isPriority) {
			//If we are going to walk and currently aren't doing default behaviour, disable it
			/**if(this.MovementStatus != MovementType.DEFAULT_BEHAVIOUR && status != MovementType.DEFAULT_BEHAVIOUR){
				this.setDefaultBehaviourStatus(false);
			}*/
			
			//If we are going to stop resting, reset secondsRestingAfterUpdate
			if(MovementStatus == MovementType.RESTING && status != MovementType.RESTING){
				secondsRestingAfterUpdate = 0;
			}
			
			//If recovered, don't start resting
			if(status == MovementType.RESTING && isSPFullyRecovered() && isHPFullyRecovered()){
				this.setMovementStatus(getDefaultBehaviourStatus() ? MovementType.DEFAULT_BEHAVIOUR : MovementType.DOING_NOTHING);
			}else{
				this.MovementStatus = status;
			}
			
			//If we start to rest, reset the variable which counts duration since last rest
			if(status == MovementType.RESTING) {
				lastRest = 0;
			}
			
			//We changed status, so we need to reset the recovered hitpoints
			if(!(status == MovementType.RESTING && MovementStatus == MovementType.RESTING)) {
				recovered_hitpoints = 0;
			}

			System.out.println("Current status to " + getMovementStatus().toString());
		}else {
			throw new UnitException(UnitException.NOT_ALLOWED_TO_CHANGE_STATUS,"It's not allowed to change the unit's status to " + status.toString());
		}
	}
	
	/**
	 * Checks whether movement status to be assigned to this unit is valid
	 * @param status	Movement status to inspect
	 * @return	Sprinting is only possible if stamina points are greater than 0 and unit has to be already moving
	 * 			| !(status == MovementType.SPRINTING && this.getStaminaPoints <= 0)
	 * 			| && !(status == MovementType.SPRINTING && this.getMovementStatus != MovementType.WALKING)
	 * 			| && !(this.getMovementStatus == MovementType.RESTING && status != MovementType.FIGHTING)
	 * 			| && !(this.getRecovered_hitpoints < 1) 
	 */
	public boolean canHaveMovementStatus(MovementType status) {

		//Falling will have priority over everything
		if(status == MovementType.FALLING) return true;

		//Falling may not be interrupted
		if(getMovementStatus() == MovementType.FALLING && status != MovementType.DOING_NOTHING) return false;

		//When we are moving, setting to default behaviour is not allowed
		//if(isMoving() && status == MovementType.DEFAULT_BEHAVIOUR) return false;
		
		//When we want to sprint and have no staminapoints, this is not allowed
		if(status == MovementType.SPRINTING && StaminaPoints <= 0) return false;
		
		//Sprinting not possible if not WALKING
		if(status == MovementType.SPRINTING && MovementStatus != MovementType.WALKING) return false;

		//Add wildcard: if moving, interruption ok
		if(isMoving()) return true; //Note: very important to be set after sprinting and default behaviour decisions
		
		//If defending, we need to keep so
		if(MovementStatus == MovementType.DEFENDING && status != MovementType.DOING_NOTHING ) return false;
		
		//If we are fighting, don't stop fighting till the end
		if(MovementStatus == MovementType.FIGHTING && getSecondsFighting() < 1) return false;
		
		//If working not finished, don't start walking
		if(MovementStatus == MovementType.WORKING && (durationOfWork > 0)
				&& status == MovementType.WALKING) return false;
		
		//If we are resting and haven't recovered enough, keep resting, except if we are being attacked
		if(MovementStatus == MovementType.RESTING && (recovered_hitpoints < 1 && !isHPFullyRecovered())&& status != MovementType.DEFENDING) return false;
		
		//We are already in default behaviour
		//if(MovementStatus == MovementType.DEFAULT_BEHAVIOUR && status == MovementType.DEFAULT_BEHAVIOUR ) return false;
		
		return true;
	}
	
	@Basic @Raw
	public MovementType getMovementStatus() {
		return this.MovementStatus;
	}

	/**
	 * Let the Unit sprint
	 * @effect setMovementStatus()
	 * 			|if(isSprintingPossible())
	 * 			| then setMovementStatus(MovementType.SPRINTING)
	 */
	public void Sprint() {
		try {
			if(isSprintingPossible()) setMovementStatus(MovementType.SPRINTING);
		}catch(UnitException e) {
			//It's ok
		}
		
	}

	@Basic @Raw
	public double getTime_sprinting(){
		return time_sprinting;
	}

	/**
	 * Updates all sprinting aspects according to time advanced
	 * @param GameTime	The time advanced
	 * @post The time this Unit is sprinting will be incremented
	 * 			|if(isSprintingPossible())
	 * 			| then new.getTime_sprinting() == time_sprinting + GameTime
	 * @post The time this Unit is sprinting will be decreased if sprinting more than 0.1s
	 * 			|if(isSprintingPossible())
	 * 			| then if(getTime_sprinting() >= 0.1)
	 * 			|  then new.getTime_sprinting() == time_sprinting - 0.1
	 * @effect setStaminaPoints()
	 * 			|if(isSprintingPossible())
	 * 			| then if(getTime_sprinting() >= 0.1)
	 * 			|  then setStaminaPoints(getStaminaPoints - 1)
	 * @effect recalculatePosition()
	 * 			|if(isSprintingPossible())
	 * 			| then recalculatePosition(gameTime,getSprintingSpeed())
	 * @effect stopSprint()
	 * 			|if(!isSprintingPossible())
	 * 			| then StopSprint()
	 *
	 * @return True if the Unit is sprinting, and sprinting is still posible
	 * 			|if( isSprinting() && isSprintingPossible() )
	 * 			| then result == true
	 * 			|else result == false
	 * @throws UnitException
     */
	public boolean Sprint(double GameTime) throws UnitException {
		//Check if sprinting is possible
		if(isSprinting()) {
			if(isSprintingPossible()) {
				//Reduce stamina points
				time_sprinting = time_sprinting + GameTime;
				if (time_sprinting >= 0.1){
					setStaminaPoints(StaminaPoints - 1);
					time_sprinting = time_sprinting -0.1;
				}
				
				//Advance
				recalculatePosition(GameTime, getSprintingSpeed());
				return true;
			}else {
				//Stop sprinting
				StopSprint();
				return false;
			}
		}else {
			return false;
		}
	}

	/**
	 * Stop the sprint of the Unit
	 * @post The time this Unit is sprinting will be set to 0
	 * 			|new.getTime_sprinting() == 0
	 * @effect setMovementStatus()
	 * 			|if(getCurrentPosition.equals(getTargetPosition())
	 * 			| then if(getDefaultBehaviourStatus)
	 * 			|  then setMovementStatus(MovementType.DEFAULT_BEHAVIOUR)
	 * 			|  else setMovementStatus(MovementType.DOING_NOTHING)
	 * 			| else setMovementStatus(MovementType.WALKING)
	 */
	public void StopSprint() {
		try {
			time_sprinting = 0;
			if(current_position.equals(target_position)) {
				if (getDefaultBehaviourStatus()){
					this.setMovementStatus(MovementType.DEFAULT_BEHAVIOUR);
				} else {
					this.setMovementStatus(MovementType.DOING_NOTHING);
				}
			}else {
				setMovementStatus(MovementType.WALKING);
			}
		}catch(UnitException e) {
			//It's ok
		}
		
			
	}
	
	/**
	 * Checks if printing is possible.
	 * @return	True if sprinting is still possible
	 * 			| result == getStaminaPoints() > 0 && (getMovementStatus() == MovementType.WALKING || isSprinting())
	 */
	@Raw
	public boolean isSprintingPossible() {
		return StaminaPoints > 0 && (MovementStatus == MovementType.WALKING || isSprinting());
	}

	/**
	 * Checks whether Unit is sprinting
	 * @return true if Unit is sprinting
	 * 			|result == getMovementStatus() == MovementType.SPRINTING
     */
	@Raw
	public boolean isSprinting() {
		return MovementStatus == MovementType.SPRINTING;
	}

	@Basic @Raw
	public boolean getStopAtNextCube(){
		return stopAtNextCube;
	}
	
	/**
	 * Facade method to initiate Rest
	 * @throws 	UnitException
	 * 			|walkToCurrentCube()
	 * 			|interruptTask()
	 *
	 * @post 	if this Unit was moving while invoked, it will stop at the next cube
	 * 			|if(isMoving())
	 * 			| then new.getStopAtNextCube() == true
	 *
	 * @effect 	getActionStack().addAction
	 * 			if the Unit was still moving while invoking this action and it was not part of a task, this action will be placed on a stacktrace for later
	 * 			|if(isMoving())
	 * 			| then if(!interruptTask())
	 * 			|  then getActionStack().addAction(MovementType.RESTING)
	 * 			|		getActionStack().addAction(getMovementStatus())
	 *
	 * @effect	setMovementStatus()
	 * 			if(!isMoving()
	 * 			| then if(setMovementStatus(MovementType.RESTING)
	 *
	 * @effect 	interruptTask()
	 *
	 * @effect 	walkToCurrentCube()
	 * 			|if(isMoving())
	 * 			| then WalkToCurrentCube()
	 */
	public void Rest() throws UnitException {
		
		if (isMoving()){
			//Finish walk to next cube
			stopAtNextCube = true;
			
			//Walk to current cube
			walkToCurrentCube();

			//Check if the walking was part of some task
			if(!interruptTask()) {
				//Add current movement type as last action to stack
				getActionStack().addAction(getMovementStatus());

				//Add resting to stack
				getActionStack().addAction(MovementType.RESTING);
			}
		} else {
			//If the unit was working as a Task, this task must also be interrupted
			interruptTask();
			setMovementStatus(MovementType.RESTING);
		}
	}

	@Basic @Raw
	public double getSecondsRestingAfterUpdate(){
		return secondsRestingAfterUpdate;
	}

	/**
	 * Updates attributes while the Unit is resting
	 * @param GameTime The time advanced since previous update
	 * @post The seconds resting will be updated
	 * 			|new.getSecondsRestingAfterUpdate() == getSecondsRestingAfterUpdate + GameTime
	 * @effect recoverHitpoints()
	 * 			|if(getSecondsRestingAfterUpdate() >= getSecondsToRecoverOneHP())
	 * 			| then recoverHitpoints(1)
	 * @effect setStaminaPoints()
	 * 			|if(!isSPFullyRecovered())
	 * 			| then if(getSecondsRestingAfterUpdate >= getSecondsToRecoverOneSP)
	 * 			|  then setStaminaPoints(getStaminaPoints + 1)
	 * @effect setMovementStatus()
	 * 			|if(isHPFullyRecovered() && isSPFullyRecovered())
	 * 			| setMovementStatus(MovmentType.DOING_NOTHING)
     */
	public void Rest(double GameTime) {
		
		try {
			if (!isHPFullyRecovered()){
				
				//Recover hitpoints
				secondsRestingAfterUpdate +=  GameTime;
				if (Util.fuzzyGreaterThanOrEqualTo(secondsRestingAfterUpdate,getSecondsToRecoverOneHP())){
				recoverHitpoints(1);
				
				recovered_hitpoints += 1;
				secondsRestingAfterUpdate -= getSecondsToRecoverOneHP();
				}
				
			} else {
				// HP is fully recovered, now SP can start recovering
				if(!isSPFullyRecovered()){
					secondsRestingAfterUpdate +=  GameTime;
					if (Util.fuzzyGreaterThanOrEqualTo(secondsRestingAfterUpdate, getSecondsToRecoverOneSP())){
						this.setStaminaPoints(this.getStaminaPoints()+1);	
						secondsRestingAfterUpdate -= getSecondsToRecoverOneSP();
					}
				} else {
					secondsRestingAfterUpdate = 0;
						this.setMovementStatus(getDefaultBehaviourStatus() ? MovementType.DEFAULT_BEHAVIOUR : MovementType.DOING_NOTHING);
					
				}
			}
		}catch(UnitException e) {
			//It's ok
		}
		
	}
	
	/**
	 * Returns the seconds needed to rest to recover 1 HP
	 * @return result == (200/getToughness())*0.2
	 */
	@Raw
	private double getSecondsToRecoverOneHP(){
		return (((double) 200)/getToughness())*0.2;
	}
	
	/**
	 * Returns the seconds needed to rest to recover one Stamina point 
	 * @return result == (100/getToughness())*0.2
	 */
	@Raw
	private double getSecondsToRecoverOneSP(){
		return (((double) 100)/getToughness())*0.2;
	}
	
	/**
	 * Check if the Unit's Hitpoints are fully recovered
	 * @return 	true if the Unit's Hitpoints are fully recovered
	 * 			| result == (getHitpoints() == getMaxPoints())
	 */
	@Raw
	private boolean isHPFullyRecovered(){
		return (this.getHitPoints() == this.getMaxPoints());
	}
	
	/**
	 * Check if the Unit's Stamina points are fully recovered
	 * @return  true if the Unit's Stamina points are fully recovered
	 * 			| result == (getStaminaPoints() == getMaxPoints())
	 */
	@Raw
	private boolean isSPFullyRecovered(){
		return (this.getStaminaPoints() == this.getMaxPoints());
	}
	
	/**
	 * Recover the hitpoints of a Unit with a given value
	 * @param nrOfHitpoints	Hitpoints to be recovered
	 * @effect	The Unit's hitpoints will be increased by one
	 * 			|setHitPointsDouble(nrOfHitpoints)
	 */
	private void recoverHitpoints(double nrOfHitpoints) {
		this.setHitPoints(Hitpoints + nrOfHitpoints);
		
	}

	/**
	 * Let the Unit start walking
	 * @effect setMovementStatus()
	 * 			|setMovementStatus(MovementType.WALKING)
	 */
	public void Walk() {
		try {
			setMovementStatus(MovementType.WALKING);
		}catch(UnitException e) {
			//It's ok
		}
		
	}

	/**
	 * Do the actual walking
	 * @param GameTime The game time advanced since previous update
	 * @effect recalculatePosition()
	 * 			|recalculatePosition(GameTime,getWalkingSpeed())
	 * @throws UnitException
	 * 			thrown by recalculatePosition()
     */
	public void Walk(double GameTime) throws UnitException {
		//Recalculate effective position
		recalculatePosition(GameTime, getWalkingSpeed());
	}
	
	/**
	 * Check if a Unit has reached it next cube position while walking and update its position to that cube when true
	 * @throws 	UnitException
	 * 			 thrown by findNextCube()
	 * 
	 * @post	if a Unit has reached it next cube, update its position to that cube's position
	 * 			and find the next cube to walk to.
	 * 			| if (getPositionX() == getNextCubePositionX() && getPositionY() == nextCubePositionY()
	 * 			|	&& getPositionZ() == getNextCubePositionZ())
	 * 			|		then 	new.getPositionX == getNextCubePositionX
	 * 			|				new.getPositionY == getNextCubePositionY
	 * 			|				new.getPositionZ == getNextCubePositionZ
	 * 			|				findNextCube() 
	 * @effect	findNextCube
	 */
	private void CheckCube() throws UnitException {
		//Check if cube has to be recalculated
		Position<Double> center_next_cube = null;
		try {
			center_next_cube = new Position<Double>(next_cube.getPosition().getPosition_x().doubleValue() + World.LENGTH_X/2,
                                                                        next_cube.getPosition().getPosition_y().doubleValue() + World.LENGTH_Y / 2,
                                                                        next_cube.getPosition().getPosition_z().doubleValue() + World.LENGTH_Z / 2);
		} catch (PositionException e) {
			//Nothing happens
		}
		if(current_position.equals(center_next_cube,0.05)) {
			try{
				//Set current position
				current_position.setPositionX((Double)(next_cube.getPosition().getPosition_x().doubleValue() + World.LENGTH_X / 2));
				current_position.setPositionY((Double)(next_cube.getPosition().getPosition_y().doubleValue() + World.LENGTH_Y / 2));
				current_position.setPositionZ((Double)(next_cube.getPosition().getPosition_z().doubleValue() + World.LENGTH_Z / 2));

				//Modify current cube
				try {
					transferToCube(next_cube);

					//One experience point is gained when target cube reached
					if(!stopAtNextCube) increaseXP(1);
				} catch (GameObjectException e) {
					//Keep silent
				}
			}catch(PositionException e){/*DO nothing, exception impossible*/}

			if(!stopAtNextCube) {
				//We successfully reached this cube, so we earned an XP successfully if not stopping at next cube
				this.increaseXP(1);

				//Only search for next cube if we aren't stopping in the current one
				findNextCube();
			}
		}
	}
	
	/**
	 * Find the next Cube a Unit has to move towards to reach its position
	 * @throws 	UnitException
	 * 			|moveToAdjacent()
	 * 
	 * @post	if the Unit's x coordinate is lower than the target position, move right
	 * 			if the Unit's x coordinate is equal to the target position, stay at this x coordinate
	 * 			if the Unit's x coordinate is higher than the target position, move left
	 * 
	 * @post	if the Unit's y coordinate is lower than the target position, move up 
	 * 			if the Unit's y coordinate is equal to the target position, stay at this y coordinate
	 * 			if the Unit's y coordinate is higher than the target position, move down
	 * 
	 * @post	if the Unit's z coordinate is lower than the target position, move deeper
	 * 			if the Unit's z coordinate is equal to the target position, stay at this z coordinate
	 * 			if the Unit's z coordinate is higher than the target position, move back
	 * @effect
	 * 			|moveToAdjacent()
	 */
	private void findNextCube() throws UnitException{
		int new_x;
		int new_y;
		int new_z;

		//First, let's construct a bag with reachable cubes
		constructGoodCubes();

		//Let's see if we can still reach our target destination
		Number[] current_cube_position_number = currentCube.getPosition().toVector();
		Integer[] current_cube_position = new Integer[3];
		current_cube_position[0] = current_cube_position_number[0].intValue();
		current_cube_position[1] = current_cube_position_number[1].intValue();
		current_cube_position[2] = current_cube_position_number[2].intValue();
		Cube current_cube = World.getInstance().getCubeByVector(current_cube_position);

		if(!checkedCubes.contains(current_cube)) {
			//Our destination isn't reachable anymore, stop walking
			target_position = current_position;
			setMovementStatus(getDefaultBehaviourStatus() ? MovementType.DEFAULT_BEHAVIOUR : MovementType.DOING_NOTHING);
		}else {
			//Our destination is reachable, find next cube
			int m = World.GAME_WORLD_BOUND_X() * World.GAME_WORLD_BOUND_Y() * World.GAME_WORLD_BOUND_Z();
			Cube next = null;
			for(Pair<Integer,Cube> oppVector : cubePath) {
				Cube opp = oppVector.getSecondValue();
				if(opp.isNeighbourCube(current_cube) && oppVector.getFirstValue() < m) {
					next = opp;
					m = oppVector.getFirstValue();
				}
			}

			//Set next cube
			this.next_cube = next;
		}
	}

	/**
	 * Computes all possible next cubes
	 * @param cube	Cube to start from
	 * @param n0	Minimum index
	 */
	private void computePossibleNextCubes(Cube cube,int n0) {
		//Loop over all neighbouring cubes of our current cube
		for(int dx = -1;dx <= 1;dx++) {
			for(int dy = -1;dy <= 1;dy++) {
				for(int dz = -1;dz <= 1;dz++) {
					//Load new position
					Integer[] position = new Integer[3];
					position[0] = cube.getPosition().getPosition_x() + dx;
					position[1] = cube.getPosition().getPosition_y() + dy;
					position[2] = cube.getPosition().getPosition_z() + dz;

					//Load cube at this position from World
					Cube opportunity = World.getInstance().getCubeByVector(position);

					//If null, cube doesn't exist
					if(opportunity == null) continue;

					//Check if cube is passable, if not continue
					if(opportunity.getSceneryType().isSolid()) continue;

					//Check if cube neighbours solid terrain, if not continue
					if(!opportunity.hasSolidNeighbour()) continue;

					//Check if this combination already exists
					if(alreadyInListAfterIndex(n0,opportunity)) continue;

					//Seems okay to pass this cube
					cubePath.add(new Pair<>(n0 + 1,opportunity));
					checkedCubes.add(opportunity);
				}
			}
		}
	}

	/**
	 * Checks if this cube is already present after the index given
	 * @param n0	Index to start searching from
	 * @param cube	Cube to search for
	 * @return True if this cube is already in the list after the given Index
	 */
	private boolean alreadyInListAfterIndex(int n0,Cube cube) {
		for (Pair<Integer, Cube> vector : cubePath) {
			if (vector.getSecondValue().equals(cube)) return true;
		}

		return false;
	}

	/**
	 * Constructs list of cubes which can be passed and can be effective
	 */
	private void constructGoodCubes() {
		//Reset cube path
		cubePath = new LinkedList<>();
		checkedCubes = new LinkedList<>();

		//Calculate destination position
		Number[] destination = target_position.toVector();
		Integer[] dest_cube_conversion = {(int)Math.floor(destination[0].doubleValue()),(int)Math.floor(destination[1].doubleValue()),
				(int)Math.floor(destination[2].doubleValue())};
		Cube finalcube = World.getInstance().getCubeByVector(dest_cube_conversion);
		cubePath.add(new Pair<>(0,finalcube));
		checkedCubes.add(finalcube);

		//Load current cube instance
		Cube currentCube = this.currentCube;

		//Try to extend path
		int next_index = 0;
		while (!checkedCubes.contains(currentCube) && (next_index <= cubePath.size() - 1)) {
			computePossibleNextCubes(cubePath.get(next_index).getSecondValue(),cubePath.get(next_index).getFirstValue());
			next_index++;
		}
	}
	
	
	/**
	 * Recalculates the position, regarding to current speed and stops the walking if Unit has reached target position
	 * @param GameTime	The game time passed
	 * @param Speed		Speed of this unit
	 * @throws 	UnitException
	 * 			|CheckCube
	 * 			|setpositionX(getNextCubePositionX)
	 * 			|setpositionY(getNextCubePositionY)
	 * 			|setpositionZ(getNextCubePositionZ)
	 * 
	 * @Post	Orientation is updated
	 * 			
	 * @Post	Position is updated
	 *
	 * @post 	if reached the target position and the Unit was following a target: isFollowing will be set on false
	 * 			the position of the target to follow will get a null reference
	 * 			|if(Util.fuzzyEquals(x, target_position.getPosition_x(),0.05) && Util.fuzzyEquals(y, target_position.getPosition_y(),0.05)
	 * 			|	&& Util.fuzzyEquals(z, target_position.getPosition_z(),0.05))
	 * 			|then if(isFollowing)
	 * 			|		then new.isFollowing == false
	 * @post	if reached the target position and the Unit was following a target: the position of the target to follow will get a null reference
	 * 			|if(Util.fuzzyEquals(x, target_position.getPosition_x(),0.05) && Util.fuzzyEquals(y, target_position.getPosition_y(),0.05)
	 * 			|	&& Util.fuzzyEquals(z, target_position.getPosition_z(),0.05))
	 * 			|then if(isFollowing)
	 * 			|		then new.getFollowPosition == null
	 * @effect	setFollowTarget()	if reached the target position and the Unit was following a target: the target to follow will get a null reference
	 * 			|if(Util.fuzzyEquals(x, target_position.getPosition_x(),0.05) && Util.fuzzyEquals(y, target_position.getPosition_y(),0.05)
	 * 			|	&& Util.fuzzyEquals(z, target_position.getPosition_z(),0.05))
	 * 			|then if(isFollowing)
	 * 			|		then setFollowedTarget(null)
	 * @effect	setMovementStatus()	if reached target position
	 * 			|if(Util.fuzzyEquals(x, target_position.getPosition_x(),0.05) && Util.fuzzyEquals(y, target_position.getPosition_y(),0.05)
	 * 			|then new.getMovementStatus() == movementStatus.WALKING
	 */
	private void recalculatePosition(double GameTime,double Speed) throws UnitException {
		//First check cube
		CheckCube();
				
		//Calculate distance
			//Calculate diffs
		Number[] diffs;
		Position<Double> center_next_cube;
		try {
			center_next_cube = new Position<Double>(next_cube.getPosition().getPosition_x().doubleValue() + World.LENGTH_X/2,
					next_cube.getPosition().getPosition_y().doubleValue() + World.LENGTH_Y / 2,
					next_cube.getPosition().getPosition_z().doubleValue() + World.LENGTH_Z / 2);
		} catch (PositionException e) {
			//Nothing happens
			throw new UnitException(UnitException.INVALID_POSITION,"Cannot calculate center position of cube");
		}

		diffs = current_position.getDiff(center_next_cube);

		//Check now if we want to pause walking
			if(stopAtNextCube && Util.fuzzyEquals(diffs[0].doubleValue(), 0) && Util.fuzzyEquals(diffs[1].doubleValue(), 0) && Util.fuzzyEquals(diffs[2].doubleValue(), 0)) {
				//Report that we are at the next cube
				setMovementStatus(MovementType.DOING_NOTHING);
				
				//Reset stop at next cube
				stopAtNextCube = false;
				
				return;
			}
			
			//Calculate distance
			double distance = current_position.calculateDistanceTo(center_next_cube);
			
		//Calculate speed vector
		double v = Speed;
		double vx = ((-diffs[0].doubleValue()) / distance) * v;
		double vy = ((-diffs[1].doubleValue()) / distance) * v;
		double vz = ((-diffs[2].doubleValue()) / distance) * v;
		
		//Calculate new positions
		double x = vx * GameTime + (double)current_position.getPosition_x().doubleValue();
		double y = vy * GameTime + (double)current_position.getPosition_y().doubleValue();
		double z = vz * GameTime + (double)current_position.getPosition_z().doubleValue();

		System.out.println("Moving to position " + x + " , " + y + " , " + z);

        Position<Double> new_current_position = null;
        try {
            new_current_position = new Position<>(x,y,z);
        }catch(PositionException e) {
            throw new UnitException(UnitException.INVALID_POSITION,e.getMessage());
        }

		
		//Check if cube not surpassed
		if(Util.fuzzyEquals(x, target_position.getPosition_x(),0.05) && Util.fuzzyEquals(y, target_position.getPosition_y(),0.05) && Util.fuzzyEquals(z, target_position.getPosition_z(),0.05)) {
            try {
                new_current_position = new Position<>(center_next_cube);
            }catch(PositionException e) {
                //Do nothing
            }

			//Stop moving
			this.setMovementStatus(getDefaultBehaviourStatus() ? MovementType.DEFAULT_BEHAVIOUR : MovementType.DOING_NOTHING);

			//Check if Unit was following a target
			if(isFollowing){
				//Delete relations
				setFollowedTarget(null);
				followPosition = null;
				isFollowing = false;
			}
		}
		//Change position
		this.setCurrentPosition(new_current_position);

		//Change Current Cube
		try {
			this.setCurrentCube(World.getInstance().getCubeByKey(
					(int) Math.floor(new_current_position.getPosition_x()),
					(int) Math.floor(new_current_position.getPosition_y()),
					(int) Math.floor(new_current_position.getPosition_z())
			));
		} catch(GameObjectException e){
			throw new UnitException(UnitException.INVALID_POSITION,"Current cube position is set to invalid values");
		}
		//Change Orientation
		this.setOrientation(Math.atan2(vy, vx));
	}
	
	@Basic @Raw
    public Cube getNextCube() {
        return next_cube;
    }

	/**
	 * Set the initial Agility for this Unit to a given agility.
	 * 
	 * @param 	agility
	 * 			The new Agility for this Unit
	 * 
	 * @post 	
	 * 			| if((agility >= getMinInitialValue()) && (agility <= getMaxInitialValue())
	 * 			| 	then new.GetAgility() == agility
	 * 			| else if (agility < getMinInitialValue())
	 * 			| 	then new.GetAgility() == getMinInitialValue()
	 * 			| else if (agility > getMaxInitialValue())
	 * 				then new.GetAgility() == getMaxInitialValue()
	 * @effect	
	 * 			| result = makeValidInitial(agility) 
	 * 			
	 */
	public void setInitialAgility(int agility){
		this.Agility = this.makeValidInitial(agility);
	}
	
	/**
	 * Set the initial Toughness for this Unit to a given agility.
	 * 
	 * @param 	toughness
	 * 			The new Toughness for this Unit
	 * 
	 * @post 	
	 * 			| if((toughness >= getMinInitialValue()) && (toughness <= getMaxInitialValue())
	 * 			| 	then new.GetToughness() == toughness
	 * 			| else if(toughness < getMinInitialValue())
	 * 			| 	then new.GetToughness() == getMinInitialValue()
	 * 			| else if(toughness > getMaxInitialValue())
	 * 			|	then new.GetToughness() == getMaxInitialValue()
	 * 
	 * @effect
	 * 			| makeValidInitial(toughness) 
	 * 			
	 */
	public void setInitialToughness(int toughness){
		
		this.Toughness = makeValidInitial(toughness);
				
	}

	@Basic @Raw
	public Unit getFollowedTarget(){
		return followedTarget;
	}

	@Basic @Raw
	public Position<Integer> getFollowPosition(){
		return followPosition;
	}

	/**
	 * Set the target to follow to a given Unit
	 * @param newTarget	The given Unit
	 * @throws UnitException 	When this Unit is already following a Unit
	 * 							|if(getFollowedTarget() != null && newTarget != null)
	 * @post The followed target field will be set to the new target
	 * 		 |new.getFollowedTarget() == newTarget
     */
	public void setFollowedTarget(Unit newTarget) throws UnitException {
		if (getFollowedTarget() != null && newTarget != null) throw new UnitException(UnitException.ALREADY_FOLLOWING,"Already following another Unit");
		followedTarget = newTarget;
	}

	/**
	 * Let this Unit follow a given Unit
	 * @param 	target	The given Unit
	 * @throws 	UnitException	When an exception is thrown by setFollowedTarget()
	 * 							|setFollowedTarget(target)
	 * 							|moveTo()
	 *
	 * @effect 	moveTo()
	 * 		   	the position to move to will be a stable neighbouring cube position of the target.
	 * 		   	if there is no stable neighbour, the target's position itself will be chosen
	 * @post	followPosition will be set to the target position
	 * 			|new.getFollowPosition() == getTargetPosition().toCubePosition()
	 * @post	isFollowing will be set to true, such that the scheduler can check this
	 * 			|new.isFollowing == true
     */
	public void Follow(Unit target) throws UnitException{

		setFollowedTarget(target);

		try {
			Number[] targetCoordinates = target.getCurrentCube().getStableNeighbour().getPosition().toVector();
			moveTo(targetCoordinates[0].intValue(),targetCoordinates[1].intValue(),targetCoordinates[2].intValue());
		} catch(CubeException e){
			//This means no neighbouring cube at the same z-lever as the target's current cube could be found
			Number[] targetCoordinates = target.getCurrentCube().getPosition().toVector();
			//moveTo the target's current Cube instead. this will be possible, since that target is standing on it as well
			moveTo(targetCoordinates[0].intValue(),targetCoordinates[1].intValue(),targetCoordinates[2].intValue());
		}

		//set targetPosition to follow
		followPosition = getTargetPosition().toCubePosition();

		//Let Scheduler know this Unit has not finished following the target
		isFollowing = true;
	}

	/**
	 * Check for liveliness of target Unit and if so, check if the position to reach must be updated
	 * @effect TargetDeadUpdates()
	 * @effect followedPositionUpdate
	 * @throws UnitException
	 * 			|TargetDeadUpdates()
	 * 			|followedPositionUpdate()
     */
	public void updatesWhileFollowing() throws UnitException{

		//Check if the target is still alive and abort the following if not
		TargetDeadUpdates();

		//if the target is still alive, check if the position to reach has changed and update if necessary
		followedPositionUpdate();

	}

	/**
	 * If the Unit is still following some target, check if this target has moved since previous advanceTime and update the position to reach if so
	 * @effect	moveTo() When the target Unit has moved, this Unit's target position must be changed as well
	 * 			|if(!getFollowedTarget.getCurrentCube().getPosition().equals(followPosition)
	 * 			|then moveTo()
	 * 			The position to move to will be a stable neighbouring cube's position of the target's current position
	 * 			if there is no such position, it will be the target's current position itself
	 * @effect	followPosition.setPosition(getTargetPosition().toCubePosition())
	 * @throws UnitException	When an error occurs during an update of followPosition
	 * 							|getFollowPosition.setPosition(getTargetPosition.toCubePosition())
	 * 							When an invalid to position to moveTo was set
	 * 							|moveTo()
     */
	public void followedPositionUpdate() throws UnitException {
		//Check if this Unit is still following some target, otherwise this check is unnecessary
		if(! isFollowing()) return;
		//Check whether target Unit has moved
		if(!followedTarget.getCurrentCube().getPosition().equals(followPosition)){
			try {
				Number[] targetCoordinates = followedTarget.getCurrentCube().getStableNeighbour(true).getPosition().toVector();
				moveTo(targetCoordinates[0].intValue(),targetCoordinates[1].intValue(),targetCoordinates[2].intValue());
			} catch(CubeException e){
				//This means no neighbouring cube at the same z-lever as the target's current cube could be found
				Number[] targetCoordinates = followedTarget.getCurrentCube().getPosition().toVector();
				//moveTo the target's current Cube instead. this will be possible, since that target is standing on it as well
				moveTo(targetCoordinates[0].intValue(),targetCoordinates[1].intValue(),targetCoordinates[2].intValue());
			}
			//set targetPosition to follow
			try {
				followPosition.setPosition(getFollowedTarget().getCurrentCube().getPosition());
			} catch (PositionException e){
				throw new UnitException(UnitException.INVALID_POSITION,e.getMessage());
			}
		}
	}

	/**
	 * Check whether the followed target is dead, and update relationships if so
	 * @post 	if the followed target is dead, the position to reach regarding the target will be set to null
	 * 			|if (getFollowedTarget().isTerminated())
	 * 			|new.getFollowPosition() == null
	 * @post	if the followed target is dead, the Unit must stop following this dead target
	 * 			|if (getFollowedTarget().isTerminated())
	 * 			|new.isFollowing() == false;
	 * @effect	setFollowedTarget()
	 * 			if the followed target is dead, the target field will be set to null
	 * 			|if (getFollowedTarget().isTerminated())
	 * 			|then setFollowedTarget(null)
	 * @effect 	moveTo()
	 * 			If the followed target is dead, this Unit must abort it's walking
	 * 			|if(getFollowedTarget().isTerminated())
	 * 			|then moveTo(getCurrentCube().getPosition().getPosition_x(),getCurrentCube().getPosition().getPosition_y(),getCurrentCube().getPosition().getPosition_z())
	 * @throws UnitException
	 * 			|if (getFollowedTarget().isTerminated())
	 * 			|then setFollowedTarget(null)
     */
	public void TargetDeadUpdates() throws UnitException {
		//Check whether the target unit is still alive
		if(followedTarget.isTerminated()){
			//delete target to follow relation
			setFollowedTarget(null);
			//delete position to reach before next advance time relation
			followPosition = null;
			//stop following
			isFollowing = false;
			//The unit must stop waling
			this.moveTo(getCurrentCube().getPosition().getPosition_x(),getCurrentCube().getPosition().getPosition_y(),getCurrentCube().getPosition().getPosition_z());
		}
	}

	/**
	 * Set the initial strength for this Unit to a given strength.
	 * 
	 * @param 	strength
	 * 			The new strength for this Unit
	 * 
	 * @post 	
	 * 			| if((strength >= getMinInitialValue()) && (strength <= getMaxInitialValue())
	 * 			| 	then new.getStrength() == strength
	 * 			| else if(strength < getMinInitialValue())
	 * 			|	then new.getStrength() == getMinInitialValue())
	 * 			| else if(strength > getMaxInitialValue())
	 * 			|	then new.getStrength() == getMaxInitialValue()		
	 * @effect
	 * 			| makeValidInitial(strength)
	 * 			
	 */
	public void setInitialStrength(int strength) {
		this.Strength = this.makeValidInitial(strength);
	}
	
	
	/**
	 * Set the initial weight of a Unit, which is dependent on its Agility and Strength
	 * @param weight	The new weight for the unit
	 * 
	 * 
	 * @post	
	 * 			| if((weight >= minWeight ) && ( getMinInitialValue() <= weight <= getMaxInitialValue())
	 * 			| 	then new.getWeight() == weight
	 * 			| else if (weight < minWeight)
	 * 			|	then new.getWeight() == minWeight
	 * 
	 */
	public void setInitialWeight(int weight) {
		// Used to calculate the weight, this could result in a double
		// while we want an integer value
		double  fracture;
		int minWeight ; 
		
		fracture = ((this.Strength + this.Agility) / 2 );
		minWeight = (int) Math.floor(fracture);
		
		if (this.makeValidInitial(weight) >= minWeight) {
			this.Weight = this.makeValidInitial(weight);
		}
		else{
			this.Weight = minWeight; 
		}
	}
	
	
	/**
	 * Checks if a valid weight is given
	 * @param value	The value to check
	 * @return 	true if the given value is valid
	 * 			| result == value > getMinWeight() && value < getMaxValue()
	 */
	@Raw
	public boolean isValidWeight(int value){
		return value > getMinWeight() && value < getMaxValue() ;
		
	}
	
	/**
	 * Checks if valid strength is given
	 * @param value	The value to check
	 * @return	true if the given value is valid
	 * 			| result == value > getMinValue() && value < getMaxValue()
	 */
	@Raw
	public boolean isValidStrength(int value){
		return value > getMinValue() && value < getMaxValue();
	}
	
	/**
	 * Checks if valid agility is given
	 * @param value	Value to check
	 * @return	true if the given value is valid
	 * 			| result == value > getMinValue() && value < getMaxValue()
	 */
	@Raw
	public boolean isValidAgility(int value){
		return value > getMinValue() && value < getMaxValue();
	}
	
	/**
	 * Checks if valid toughness is given
	 * @param value	Value to check
	 * @return	true if the given value is valid
	 * 			| result == value > getMinValue() && value < getMaxValue()
	 */
	@Raw
	public boolean isValidToughness(int value){
		return value > getMinValue() && value < getMaxValue();
	}


	@Basic @Raw
	public boolean isCarryingLog(){
		return log != null;
	}

	/**
	 * Returns log this unit is carrying
	 * @return the log a Unit is carrying
	 * @throws 	UnitException
	 * 			|if (!isCarryingLog())
     */
	public Log getLog() throws UnitException {
		if (isCarryingLog()){
			return log;
		} else throw new UnitException(UnitException.NO_MATERIAL_CARRYING, "Tried to get the carried log when there is none");
	}

	/**
	 * Sets the log being carried by a Unit to a given Log
	 * @param 	newLog the given Log
	 * @post	The log being carried will be set to the given Log
	 * 			|new.getLog() == newLog
     */
	public void setLog(Log newLog) {
		log = newLog;
	}

	@Basic @Raw
	public boolean isCarryingBoulder(){
		return boulder != null;
	}

	public Boulder getBoulder() throws UnitException {
		if(isCarryingBoulder()){
			return boulder;
		} else throw new UnitException(UnitException.NO_MATERIAL_CARRYING, "Tried to get the carried boulder when there is none");
	}

	/**
	 * Sets the boulder being carried by a Unit to a given Boulder
	 * @param 	newBoulder the given Boulder
	 * @throws 	UnitException
	 * 			|if((isCarryingBoulder()) ||(newBoulder.getClass() != Boulder.class))
	 * @post	The boulder being carried will be set to the given Boulder
	 * 			new.getBoulder() == newBoulder
	 */
	public void setBoulder(Boulder newBoulder){
			boulder = newBoulder;
	}


	@Basic @Raw
	public int getWeight(){
		return this.Weight;
	}
	
	@Basic @Raw
	public int getStrength(){
		return this.Strength;
	}
	
	@Basic @Raw
	public int getAgility(){
		return this.Agility; 
	}
	
	@Basic @Raw
	public int getToughness(){
		return this.Toughness;
	}
	

	@Immutable @Raw @Basic
	public int getMinInitialValue() {
		return MIN_INITIAL_VALUE ;
	}
	
	
	@Immutable @Raw @Basic
	public int getMaxInitialValue() {
		return MAX_INITIAL_VALUE ;
	}

	/**
	 * Make a valid initial value for a given value
	 * @param 	value the given value
	 * 
	 * @return 	
	 * 			| if((value >= getMinInitialValue()) && (value <= getMaxInitialValue())
	 * 			| 	then result == value;
	 * 			| else if(value < getMinInitialValue())
	 * 			|	then result == getMinInitialValue()
	 * 			| else if(value > getMinInitialValue())
	 * 			|	then result == getMaxInitialValue()
	 */
	public int makeValidInitial(int value){
		
		if ((value >= this.getMinInitialValue()) && (value <= this.getMaxInitialValue())) {
			return value;
		}
		else if (value < this.getMinInitialValue()) {
			return this.getMinInitialValue() ;
		}
		else {
			return this.getMaxInitialValue();
		}
	}	
	
	
	/**
	 * Checks if the given name is valid
	 * @param name	The name to be checked
	 * @return	True if the given name is valid :
	 * 				minimal 2 chars long
	 * 				start with a capital letter
	 *				only contain letters, numbers, quotes and white spaces
	 *
	 * 			| if (name.matches("^[A-Z]{1}[A-Za-z0-9\\'\\\"\\s]{1,}$"))
	 * 			|	then result == true
	 * 			| else 
	 * 			|	result == false
	 */
	public static boolean isValidName(String name){
		
		String pattern = "^[A-Z]{1}[A-Za-z0-9\\'\\\"\\s]{1,}$";
		if (name.matches(pattern)){
			return true;
		} else {
			return false;
		}
		
	}
	
	/**
	 * Set the name of a Unit
	 * @param name	The new name to assign
	 * 
	 * @throws UnitException
	 * 			Throws exception when given name is not valid
	 * 			| !isValidName(name)
	 * @post
	 * 			|if isValidName(name)
	 * 			|then new.Name = name 
	 * @effect 	
	 * 			|isValidName(name)
	 */
	public void setName(String name) throws UnitException{
		
		if (! isValidName(name)) {
		throw new UnitException(UnitException.INVALID_NAME_INPUT, "The given name may only contain letters, numbers and quotes. The first letter must be uppercase");				
		} else{
			this.Name = name ;
		}
	}
	
	@Basic @Raw
	public String getName() {
		return this.Name;
	}

	@Basic
	public int getExperiencePoints(){
		return experiencePoints;
	}

	/**
	 * Sets the Experience points to a given value
	 * @param newXP : the given value
	 * @throws UnitException
	 * 			| if (newXP < 0 )
	 * @post 	the experiencePoints of a Unit will be updated
	 * 			| if (newXP >= 0)
	 * 			| then new.getExperiencePoints() == newXP
     */
	private void setExperiencePoints(int newXP) throws UnitException {
		if (newXP >= 0){
			experiencePoints = newXP;
		} else throw new UnitException(UnitException.INVALID_XP_INPUT,"Tried to set XP to a negative number");
	}

	/**
	 * Increase the Experience points with a given value
	 * @param 	newXP the new XP earned
	 * @throws 	UnitException: when newXP is a negative integer
	 * 			| if (newXP < 0 )
	 * @post 	The new XP earned will be updated
	 * 			|if(newXP >= 0)
	 * 			|then new.getNewXPEarned() == getNewXPEarned() + newXP
     */
	public void increaseXP(int newXP) throws UnitException {
		if (newXP >= 0){
			setExperiencePoints(getExperiencePoints()+newXP);
			newXPEarned += newXP;
		} else throw new UnitException(UnitException.INVALID_XP_INPUT, "Tried to increase Unit's XP with a negative number");
	}

	@Basic
	public int getNewXPEarned(){
		return newXPEarned;
	}

	/**
	 *	Checks if XPs imply raise of characteristic
	 * @return 	true if a Unit has earned enough XP to level up one of its attributed
	 * 			| result == (getNewXPEarned() >= 10 )
     */
	public boolean canRaise(){
		return getNewXPEarned() >= 10;
	}

	/**
	 * Checks if unit has max strength
	 * @return 	true if the Unit has reached it's maximum Strength
	 * 			result == (getStrength() == getMaxValue())
     */
	public boolean hasMaxStrength(){
		return getStrength() == getMaxValue();
	}

	/**
	 * Checks if unit has max agility
	 * @return true if the Unit has reached it's maximum Agility
	 * 			| result == (getAgility() == getMaxValue())
     */
	public boolean hasMaxAgility(){
		return getAgility() == getMaxValue();
	}

	/**
	 * Checks if unit has max toughness
	 * @return true if the Unit has reached it's maximum Toughness
	 * 			| result == (getToughness() == getMaxValue())
     */
	public boolean hasMaxToughness(){
		return getToughness() == getMaxValue();
	}

	/**
	 * Raise the Agility with 1 point
	 * @effect setAgility(getAgility() +1)
	 *
	 */
	public void raiseAgility(){
		setAgility(getAgility() + 1);
	}

	/**
	 * Raise the Toughness with 1 point
	 * @effect setToughness(getToughness() +1)
	 */
	public void raiseToughness() {
		setToughness(getToughness() + 1);
	}

	/**
	 * Raise the Strength with 1 point
	 * @effect setStrength(getStrength() + 1)
	 */
	public void raiseStrength() {
		setStrength(getStrength() + 1);
	}

	/**
	 * Returns all methods that can be called for a raise
	 * @return the methods which raises the Units attributes, for these attributes that still can be raised
	 * @throws NoSuchMethodException
     */
	private LinkedList<Method> getRaisePossibility() throws NoSuchMethodException {
		// A linked list where all the possible methods will be stored
		LinkedList<Method> method = new LinkedList();

		if(!hasMaxStrength()){
			method.add(Unit.class.getMethod("raiseStrength"));
		}
		if(!hasMaxAgility()){
			method.add(Unit.class.getMethod("raiseAgility"));
		}
		if(!hasMaxToughness()){
			method.add(Unit.class.getMethod("raiseToughness"));
		}

		return method;
	}

	/**
	 * Raise one of the Unit's attributes if it has reached enough XP and its attributes can still be raised
	 *
	 * @effect getRaisePossibility
	 *
	 * @effect canRaise
	 *
	 * @effect raiseAgility
	 *
	 * @effect raiseStrength
	 *
	 * @effect raiseAgility
	 *
	 * @throws UnitException
	 * 			|getRaisePossibility
     */
	private void Raise() throws UnitException {
		try {
			if(canRaise()){
				//List of raise methods of all possible attributes that can be raised
				LinkedList<Method> methods = getRaisePossibility();

				//How many Attributes that can be raised
				int nrOfMethods = methods.size() ;

				//Define a probability to choose between which attributes to raise
				double probability = 1/(nrOfMethods);

				//Make it random
				double randomNumber = Math.random();

				for (int i = 1 ; i <= nrOfMethods ; i++){
					if (randomNumber < i*probability){
						methods.get(i-1).invoke(this);
					}
				}
			}
		}catch(NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
			throw new UnitException(UnitException.RAISE_METHOD_CONFLICT,e.getMessage());
		}
	}


	
	/**
	 * Returns maximum hitpoints
	 * @return	The maximal possible values for Stamina points and Hitpoints
	 * 			| result == (int)(Math.floor( 200 *(((double)this.getWeight())/100)*(((double)this.getToughness())/100)))
	 */
	public int getMaxPoints(){

		return (int)(Math.floor( 200 *(((double)this.getWeight())/100)*(((double)this.getToughness())/100))); // must always be rounded up
	}
	
	
	/**
	 * Check if the given points are valid
	 * @return
	 * 			| if (points <= getMaxPoints()) && (points >= 0)
	 * 			|	then result == true
	 * 			| else 
	 * 			| 	result == false;
	 */
	public boolean canHaveAsPoints(int points){
		return ((points <= this.getMaxPoints()) && (points >= 0));
	}
	
	/**
	 * Set the Stamina points of the Unit to a given value
	 * @param points	Points to be set
	 * 
	 * @pre	The given points must be valid
	 * 		|canHaveAsPoints(points) == true
	 * @post Unit's stamina points will be set to given points
	 * 		|new.getStaminaPoints() == points
	 */
	public void setStaminaPoints(int points){
		assert canHaveAsPoints(points);
		this.StaminaPoints = points;
		}
	
	/**
	 * Set the Hitpoints of the Unit to a given value
	 * @param points	Points to be set
	 * 
	 * @pre	The given points must be valid
	 * 		|canHaveAsPoints(points) == true
	 * @post Unit's HP will be set to given points
	 * 		|new.getHitPoints() == points
	 */
	public void setHitPoints(int points){
		assert canHaveAsPoints(points);
		this.Hitpoints = points ;
	}

	/**
	 * Decreases the hit points of a Unit with a given value
	 * @param 	points with how many points the HP must be decreased
	 * @post 	if after decreasing, the Unit's HP reached 0, it will be terminated
	 * 			|if(getHitpoints() - points <= 0)
	 * 			|then terminate()
	 * 			|else setHitPoints(getHitPoints()-points)
	 * @effect setHitPoints()
	 * @effect terminate()
     */
	public void decreaseHitPoints(int points){
		if (getHitPoints()-points <= 0){
			setHitPoints(0);
			this.terminate();
			System.out.println("The Unit is dead due to this Fall");
		}  else{
			setHitPoints(getHitPoints()-points);
		}
	}
	
	/**
	 * This method just floors the points and sets them
	 * @param points	Points to be set
	 * @effect	|setHitpoints(int points)
	 */
	public void setHitPoints(double points) {
		setHitPoints((int) Math.floor(points));
	}
	
	/**
	 * This method floors the points and sets them
	 * @param points	Points to be set
	 * @effect	|setStaminaPoints(int points)
	 */
	public void setStaminaPoints(double points) {
		setStaminaPoints((int) Math.floor(points));
	}
	
	@Basic @Raw
	public int getStaminaPoints(){
		return this.StaminaPoints;
	}
	
	@Basic @Raw
	public int getHitPoints(){
		return this.Hitpoints;
	}


	
	/**
	 * 
	 * @return 	the maximal value of the Unit its primary attributes during its lifetime
	 * 			| 200
 	 */
	@Immutable @Raw @Basic
	public int getMaxValue(){
		return 200;
	}
	
	/**
	 * 
	 * @return 	the minimal value of the Unit its primary attributes during its lifetime
	 * 			| 1
	 */
	@Immutable @Raw @Basic
	public int getMinValue(){
		return 1;
	}
	
	/**
	 * Set the Strength to a given value during the Unit its lifetime, if this is a valid value
	 * @param value	Value to set
	 * 
	 * @post 
	 * 			|if( value <= getMinValue())
	 * 			|	then new.getStrength() == getMinValue()
	 * 			|else if (value <= getMaxValue())
	 * 			|	then new.getStrength() == value
	 * 			| else new.getStrength() == getMaxValue()
	 * 
	 * @effect 	Since a Unit its Weight is dependent on its Strength, this must also be updated
	 * 			| updateWeight()
 	 */
	public void setStrength(int value){
		
		if(value <= this.getMinValue()){
			this.Strength = this.getMinValue();
		} else if (value <= getMaxValue()){
			this.Strength = value;
		} else {
			this.Strength = getMaxValue();
		}
		
		this.updateWeight();
		
		
	}
	
	/**
	 * Set the Agility to a given value during the Unit its lifetime, if this is a valid value
	 * @param value New agility to be set
	 * 
	 * @post
	 * 			|if(value <= getMinValue())
	 * 			|	then new.getAgility() == getMinValue()
	 * 			|else if (value <= getMaxValue())
	 * 			|	then new.getAgility() == value
	 * 			| else new.getAgility() == getMaxValue()
	 * @effect
	 * 			|updateWeight()
	 * 
	 */
	public void setAgility(int value){
		
		if(value <= this.getMinValue()){
			this.Agility = this.getMinValue();
		} else if (value <= getMaxValue()){
			this.Agility = value;
		} else {
			this.Agility = getMaxValue();
		}
		
		this.updateWeight();
		
	}
	
	/**
	 * Set the Toughness to a given value during the Unit its lifetime, if this is a valid value
	 * @param value	Newtoughness to be set
	 * 
	 * @post	
	 * 			|if(value <= getMinValue())
	 * 			|	then new.getToughness() == getMinValue()
	 * 			|else if(value <= getMaxValue())
	 * 			|	then new.getToughness() == value
	 * 			|else new.getToughness() == getMaxValue()
	 * @effect
	 * 			|updatePoints()
	 */
	public void setToughness(int value){
		
		if(value <= this.getMinValue()){
			this.Toughness = this.getMinValue();
		} else if (value <= getMaxValue()){
			this.Toughness = value;
		} else {
			this.Toughness= getMaxValue();
		}
		
		// Update Unit's point
		this.updatePoints();
		// Update Unit's Weight
		this.updateWeight();
		
	}
	
	/**
	 * Sets the toughness for a Unit when a value is a double
	 * @param value	New toughness to be set
	 * @effect
	 * 			|setToughness((int) Math.floor(value))
	 */
	public void setToughnessDouble(double value) {

		setToughness(((int) Math.floor(value)));
	}

	/**
	 * set the Weight to a given value during the Unit its lifetime, if this is a valid value
	 * @param value	New weight to be set
	 * 
	 * @post	
	 * 			| if (value <= minWeight)
	 * 			|	then new.getWeight() == minWeight
	 * 			| else if (value <= getMaxValue())
	 * 			|	then new.getWeight() == value
	 * 			| else new.getWeight() == getMaxValue()
	 * 			
	 * @effect	Stamina- and Hitpoints must also be updated if necessary
	 * 			|updatePoints()
	 */
	public void setWeight(int value){
		
		double	fracture = ((this.Strength + this.Agility) / 2 );
		int 	minWeight = (int) Math.floor(fracture);
		
		if (value <= minWeight){
			this.Weight = minWeight;
		} else if (value <= this.getMaxValue()){
			this.Weight = value;
		} else this.Weight = this.getMaxValue();
		
		this.updatePoints();
	}
	
	/**
	 * Calculates min weight
	 * @return
	 * 			| (int) Math.floor((this.getStrength() + this.getAgility()) / 2)
	 */
	@Raw
	public int getMinWeight(){
		return (int) Math.floor((this.Strength + this.Agility) / 2);
	}
	
	/**
	 * update the Weight of a Unit if necessary when its Strength or Agility have changed
	 * @post	
	 * 			| if (this.getWeight() <= minWeight)
	 * 			|	then new.getWeight() == minWeight
	 * 			|			
	 * @effect
	 * 			|updatePoints()
	 * 
	 */
	public void updateWeight(){
		
		double	fracture = ((this.Strength + this.Agility) / 2 );
		int 	minWeight = (int) Math.floor(fracture);
		
		if (this.Weight <= minWeight){
			this.Weight = minWeight;
			this.updatePoints();
		}
		
	}

	/**
	 * Add the weight of a material to the carrying Unit
	 * @param carriedMaterial
	 * @throws UnitException
	 */
	public void addWeightOfMaterial(Material carriedMaterial) throws UnitException {
		if (carriedMaterial != null) {
			//Temporary weight increase
			Weight += carriedMaterial.getWeight();
		} else throw new UnitException(UnitException.INVALID_MATERIAL_INPUT,"Tried to add the weight of a material" +
				"with value NULL");
	}
	
	/**
	 * Returns current speed
	 * @return	Returns speed according to current movement status
	 * 			| if this.getMovementStatus() == MovementType.WALKING then result == this.getWalkingSpeed()
	 * 			| else if this.getMovementStatus() == MovementType.SPRINTING then result == this.getSprintingSpeed
	 * 			| else result == 0
	 */
	public double getCurrentSpeed() {
		switch(MovementStatus) {
			case WALKING:
				return getWalkingSpeed();
			case SPRINTING:
				return getSprintingSpeed();
			default: return 0;
			
		}
	}
	
	/**
	 * Returns true if unit is moving
	 * @return	True if unit is sprinting or walking
	 * 			| this.getMovementStatus() == MovementType.SPRINTING || this.getMovementStatus() == MovementType.WALKING
	 */
	public boolean isMoving() {
		return MovementStatus == MovementType.SPRINTING || MovementStatus == MovementType.WALKING;
	}
	
	/**
	 * Update the Stamina- and hitpoints if necessary, when weight or agility have changed
	 * 
	 * @post	
	 * 			| if(getStaminaPoints() > getMaxPoints())
	 * 			|	then new.getStaminaPoints() == getMaxPoints()
	 * @post	
	 * 			| if(getHitPoints() > getMaxPoints())
	 * 			|	then new.getHitPoints() == getMaxPoints()
	 * 
	 */
	public void updatePoints(){
		
		// Update Stamina points if necessary
		if (this.getStaminaPoints() > this.getMaxPoints()){
			this.setStaminaPoints(this.getMaxPoints());
		}
		
		// Update Hitpoints if necessary
		if (this.getHitPoints() > this.getMaxPoints()){
			this.setHitPoints(this.getMaxPoints());
		}
	}
	
	
	/**
	 * Move the Unit to a neighbouring cube
	 * @param dx
	 * @param dy
	 * @param dz
	 * 
	 * @post	If the input is invalid, the Unit will not move.
	 * 			| if((this.isNeighbouringCube(x, y, z))&& 
	 *			|	(isValidPosition(this.getPositionX() + x*(LENGTH_X /2),this.getPositionY() + y*(LENGTH_Y /2)
	 *			|	, this.getPositionZ() + z*(LENGTH_Z /2))))
	 *			| 		then new.getPositionX() == getPositionX() + x*(LENGTH_X /2)
	 *			|		then new.getPositionY() == getPositionY() + y*(LENGTH_Y /2)
	 *			|		then new.getPositionZ() == getPositionZ() + z*(LENGTH_Z /2)
	 * 
	 * @effect	
	 * 			| isValidPosition(new.getPositionX(),new.getPositionY(),new.getPositionZ())
	 */
	public void moveToAdjacent(int dx, int dy, int dz) throws UnitException{
		
		if ((this.isNeighbouringCube(dx, dy, dz))){
			int next_cube_position_x = (int)Math.floor(currentCube.getPosition().getPosition_x() + dx*(World.LENGTH_X));
            int next_cube_position_y = (int)Math.floor(currentCube.getPosition().getPosition_y() + dy*(World.LENGTH_Y));
			int next_cube_position_z = (int)Math.floor(currentCube.getPosition().getPosition_z() + dz*(World.LENGTH_Z));
            try {
				Position<Integer> cubePosition = new Position<Integer>(next_cube_position_x,next_cube_position_y,next_cube_position_z);
				if(!canHaveAsCubePosition(cubePosition)) throw new UnitException(UnitException.INVALID_POSITION,"Position requested is solid");
                next_cube.getPosition().setPosition(cubePosition);
            }catch(PositionException e) {
                throw new UnitException(UnitException.INVALID_POSITION,"Invalid position");
            }
		}else {
			throw new UnitException(UnitException.INVALID_POSITION + UnitException.POSITION_OUT_OF_BOUNDS,"Invalid position");
		}
		
	}

	public boolean canHaveAsCubePosition(Position<Integer> cubePosition) {
		//Retrieve cube from game world
		Cube cube = World.getInstance().getCubeByVector(NumberConverter.vectorToInteger(cubePosition.toVector()));

		//Check if cube is solid or not
		return !cube.getSceneryType().isSolid();//Note: only requirement here, because if next cube isn't surrounded by solid ones, movement still has to take place and unit will Fall
	}
	
	/**
	 * Check if the given x- y- and z direction leads to a neighbouring cube 
	 * @param x 1 means move right
	 * 			-1 means move left
	 * @param y 1 means move up
	 * 			-1 means move down
	 * @param z 1 means move further away from the game world start coordinates
	 * 			-1 means go nearer to the game world start coordinates
	 * 
	 * @return	true if the given new cube is a neighbouring cube
	 * 			if (x == 0 && y == 0 && z == 0)
	 * 				then return false
	 * 			if (( x > 1 ) || ( x < - 1 ))
	 *				then result == false
	 * 			else if (( y > 1 ) || ( y < -1 ))
	 *				then result == false
	 *			else if (( z > 1 ) || ( z < -1 ))
	 *				then result==false
	 * 			else
	 *				result==true;
	 */
	@Raw
	public boolean isNeighbouringCube(int x, int y, int z){
		
		if(x == 0 && y == 0 && z == 0) {
			return false;
		}else if (( x > 1 ) || ( x < - 1 )){
			return false ;
		} else if (( y > 1 ) || ( y < -1 )){
			return false ;
		} else if (( z > 1 ) || ( z < -1 )){
			return false; 
		} else{
			return true;
		}
	}
	
	/**
	 * Initiate a movement to a specific target position, if this position is valid
	 * 
	 * @param x The x coordinate of the destination cube.
	 * @param y The y coordinate of the destination cube.
	 * @param z The z coordinate of the destination cube.
	 * 
	 * @post	when the target position is not valid, nothing will happen
	 * 			|if (isValidPosition(x,y,z))
	 * 			|	then (new.getTargetPositionX() == x) && (new.getTargetPositionY() == y)
	 * 						(new.getTargetPositionZ() == z)
	 * @throws	UnitException
	 * 			| !isValidPosition(x,y,z)
	 */
	public void moveTo(int x, int y, int z) throws UnitException{
		try {
			if(!canHaveAsCubePosition(new Position<Integer>(x,y,z))) throw new UnitException(UnitException.INVALID_POSITION,"Position requested is solid");
		} catch (PositionException e) {
			throw new UnitException(UnitException.INVALID_POSITION,"Position request is invalid");
		}
		if (!this.isMoving()){
			try {
                Position<Double> new_target = new Position<Double>((double)x + World.LENGTH_X /2,(double)y + World.LENGTH_Y /2,(double)z + World.LENGTH_Z /2);
                setTargetPosition(new_target);
            }catch(PositionException e) {
                throw new UnitException(UnitException.INVALID_POSITION,e.getMessage());
            }

			this.setMovementStatus(MovementType.WALKING);
			this.findNextCube();

		}else {
			try {
                Position<Double> new_target = new Position<Double>((double)x + World.LENGTH_X /2,(double)y + World.LENGTH_Y /2,(double)z + World.LENGTH_Z /2);
                setTargetPosition(new_target);
			}catch(PositionException e) {
				throw new UnitException(UnitException.POSITION_OUT_OF_BOUNDS,"Position isn't valid");
			}
			
		}
	}
	
	@Basic @Raw
	public ActionStack<MovementType> getActionStack(){
		return actionStack;
	}
	
	/**
	 * Check if a Unit was resting while invoked other action, but no hitpoints were recovered in the resting period
	 * 
	 * @post	if no hitpoints were recovered, Resting will be resumed after combat is finished
	 * 			|if(getRecovered_hitpoints() == 0)
	 * 			|	then getActionStack().addAction(MovementType.RESTING)	
	 * @effect 
	 * 			|getActionStack().addAction(MovementType.RESTING)
	 * 
	 */
	public void checkIfMinimalHitpointsRecovered(){
		if (this.getRecovered_hitpoints() == 0){
			getActionStack().addAction(MovementType.RESTING);
		}
	}
	
	/**
	 * When a Unit is attacked by another Unit (A), it will try to defend itself
	 * 
	 * @param A The attacking unit
	 * 
	 * @throws 	UnitException
	 * 			|dodgeAttack()
	 * 			|isUnitMovingWhileInvoking(MovementType.DEFENDING)
	 * 
	 * @post
	 * 			|if (isSuccessfulDodge(A))
	 * 			|	then dodgeAttack()
	 * 			|else if(!isSuccessfulBlock(A))
	 * 			|	then new.getHitPoints() == getHitPoints() - (A.getStrength/10)
	 * @effect
	 * 			|isUnitMovingWhileInvoking(MovementType.DEFENDING)
	 * 
	 */
	public void Defend(Unit A) throws UnitException{
		//Check if we are moving
		if(isMoving()) {
			//Move to next cube
			stopAtNextCube = true;
			walkToCurrentCube();

			//After we defended, continue walking
			getActionStack().addAction(getMovementStatus());

			//Add defending to stack trace
			getActionStack().addAction(MovementType.DEFENDING);
			
		}else if(MovementStatus == MovementType.RESTING) {
			this.checkIfMinimalHitpointsRecovered();
		}else {
			if(getMovementStatus() != MovementType.DEFENDING)
			        this.setMovementStatus(MovementType.DEFENDING);
		}
		
		//Save the attacker
		secondsDefending = 0;
		this.Attacker.addElement(A);
	}
	
	/**
	 * Sets next cube to reach as current cube we are in
	 */
	private void walkToCurrentCube() throws UnitException {
        try {
            int next_cube_position_x = (int)Math.floor(current_position.getPosition_x());
            int next_cube_position_y = (int)Math.floor(current_position.getPosition_y());
            int next_cube_position_z = (int)Math.floor(current_position.getPosition_z());
            Position<Integer> new_next_cube = new Position<Integer>(next_cube_position_x,next_cube_position_y,next_cube_position_z);
            next_cube.getPosition().setPosition(new_next_cube);
        }catch(PositionException e) {
            throw new UnitException(UnitException.INVALID_POSITION,e.getMessage());
        }
	}
	
	@Basic @Raw
	public Unit getAttacker() {
		return Attacker.getFirst();
	}

	/**
	 * Makes a step in time when defending
	 * @param GameTime the time passed since previous invocation
	 * @effect	| if(this.isSuccessfulDodge(this.getAttacker()) then this.dodgeAttack(this.getAttacker())
	 * @throws UnitException
	 */
	public void Defend(double GameTime) throws UnitException{
		
		//increase secondsDefending
		secondsDefending += GameTime;

		if (Util.fuzzyLessThanOrEqualTo(secondsDefending,1) && startedDefending == false){
			// Unit will dodge, block or take the hit
			// this must only happen one time
			startedDefending = true;

			if (this.isSuccessfulDodge(Attacker.getFirst())) {
				try {
					this.dodgeAttack();
				}catch(UnitException e) {
					if((e.getErrorCode() & UnitException.CANNOT_DODGE) > 0) {
						takeHit();
					}else {
						throw e;
					}
				}
				//gained 20XP by successfully dodging
				increaseXP(20);

				System.out.println("The Unit dodged");
			} else if (this.isSuccessfulBlock(Attacker.getFirst())) {

				//gained 20XP by successfully blocking
				increaseXP(20);

				System.out.println("The Unit Blocked");
			} else {
				takeHit();
				System.out.println("The Unit took a hit");
			}
		} else if (Util.fuzzyLessThanOrEqualTo(secondsDefending,1)){
			//nothing must happen, the Unit has already defended itself from the attack.
			//but an attack will last for 1 second
		} else {
			// The fight has ended
			Attacker.popElement();
			if(Attacker.isEmpty()) {
				MovementStatus = (getDefaultBehaviourStatus()? MovementType.DEFAULT_BEHAVIOUR : MovementType.DOING_NOTHING);
			}
			startedDefending = false;
			secondsDefending = 0;

		}
	}

	private void takeHit() {
		decreaseHitPoints((int) (Math.ceil((double) (Attacker.getFirst().getStrength()) / 10)));
	}


	/**
	 * Check if the Unit was able to successfully dodge away from the attacking Unit
	 * @param A The attacking Unit
	 * @effect	
	 * 			| isSuccessful(dodgeProbability) 
	 */
	public boolean isSuccessfulDodge(Unit A){
		
		// The probability of successfully dodging an attack
		double dodgeProbability;
		dodgeProbability = 0.20 * ((double)this.getAgility() /A.getAgility());
		return this.isSuccessful(dodgeProbability) ;
		
	}
	
	/**
	 * Check if the Unit was able to successfully block the attack from the attacking Unit
	 * @param A : The attacking Unit
	 * @return	True if block is successful
	 * 			| result == isSuccessful(blockProbability)
	 */
	public boolean isSuccessfulBlock(Unit A){
		
		double blockProbability;
		blockProbability =  0.25*((double)(this.getStrength()+this.getAgility())/(A.getStrength() + A.getAgility()));
		return this.isSuccessful(blockProbability);
		
	}
	
	/**
	 * Check if a move was successful dependent on a given probability
	 * @param probability
	 * @return 	true if the dodge has been successful 
	 * 			| if (probability>= 1)
	 * 			|	then result == true
	 * 			| else if (Math.random() <= probability) 
	 * 			|	then result == true
	 * 			| else result == false 
	 */
	public boolean isSuccessful(double probability){
		if (probability >= 1) {
			return true;
		} else {
			// Math.random() will generate a random variable, uniformly distributed between 0 and 1
			// Hence, the probability that a uniformly random variable <= value is equal to that value
			return (Math.random() <= probability);
		}
	}
	
	/**
	 * Generate a random double between -1 and 1
	 * @return 	A value between -1 and 1
	 * 			| -1 + Math.random()*2
	 */
	private double generateRandomAdjacentDirection(){

		return -1 + Math.random()*2;
	}

	/**
	 * Generate a random position where a Unit can dodge to
	 * @return 	a random position where a Unit can dodge to
	 * 			| result == new Position(getCurrentPosition().getPosition_x() + generateRandomAdjacentDirection(),
	 * 			|		getCurrentPosition().getPosition_y() + generateRandomAdjacentDirection(),
	 * 			|		getCurrentPosition().getPosition_z())
	 * @param recursiveCounter
     */
	public Position<Double> generatePositionToDodgeTo(ReferencedInteger recursiveCounter) {
		if(recursiveCounter.intValue() >= 100) return null;
		try{
			Position<Double> possibleTarget = new Position<Double>(getCurrentPosition().getPosition_x() + generateRandomAdjacentDirection(),
					getCurrentPosition().getPosition_y() + generateRandomAdjacentDirection(),
					getCurrentPosition().getPosition_z());
			if (!canDodgeTo(possibleTarget)){ // Corresponding cube was solid, or position was the same as current
				recursiveCounter.raise();
				return generatePositionToDodgeTo(recursiveCounter);
			}

			return possibleTarget;

		} catch (PositionException e){

			//Position wasn't valid while generated, try again
			recursiveCounter.raise();
			generatePositionToDodgeTo(recursiveCounter);
		}

		//Wil never be reached, compiler doesn't consider recursive operation
		return null;
	}

	/**
	 * Check whether a given position is a Position where a Unit can dodge to.
	 * @param newPosition	The given position
	 * @return	true if the corresponding cube is passable and the position is valid but different from the Unit's current position
	 * 			|result == 	!World.getInstance().getCubeByVector(newPosition.toCube().toVector()).getSceneryType().isSolid()
	 * 			|			&& World.isValidPosition(newPosition.getPosition_x(),newPosition.getPosition_y(),newPosition.getPosition_z())
	 * 			|			&& !getCurrentPosition().equals(newPosition,0.2)
     */
	public boolean canDodgeTo(Position newPosition){

		return !World.getInstance().getCubeByVector(newPosition.toCubePosition().toVector()).getSceneryType().isSolid() //Can not be solid
				&& World.isValidPosition(newPosition.getPosition_x(),newPosition.getPosition_y(),newPosition.getPosition_z()) //Must be valid considering game bounds
				&& !getCurrentPosition().equals(newPosition,0.2); //Must differ from Unit's current position
	}

	/**
	 * Let the Unit dodge from an attack
	 * @throws UnitException
	 * 			|getCurrentPosition().setPosition(generatePositionToDodgeTo())
	 * 			|setCurrentCube(generatePositionToDodgeTo())
	 * @effect 	getCurrentPosition().setPosition()
	 * 			|getCurrentPosition().setPosition(generatePositionToDodgeTo())
	 * @effect	setCurrentCube()
	 * 			|setCurrentCube( World.getInstance().getCubeByVector(generatePositionToDodgeTo().toCube.toVector) )
	 *
     */
	public void dodgeAttack()throws UnitException{
		try {
			ReferencedInteger rc = new ReferencedInteger(0);
			Position<Double> targetPos = generatePositionToDodgeTo(rc);
			if(targetPos == null) throw new UnitException(UnitException.CANNOT_DODGE,"Can't dodge");
			getCurrentPosition().setPosition(targetPos);
			//Update Unit's current cube as well
			setCurrentCube( World.getInstance().getCubeByVector(targetPos.toCubePosition().toVector()) );
		} catch (PositionException e) {
			throw new UnitException(UnitException.INVALID_POSITION,e.getMessage());
		} catch (GameObjectException e){
			throw new UnitException(UnitException.DODGE_CONFLICT, e.getMessage());
		}
	}

	/**
	 * Check if the Attacking Unit's cube is a neighbouring (or the same) cube of the defending Unit's cube
	 * @param D The Defending Unit
	 * @return	
	 * 			|getCurrentCubePosition().isNeighbouringOrSameCube(D.getCurrentCubePosition())
	 */
	public boolean isNeighbouringCube(Unit D){

		return currentCube.getPosition().isNeighbouringOrSameCube(D.currentCube.getPosition());
	}
	
	/**
	 * Attack another Unit D if Unit D is at a neighbouring position
	 * @param D	Unit to be attacked
	 * 
	 * @post	Unit D will instantly defend itself
	 * 			|if (isNeighBouringCube(D))
	 * 			| then D.Defend(this)
	 * @effect 	setMovementStatus()
	 * 			|if(isNeighbouringCube(D))
	 * 			| then setMovementStatus(MovementType.FIGHTING)
	 * @effect	D.Defend()
	 * 			|if(isNeighbouringCube(D))
	 * 			| then D.Defend(this)
	 * @effect 	interruptTask()
	 * 			|if( isNeighbouringCube(D) && (isMoving() || isWorking()) )
	 * 			| then interruptTask()
	 * 
	 * @throws	UnitException
	 * 			|D.Defend(this)
	 * 			|if(D == this || getFaction() == D.getFaction())
	 * 			|interruptTask()
	 */
	public void Attack(Unit D) throws UnitException {
		if (D == this || getFaction() == D.getFaction())
			throw new UnitException(UnitException.ATTACKING_CONFLICT, "impossible to attack itself, or attack a friendly Unit");

		if (this.isNeighbouringCube(D)){

			//Store the Defender
			Defender = D;
			D.Defend(this);

			//If the Unit was working and this was a Task, interrupt this Task
			if (isWorking()) interruptTask();

			if (isMoving()){
				//Abort movement, but first, walk to the centre of our current cube for calibration
				stopAtNextCube = true;
				walkToCurrentCube();

				//check if it was a movement related to a Task, if so this task must be interrupted
				if (!interruptTask()) {
					//After we've been attacked attacked, continue walking
					getActionStack().addAction(getMovementStatus());

					//Add FIGHTING to stack trace
					getActionStack().addAction(MovementType.FIGHTING);
				}

			} else {
				this.setMovementStatus(MovementType.FIGHTING);

			}
		}else {
			throw new UnitException(UnitException.ATTACKING_CONFLICT,"Not neighbouring");
		}
	}

	/**
	 * Updates while Attacking
	 * @param gameTime The time advanced
	 * @effect setSecondsFighting()
	 * 			|setSecondsFighting(getSecondsFighting + gameTime)
	 * @effect setMovementStatus()
	 * @effect setOrientation()
	 * @throws UnitException
     */
	public void Attack(double gameTime) throws UnitException{

		//Update time
		this.setSecondsFighting(this.getSecondsFighting() + gameTime);

		if (Util.fuzzyLessThanOrEqualTo(this.getSecondsFighting(),1) && startedAttacking == false){

			//Unit has started the fight
			startedAttacking = true;
			//the fighting Units must face each other
			this.setOrientation((Math.atan2((Double)Defender.getCurrentPosition().getPosition_y() - current_position.getPosition_y(),
					(Double)Defender.getCurrentPosition().getPosition_x() - current_position.getPosition_x())));
			Defender.setOrientation((Math.atan2(current_position.getPosition_y() - (Double)Defender.getCurrentPosition().getPosition_y(),
					current_position.getPosition_x() - Defender.getCurrentPosition().getPosition_x())));
			//The attacked Unit will immediately defend itself
			//Defender.Defend(this);


		} else if (Util.fuzzyLessThanOrEqualTo(this.getSecondsFighting(),1)){
			// nothing must happen, but the fight will last for 1 second
		} else {
			//The fight has ended
			this.setMovementStatus(getDefaultBehaviourStatus() ? MovementType.DEFAULT_BEHAVIOUR : MovementType.DOING_NOTHING);
			this.setSecondsFighting(0);
			startedAttacking = false;
			Defender = null;
		}			
	}

	
	@Basic @Raw
	public double getSecondsFighting(){
		return secondsFighting;
	}

	/**
	 * Set the seconds a Unit is fighting
	 * @param value	New value for seconds fighting
	 * @throws 	UnitException
	 * 			|if (value < 0)
	 * @post	Seconds fighting will be updated
	 * 			|if (value >= 0)
	 * 			| then new.getSecondsFighting() = value
	 */
	public void setSecondsFighting(double value)throws UnitException{
		
		if (value < 0){
			throw new UnitException(UnitException.INVALID_SECONDS_INPUT, "Given seconds are negative");
		} else {
			secondsFighting = value;
		}
		
		
	}

	/**
	 * Calculate how many Statements can be executed
	 * @param timeAdvanced  The time advanced since previous advanceTime
	 * @return The number of statements that can be executed
	 *          |if(timeAdvanced >= 0.001)
	 *          | then result == (int)Math.floor(timeAdvanced/0.001)
	 *          |else result == 1
	 */
	public int calculateStatements(double timeAdvanced){
		return (Util.fuzzyGreaterThanOrEqualTo(timeAdvanced,0.001))? (int)Math.floor(timeAdvanced/0.001): 1;
	}

	/**
	 * Starts the default behaviour of a Unit
	 * @effect getFaction().getScheduler().schedule()
	 * 			|if(getFaction().getScheduler().hasFreeTask())
	 * 			| then 	getFaction().getScheduler().schedule(this)
	 * @effect getTask().setStatus()
	 * 			|if(getFaction().getScheduler().hasFreeTask())
	 * 			| then getTask().setStatus(TaskStatus.EXECUTING)
	 * @effect DefaultExecutor.execDefaultAction()
	 * 			|if(!getFaction().getScheduler().hasFreeTask())
	 * 			|DefaultExecutor.execDefaultAction()
	 * @throws UnitException
	 * 			if an error occurs related with DefaultExecutor class
	 *				|DefaultExecutor.execDefaultAction()
	 *			if an error occurs during the scheduling of a free Task with this Unit
	 *				|getFaction().getScheduler().schedule(this)
	 *
     */
	public void StartDefaultBehaviour(double gameTime) throws UnitException{

		//Check if the scheduler has some free Task to be executed
		if(hasTask()) {
			taskUpdates(gameTime);
		}else if(getFaction().getScheduler().hasFreeTask()){
			//It appears so, let the Scheduler schedule this Unit
			try {
				getFaction().getScheduler().schedule(this);
				getTask().setStatus(TaskStatus.EXECUTING);
				taskUpdates(gameTime);
			} catch (SchedulerException | TaskException e){
				throw new UnitException(UnitException.DEFAULT_BEHAVIOUR_CONFLICT,e.getMessage());
			}

		} else {
			//No free Task was found, execute authentic DB instead
			try {
				//Create a new object with all the possible actions that this Unit can perform
				DefaultExecutor exeObject = new DefaultExecutor(this);
				//Randomly choose one of these actions and let the Unit execute this one
				exeObject.execDefaultAction();
			} catch (DefaultBehaviourException e) {
				throw new UnitException(UnitException.DEFAULT_BEHAVIOUR_CONFLICT, e.getMessage());
			}
		}
	}

	public void StartDefaultBehaviour() throws UnitException {
		setMovementStatus(MovementType.DEFAULT_BEHAVIOUR);
		setDefaultBehaviourStatus(true);
	}

	/**
	 * Check whether the Unit is fully recovered
	 * @return 	true if the Unit's HP and Stamina Points are full
	 * 			|result == ( getHitPoints() == getMaxPoints() && getStaminaPoints() == getMaxPoints() )
     */
	public boolean isFullyRecovered(){
		return (this.getHitPoints() == this.getMaxPoints() && this.getStaminaPoints() == this.getMaxPoints());
	}
		
	/**
	 * 
	 * @return 	true if Unit was moving while another action was invoked on it
	 * 			| if(isMoving)
	 * 			|	then result == true
	 * 			| else result == false 
	 * @throws 	UnitException
	 * 			|moveTo(getNextCubePositionX,getNextCubePositionY,getNextCubePositionZ)
	 * @effect
	 * 			|moveTo(getNextCubePositionX,getNextCubePositionY,getNextCubePositionZ)
	 * @effect
	 * 			|actionStack.addAction(currentMovement)
	 */
	public boolean isUnitMovingWhileInvoking(MovementType newMovement) throws UnitException{
		if (this.isMoving()){
			 	this.moveTo((int)Math.floor(next_cube.getPosition().getPosition_x()), (int) Math.floor(next_cube.getPosition().getPosition_y()),
			 				(int) Math.floor(next_cube.getPosition().getPosition_z()));
			 	actionStack.addAction(newMovement);
			 	return true;
		} else {
			return false;
		}
	}

	/**
	 * Check whether this Unit is currently carrying any Object
	 * @return True if this Unit is currently carrying any Object
	 * 			|result == (isCarryingBoulder() || isCarryingLog())
     */
	public boolean isCarryingObject(){
		return isCarryingBoulder() || isCarryingLog();
	}

	/**
	 * Check whether the Unit is working
	 * @return True when the Unit is working
	 * 			|result == getMovementStatus() == MovementType.WORKING
     */
	public boolean isWorking(){
		return getMovementStatus() == MovementType.WORKING;
	}

	/**
	 *
	 * @param x	the x coordinate of the cube to work on
	 * @param y	the y coordinate of the cube to work on
	 * @param z	the z coordinate of the cube to work on
     * @return	true if it is a neighbouring cube of the Unit's current cube position, and the Unit is currently not
	 * 			carrying any materials which will be dropped on a solid cube
	 * 			|if(!getCurrentCube.getPosition().isNeighbouringOrSameCube(new Position(x,y,z)))
	 * 			| then result == false
	 * 			|else if (isCarryingObject())
	 * 			|	then result == !World.getInstance().getCubeByKey(x,y,z).getSceneryType().isSolid()
	 * 			|else result == true
     */
	private boolean possibleToWorkOn(int x, int y, int z){

		try {
			//Work must be pointed at a neighbouring cube, or the Unit's current position
			if ( !currentCube.getPosition().isNeighbouringOrSameCube(new Position<Integer>(x, y, z)) ){
				return false;
			} else if ( isCarryingObject() ) {
				//A material can never be dropped on a solid cube, this is contradictory with Material's specification
				return !World.getInstance().getCubeByKey(x,y,z).getSceneryType().isSolid();
			} else
				return true;

		} catch (PositionException e){
			//Won't happen, position is retrieved from the Game World. This will always be valid
		}

		//Will never be reached, but intelliJ doesn't know this
		return false;
	}

	@Basic @Raw
	public Cube getWorkCube(){
		return workCube;
	}

	/**
	 * Set the cube where a Unit has to work on
	 * @param x	x coordinate of the cube
	 * @param y y coordinate of the cube
	 * @param z z coordinate of the cube
     * @post the cube where the Unit has to work on will be set to the cube with the given coordinates
	 * 		 |new.getWorkCube() == World.getInstance().getCubeByKey(x,y,z)
     */
	private void setWorkCube(int x, int y, int z){
		workCube = World.getInstance().getCubeByKey(x,y,z);
	}
	
	/**
	 * Conduct work, if possible, on a specific cube
	 * @param x 	The x coordinate of the specified cube
	 * @param y 	The y coordinate of the specified cube
	 * @param z 	The z coordinate of the specified cube
	 * @post	Duration will be calculated
	 * 			|new.getDurationOfWork() == 500 / this.getStrength()
	 * @effect setMovementStatus()
	 * 			|setMovementStatus(Working)
	 * @post	Type of work will be specified
	 * 			|new.getWorkToBeDone() == type
	 * @effect setWorkCube()
	 * 			|setWorkCube(x,y,z)
	 * @effect walkToCurrentCube()
	 * @throws UnitException
	 * 			|if(!possibleToWorkOn(x,y,z))
	 * 			thrown by setWorkToBeDone(x,y,z)
	 * 			thrown by WalkToCurrentCube()
	 * 			thrown by setMovementStatus()
	 */
	public void Work(int x, int y, int z) throws UnitException {

		//Check if it is possible to work on the target cube
		if(!possibleToWorkOn(x,y,z)){
			throw new UnitException(UnitException.CANNOT_WORK_AT_TARGET,"Impossible to work at given position");
		}
		//We can work on the given position
		//Calculate duration of work
		durationOfWork = 500 / Strength;

		//set cube to work on
		setWorkCube(x,y,z);

		//Let the Unit face to the right direction
		this.setOrientation(Math.atan2((getWorkCube().getPosition().getPosition_y() +0.5 - getCurrentPosition().getPosition_y()),
				(getWorkCube().getPosition().getPosition_x() + 0.5 - getCurrentPosition().getPosition_x())));
		
		//Specify what to do
		setWorkToBeDone(x,y,z);

		//Check if the Unit was currently moving while this action was invokes, if so, let it first move to the cube position for calibration
		if (isMoving()){
			//Walk to next cube
			stopAtNextCube = true;
			walkToCurrentCube();
			
			//Add walking to stack
			getActionStack().addAction(getMovementStatus());
			
			//Add working to action stack
			getActionStack().addAction(MovementType.WORKING);
		} else {
			//Do some work
			setMovementStatus(MovementType.WORKING);
		}
	}

	@Basic @Raw
	public WorkType getWorkToBeDone(){
		return WorkToBeDone;
	}

	/**
	 * Set the work that has to be done to a given WorkType
	 * @param x	the x coordinate of the targeted cube
	 * @param y	the y coordinate of the targeted cube
     * @param z	the z coordinate of the targeted cube
	 *
	 * @post	Depending on what is available on the target cube, and what the unit is already carrying
	 * 			the work that has to be done will be set
	 * 			|if(isCarryingBoulder())
	 * 			| then new.getWorkToBeDone() == WorkType.DROP_BOULDER
	 * 			|else if (isCarryingBoulder())
	 * 			| then new.getWorkToBeDone() == WorkType.DROP_LOG
	 * 			|else if ((World.getInstance().getCubeByKey(x,y,z).getSceneryType() == SceneryType.WORKSHOP) &&
	 * 			|		(World.getInstance().getCubeByKey(x,y,z).containsBoulder()) &&
	 * 			|		(World.getInstance().getCubeByKey(x,y,z).containsLog())
	 * 			| then new.getWorkToBeDone() == WorkType.CONSUME_BOULDER_AND_LOG
	 * 			|else if (World.getInstance().getCubeByKey(x,y,z).containsBoulder())
	 * 			| then new.getWorkToBeDone() == WorkType.PICK_UP_BOULDER
	 * 			|else if (World.getInstance().getCubeByKey(x,y,z).containsLog()
	 * 			| then new.getWorkToBeDone() == WorkType.PICK_UP_LOG
	 * 			|else if(World.getInstance().getCubeByKey(x,y,z).getSceneryType == SceneryType.TREE
	 * 			| then new.getWorkToBeDone() == WorkType.COLLAPSE_WOOD_CUBE
	 * 			|else if(World.getInstance().getCubeByKey(x,y,z).getSceneryType == SceneryType.ROCK)
	 * 			| then new.getWorkToBeDone() == WorkType.COLLAPSE_ROCK_CUBE
     */
	private void setWorkToBeDone(int x, int y, int z) throws UnitException {

		if(getWorkCube() == null){
			throw new UnitException(UnitException.WORK_METHOD_CONFLICT,"Cube where Unit has to work on is NULL");
		}

		if(this.isCarryingBoulder()){
			WorkToBeDone = WorkType.DROP_BOULDER;
			return;
		}

		if(this.isCarryingLog()){
			WorkToBeDone = WorkType.DROP_LOG;
			return;
		}

		if((workCube.getSceneryType() == SceneryType.WORKSHOP) && (workCube.containsBoulder()) && (workCube.containsLog())){
			WorkToBeDone = WorkType.CONSUME_BOULDER_AND_LOG;
			return;
		}

		if(workCube.containsBoulder()){
			WorkToBeDone = WorkType.PICK_UP_BOULDER;
			return;
		}

		if(workCube.containsLog()){
			WorkToBeDone = WorkType.PICK_UP_LOG;
			return;
		}

		if(workCube.getSceneryType() == SceneryType.TREE){
			WorkToBeDone = WorkType.COLLAPSE_WOOD_CUBE;
			return;
		}

		if(workCube.getSceneryType() == SceneryType.ROCK){
			WorkToBeDone = WorkType.COLLAPSE_ROCK_CUBE;
			return;
		}

		//if none of these conditions were met, nothing has to be done
		WorkToBeDone = WorkType.NOTHING;

	}

	/**
	 * Do the work that has to be done based on the specifications of the cube to work on
	 * @effect getLog().Drop()
	 * @effect getBoulder().Drop()
	 * @effect getWorkCube.getLog().terminate()
	 * @effect getWorkCube.getBoulder().terminate()
	 * @effect setWeight()
	 * @effect setToughness()
	 * @effect getWorkCube().getLog().PickUp()
	 * @effect getWorkCube().getBoulder().PickUp()
	 * @effect getworkCube().caveIn()
	 * 			|getWorkCube().caveIn(false)
	 * @throws 	UnitException
	 * 				converted from getLog().Drop()
	 * 				converted from getBoulder().Drop()
	 * 				converted from getWorkCube().getBoulder().PickUp()
	 * 				converted from getWorkCube().getLog().PickUp()
	 * 				converted from getWorkCube().caveIn()
     */
	private void doWork() throws UnitException{
		try {

			switch (getWorkToBeDone()) {
				case DROP_LOG:
					getLog().Drop();
					break;
				case DROP_BOULDER:
					getBoulder().Drop();
					break;
				case CONSUME_BOULDER_AND_LOG:
					//Terminate material
					getWorkCube().getLog().terminate();
					getWorkCube().getBoulder().terminate();

					//Increase attributes
					setWeight(getWeight()+1);
					setToughness(getToughness()+1);
					break;
				case PICK_UP_BOULDER:
					if(getWorkCube().getBoulder() != null)
					getWorkCube().getBoulder().PickUp(this);
					break;
				case PICK_UP_LOG:
					if(getWorkCube().getLog() != null)
					getWorkCube().getLog().PickUp(this);
					break;
				case COLLAPSE_WOOD_CUBE:
					getWorkCube().caveIn(false);
					break;
				case COLLAPSE_ROCK_CUBE:
					getWorkCube().caveIn(false);
					break;
			}

		}catch (MaterialException | CubeException e) {
			throw new UnitException(UnitException.WORK_METHOD_CONFLICT,
					"A conflict happened during Unit's work");
		}

	}

	/**
	 * Let the Unit work
	 * @param GameTimeAdvanced	The time advanced since previous update
	 * @effect doWork()
	 * @effect increaseXP()
	 * @effect setMovementStatus(MovementType.DOING_NOTHING)
	 * @post
	 * 		|new.getDurationOfWork() == getDurationOfWork() - GameTimeAdvanced >= 0 ? getDurationOfWork() - GameTimeAdvanced : 0
	 * 		|if(new.getDurationOfWork() == 0)
	 * 		|then
	 * 		|	doWork()
	 * 		|	if(getWorkToBeDone() != WorkType.NOTHING)
	 * 		|	then increaseXP(10)
	 * 		|		 new.getWorkToBeDone == WorkType.NOTHING
	 * 		|	setMovementStatus(MovementType.DOING_NOTHING)
	 * 		|	new.getWorkCube() == null
     */
	public void Work(double GameTimeAdvanced) {
		try {
			//Recalculate work to be done
			durationOfWork = getDurationOfWork() - GameTimeAdvanced >= 0 ? getDurationOfWork() - GameTimeAdvanced : 0;

			//After work is done
			if(durationOfWork == 0) {
				//Changes are only reported upon completion
				doWork();

				//Check if need to increase XP, when the Unit did nothing, no XP shall be granted
				if(getWorkToBeDone() != WorkType.NOTHING){
					increaseXP(10);
					WorkToBeDone = WorkType.NOTHING;
				}

				//Work has finished
				this.setMovementStatus( getDefaultBehaviourStatus() ? MovementType.DEFAULT_BEHAVIOUR : MovementType.DOING_NOTHING);
				workCube = null;
			}
		}catch(UnitException e) {
			//It's ok
		}
	}

	@Basic @Raw
	public double getDurationOfWork() {
		return durationOfWork;
	}

	/**
	 * Stop the Unit from executing its DB
	 * @effect setDefaultBehaviourStatus()
	 * 			|setDefaultBehaviourStatus(false)
	 * @effect setMovementStatus()
	 * 			|setMovementStatus(MovementType.DOING_NOTHING)
	 */
	public void StopDefaultBehaviour(){
		
		try {
			this.setDefaultBehaviourStatus(false);
			this.setMovementStatus(MovementType.DOING_NOTHING);
		}catch(UnitException e) {
			//It's ok
		}
	}

	/**
	 * Calculates base speed of Unit
	 * @return	Returns the current base speed of the unit
	 * 			|result == 1.5 * (((double)this.getStrength() + (double)this.getAgility())/(200 * ((double)this.getWeight() / 100)))
	 */
	public double getBaseSpeed() {
		return 1.5 * (((double)Strength + (double)Agility)/(200 * ((double)Weight / 100)));
	}

	/**
	 * Get the walking speed
	 * @return The walking speed of this Unit
     */
	public double getWalkingSpeed() {
		double multiplication;
		Number[] diffs = new Double[0];
		diffs = current_position.getDiff(next_cube.getPosition());
		double diff = diffs[2].doubleValue();
		if(Util.fuzzyLessThanOrEqualTo(diff,-0.4)) {
			multiplication = 0.5;
		}else if(Util.fuzzyGreaterThanOrEqualTo(diff, 0.4)) {
			multiplication = 1.2;
		}else {
			multiplication = 1;
		}
		
		return getBaseSpeed() * multiplication;
	}
	
	/**
	 * Calculates sprinting speed of unit
	 * @return	Sprinting speed of unit
	 * 			|result == 2 * getWalkingSpeed()
	 */
	public double getSprintingSpeed() {
		return 2 * getWalkingSpeed();
	}

	@Basic @Raw
	public double getFallSpeed(){
		return 3;
	}

	/**
	 * Checks if the Unit is still on a stable position, or that it has to start falling
	 *
	 * @effect 	setMovementStatus()
	 * 			|if(getMovementStatus() != MovementType.FALLING)
	 * 			| then if(!currentCube.hasSolidNeighbour())
	 * 			|   then setMovementStatus(MovementType.FALLING)
	 * @effect 	interruptTask()
	 * 			|if(getMovementStatus() != MovementType.FALLING)
	 * 			| then if(!currentCube.hasSolidNeighbour())
	 * 			|	then interruptTask()
	 * @throws 	UnitException
	 * 			|setMovementStatus()
	 * 			|interruptTask()
     */
	private void checkStability() throws UnitException {
		//A units stability can only be checked if it is not already falling
		//While a Unit is falling, it doesn't have a currentCube
		if(getMovementStatus() != MovementType.FALLING) {
			if (!currentCube.hasSolidNeighbour()) {
				setMovementStatus(MovementType.FALLING);

				//if executing a Task while the unit wasn't stable anymore, interrupt this task
				interruptTask();
			}
		}
	}

	/**
	 * Let the Unit Fall towards lower Z levels, until it has reached Z = 0 or the cube under it is solid
	 * @param gameTime	the time advanced since previous update
	 * @effect 	initiateFall
	 * @effect 	HPDecreaseWhileFalling
	 * @effect 	recalculatePositionWhileFalling
	 * 			|if(!isTerminated)
	 * 			| then recalculatePositionWhileFalling(Math.max(0, (getCurrentPosition().getPosition_z().doubleValue() - getFallSpeed() * gameTime)))
	 * @throws 	UnitException
	 * 				Thrown by initiateFall
	 * 				Thrown by recalculatePositionWhileFalling()
	 *
     */
	public void Fall(double gameTime) throws  UnitException {
			//Initiate the Fall, will only have effect when this method is invoked for the first time
			initiateFall();

			//Calculate the new Z coordinate
			double newZ = Math.max(0,(getCurrentPosition().getPosition_z().doubleValue() - getFallSpeed() * gameTime));

			//Decrease HP if necessary
			HPDecreaseWhileFalling(newZ);

			//By falling a Unit can die, check this before calculating other stuff
			if (!isTerminated) {
				recalculatePositionWhileFalling(newZ);
			}
	}

	/**
	 * Recalculates the Position of a Unit while falling
	 * @param newZ	the new Z position of a Unit after the time has been advanced
	 * @throws 	UnitException
	 * 				converted from PositionException
	 * 				converted from GameObjectException
	 * @effect	setCurrentPosition	the Units position will change while falling
	 * @effect	transferToCube
	 * @post	if the Unit has reached a stable position again, its Fall will be ended
	 * 			|if(((int)Math.floor(newZ) == 0) || getLowerCube(newZ).getSceneryType().isSolid())
	 * 			|then new.getStartedFalling() == false
	 * @post	if the Unit has reached a stable position again, its movementStatus will be set to DOING_NOTHING
	 * 			|if(((int)Math.floor(newZ) == 0) || getLowerCube(newZ).getSceneryType().isSolid())
	 * 			|then new.getMovementStatus() == MovementType.DOING_NOTHING
     */
	private void recalculatePositionWhileFalling(double newZ) throws UnitException {
		try{
			//Check if reached Z cube  = 0
			if ((int)Math.floor(newZ) == 0){
				setCurrentPosition(new Position<Double>(
						getCurrentPosition().getPosition_x(),
						getCurrentPosition().getPosition_y(),
						0 + World.getInstance().LENGTH_Z / 2) );
				transferToCube(World.getInstance().getCubeByKey(
						(int)Math.floor(getCurrentPosition().getPosition_x()),
						(int)Math.floor(getCurrentPosition().getPosition_y()),
						0));
				//Falling is finished
				MovementStatus = getDefaultBehaviourStatus() ? MovementType.DEFAULT_BEHAVIOUR : MovementType.DOING_NOTHING;//Force set
				startedFalling = false;

				return;

			} else {
				//get the cube under the Unit's current cube position
				Cube lowerCube = getLowerCube(newZ);
				//Check if this lowerCube is solid
				if (lowerCube.getSceneryType().isSolid()) {
					System.out.println(getName() + "Lower cube is solid, stop falling!");
					//This means the Unit is stable again
					MovementStatus = getDefaultBehaviourStatus() ? MovementType.DEFAULT_BEHAVIOUR : MovementType.DOING_NOTHING;
					//Falling is finished
					startedFalling = false;

					transferToCube(World.getInstance().getCubeByKey(
							(int)Math.floor(getCurrentPosition().getPosition_x()),
							(int)Math.floor(getCurrentPosition().getPosition_y()),
							(int)Math.floor(newZ)));

					setCurrentPosition(new Position<Double>(
							getCurrentPosition().getPosition_x(),
							getCurrentPosition().getPosition_y(),
							Math.floor(newZ) + World.getInstance().LENGTH_Z / 2));

					return;
				}

				//Unit is still falling, update current position
				setCurrentPosition(new Position<Double>(getCurrentPosition().getPosition_x(), getCurrentPosition().getPosition_y(),
						(newZ)));
			}
		} catch (PositionException | GameObjectException e){
			throw new UnitException(UnitException.FALLING_CONFLICT,e.getMessage());
		}
	}

	/**
	 * Initiate a Fall of a Unit
	 * @post	a Unit will start falling
	 * 			new.getStartedFalling() == true;
	 * @effect 	getCurrentCube().removeGameObject()
	 * 			|if(!getStartedFalling())
	 * 			|then getCurrentCube().removeGameObject(this)
	 * @effect 	setCurrentPosition()
	 * 			|if(!getStartedFalling())
	 * 			|then setCurrentPosition(new Position<Double>(
	 *			|Math.floor(getCurrentPosition().getPosition_x()) + World.getInstance().LENGTH_X / 2,
	 *			|Math.floor(getCurrentPosition().getPosition_y()) + World.getInstance().LENGTH_Y / 2,
	 *			|getCurrentPosition().getPosition_z()))
	 * @throws UnitException
	 * 			|converted from setCurrentPosition()
     */
	private void initiateFall() throws UnitException {
		try {
			if (!startedFalling) {
				//Unit must Fall towards the centre of a Cube
				setCurrentPosition(new Position<Double>(
						Math.floor(getCurrentPosition().getPosition_x()) + World.getInstance().LENGTH_X / 2,
						Math.floor(getCurrentPosition().getPosition_y()) + World.getInstance().LENGTH_Y / 2,
						getCurrentPosition().getPosition_z()));
				//So that controller knows it must not check its stability anymore
				startedFalling = true;
			}
		} catch (PositionException e){
			throw new UnitException(UnitException.FALLING_CONFLICT,e.getMessage());
		}
	}

	@Basic @Raw
	private boolean getStartedFalling(){
		return startedFalling;
	}

	/**
	 *
	 * @param 	newZ	the Z coordinate after the has been advanced
	 * @return	The cube under the Unit's new position cube while falling
	 * 			|World.getInstance().getCubeByKey(
	 *			|	(int) Math.floor(getCurrentPosition().getPosition_x()),
	 *			|	(int) Math.floor(getCurrentPosition().getPosition_y()),
	 *			|	(int) Math.floor(newZ - 1))
     */
	private Cube getLowerCube(double newZ) {
		return World.getInstance().getCubeByKey(
				(int) Math.floor(getCurrentPosition().getPosition_x()),
				(int) Math.floor(getCurrentPosition().getPosition_y()),
				(int) Math.floor(newZ - 1));
	}

	/**
	 * Decreases the Unit's HP while falling, if necessary
	 * @param 	newZ	the Z coordinate after the time has been advanced
	 * @effect  decreaseHitPoints
	 * 			|decreaseHitPoints(10*((int) Math.floor(getCurrentPosition().getPosition_z()) - (int) Math.floor(newZ))
     */
	private void HPDecreaseWhileFalling(double newZ) {
		int multiplier = (int) Math.floor(getCurrentPosition().getPosition_z()) - (int) Math.floor(newZ);
		decreaseHitPoints(10 * multiplier);
	}

	@Basic @Raw
	public Task getTask(){
		return assignedTask;
	}

	/**
	 * Check whether this Unit has a Task assigned to it
	 * @return true when this Unit has a Task assigned to it
	 * 			|result == getTask() != null
     */
	public boolean hasTask(){
		return getTask() != null;
	}
	/**
	 * Assign a given Task to this Unit
	 * @param newTask	The given Task
	 * @throws 	UnitException
	 * 			|if(!canHaveAsTask(newTask))
	 * @post 	The given Task will be assigned to this Unit
	 * 		 	|new.getTask() == newTask
	 * @effect 	newTask.assignToUnit() The relation must be updated on both sides
     */
	public void assignTask(Task newTask) throws UnitException {

		//To avoid a loop, since newTask.assignToUnit(this) will call this method again
		if (newTask == getTask()) return;

		if (! canHaveAsTask(newTask)) throw new UnitException(UnitException.TASK_RELATIONSHIP_CONFLICT,
				"Unable to assign this Task to a Unit ");
		assignedTask = newTask;

		try {
			newTask.assignToUnit(this);
		} catch (TaskException e) {
			throw new UnitException(UnitException.TASK_RELATIONSHIP_CONFLICT,e.getMessage());
		}
	}

	/**
	 * Remove the Task assigned to this Unit
	 * @post There will be no more task assigned to this Unit
	 * 		 |new.getTask == null
	 * @effect getTask().deAssignUnit()
	 * @throws 	UnitException
	 * 		   	|getTask.deAssignUnit()
	 */
	public void removeTask() throws UnitException {

		try{
			//To avoid a loop
			if ( getTask() == null ) return;

			Task task = assignedTask;

			assignedTask = null;

			task.deAssignUnit();
		} catch (TaskException e ){
			throw new UnitException(UnitException.TASK_RELATIONSHIP_CONFLICT,e.getMessage());
		}
	}

	/**
	 * Check whether a Task can be assigned to this Unit
	 * @return  true if this Unit has no Task assigned to it
	 * 			|result == (getTask() == null)
     */
	public boolean canAssignTaskTo(){
		return getTask() == null;
	}

	/**
	 * Check whether the Unit can have a given Task as its assigned Task
	 * @param newTask	The given task
	 * @return 	true when this Unit has no Task assigned to it yet and newTask has no null references
	 * 			|result == canAssignTaskTo() && newTask != null
     */
	public boolean canHaveAsTask(Task newTask){
		return canAssignTaskTo() && newTask != null;
	}

	/**
	 * Check whether this Unit is currently executing a Task
	 * @return True when it is executing a Task
	 * 			|result == getTask() != null
     */
	public boolean isExecutingTask(){
		return getTask() != null;
	}

	/**
	 * Check if we can interrupt a Task, and do so if possible
	 * @return True if a Task can be interrupted
	 * 			|result == if(isExecutingTask())
	 *
	 * @effect getFaction().getScheduler().interruptTask()
	 * 			|if(isExecutingTask())
	 * 			| then getFaction().getScheduler().interruptTask(getTask())
	 * @throws UnitException
	 * 			|getFaction().getScheduler().interruptTask(getTask())
     */
	public boolean interruptTask() throws UnitException {
		//First check if we can interrupt a Task
		if (isExecutingTask()) {

			try {
				getFaction().getScheduler().interruptTask(getTask());
				//The Unit was executing a Task, and it is now finished
				return true;
			} catch (SchedulerException e){
				throw new UnitException(UnitException.SCHEDULER_INTERRUPTION_CONFLICT, "An error occurred during the interruption of the Task");
			}

		} else return false;
	}

	/**
	 * Check whether this Unit is must currently execute some Task and let it execute this Task if so
	 * @param timeAdvanced	The game time advanced since previous update
	 * @effect Executor.execute()
	 * 			|if(hasTask())
	 * 			| then Executor.execute(getTask(), calculateStatements(timeAdvanced))
	 * @throws UnitException
	 * 			If an error occurs during the execution of the task, converted from ExecutionException from hillbillies.scheduler.execution.runtime
     */
	private void taskUpdates(double timeAdvanced) throws UnitException {
		if(hasTask()){
			try {
				Executor.execute(getTask(), calculateStatements(timeAdvanced));
			} catch (ExecutionException e){
				throw new UnitException(UnitException.TASK_EXECUTION_ERROR,e.getMessage());
			}
		}
	}

}
