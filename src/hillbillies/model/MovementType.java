package hillbillies.model;

import be.kuleuven.cs.som.annotate.Value;

/**
 * The type of movement a unit is doing
 *
 * @version 1.0
 * @author Jens Mortier
 */
@Value
public enum MovementType {
	WALKING (false),
	SPRINTING (false),
	RESTING (true),
	WORKING (false),
	DEFAULT_BEHAVIOUR (false),
	FIGHTING (false),
	DOING_NOTHING (true),
	DEFENDING (false),
	FALLING (false);

	/**
	 * True if this is rather a passive Status i.e. not that much movement
	 */
	private boolean isPassive;

	MovementType(boolean passive){
		this.isPassive = passive;
	}

	/**
	 * Checks whether this is a passive status
	 * @return true when it is a passive status
     */
	public boolean isPassive(){
		return isPassive;
	}
}
