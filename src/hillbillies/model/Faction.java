package hillbillies.model;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Raw;
import hillbillies.exceptions.FactionException;
import hillbillies.exceptions.SchedulerException;
import hillbillies.exceptions.UnitException;
import hillbillies.scheduler.execution.components.Scheduler;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

/**
 * Kind of group which contains a number of Units that belong to the same team
 *
 * @version 1.0
 * @author  Jens Mortier
 */
public class Faction {

    /**
     *  Constant containing the maximum number of units that can belong to this faction
     */
    public static final int MAX_NUMBER_OF_UNITS = 50;

    /**
     * Contains the units that belong to this faction
     */
    private LinkedList<Unit> units;

    /**
     * True if Faction is terminated
     */
    private boolean isTerminated;

    /**
     * The Scheduler associated with this Faction
     */
    private Scheduler scheduler;

    /**
     * Constructor for faction
     */
    public Faction() throws FactionException {
        isTerminated = false;
        units = new LinkedList<Unit>();
        try {
            scheduler = new Scheduler(this);
        } catch(SchedulerException e){
            throw new FactionException(FactionException.SCHEDULER_ASSIGNEMENT_ERROR,e.getMessage());
        }
    }

    /**
     * Returns true if Faction is terminated
     * @return  True if faction is terminated
     */
    public boolean isTerminated() {
        return isTerminated;
    }

    /**
     * Destructor for objects of Faction class
     */
    public void terminate() {
        //Terminate all units
        for(Unit unit : units) {
            unit.terminate();
        }

        //Delete from game world
        World.getInstance().removeFaction(this);

        isTerminated = true;
    }

    /**
     * Returns all units of this faction
     * @return  All units that belong to this faction
     */
    public LinkedList<Unit> getUnits() {
        return units;
    }

    /**
     * Converts units to set
     * @return  All units that belong to this factions
     */
    public Set<Unit> unitsToSet() {
        HashSet<Unit> result = new HashSet<>();
        for(Unit unit : units) {
            result.add(unit);
        }

        return result;
    }

    /**
     * Returns the number of units in this faction
     * @return  The number of units in this faction
     */
    public int getNbOfUnits() {
        return units.size();
    }

    /**
     * Checks whether unit given can be added to units
     * @param unit  Unit to be added
     * @return      True if unit can be added
     */
    public boolean canHaveAsUnit(Unit unit) {
        return !isTerminated() && unit != null && !unit.isTerminated() && getNbOfUnits() < MAX_NUMBER_OF_UNITS;
    }

    /**
     * Adds unit at the end of the unit list
     * @param unit  The unit to add
     * @throws FactionException
     *          | !canHaveAsUnit(unit)
     */
    public void addUnit(Unit unit) throws FactionException{
        if(!canHaveAsUnit(unit)) throw new FactionException(FactionException.UNIT_CANT_BE_ADDED,"Unit can't be added to this faction");
        //If a faction already contains this unit, it is impossible
        if(units.contains(unit)) return;
        try{
            unit.setFaction(this);
            units.addLast(unit);
            System.out.println("Unit added to faction");
        } catch (UnitException e){
            throw new FactionException(FactionException.UNIT_CANT_BE_ADDED,"Unable to add Unit to a faction");
        }


    }

    /**
     * Returns unit at the position given
     * @param index The position to get the unit of
     * @return      The unit at the position given
     */
    public Unit getUnitAt(int index) throws FactionException {
        if(index < 0 || index > units.size() - 1) throw new FactionException(FactionException.INDEX_OUT_OF_RANGE,"Index is out of range");
        return units.get(index);
    }

    /**
     * Removes unit from faction
     * @param unit  Unit to remove
     * @throws FactionException
     *          | !unit.isTerminated() && unit.getFaction() == this
     */
    public void removeUnit(Unit unit) throws FactionException{
        if(!unit.isTerminated() && unit.getFaction() == this) throw new FactionException(FactionException.CANNOT_REMOVE_UNIT_FROM_FACTION,"Unit has to be transferred to other faction first");
        units.remove(units.indexOf(unit));
    }

    @Basic @Raw
    public Scheduler getScheduler(){
        return scheduler;
    }


}
