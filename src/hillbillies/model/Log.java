package hillbillies.model;

import hillbillies.exceptions.CubeException;
import hillbillies.exceptions.MaterialException;
import hillbillies.exceptions.UnitException;
import hillbillies.exceptions.WorldException;
import hillbillies.gameworld.Cube;
import hillbillies.gameworld.Material;
import hillbillies.gameworld.Position;

/**
 * Type of material that results from TREE types
 *
 * @version 1.0
 * @author Maxim Verbiest
 */
public class Log extends Material {

    /**
     * Constructor
     * @param newPosition   The position where this log will be located after creation
     * @param newCube   The cube in which this log will be located after creation
     * @throws MaterialException
     */
    public Log(Position newPosition, Cube newCube) throws MaterialException {
        super(newPosition, newCube);
    }

    /**
     * Let the given carrier pick up this log
     * @param newCarrier    The given carrier
     * @effect newCarrier.setLog()
     * @effect newCarrier.addWeightOfMaterial()
     * @throws MaterialException
     */
    @Override
    public void pickUpMaterial(Unit newCarrier) throws MaterialException {
        try {
            newCarrier.setLog(this);
            newCarrier.addWeightOfMaterial(this);
        }catch(UnitException e) {
            throw new MaterialException(MaterialException.MATERIAL_MANIPULATION_FAILED,e.getMessage());
        }
    }

    /**
     * Let the given carrier drop this log
     * @param carrier The given carrier
     * @effect carrier.setWeight()
     * @effect carrier.setLog()
     * @throws MaterialException if the given carrier has null references
     */
    @Override
    public void dropMaterial(Unit carrier) throws MaterialException {
        if (carrier != null) {
            carrier.setWeight(carrier.getWeight() - this.getWeight());
            carrier.setLog(null);
        } else throw new MaterialException(MaterialException.NULL_VALUE_AS_PARAMETER,"Tried to drop a material while carrier is NULL");
    }
}
