package hillbillies.model;

import hillbillies.exceptions.CubeException;
import hillbillies.exceptions.MaterialException;
import hillbillies.exceptions.UnitException;
import hillbillies.exceptions.WorldException;
import hillbillies.gameworld.Cube;
import hillbillies.gameworld.Material;
import hillbillies.gameworld.Position;

/**
 * Type of material which results from ROCK types.
 *
 * @version 1.0
 * @author Maxim Verbiest
 */
public class Boulder extends Material {

    /**
     * Constructor
     * @param position  The position where this boulder will be located after creation
     * @param cube  The cube in which this boulder will be located after creation
     * @throws  MaterialException
     */
    public Boulder(Position position, Cube cube) throws MaterialException {

       super(position,cube);

    }

    /**
     * Let the given carrier pick up this boulder
     * @param newCarrier  The given carrier
     * @effect newCarrier.setBoulder()
     * @effect newCarrier.addWeightOfMaterial()
     * @throws MaterialException
     */
    @Override
    public void pickUpMaterial(Unit newCarrier) throws MaterialException {
        try {
            newCarrier.setBoulder(this);
            newCarrier.addWeightOfMaterial(this);
        }catch(UnitException e) {
            throw new MaterialException(MaterialException.MATERIAL_MANIPULATION_FAILED,e.getMessage());
        }
    }

    /**
     * Let the given carrier drop this boulder
     * @param carrier   The given carrier
     * @effect carrier.setWeight()
     * @effect carrier.setBoulder()
     * @throws MaterialException if the given carrier has null references
     */
    @Override
    public void dropMaterial(Unit carrier) throws MaterialException {
        if (carrier != null) {
            carrier.setWeight(carrier.getWeight() - this.getWeight());
            carrier.setBoulder(null);
        } else throw new MaterialException(MaterialException.NULL_VALUE_AS_PARAMETER,"Tried to drop a material while carrier is NULL");

    }


}
