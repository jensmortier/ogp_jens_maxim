package hillbillies.model;

import be.kuleuven.cs.som.annotate.Value;

/**
 * Helper class that will be used in the filter methods
 * @version 1.0
 * @author Maxim Verbiest
 */
@Value
public enum Type {
    FRIEND,
    ENEMY,
    ANY;
}
