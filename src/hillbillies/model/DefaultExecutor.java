package hillbillies.model;

import hillbillies.DefaultBehaviour.DefaultAction;
import hillbillies.DefaultBehaviour.DefaultAttack;
import hillbillies.DefaultBehaviour.DefaultMoveToRandom;
import hillbillies.DefaultBehaviour.DefaultWork;
import hillbillies.exceptions.DefaultBehaviourException;
import hillbillies.model.Unit;
import ogp.framework.util.Util;
import org.reflections.Reflections;
import org.reflections.scanners.ResourcesScanner;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.reflections.util.FilterBuilder;

import java.lang.reflect.InvocationTargetException;
import java.util.*;

/**
 * Class that will be used to perform the default behaviour of a given Unit
 * @version 1.0
 * @author Maxim Verbiest
 */
public class DefaultExecutor {

    /**
     * Contains the Actions that can be performed for a given Unit
     */
    private LinkedList<DefaultAction> possibleActions;

    /**
     * The unit that will perform this actions when it's in default behaviour
     */
    private Unit unit;

    /**
     * Constructor
     * @param newUnit   the given Unit
     * @throws DefaultBehaviourException
     *          When the given Unit is a null value
     */
    public DefaultExecutor(Unit newUnit) throws DefaultBehaviourException {

        if (newUnit == null) throw new DefaultBehaviourException(DefaultBehaviourException.NULL_VALUE_AS_PARAMETER,
                "Default behaviour asked to perform actions for a NULL value");

        //Construction of a new linked list
        possibleActions = new LinkedList<>();

        //assign unit
        unit = newUnit;

        //Load all possible Actions that this Unit can perform given the current state of the game World
        loadActions();

    }

    /**
     * Execute a randomly chosen action to perform in its default behaviour
     * @Throws DefaultBehaviourException
     *          In the execution of the chosen Action
     */
    public void execDefaultAction() throws DefaultBehaviourException{

        double randomValue = Math.random();
        for(int i = 0; i< possibleActions.size(); i++){
            if (Util.fuzzyLessThanOrEqualTo(randomValue , ( (double)(i+1) /(double) possibleActions.size() ))){
                possibleActions.get(i).executeIfChosen();
                break;
            }
        }

    }

    /**
     * Load all the actions that a Unit can perform given the current state of the game world
     * @throws DefaultBehaviourException
     *          When an error occurred during the loading of all the objects representing these actions
     */
    private void loadActions() throws DefaultBehaviourException {
       try {
           //Load all the default actions from the DefaultBehaviour package
           List<ClassLoader> classLoadersList = new LinkedList<ClassLoader>();
           classLoadersList.add(ClasspathHelper.contextClassLoader());
           classLoadersList.add(ClasspathHelper.staticClassLoader());

           Reflections reflections = new Reflections(new ConfigurationBuilder()
                   .setScanners(new SubTypesScanner(false /* don't exclude Object.class */), new ResourcesScanner())
                   .setUrls(ClasspathHelper.forClassLoader(classLoadersList.toArray(new ClassLoader[0])))
                   .filterInputsBy(new FilterBuilder().include(FilterBuilder.prefix("hillbillies.DefaultBehaviour"))));
           Set<Class<? extends DefaultAction>> classes = reflections.getSubTypesOf(DefaultAction.class);

           //Check which of the default actions can be performed given the current state of the game World
           Iterator<Class<? extends DefaultAction>> it = classes.iterator();
           while(it.hasNext()){
                 try{
                     possibleActions.add(it.next().getConstructor(Unit.class).newInstance(unit));
                 } catch (InvocationTargetException e){
                     //Do nothing, Instantiation of this object has aborted since it is impossible to perform this action atm.
                 }
           }
       } catch (IllegalAccessException| InstantiationException|NoSuchMethodException e){
           throw new DefaultBehaviourException(DefaultBehaviourException.LOAD_CONFLICT,
                   e.getMessage());
       }


    }

}
