package hillbillies.model;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Raw;
import hillbillies.exceptions.*;
import hillbillies.gameworld.*;
import hillbillies.part2.listener.TerrainChangeListener;
import hillbillies.util.ConnectedToBorder;
import hillbillies.util.Pair;

import java.util.*;

/*************
 * Singleton * class which represents the world with all its objects, cubes and monitoring algorithms
 *************
 *
 * @version 1.0
 * @author Jens Mortier
 *          Maxim Verbiest
 */
public class World implements Advanceable {

    /**
     * Counter of generated Units
     */
    private int unitCounter = 0;

    /**
     * Listener to be called when terrain changes
     */
    private TerrainChangeListener terrainChangeListener;

    /**
     * Contains the cubes of the game world with a key based on its coordinates
     */
    private HashMap<int[], Cube> cubes;

    /**
     * Contains all the factions of the current world
     */
    private LinkedList<Faction> factions;

    /**
     * Contains all the gameobject of the world
     */
    private HashSet<GameObject> gameObjects;

    /**
     * Singleton container
     */
    private static World instance = null;

    /**
     * Lock such that it is impossible for concurrent modifications in gameObjects
     */
    private boolean objectLock = false;

    /**
     * Buffer that keeps track of all the game objects that has to be deleted
     */
    private HashSet<GameObject> deleteBuffer;

    /**
     * Buffer that keeps track of all the game objects that has to be added
     */
    private HashSet<GameObject> addBuffer;

    /**
     * World connectivity status
     */
    ConnectedToBorder connectedToBorder;

    /**
     * Define game world characteristics
     */
    private static int GAME_WORLD_BOUND_X = 50;
    private static int GAME_WORLD_BOUND_Y = 50;
    private static int GAME_WORLD_BOUND_Z = 50;
    public static final double LENGTH_X = 1;
    public static final double LENGTH_Y = 1;
    public static final double LENGTH_Z = 1;

    /**
     * Define number characteristics
     */
    public static final int MAX_FACTIONS = 5;

    /**
     * Maximum number of units
     */
    public static final int MAX_UNITS = 100;

    /**
     * Returns true if the given double coordinates form a valid position
     *
     * @param x The x coordinate of the position
     * @param y The y coordinate of the position
     * @param z The z coordinate of the position
     * @return True if the given coordinates form a valid position considering the gameWorld's bounds
     */
    public static boolean isValidDoublePosition(double x, double y, double z) {
        return x <= GAME_WORLD_BOUND_X - LENGTH_X / 2 && y <= GAME_WORLD_BOUND_Y - LENGTH_Y / 2 && z <= GAME_WORLD_BOUND_Z - LENGTH_Z / 2
                && x >= 0 && z >= 0 && y >= 0;
    }

    /**
     * Returns true if the given integer coordinates form a valid position
     *
     * @param x The x coordinate of the position
     * @param y The y coordinate of the position
     * @param z The z coordinate of the position
     * @return True if the given coordinates form a valid position considering the gameWorld's bounds
     */
    public static boolean isValidIntegerPosition(int x, int y, int z) {
        return x <= GAME_WORLD_BOUND_X - LENGTH_X / 2 && y <= GAME_WORLD_BOUND_Y - LENGTH_Y / 2 && z <= GAME_WORLD_BOUND_Z - LENGTH_Z / 2
                && x >= 0 && z >= 0 && y >= 0;
    }

    /**
     * General inspector for validness of a position from any kind
     * @param x The x coordinate of the position
     * @param y The y coordinate of the position
     * @param z The z coordinate of the position
     * @return True if the given coordinates form a valid position considering the gameWorld's bounds
     */
    public static boolean isValidPosition(Number x, Number y, Number z) {
        if (x instanceof Integer) {
            return isValidIntegerPosition((Integer) x.intValue(), (Integer) y.intValue(), (Integer) z.intValue());
        } else if (x instanceof Double) {
            return isValidDoublePosition((Double) x.doubleValue(), (Double) y.doubleValue(), (Double) z.doubleValue());
        } else {
            return false;
        }
    }

    @Basic @Raw
    public static int GAME_WORLD_BOUND_X() {
        return GAME_WORLD_BOUND_X;
    }

    @Basic @Raw
    public static int GAME_WORLD_BOUND_Y() {
        return GAME_WORLD_BOUND_Y;
    }

    @Basic @Raw
    public static int GAME_WORLD_BOUND_Z() {
        return GAME_WORLD_BOUND_Z;
    }


    /**
     * Constructor
     * @post A connectedToBorder object will be instantiated
     * @post A list of factions will be instantiated
     * @post A map of cubes will be instantiated
     * @post A set of gameObjects will be instantiated
     */
    private World() {
        //Init connected to border algorithm
        connectedToBorder = new ConnectedToBorder(GAME_WORLD_BOUND_X, GAME_WORLD_BOUND_Y, GAME_WORLD_BOUND_Z);

        //Init factions
        factions = new LinkedList<Faction>();

        //Init cubes
        cubes = new HashMap<>();

        //Init game objects
        gameObjects = new HashSet<>();
    }

    /**
     * Advances time of gameworld
     * @param gameTime Game time elapsed since previous update
     * @post The objectLock will be set to true while advancing this World's object's time
     * @post The objectLock will be set to false after advancing this World's object's time
     * @effect advanceTime() for all objects implementing the Advanceable interface
     * @effect emptyAddBuffer()
     * @effect emptyDeleteBuffer()
     * @throws WorldException
     *          Thrown by the advanceTime from an advanceable object of this World
     */
    public void advanceTime(double gameTime) throws WorldException {
        //Advance all game objects
        try {
            objectLock = true;
            //Create the buffers
            addBuffer = new HashSet<>();
            deleteBuffer = new HashSet<>();

            for (GameObject object : gameObjects) {
                try {
                    object.advanceTime(gameTime);
                } catch (Exception e) {
                    throw new WorldException(WorldException.ADVANCE_TIME_ERROR, e.getMessage());
                }
            }
        } catch (Exception e) {
            throw new WorldException(WorldException.ADVANCE_TIME_ERROR,e.getMessage());
        }

        objectLock = false;

        //Empty the buffers
        emptyAddBuffer();
        emptyDeleteBuffer();

        //Advance all cubes
        for (int[] key : cubes.keySet()) {
            try {
                cubes.get(key).advanceTime(gameTime);
            } catch (Exception e) {
                throw new WorldException(WorldException.ADVANCE_TIME_ERROR, e.getMessage());
            }
        }
    }

    /**
     * Add all the gameObjects from the addBuffer
     * @effect addGameObject()
     * @throws WorldException
     *          |addGameObject()
     */
    private void emptyAddBuffer() throws WorldException {
        for (GameObject object : addBuffer) {
            addGameObject(object);
        }
    }

    /**
     * Delete all the gameObjects from the deleteBuffer
     * @effect removeGameObject()
     */
    private void emptyDeleteBuffer(){
        for(GameObject object : deleteBuffer)
            removeGameObject(object);
    }

    /**
     * Sets configuration of cubes for this world
     * @param initialConfig The input configuration
     * @effect The map of cubes will be filled
     * @throws WorldException
     *          If terrain type in initial config isn't valid
     */
    public void setConfig(int[][][] initialConfig) throws WorldException {
        //Set game world bounds
        GAME_WORLD_BOUND_X = initialConfig.length;
        GAME_WORLD_BOUND_Y = initialConfig[0].length;
        GAME_WORLD_BOUND_Z = initialConfig[0][0].length;

        //Set config
        for (int i = 0; i < GAME_WORLD_BOUND_X; i++) {
            for (int j = 0; j < GAME_WORLD_BOUND_Y; j++) {
                for (int k = 0; k < GAME_WORLD_BOUND_Z; k++) {
                    int current = initialConfig[i][j][k];
                    SceneryType type = SceneryType.getSceneryType(current);
                    if (type == null)
                        throw new WorldException(WorldException.INVALID_TERRAIN_TYPE, "Type given isn't valid");

                    //Create cube
                    Cube new_cube = null;
                    try {
                        new_cube = new Cube(type, new Position<Integer>(i, j, k));
                    } catch (PositionException | CubeException e) {/*Do nothing*/}

                    //Add to hashmap
                    int[] coordinates = new int[3];
                    coordinates[0] = i;
                    coordinates[1] = j;
                    coordinates[2] = k;
                    cubes.put(coordinates, new_cube);
                }
            }
        }
    }

    /**
     * If cube became passable, it reports it's new state to the connected to border algorithm and to the world's terrain listener
     * @param cube       The cube to report
     * @param isPassable True if cube became passable
     */
    public void reportCubeStateChange(Cube cube, boolean isPassable) {
        //Calculate vector
        Number[] vector = cube.getPosition().toVector();

        if (isPassable) {
            //Report to connectedToBorder algorithm
            connectedToBorder.changeSolidToPassable((Integer) vector[0], (Integer) vector[1], (Integer) vector[2]);
        }

        //Report to terrain listener
        terrainChangeListener.notifyTerrainChanged((Integer) vector[0], (Integer) vector[1], (Integer) vector[2]);
    }

    /**
     * Returns the pointer corresponding with the given coordinates
     * @param coordinates   The given coordinates
     * @return An integer array containing the pointer from the given coordinates or null if there is no match
     */
    public int[] getArrayPointer(Number[] coordinates) {
        for (int[] pos : cubes.keySet()) {
            if (pos[0] == coordinates[0].intValue() && pos[1] == coordinates[1].intValue() && pos[2] == coordinates[2].intValue())
                return pos;
        }
        return null;
    }

    /**
     * Checks if cube is connected to the border
     * @param cube Cube to check
     * @return True if cube is connected to the border
     */
    public boolean isCubeAttachedToBorder(Cube cube) {
        if (cube != null) {
            Number[] v = cube.getPosition().toVector();
            return connectedToBorder.isSolidConnectedToBorder((Integer) v[0], (Integer) v[1], (Integer) v[2]);
        } else {
            return true;
        }
    }

    /**
     * Sets terrain listener. Can happen only once.
     * @param listener The listener to set
     * @post First invocation will set the terrainChangeListener to the given listener
     */
    public void setTerrainChangeListener(TerrainChangeListener listener) {
        if (terrainChangeListener == null) terrainChangeListener = listener;
    }

    /**
     * Function which delivers singleton instance of World
     * @return Singleton instance
     */
    public static World getInstance() {
        if (instance == null) {
            instance = new World();
        }

        return instance;
    }

    /**
     * Returns a new instance of this World
     * @return A new instance of this World
     */
    public static World resetInstance() {
        instance = new World();
        return instance;
    }

    /**
     * Returns the number of factions present in this World
     * @return The number of factions present in this World
     */
    private int getNrOfFactions() {
        if (getFactions() == null) {
            return 0;
        } else return getFactions().size();
    }

    /**
     * Let a Unit join the faction with the least number of Units in it.
     * @param newUnit   the Unit that has to join the smallest faction
     * @post The given Unit will join the smallest faction of this World
     * @throws FactionException
     */
    private void joinSmallestFaction(Unit newUnit) throws FactionException {

        //Such that we have a start for the iteration
        Faction smallestFaction = factions.get(0);

        //Search the smallest faction
        for (int i = 0; i < factions.size(); i++) {
            if (factions.get(i).getNbOfUnits() < smallestFaction.getNbOfUnits()) {
                smallestFaction = factions.get(i);
            }
        }

        //Join the smallest faction
        smallestFaction.addUnit(newUnit);

    }

    /**
     * Checks whether all factions are full
     * @return true if all the factions are full and no factions can be created anymore
     */
    private boolean allFactionsFull() {
        //If the list is still NULL, there have still been no Units created
        if (getFactions() == null) return false;

        if (getNrOfFactions() < MAX_FACTIONS) return false;

        //Since we've reached this, there are as much active factions as possible
        //Check if all these factions are also full
        //This will probably be false always, since the maximum number of units is smaller than sum of maximum number of units of all factions
        for (int i = 0; i < factions.size(); i++) {
            if (factions.get(i).getNbOfUnits() < Faction.MAX_NUMBER_OF_UNITS) {
                return false;
            }
        }
        return true;
    }

    /**
     * Generates a random unit with random properties
     * @return a randomly generated Unit
     * @throws WorldException
     *          thrown by genValidPos()
     *          thrown by constructor of Unit
     */
    private Unit generateUnit(boolean defaultBehaviourStatus) throws WorldException {
        try {
            unitCounter ++;
            String index = "Unit " + unitCounter;

            Position<Double> generatedPos = genValidPos();

            return new Unit(index, generatedPos.getPosition_x().doubleValue(), generatedPos.getPosition_y().doubleValue(),
                    generatedPos.getPosition_z().doubleValue(), genAttribute(), genAttribute(), genAttribute(), genAttribute(), defaultBehaviourStatus);

        } catch (UnitException e) {
            throw new WorldException(WorldException.CONFLICT_WHILE_GENERATING_UNIT, e.getMessage());
        }
    }

    /**
     * Let a randomly generated Unit spawn into the game world, if the game world isn't full yet
     * @param defaultBehaviourStatus determines if the Unit will spawn in default behaviour or not
     * @post If this world isn't full yet, a newly generated Unit will join the smallest faction available or
     *          a new faction if the maximum number of factions has not been reached yet
     * @throws WorldException
     *          If an error occurs during the join of the generated Unit in a faction
     */
    public Unit addUnitToWorld(Boolean defaultBehaviourStatus) throws WorldException {

        try {
            //Check if it is still possible to generate a Unit
            if ((!allFactionsFull()) && (getActiveUnitsInWorld().size() < MAX_UNITS)) {
                //generate random Unit
                Unit newUnit = generateUnit(defaultBehaviourStatus);
                System.out.println(newUnit == null);

                //Check if a new Faction can be created
                if (getNrOfFactions() < MAX_FACTIONS) {
                    //create new Faction
                    Faction newFaction = new Faction();

                    //add new Unit to the new Faction
                    newFaction.addUnit(newUnit);

                    //add new Faction to the list of factions
                    factions.add(newFaction);

                    //Add to world
                    gameObjects.add(newUnit);

                } else { //Maximum number of Factions has been reached
                    joinSmallestFaction(newUnit);
                }

                return newUnit;
            } // It was not possible to generate a Unit, because the world was full
            return null;

        } catch (FactionException e) {
            throw new WorldException(WorldException.CANNOT_ADD_GAME_OBJECT, "Conflict occurred during spawn of Unit: " + e.getMessage());
        }
    }

    /**
     * Generates a random coordinate
     * @param bound a game world bound
     * @return a randomly generated coordinate between the world's bounds
     */
    private double genCoordinate(int bound) {
        return 0.5 + (int) Math.round(Math.random() * (bound - 1));
    }

    /**
     * Generates a fully random position
     * @return an array with an x, y and z coordinate
     */
    private double[] genPosition() {
        double[] pos = new double[3];

        pos[0] = genCoordinate(GAME_WORLD_BOUND_X());
        pos[1] = genCoordinate(GAME_WORLD_BOUND_Y());
        pos[2] = genCoordinate(GAME_WORLD_BOUND_Z());

        return pos;
    }

    /**
     * Generates a valid position
     * @return a random, and valid position
     * @throws WorldException if this position is still not valid
     */
    private Position<Double> genValidPos() throws WorldException {

        try {
            //The cube based on the generated position must be passable
            boolean passable = false;
            //The cube based on the generated position must be at z = 0 or the lower cube must be solid
            boolean solidConstraint = false;
            //Where the generated position will be currently saved
            double[] pos = new double[3];

            //while passable or solidConstraint is false, the generated position will not be valid
            while (!passable || !solidConstraint) {

                //generate a position
                pos = genPosition();
                //get the cube coordinates from the generated position
                int[] cubeCoordinates = new int[3];
                for (int i = 0; i < 3; i++) {
                    cubeCoordinates[i] = (int) Math.floor(pos[i]);
                }

                passable = constraintChecker(cubeCoordinates)[0];
                solidConstraint = constraintChecker(cubeCoordinates)[1];

            }

            return new Position<Double>(pos[0], pos[1], pos[2]);
        } catch (PositionException e) {
            throw new WorldException(WorldException.CONFLICT_IN_GENERATING_RANDOM_VALID_POS,
                    "A conflict occurred during the generation of a random and valid position");
        }
    }

    /**
     * Checks constraints depending on Cube for a generated Unit
     * @param cubeCoordinates the coordinates corresponding with the cube to check
     * @return true if the generated position is in a passable cube and the lower cube is solid or the generated cube is at z = 0
     */
    private boolean[] constraintChecker(int[] cubeCoordinates) {

        //Result array, first value for passable, second for solidConstraint
        boolean[] boolArray = new boolean[2];

        //check passable constraint
        Cube generatedCube = getCubeByKey(cubeCoordinates[0], cubeCoordinates[1], cubeCoordinates[2]);
        boolArray[0] = (!generatedCube.getSceneryType().isSolid());

        //check for solidConstraint
        //Check if Unit's cube is at Z = 0
        if (generatedCube.getPosition().getPosition_z() == 0) {
            boolArray[1] = true;
        } else {
            //check if cube under Unit is solid
            Cube lowerCube = getCubeByKey(cubeCoordinates[0], cubeCoordinates[1], cubeCoordinates[2] - 1);
            boolArray[1] = (lowerCube.getSceneryType().isSolid());
        }

        return boolArray;

    }

    /**
     * Generates a random attribute value
     * @return a randomly generated value between 25 and 100
     */
    private int genAttribute() {
        return 25 + (int) Math.round(Math.random() * 75);
    }

    /**
     * Returns the cube matching the given coordinates
     * @param x The given x coordinate
     * @param y The given y coordinate
     * @param z The given z coordinate
     * @return A cube matching the given coordinates, or null if there is no match
     */
    public Cube getCubeByKey(int x, int y, int z) {
        Integer[] coordinates = new Integer[3];
        coordinates[0] = x;
        coordinates[1] = y;
        coordinates[2] = z;

        return getCubeByVector(coordinates);
    }

    /**
     * Returns the cube matching a given array of coordinates
     * @param coordinates The given array of coordinates
     * @return A cube matching the given array of coordinates or null if there is no match
     */
    public Cube getCubeByVector(Number[] coordinates) {
        int[] key = getArrayPointer(coordinates);
        if (cubes.containsKey(key)) {
            return cubes.get(key);
        } else {
            return null;
        }
    }

    /**
     * Adds faction to world
     * @param faction Faction to add
     * @post If this world can have the given faction as one of its factions, it will be added to the list of factions
     */
    public void addFaction(Faction faction) {
        if (canHaveAsFaction(faction)) {
            factions.add(faction);
        }
    }

    @Basic
    public LinkedList<Faction> getFactions() {
        return factions;
    }

    /**
     * Removes faction from GameWorld
     * @param faction Faction to remove
     * @post The given faction will be removed from this world
     */
    public void removeFaction(Faction faction) {
        factions.remove(faction);
    }

    /**
     * Checks whether a given faction can be added to this World
     * @param faction the given faction
     * @return True if this faction can be added to this World
     */
    public boolean canHaveAsFaction(Faction faction) {
        return faction != null && !faction.isTerminated() && factions.size() < MAX_FACTIONS;
    }

    /**
     * Returns all boulders which are not currently carried by some Unit
     * @return All the active boulders in this World
     */
    public HashSet<Boulder> getActiveBouldersInWorld() {
        HashSet<Boulder> result = new HashSet<>();
        for (GameObject object : gameObjects) {
            if (object instanceof Boulder && !((Boulder) object).isCurrentlyCarried()) result.add((Boulder) object);
        }
        return result;
    }

    /**
     * Returns all logs which are not currently carried by some Unit
     * @return All the active logs in this World
     */
    public HashSet<Log> getActiveLogsInWorld() {
        HashSet<Log> result = new HashSet<>();
        for (GameObject object : gameObjects) {
            if (object instanceof Log && !((Log) object).isCurrentlyCarried()) result.add((Log) object);
        }
        return result;
    }

    /**
     * Returns all units which are currently in the game world
     * @return Units which are not terminated
     */
    public HashSet<Unit> getActiveUnitsInWorld() {
        HashSet<Unit> result = new HashSet<>();
        for (GameObject object : gameObjects) {
            if (object instanceof Unit && !object.isTerminated()) result.add((Unit) object);
        }
        return result;
    }

    /**
     * Adds a given gameObject to game world
     * @param object the given gameObject
     * @post The given object will be added to this world
     * @throws WorldException if this world cannot have this object as one of its gameObjects
     */
    public void addGameObject(GameObject object) throws WorldException {
        if (!canHaveAsObject(object))
            throw new WorldException(WorldException.CANNOT_ADD_GAME_OBJECT, "Game object is not valid to be added");
        if (objectLock) addBuffer.add(object);
        else gameObjects.add(object);
    }

    /**
     * Checks whether object can be added to world
     * @param object Object to be added
     * @return True if object can be added
     */
    public boolean canHaveAsObject(GameObject object) {
        boolean result = object != null && !object.isTerminated();

        if (object instanceof Unit) {
            result = result && getActiveUnitsInWorld().size() < MAX_UNITS;
        }

        return result;
    }

    /**
     * Converts the list of factions to a set
     * @return A set containing all the factions
     */
    public Set<Faction> factionsToSet() {
        HashSet<Faction> result = new HashSet<>();

        for (Faction faction : factions) {
            result.add(faction);
        }

        return result;
    }

    /**
     * Remove a given gameObject from this world
     * @param object The given gameObject
     * @post The given gameObject will be deleted from this world
     */
    public void removeGameObject(GameObject object) {
        if (objectLock) deleteBuffer.add(object);
        else gameObjects.remove(object);
    }

    /**
     * Get the SceneryType which matches with a given integer value
     * @param i the given integer value
     * @return the matched sceneryType
     * @throws WorldException When the given integer value didn't match with any SceneryType
     */
    public SceneryType getCubeByType(int i) throws WorldException {
        if (i < 0 || i > 3) {
            throw new WorldException(WorldException.INVALID_TERRAIN_TYPE, "Number to request terrain type did not match");
        } else {
            switch (i) {
                case 0:
                    return SceneryType.AIR;
                case 1:
                    return SceneryType.ROCK;
                case 2:
                    return SceneryType.TREE;
                case 3:
                    return SceneryType.WORKSHOP;
            }
        }
        //Won't happen
        return null;
    }

    /**
     * Filter the hashSet of GameObjects to obtain the nearest possible Object from the referencing Unit
     * @param classRef  The result must be an instance of this class
     * @param type      A type of comparison, only used if classRef is Unit. Otherwise this parameter must set be null or NONE
     * @param reference The referencing Unit to take in effect for distance calculations
     * @return  The nearest possible object satisfying the given requirements
     * @throws WorldException
     *          if classRef is Unit while type is null or NONE
     *          if classRef has null references
     */
    public GameObject filterByObject(Class<? extends GameObject> classRef, Type type, Unit reference ) throws WorldException {

        if (classRef == null ) throw new WorldException(WorldException.NULL_VALUE_AS_PARAMETER,"The reference class was a null value");

        //Declare an Iterator
        Iterator<? extends GameObject> it;

        //If the Objects to filter are Unit, special actions has to be taken. Considering a type of comparison
        if (Unit.class.equals(classRef)){
            if(type == null) throw new WorldException(WorldException.FILTER_CONFLICT,
                    "Tried to filter for Unit when no filter type was given as parameter ");

            //Filter the HashSet of gameObjects for the given Class and type. Only these objects remain
            it = gameObjects.stream().filter(t -> t.getClass().equals(classRef))
                    .filter(u -> hasType(reference ,(Unit) u,type))
                    .iterator();
        } else it = gameObjects.stream().filter(t -> t.getClass().equals(classRef))
                    .iterator();

        //Init the nearest object with an index representing in what perimeter this object is found from the given Unit
        Pair<Object,Integer> nearestObject = null;

        try{
            //if no object found in iterator, noSuchElementException will be thrown
            GameObject firstObject = it.next();
            //To get a reference for the remaining objects in the iterator
            nearestObject = new Pair<>(firstObject, getPerimeter(reference,firstObject));

            //Compare the perimeter of the first Object to the remaining objects from the filter
            while (it.hasNext()){

                GameObject nextObject = it.next();
                int p = nearestObject.getSecondValue().intValue();

                if(getPerimeter(reference,nextObject) < p && getPerimeter(reference,nextObject) != -1) // if -1 a null value was found, skip this
                    nearestObject = new Pair<>(nextObject,p);
            }

        } catch (NoSuchElementException e){
            //Do nothing, this checks if the iterator had one element to start with
        }
        return  nearestObject == null ? null : (GameObject) nearestObject.getFirstValue();
    }

    /**
     * Calculate the perimeter, an index representing the distance in number of cubes, of a given object from a reference Unit
     * @param reference The Unit for which the distance must be compared to
     * @param object    The given Object to calculate the distance from
     * @throws WorldException
     *              When reference or object have null references
     * @return  An index representing the distance in numbers of cubes to pass from the reference to the given object
     *          -1 is returned if a null reference as parameter was given
     */
    private int getPerimeter(Unit reference, GameObject object) throws WorldException {

        if (reference == null || object == null) return -1;

        Number[] diffs = reference.getCurrentCube().getPosition().getDiff(object.getCurrentCube().getPosition());

        return Math.abs(diffs[0].intValue()) + Math.abs(diffs[1].intValue()) + Math.abs(diffs[2].intValue());
    }

    /**
     * Check whether a given Unit is from a given Type compared with a reference Unit
     * @param reference The reference Unit
     * @param unit      The given Unit to compare with the reference Unit
     * @param newType   The given Type for the comparison
     * @return  True when the given unit has the given type compared with the reference unit
     */
    private boolean hasType(Unit reference, Unit unit ,Type newType) {

        if (reference == null || unit == null || newType == null || unit == reference) return false;

        switch (newType) {
            case FRIEND:
                return reference.getFaction() == unit.getFaction();
            case ENEMY:
                return reference.getFaction() != unit.getFaction();
            case ANY:
                return true;
        }

        return false;
    }

    /**
     * Filter the HashMap of Cubes to retrieve a Cube satisfying a SceneryType-, neighbouring- and a Unit reference condition
     *
     * @param terrain  The SceneryType condition: the returned Cube must have the same SceneryType
     * @param isNextTo The neighbouring Condition: If true, the returned Cube must be a Cube next to the given Unit
     *                 If false, the returned Cube must be the nearest Cube from the given Unit
     * @param unit     The given Unit
     * @return A cube satisfying the 3 conditions: Terrain, nextTo and Unit
     * @throws WorldException When null references are given as parameter
     */
    public Cube filterByCube(SceneryType terrain, boolean isNextTo, Unit unit) throws WorldException {
        return filterByCube(createScenerySet(terrain), isNextTo, unit.getCurrentCube().getPosition());
    }

    /**
     * Filter the HashMap of Cubes to retrieve a Cube satisfying a SceneryType-, neighbouring- and position reference condition
     *
     * @param terrain  The SceneryType condition: the returned Cube must have the same SceneryType
     * @param isNextTo The neighbouring Condition: If true, the returned Cube must be a Cube next to the given position
     *                 If false, the returned Cube must be the nearest Cube from the given position
     * @param position The given position
     * @return A cube satisfying the 3 conditions: Terrain, nextTo and position
     * @throws WorldException When null references are given as parameter
     */
    public Cube filterByCube(SceneryType terrain, boolean isNextTo, Position<Integer> position) throws WorldException {

        return filterByCube(createScenerySet(terrain), isNextTo, position);
    }

    /**
     * Filter the HashMap of Cubes to retrieve a Cube satisfying a SceneryType Set-, neighbouring- and position reference condition
     *
     * @param terrains The SceneryType condition: the returned Cube must have a SceneryType corresponding to one of these in the given Set
     * @param isNextTo The neighbouring Condition: If true, the returned Cube must be a Cube next to the given position
     *                 If false, the returned Cube must be the nearest Cube from the given position
     * @param position The given position
     * @return A Cube satisfying the 3 conditions: Terrain, NextTo and position
     * @throws WorldException When null references are given as parameter
     */
    public Cube filterByCube(HashSet<SceneryType> terrains, boolean isNextTo, Position<Integer> position) throws WorldException {
        return isNextTo ? cubeNextTo(position, terrains) : nearestCube(position, terrains);
    }

    /**
     * Find the nearest Cube from a given position satisfying some scenery type condition
     *
     * @param position The given position
     * @param terrains A set of Scenery types, the returned cube must be one of these kinds
     * @return A cube at the nearest position from the given position with a SceneryType the given Set of Scenery types contains
     * @throws WorldException When null references are given for position or terrains
     */
    private Cube nearestCube(Position<Integer> position, HashSet<SceneryType> terrains) throws WorldException {
        if (terrains.contains(null) || terrains.isEmpty())
            throw new WorldException(WorldException.NULL_VALUE_AS_PARAMETER, "null references given as parameter");
        //Retrieve an iterator over all Cubes corresponding to the given set of scenery types
        Iterator<Cube> it = cubes.values().stream()
                .filter(c -> c.getClass().equals(Cube.class))
                .filter(c -> !c.getPosition().equals(position))
                .filter(c -> terrains.contains(c.getSceneryType())).iterator();

        try {
            Cube resultCube = it.next();
            Pair<Cube, Integer> result = new Pair<>(resultCube, getPerimeter(position, resultCube));

            int p;
            Cube possibleCube;
            while (it.hasNext()) {
                possibleCube = it.next();
                p = getPerimeter(position, possibleCube);
                //Check if the possible cube is nearer than current result
                if (p < result.getSecondValue().intValue()) result = new Pair<>(possibleCube, p);
            }

            return result.getFirstValue();

        } catch (NoSuchElementException e) {
            //No value could be found, this exception will be thrown if resultCube couldn't be instantiated
            return null;
        }
    }

    /**
     * Calculate the perimeter, an index representing the distance in number of cubes, of a given cube from a reference position
     *
     * @param reference The position for which the distance must be compared to
     * @param cube      The given cube to calculate the distance from
     * @return An index representing the distance in numbers of cubes to pass from the reference to the given cube
     * @throws WorldException When null references are given for reference or cube
     */
    private int getPerimeter(Position<Integer> reference, Cube cube) throws WorldException {
        if (reference == null || cube == null)
            throw new WorldException(WorldException.NULL_VALUE_AS_PARAMETER, "Null references given as parameter");

        Number[] diffs = reference.getDiff(cube.getPosition());
        return Math.abs(diffs[0].intValue()) + Math.abs(diffs[1].intValue()) + Math.abs(diffs[2].intValue());
    }

    /**
     * Find a cube next to a given position that satisfies a kind of SceneryType restriction
     *
     * @param position The given position
     * @param terrains a set of SceneryTypes the cube must satisfy one of
     * @return A cube next to the given position satisfying a kind of SceneryType, or null if there is no such cube
     * @throws WorldException When null references are given in terrains
     */
    private Cube cubeNextTo(Position<Integer> position, HashSet<SceneryType> terrains) throws WorldException {
        if (terrains.isEmpty() || terrains.contains(null))
            throw new WorldException(WorldException.NULL_VALUE_AS_PARAMETER,
                    "Null references given in the Set of SceneryTypes");
        //Request the cube corresponding to the given position
        Cube referenceCube = World.getInstance().getCubeByVector(position.toVector());
        //Retrieve all the neighbouring cubes from the corresponding cube
        LinkedList<Cube> neighbours = referenceCube.getNeighbourCubes();

        Iterator<Cube> it = neighbours.iterator();

        Cube possibleCube;
        //Iterate over all these possible cubes. if a right one is found,return it
        while (it.hasNext()) {
            possibleCube = it.next();
            if (terrains.contains(possibleCube.getSceneryType())) return possibleCube;
        }
        //If no such Cube exists, return null
        return null;
    }

    /**
     * Create a HashSet of SceneryType containing all the SceneryTypes given as parameters
     *
     * @param terrain Optional parameter containing the SceneryTypes
     * @return A HashSet containing all the given SceneryTypes
     * @throws WorldException When terrain has null references
     */
    private HashSet<SceneryType> createScenerySet(SceneryType... terrain) throws WorldException {
        if (terrain.length == 0)
            throw new WorldException(WorldException.NULL_VALUE_AS_PARAMETER, "Tried to create a Set of terrains when no SceneryTypes were given");
        HashSet<SceneryType> terrainSet = new HashSet<>(terrain.length);
        for (int i = 0; i < terrain.length; i++) {
            if (terrain[i] == null)
                throw new WorldException(WorldException.NULL_VALUE_AS_PARAMETER, "Tried to create a Set of terrains when a null value was given");
            terrainSet.add(terrain[i]);
        }
        return terrainSet;
    }
}
