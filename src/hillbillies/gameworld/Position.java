package hillbillies.gameworld;

import be.kuleuven.cs.som.annotate.*;
import hillbillies.exceptions.PositionException;
import hillbillies.model.World;
import ogp.framework.util.Util;

import java.lang.reflect.Array;
import java.util.LinkedList;

/**
 * Value class that represents a certain position in the game world
 * @version 1.0
 * @author Jens Mortier
 *          Maxim Verbiest
 */

@Value
public class Position<E extends Number> {
    /**
     * X position
     */
    E position_x;

    /**
     * Y position
     */
    E position_y;

    /**
     * Z position
     */
    E position_z;

    /**
     * Constructor
     * @param x The x coordinate of the position
     * @param y The y coordinate of the position
     * @param z The z coordinate of the position
     * @throws PositionException if the position made by the given coordinates doesn't form a valid position given the gameWorld
     */
    public Position(E x,E y,E z) throws PositionException {
        if(!World.isValidPosition(x,y,z)) throw new PositionException(PositionException.POSITION_OUT_OF_BOUNDS,"Position isn't valid");
        position_x = x;
        position_y = y;
        position_z = z;
    }

    /**
     * Constructor mask
     * @param position  The position to be set
     * @effect Position()
     * @throws PositionException if the given position isn't a valid position given the gameWorld
     */
    public Position(Position position) throws PositionException {
        this((E)position.getPosition_x(),(E) position.getPosition_y(),(E)position.getPosition_z());
    }

    @Raw @Basic
    public E getPosition_x() {
        return position_x;
    }

    @Raw @Basic
    public E getPosition_y() {
        return position_y;
    }

    @Raw @Basic
    public E getPosition_z() {
        return position_z;
    }

    /**
     * Calculates the distance to a given position
     * @param position The given position
     * @return The distance to a given position
     */
    public double calculateDistanceTo(Position position) {
        //Calculate differences
        double x_diff = (Double)position_x.doubleValue() - (Double)position.getPosition_x().doubleValue();
        double y_diff = (Double)position_y.doubleValue() - (Double)position.getPosition_y().doubleValue();
        double z_diff = (Double)position_z.doubleValue() - (Double)position.getPosition_z().doubleValue();

        //Calculate distance
        return Math.sqrt(Math.pow(x_diff,2) + Math.pow(y_diff,2) + Math.pow(z_diff,2));
    }

    /**
     * Sets the x coordinate to a given coordinate
     * @param positionX The given x coordinate
     * @post The x coordinate will be set to the given coordinate
     * @throws PositionException if the position formed with the new coordinate isn't a valid position
     */
    public void setPositionX(E positionX) throws PositionException{
        if (World.isValidPosition(positionX, position_y, position_z)) {
            position_x = positionX;
        } else throw new PositionException(PositionException.POSITION_OUT_OF_BOUNDS,"Invalid X position");
    }

    /**
     * Sets the z coordinate to a given coordinate
     * @param positionZ The given z coordinate
     * @post The z coordinate will be set to the given coordinate
     * @throws PositionException if the position formed with the new coordinate isn't a valid position
     */
    public void setPositionZ(E positionZ) throws PositionException{
        if (World.isValidPosition( position_x,  position_y,  positionZ)){
            position_z = positionZ;
        } else throw new PositionException(PositionException.POSITION_OUT_OF_BOUNDS,"Invalid Z position");
    }

    /**
     * Sets the y coordinate to a given coordinate
     * @param positionY The given y coordinate
     * @post The y coordinate will be set to the given coordinate
     * @throws PositionException if the position formed with the new coordinate isn't a valid position
     */
    public void setPositionY(E positionY) throws PositionException {
        if (World.isValidPosition( position_x,  positionY,  position_z)) {
            position_y = positionY;
        } else throw new PositionException(PositionException.POSITION_OUT_OF_BOUNDS,"Invalid Y position");
    }

    /**
     * Sets the position to a given position
     * @param new_position The given position
     * @effect setPositionX()
     * @effect setPositionY()
     * @effect setPositionZ()
     * @throws PositionException thrown by the setters of every coordinate
     */
    public void setPosition(Position new_position) throws PositionException {
        setPositionX((E)new_position.getPosition_x());
        setPositionY((E)new_position.getPosition_y());
        setPositionZ((E)new_position.getPosition_z());
    }

    /**
     * Converts the position to an array {x,y,z} with the coordinates of this position
     * @return An array with the coordinates of this position
     */
    public E[] toVector() {
        //Init result
        Number[] result = new Number[3];
        result[0] = this.getPosition_x();
        result[1] = this.getPosition_y();
        result[2] = this.getPosition_z();
        return (E[])result;
    }

    /**
     * Checks if positions are equal
     * @param position  Position to check with
     * @return  True if positions are equal
     */
    @Override
    public boolean equals(Object position) {
        return equals((Position)position,Util.DEFAULT_EPSILON);
    }

    /**
     * Check if a given position equals this position
     * @param position  The given position
     * @param epsilon   Epsilon of comparison bounds
     * @return  True if equal
     */
    public boolean equals(Position position,double epsilon) {
        boolean x_equal = Util.fuzzyEquals( position_x.doubleValue(),position.getPosition_x().doubleValue(),epsilon);
        boolean y_equal = Util.fuzzyEquals( position_y.doubleValue(),position.getPosition_y().doubleValue(),epsilon);
        boolean z_equal = Util.fuzzyEquals( position_z.doubleValue(),position.getPosition_z().doubleValue(),epsilon);
        return x_equal && y_equal && z_equal;
    }

    /**
     * Calculates the difference of every coordinate with a given position
     * @param position  The given position
     * @return Array {x,y,z} containing the differences between every coordinate
     */
    public Number[] getDiff(Position position) {

        @SuppressWarnings("unchecked")
        Number[] result = new Number[3];
        result[0] = new Double(position_x.doubleValue() - position.getPosition_x().doubleValue());
        result[1] = new Double(position_y.doubleValue() - position.getPosition_y().doubleValue());
        result[2] = new Double(position_z.doubleValue() - position.getPosition_z().doubleValue());
        return result;
    }

    /**
     * Check if a given cube is a neighbouring cube position from, or the same cube as this position's cube
     * @param cubePosition  the given cube position
     * @return true if it is a neighbouring or same cube
     */
    public boolean isNeighbouringOrSameCube(Position<Integer> cubePosition) {
        Boolean[] result = new Boolean[3];
        result[0] = (Math.abs( (int)Math.floor(this.getPosition_x().doubleValue()) - cubePosition.getPosition_x()) <= 1);
        result[1] = (Math.abs( (int)Math.floor(this.getPosition_y().doubleValue()) - cubePosition.getPosition_y()) <= 1);
        result[2] = (Math.abs( (int)Math.floor(this.getPosition_z().doubleValue()) - cubePosition.getPosition_z()) <= 1);
        return (result[0] && result[1] && result[2]);
    }

    /**
     * Check if a given cube position is a neighbouring cube from the this position's cube
     * @param cubePosition The given cube position
     * @return True if it is a neighbouring, but not the same cube as this position's cube
     */
    public boolean isNeighbouringCube(Position<Integer> cubePosition) {
        return isNeighbouringOrSameCube(cubePosition) && !cubePosition.equals(this);
    }

    /**
     * Calculates the cube position of this position. i.e the position with floored down coordinates
     * @return the cube position of this position
     */
    public Position<Integer> toCubePosition() {
        try {
            return new Position<Integer>((int) Math.floor( getPosition_x().doubleValue()), (int) Math.floor(getPosition_y().doubleValue()),
                        (int) Math.floor(getPosition_z().doubleValue()));
        } catch (PositionException e) {
                //Won't happen
                return null;
        }
    }

    @Override
    public Position<E> clone() {
        try {
            Position<E> new_position = new Position<>(getPosition_x(),getPosition_y(),getPosition_z());
            return new_position;
        }catch(PositionException e) {
            //Nothing to do
            return null;
        }
    }

    @Override
    public String toString() {
        return "(" + position_x.toString() + " , " + position_y.toString() + " , " + position_z.toString() + ")";
    }
}
