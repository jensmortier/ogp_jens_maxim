package hillbillies.gameworld;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Raw;
import hillbillies.exceptions.*;
import hillbillies.model.Unit;
import hillbillies.model.World;
import ogp.framework.util.Util;


/**
 * Parent class which represents material objects in the game world.
 *
 * @version 1.0
 * @author Maxim Verbiest
 */
public abstract class Material extends GameObject {

    /**
     * The weight of a material
     */
    int weight;

    /**
     * The position of the material in the game World
     */
    private Position<Double> position = null;

    /**
     * True if the material is currently carried around
     */
    private boolean currentlyCarried = false;

    /**
     * The carrier of this material, if there is one
     */
    private Unit carrier = null;

    /**
     * Whether the material is stable or not
     */
    private StabilityType stabilityStatus;

    /**
     * Whether the material has started falling or not
     */
    private boolean startedFalling = false;


    /**
     * Constructor
     * @param   newPosition the initial position of this material
     * @param   newCube     the initial cube where this material is in
     * @throws  MaterialException
     *              When the given position assigned to this material isn't valid
     * @effect  setWeight()
     * @effect  setPosition()
     * @effect  setStabilityStatus()
     */
    protected Material(Position<Double> newPosition, Cube newCube) throws MaterialException {
        //For registration in world and setting of termination value
        super(newCube);
        if(isTerminated()) return;
        
        //Initialize random weight upon creation
        setWeight();

        setPosition(newPosition);

        //Set stability status
        setStabilityStatus(StabilityType.STABLE_RESTING);
    }

    /**
     * Checks if material is currently carried
     * @return  True if currently carried
     */
    public boolean isCurrentlyCarried() {
        return currentlyCarried;
    }

    /**
     * Gets the carrier of the material
     * @throws MaterialException
     *          when there is no carrier
     */
    @Basic
    public Unit getCarrier() throws MaterialException {
        if (carrier != null){
            return carrier;
        } else throw new MaterialException(MaterialException.IMPOSSIBLE_METHOD_INVOCATION,"Tried to get the Carrier while there is none");
    }

    /**
     * Sets the carrier of the material to a given Unit
     * @param   newCarrier the Unit who is currently carrying this material
     * @post    If the carrier isn't a NULL value, the new carrier of this material will be set to this value
     * @throws  MaterialException
     *              when the given carrier is a NULL value
     */
    protected void setCarrier(Unit newCarrier) throws MaterialException {
        if (newCarrier != null){
            carrier = newCarrier;
        } else throw new MaterialException(MaterialException.NULL_VALUE_AS_PARAMETER,"Tried to set a NULL value as carrier of the material");
    }

    @Basic
    public Position<Double> getPosition(){
        return position;
    }

    /**
     * Sets the position of the material to a given position
     * @param   newPosition
     * @post    if the new Position isn't a NULL value, the position of this material will be set to this value
     * @throws  MaterialException
     *          if the given position is a NULL value
     */
    protected void setPosition(Position<Double> newPosition) throws MaterialException {
        if (newPosition != null){
            position = newPosition;
        } else throw new MaterialException(MaterialException.NULL_VALUE_AS_PARAMETER, "Tried to set the position of a " +
                "material to a NULL value");
    }

    @Basic @Raw
    public StabilityType getStabilityStatus() {
        return stabilityStatus;
    }


    /**
     * Set the stability type of the material
     * @param   newStability
     * @post    if the given stability is a valid stability, the Materials stability type will be set to this value
     * @throws  MaterialException
     *              if the new stability is a NULL value
     */
    protected void setStabilityStatus(StabilityType newStability) throws MaterialException {
        if(newStability != null) {
            stabilityStatus = newStability;
        } else
            throw new MaterialException(MaterialException.NULL_VALUE_AS_PARAMETER, "Tried to set StabilityType of material to NULL");

    }

    @Basic
    public int getWeight() {
        return weight;
    }

    /**
     * Sets the weight of a material.
     * @post the Weight will be set to a randomly chosen value between 10 and 50 inclusively
     */
    protected void setWeight() {
        weight = 10 + (int) Math.round(40 * Math.random());
    }

    /**
     * let the new carrier pick up the material
     * @param newCarrier    The Unit which wants to carry this material
     * @throws MaterialException
     */
    protected abstract void pickUpMaterial(Unit newCarrier) throws MaterialException;

    /**
     * Let the carrier drop the material
     * @param carrier   The Unit which wants to drop this material
     * @throws MaterialException
     */
    protected abstract void dropMaterial(Unit carrier) throws MaterialException;

    /**
     * Pick up a material
     * @param   newCarrier  The new carrier of this material
     * @post    If the new carrier isn't a NULL value, the material will be carried by this carrier
     * @post    The material will be deleted from it's current cube
     * @throws  MaterialException
     *              when the carrier is a NULL value
     * @effect  setCarrier()
     * @effect  pickUpMaterial()
     */
    public void PickUp(Unit newCarrier) throws MaterialException {
        if (newCarrier != null){
            if(!currentlyCarried){
                currentlyCarried = true;
                setCarrier(newCarrier);

                //let the carrier pick up the material
                pickUpMaterial(newCarrier);

                //Delete from cube
                this.currentCube.removeGameObject(this);
                this.currentCube = null;
            }
        } else throw new MaterialException(MaterialException.NULL_VALUE_AS_PARAMETER, "A NULL value tried to pick up the material");
    }

    /**
     * Drop the material at the carrier's current position
     * @post    If the material is currently carrier, it will be dropped at the carrier's current position
     * @post    The material's current cube will be updated
     * @post    The material will not be carried anymore
     * @throws  MaterialException
     *              if the new position is NULL
     * @effect  setPosition()
     * @effect  dropMaterial()
     *
     */
    public void Drop() throws MaterialException {
      try{
          if (currentlyCarried) {
              //The position where the Material has to be dropped
              Position<Double> dropPosition = new Position<Double>(carrier.getWorkCube().getPosition().getPosition_x() + 0.5,
                      carrier.getWorkCube().getPosition().getPosition_y() + 0.5,
                      carrier.getWorkCube().getPosition().getPosition_z() + 0.5);
              setPosition(dropPosition);

              //Drop the material
              dropMaterial(getCarrier());

              //Add to cube
              this.currentCube = carrier.getWorkCube();

              transferToCube(getCurrentCube());

              //Delete current carrier
              carrier = null;

              currentlyCarried = false;
            }
          } catch (PositionException |GameObjectException e){
            throw new MaterialException(MaterialException.DROP_CONFLICT, e.getMessage());
          }

    }

    /**
     * Checks whether material lies on a stable position
     * @return true if the material is currently on a stable position
     */
    protected boolean isStablePosition() {
        if(!currentCube.getSceneryType().isSolid()){

            // if Z = 0 return true, else lower z must be solid
            if(currentCube.getPosition().getPosition_z() != 0){
                Cube lowerCube = World.getInstance().getCubeByKey(currentCube.getPosition().getPosition_x(),
                        currentCube.getPosition().getPosition_y(),
                        currentCube.getPosition().getPosition_z()-1);

                if (lowerCube.getSceneryType().isSolid()) {
                    return true;

                // lower cube is not solid hence false
                } else return false;

            // this cube has Z =0 hence this is a stable position
            } else return true;

        // must be located on a passable cube
        } else return false;
    }

    /**
     * Method used in Fall to check whether the cube the Material is falling towards provides a stable position
     * @param futureCube    the cube the Material is falling towards
     * @return  true when the cube the Material is falling towards provides a stable position
     */
    protected boolean isStablePosition(Cube futureCube){
        if(!futureCube.getSceneryType().isSolid()){

            // if Z = 0 return true, else lower z must be solid
            if(futureCube.getPosition().getPosition_z() != 0){
                Cube lowerCube = World.getInstance().getCubeByKey(futureCube.getPosition().getPosition_x(),
                        futureCube.getPosition().getPosition_y(),
                        futureCube.getPosition().getPosition_z()-1);

                if (lowerCube.getSceneryType().isSolid()) {
                    return true;

                    // lower cube is not solid hence false
                } else return false;

                // this cube has Z =0 hence this is a stable position
            } else return true;

            // must be located on a passable cube
        } else return false;
    }

    /**
     * Returns the Fall speed of this material
     * @return the speed of falling in m/s of a Material
     */
    public int getFallSpeed(){
        return 3;
    }

    /**
     * Let the material Fall
     * @param   gameTime  The advanced time since previous update
     * @post    The Material will Fall until it has reached a stable position
     * @throws  MaterialException
     * @effect  setPosition()
     * @effect  transferToCube()
     * @effect  setStabilityStatus()
     */
    public void Fall(double gameTime) throws MaterialException {

        try {
            // Update z coordinate since the material is falling
            double new_z = (getPosition().getPosition_z() - getFallSpeed() * gameTime);

            if(!startedFalling){
                //Remove Material from its current cube
                getCurrentCube().removeGameObject(this);

                startedFalling = true;
            }

            //check if reached the ground
            if (Util.fuzzyLessThanOrEqualTo(new_z,0)){
                //A coordinate cannot be negative
                getPosition().setPositionZ(new Double(0));

                //add this material to its new Cube
                transferToCube(World.getInstance().getCubeByKey(
                        (int)Math.floor(getPosition().getPosition_x()),
                        (int)Math.floor(getPosition().getPosition_y()),
                        0));

                // Material will be stable again
                setStabilityStatus(StabilityType.STABLE_RESTING);

                startedFalling = false;

            } else {
                //Update the position of the material
                getPosition().setPositionZ(new_z);

                //Cube where the Material is falling towards
                Cube futureCube = World.getInstance().getCubeByKey(
                        (int)Math.floor(getPosition().getPosition_x()),
                        (int)Math.floor(getPosition().getPosition_y()),
                        (int)Math.floor(new_z));

                //Check if the Material has reached a stable position and update if so
                if (isStablePosition(futureCube)) {
                    //Check whether the Material has passed half of the z cube fot better transitions in GUI
                    if(Util.fuzzyLessThanOrEqualTo(new_z - Math.floor(new_z),World.getInstance().LENGTH_Z / 2)) {

                        //Material will be stable again
                        setStabilityStatus(StabilityType.STABLE_RESTING);
                        //Add this material to its new Cube
                        transferToCube(futureCube);
                        //Update position to final z coordinate
                        getPosition().setPositionZ(Math.floor(new_z) + World.getInstance().LENGTH_Z / 2);
                        
                        startedFalling = false;
                    }
                }
            }
        }catch(PositionException |GameObjectException e) {
            throw new MaterialException(MaterialException.FALLING_CONFLICT,e.getMessage());
        }

    }

    /**
     * Advance the time in class Material
     * @param   gameTime  the time advanced
     * @post    The material will start falling, if it isn't stable anymore
     * @effect  Fall()
     * @effect  setStabilityStatus()
     * @throws  MaterialException
     *          threw by an invalid stability type or a Fall conflict
     */
    public void advanceTime(double gameTime) throws MaterialException{

        if(currentlyCarried){
            return;
        }

        if(getStabilityStatus() != StabilityType.UNSTABLE_FALLING){
            if (!isStablePosition()) {
                setStabilityStatus(StabilityType.UNSTABLE_FALLING);
            }
        }

        switch (stabilityStatus) {
            case UNSTABLE_FALLING:
                Fall(gameTime);
                break;
            case STABLE_RESTING:
                break;
        }
    }

}
