package hillbillies.gameworld;
import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Raw;
import be.kuleuven.cs.som.annotate.Value;
import hillbillies.exceptions.*;
import hillbillies.model.*;
import ogp.framework.util.Util;
import java.util.HashSet;
import java.util.LinkedList;


/**
 * A cube is an entity which can contain gameObjects.
 *
 * @version 1.0
 * @author Maxim Verbiest
 */


@Value
public class Cube implements Advanceable {

    /**
     * The Scenery type of this cube
     */
    private SceneryType terrain;

    /**
     * The position of this cube in the Game World
     */
    private Position<Integer> position;

    /**
     * A set containing all the gameObjects present on this Cube
     */
    private HashSet<GameObject> gameObjects;

    /**
     * Contains the game time which has elapsed since cube is possible going to cave in
     */
    private double gameTimeElapsedSinceCaveIn;

    /**
     * True if this cube is caving in
     */
    private boolean isCavingIn;

    /**
     * True if this cube is terminated
     */
    private boolean isTerminated;


    /**
     * Main constructor
     *
     * @param terrain   the SceneryType of the Cube
     * @param  position the position of the Cube in the game World
     * @effect setPosition()
     * @effect setSceneryType()
     * @throws PositionException    thrown by setPosition()
     * @throws CubeException    thrown by setSceneryType()
     */
    public Cube(SceneryType terrain, Position<Integer> position) throws PositionException, CubeException {
        setPosition(position);
        setSceneryType(terrain);
        gameTimeElapsedSinceCaveIn = 0;
        isCavingIn = false;
        isTerminated = false;
        gameObjects = new HashSet<>();
    }

    /**
     * Constructor mask with default value AIR for terrain
     *
     * @param position  Position for this Cube
     * @effect Cube()
     *          |this(SceneryType.AIR,position)
     * @throws PositionException
     * @throws CubeException
     */
    public Cube(Position<Integer> position) throws PositionException, CubeException {
        this(SceneryType.AIR, position);
    }

    /**
     * Adds a given gameObject to this cube
     * @param object    The given gameObject
     * @effect object.setCurrentCube()
     * @post The given object will be added to the list of gameObjects on this cube
     * @post The given object will be deleted again from the list if an error should occur
     * @throws CubeException
     *          If this cube cannot have this object as its object
     *          When an error occurs during the setting of current cube of the given gameObject
     */
    public void addGameObject(GameObject object) throws CubeException {
        if(!canHaveAsGameObject(object)) throw new CubeException(CubeException.COULDNT_ADD_GAME_OBJECT,"Couldn't add game object");
        gameObjects.add(object);
        try {
            object.setCurrentCube(this);
        }catch(GameObjectException e) {
            gameObjects.remove(object);
            throw new CubeException(CubeException.COULDNT_ADD_GAME_OBJECT,"Failed to add game object");
        }
    }

    /**
     * Check if it is possible to add an object to this cube
     * @param object The object to be added to this cube
     * @return true if the object is: not a NULL value, not terminated, not already a member of this cube
     */
    public boolean canHaveAsGameObject(GameObject object) {
        return object != null && !object.isTerminated() && !gameObjects.contains(object);
    }

    /**
     * Tears the relation down between a given object and this Cube
     * @param object Object to tear relation down of
     * @post The object will be removed from this cube
     * @effect setCurrentCube()
     */
    public void removeGameObject(GameObject object) {
        if(object == null) return;//Silent, sssht
        gameObjects.remove(object);
        try {
            object.setCurrentCube(null);
        }catch(GameObjectException e) {
            //Do nothing
        }
    }

    /**
     * Returns a Log present on this Cube
     * @return a Log present on this Cube, or null if there is none
     */
    public Log getLog() {
        for(GameObject object : gameObjects)
            if(object instanceof Log) return (Log) object;
        return null;
    }

    /**
     * Returns a Boulder present on this Cube
     * @return a Boulder present on this Cube, or null if there is none
     */
    public Boulder getBoulder() {
        for(GameObject object : gameObjects)
            if(object instanceof Boulder) return (Boulder) object;
        return null;
    }

    /**
     * Returns a HashSet of all the units on this cube
     * @return  All units in this cube
     */
    public HashSet<Unit> getUnits() {
        HashSet<Unit> result = new HashSet<>();
        for(GameObject object : gameObjects)
            if(object instanceof Unit) result.add((Unit)object);
        return result;
    }

    /**
     * Checks whether cube is terminated
     * @return  True if cube is terminated
     */
    public boolean isTerminated() {
        return isTerminated;
    }

    /**
     * Terminates this Cube
     * @post Object will be terminated and all objects in it too
     */
    public void terminate() {
        for(GameObject object : gameObjects) object.terminate();
        this.isTerminated = true;
    }

    /**
     * Advance time aspect of this cube
     *
     * @param gameTime Amount of game time passed
     * @post The cube will cave in if it isn't stable anymore
     * @effect caveIn()
     * @throws CubeException thrown by CaveIn()
     */
    public void advanceTime(double gameTime) throws CubeException {
       if ((!isStable() || isCavingIn) && getSceneryType().isSolid()) {
            isCavingIn = true;
            gameTimeElapsedSinceCaveIn += gameTime;
            if (Util.fuzzyGreaterThanOrEqualTo(gameTimeElapsedSinceCaveIn, 4.5)) {
                if (isCavingIn) caveIn();
                isCavingIn = false;
            }
        }
    }

    /**
     * Makes cube cave in
     *
     * @param withProbability_check If true, boulder or log placement only happens with probability 25%
     * @effect World.getInstance().addGameObject()  After the Cube have caved in, a boulder or log could be created
     * @effect setMaterial()
     * @effect setSceneryType()
     * @throws CubeException
     *          Converted from constructor of a new Boulder/Log
     *          Converted from constructor of the Position the newly created Material will have
     *          Converted from addGameObject() from World
     *
     */
    public void caveIn(boolean withProbability_check) throws CubeException {
        try {
            //Are we solid?
            if (getSceneryType().isSolid()) {
                //Are we going to create a boulder or log?
                if(materialAfterCollapse() || !withProbability_check) {//Yes
                    Position<Double> newPosition = new Position<Double>(this.getPosition().getPosition_x() + 0.5,this.getPosition().getPosition_y() + 0.5,this.getPosition().getPosition_z() + 0.5);
                    switch(getSceneryType()) {
                        case ROCK:
                            Boulder boulder = new Boulder(newPosition, this);
                            World.getInstance().addGameObject(boulder);
                            setMaterial(boulder);
                            break;
                        case TREE:
                            Log log = new Log(newPosition, this);
                            World.getInstance().addGameObject(log);
                            setMaterial(log);
                            break;
                    }
                }

            }
        } catch (MaterialException | WorldException | PositionException e) {
            throw new CubeException(CubeException.COULDNT_ADD_GAME_OBJECT, e.getMessage());
        }

        //Let's change this cube to air
        setSceneryType(SceneryType.AIR);

    }

    /**
     * Executes caveIn with probability check
     * @effect caveIn()
     * @throws CubeException thrown by caveIn()
     */
    public void caveIn() throws CubeException {
        caveIn(true);
    }

    /**
     * Sets a new Material on this Cube and deletes some older Material from the same type
     * @param material  Material to set
     * @effect addGameObject()
     * @effect removeGameObject()
     * @throws CubeException
     *          If this Cube can't have the given material as its material
     *          thrown by addGameObject()
     *
     */
    public void setMaterial(Material material) throws CubeException {
        if(!canHaveMaterial(material)) throw new CubeException(CubeException.COULDNT_ADD_GAME_OBJECT,"Couldn't add material to cube");
        if(material instanceof Log) removeGameObject(getLog());
        if(material instanceof Boulder) removeGameObject(getBoulder());
        addGameObject(material);
    }

    /**
     * Checks whether this Cube can have a given material
     * @param material  The given Material
     * @return True if the given Material is not null and not terminated
     */
    public boolean canHaveMaterial(Material material) {
        return material != null && !material.isTerminated();
    }

    /**
     * Checks if material has to be added to cube after collapse
     * @return True if material has te be added, depended on probability of 25%
     */
    private boolean materialAfterCollapse() {
        return Math.random() <= 0.25;
    }

    /**
     * Checks whether cube is connected to border
     * @return True if cube is connected to border
     */
    public boolean isStable() {
        return World.getInstance().isCubeAttachedToBorder(this);
    }


    @Basic
    public Position<Integer> getPosition() {
        return position;
    }

    /**
     * Sets the position of this cube in the Game World
     * @param new_position  the position of the Game World
     * @post the position of this cube will be set to the given position
     * @throws PositionException if the position isn't valid
     */
    private void setPosition(Position<Integer> new_position) throws PositionException {
        position = new Position<Integer>(new_position.getPosition_x(), new_position.getPosition_y(), new_position.getPosition_z());
    }

    @Basic @Raw
    public SceneryType getSceneryType() {
        return terrain;
    }

    /**
     * Sets the terrain type of this Cube to a given type
     * @param newScenery    The given type
     * @post the terrain type of this cube will be set to the given terrain type
     * @effect World.getInstance().reportCubeStateChange()
     * @throws CubeException
     *          if the given terrain type is a NULL value
     */
    public void setSceneryType(SceneryType newScenery) throws CubeException {
        if (newScenery != null) {
            terrain = newScenery;

            //Report change
            World.getInstance().reportCubeStateChange(this, !newScenery.isSolid());
        } else {
            throw new CubeException(CubeException.NULL_VALUE_AS_PARAM, "Tried to set NULL as cube's Scenery");
        }
    }

    /**
     * Checks if this specific cube has a neighbour which is solid
     * @return True if this cube has a solid neighbour
     */
    public boolean hasSolidNeighbour() {
        //Check for lower bounds
        if(position.getPosition_x() == 0 || position.getPosition_y() == 0 || position.getPosition_z() == 0) return true;

        //If these cube is a boundary cube, it may be stated that this cube has solid borders
        if (position.getPosition_x() == World.GAME_WORLD_BOUND_X() - 1 ||
                position.getPosition_y() == World.GAME_WORLD_BOUND_Y() - 1 || position.getPosition_z() == World.GAME_WORLD_BOUND_Z() - 1)
            return true;

        //Check neighbour cubes
        for (Cube neighbour : getNeighbourCubes()) {
            if (neighbour.getSceneryType().isSolid()) return true;
        }

        return false;
    }

    /**
     * Returns a List containing all the neighbours of this Cube
     * @return A linked list containing all the neighbouring cubes
     */
    public LinkedList<Cube> getNeighbourCubes() {
        //Init result
        LinkedList<Cube> cubes = new LinkedList<>();

        //Check neighbour cubes
        for (int dx = -1; dx <= 1; dx++) {
            for (int dy = -1; dy <= 1; dy++) {
                for (int dz = -1; dz <= 1; dz++) {
                    if(dx == 0 && dy == 0 && dz == 0) continue;

                    //Construct vector
                    Integer[] vector = {position.getPosition_x() + dx, position.getPosition_y() + dy, position.getPosition_z() + dz};

                    Cube neighbour = World.getInstance().getCubeByVector(vector);

                    if(neighbour != null) cubes.add(neighbour);
                }
            }
        }

        return cubes;
    }

    /**
     * Get all the neighbouring cubes, at the same Z-level, from this Cube
     * @return a LinkedList of cubes containing all the neighbouring cubes at the same z-level
     * @throws CubeException    When no cubes could be found
     */
    public LinkedList<Cube> getNeighbourCubes_sameZ() throws CubeException{
        //Init result
        LinkedList<Cube> cubes = new LinkedList<>();

        //Check neighbour cubes
        for (int dx = -1; dx <= 1; dx++) {
            for (int dy = -1; dy <= 1; dy++) {
                //skip the same cube
                if(dx == 0 && dy == 0) continue;

                //Construct vector
                Integer[] vector = {position.getPosition_x() + dx, position.getPosition_y() + dy, position.getPosition_z()};
                Cube neighbour = World.getInstance().getCubeByVector(vector);

                //check if this cube is actually in the game world.
                if(neighbour != null) cubes.add(neighbour);
                }
            }

        if (!cubes.isEmpty())
            return cubes;
        else throw new CubeException(CubeException.NO_CUBES_FOUND,"No neighbouring cubes could be found");
    }

    /**
     * Get a a cube, which is a neighbouring cube at the same z-level from this Cube. The cube will also be a stable position for a Unit to stand on
     * @return  A stable neighbouring cube
     * @throws CubeException    if no stable neighbour can be found
     *                          thrown by getNeighbourCubes_sameZ()
     */
    public Cube getStableNeighbour() throws CubeException{
        return getStableNeighbour(false);
    }

    public Cube getStableNeighbour(boolean strict) throws CubeException {
        //get all the neighbouring cubes at the same z-level
        LinkedList<Cube> neighbours = getNeighbourCubes_sameZ();
        //Check for stability
        for(Cube cube : neighbours){
            if(cube.hasSolidNeighbour())
                if((strict && !cube.getSceneryType().isSolid()) || !strict) return cube;
        }
        //If reached upon this point, no stable cube could be found
        throw new CubeException(CubeException.NO_CUBES_FOUND,"No stable neighbour could be found");
    }

    /**
     * Check whether this cube is a neighbouring cube of a given cube
     * @param otherCube the given cube
     * @return true if this cube is a neighbouring cube of the given cube
     */
    public boolean isNeighbourCube(Cube otherCube) {
        //Set borders of this cube
        int x_left = position.getPosition_x() - 1;
        int x_right = position.getPosition_x() + 1;
        int y_bottom = position.getPosition_y() - 1;
        int y_top = position.getPosition_y() + 1;
        int z_d = position.getPosition_z() - 1;
        int z_s = position.getPosition_z() + 1;

        Position<Integer> other_position = otherCube.getPosition();

        //Setup booleans
        boolean x_ok = other_position.getPosition_x() >= x_left && other_position.getPosition_x() <= x_right;
        boolean y_ok = other_position.getPosition_y() >= y_bottom && other_position.getPosition_y() <= y_top;
        boolean z_ok = other_position.getPosition_z() >= z_d && other_position.getPosition_z() <= z_s;

        return (x_ok && y_ok && z_ok) && !(otherCube.getPosition().equals(this.getPosition()));
    }

    /**
     * Check whether this Cube contains a Log
     * @return true if this Cube contains a Log
     */
    public boolean containsLog() {
        return getLog() != null;
    }

    /**
     * Check whether this Cube contains a Boulder
     * @return true if this Cube contains a Boulder
     */
    public boolean containsBoulder() {
        return getBoulder() != null;
    }

    /**
     * Returns a list of Enemy Units
     * @param friendlyFaction To decide which Units are actually enemies
     * @return A list of Enemies from a given faction, present on this Cube, or null if there are none
     */
    public LinkedList<Unit> getEnemies(Faction friendlyFaction) {

        //Check if there are Units on this cube
        if (!getUnits().isEmpty() ) {

            //LinkedList containing all the enemies of a unit
            LinkedList<Unit> enemies = new LinkedList<>();

            for (Unit potentialEnemy :  getUnits() ){

               if (potentialEnemy.getFaction() != friendlyFaction && potentialEnemy.getMovementStatus().isPassive()) {
                   enemies.add(potentialEnemy);
               }

            }

            return enemies;

        }  else return null;
    }

    /**
     * Get a random Unit present on this Cube which is an enemy of a given faction
     * @param friendlyFaction   the given faction
     * @return a random enemy, or NULL if there is none
     */
    public Unit getRandomEnemy(Faction friendlyFaction){

        LinkedList<Unit> enemies = getEnemies(friendlyFaction);
        if (!enemies.isEmpty()){

            return enemies.get((int) Math.round(Math.random()*(enemies.size()-1)) );

           //There are no enemies, return NULL instead
        } else return null;
    }

    /**
     * Checks whether there are enemies, from a given Faction, present in the gameWorld on this Cube
     * @param friendlyFaction   the given Faction
     * @return true if this cube contains units which are no member from a given faction
     */
    public boolean hasPotentialEnemy(Faction friendlyFaction){
        return getEnemies(friendlyFaction) != null;
    }

}