package hillbillies.gameworld;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Raw;
import be.kuleuven.cs.som.annotate.Value;

/**
 * Represents the scenery type of a certain cube
 *
 * @version 1.0
 * @author Maxim Verbiest
 */
@Value
public enum SceneryType {

    AIR(false,0),
    ROCK(true,1),
    TREE(true,2),
    WORKSHOP(false,3);

    private boolean isSolid;
    private int number;

    SceneryType(boolean isSolid,int n) {
        this.isSolid = isSolid;
        number = n;
    }

    @Basic
    public boolean isSolid(){
        return this.isSolid;
    }

    /**
     * Check whether this cube is interesting to work on, hence when it's not AIR
     * @return true when this cube is not AIR
     */
    @Raw
    public boolean notAir(){
        return this.getNumber() != 0;
    }

    @Basic
    public int getNumber() {
        return number;
    }

    public static SceneryType getSceneryType(int ID) {
        if(ID == SceneryType.AIR.getNumber()) return SceneryType.AIR;
        if(ID == SceneryType.ROCK.getNumber()) return SceneryType.ROCK;
        if(ID == SceneryType.TREE.getNumber()) return SceneryType.TREE;
        if(ID == SceneryType.WORKSHOP.getNumber()) return SceneryType.WORKSHOP;
        return null;
    }
}
