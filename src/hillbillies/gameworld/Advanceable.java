package hillbillies.gameworld;

/**
 * Interface for all classes that are advanceable in time
 *
 * @version 1.0
 * @author Jens Mortier
 */
public interface Advanceable {

    public void advanceTime(double gameTime) throws Exception;
}
