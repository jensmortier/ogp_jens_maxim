package hillbillies.gameworld;

import be.kuleuven.cs.som.annotate.Value;

/**
 * Represents the type of a certain material
 *
 * @version 1.0
 * @author Maxim Verbiest
 */
@Value
public enum StabilityType {

    UNSTABLE_FALLING,
    STABLE_RESTING
}
