package hillbillies.gameworld;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Raw;
import hillbillies.exceptions.CubeException;
import hillbillies.exceptions.GameObjectException;
import hillbillies.exceptions.UnitException;
import hillbillies.exceptions.WorldException;
import hillbillies.model.World;

/**
 * Super class for all kinds of objects present in the game world.
 * @version 1.0
 * @author  Jens Mortier
 *          Maxim Verbiest
 */
abstract public class GameObject implements Advanceable {

    /**
     * True if object is terminated
     */
    protected boolean isTerminated;

    /**
     * The current cube where this object is located
     */
    protected Cube currentCube;

    /**
     * Constructor
     * @param currentCube   The cube where the object is located
     * @post The gameObject's current cube will be set to the given cube
     * @post The gameObject will be stated as alive
     * @effect World.getInstance().addGameObject()
     * @post If some error should occur during the adding of this object to its gameWorld, it will be terminated
     */
    public GameObject(Cube currentCube) {
        //Set terminated
        isTerminated = false;

        //Add object to game world
        try {
            World.getInstance().addGameObject(this);
            this.currentCube = currentCube;
            currentCube.addGameObject(this);
        }catch(WorldException | CubeException e) {
            //Means that world is full, so terminate
            terminate();
        }

    }

    /**
     * Sets the gameObject's current cube to a given cube
     * @param cube  The given cube
     * @throws GameObjectException if the given cube is invalid
     */
    public void setCurrentCube(Cube cube) throws GameObjectException {
        if(!canHaveAsCube(cube)) throw new GameObjectException("Invalid cube");

        currentCube = cube;
    }

    /**
     * Check whether object can be located on a given cube
     * @param cube  The given cube
     * @return  True if the given cube is not a null value and not terminated
     */
    public boolean canHaveAsCube(Cube cube) {
        return cube != null && !cube.isTerminated();
    }

    @Basic @Raw
    public Cube getCurrentCube() {
        return currentCube;
    }

    /**
     * Transfers this object to a given cube
     * @param newCube The given cube
     * @effect getCurrentCube().removeGameObject()
     * @effect newCube.addGameObject()
     * @throws GameObjectException
     *          When an error occurs during the adding of this object to the new cube
     */
    public void transferToCube(Cube newCube) throws GameObjectException {
        //Remove from old cube, if it has not been removed already
        if(getCurrentCube() != null) {
            currentCube.removeGameObject(this);
        }
        try {
            newCube.addGameObject(this);
        } catch (CubeException e) {
            throw new GameObjectException("Couldn't change cube");
        }
    }

    @Basic @Raw
    public boolean isTerminated() {
        return isTerminated;
    }

    /**
     * Terminates this object
     * @effect World.getInstance.removeGameObject()
     * @effect getCurrentCube().removeGameObject()
     * @post This object will be stated as terminated
     */
    public void terminate() {
        //Remove this object from the game world
        World.getInstance().removeGameObject(this);

        //Remove from cube
        currentCube.removeGameObject(this);

        //Set it
        isTerminated = true;
    }


}
