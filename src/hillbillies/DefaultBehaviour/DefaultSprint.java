package hillbillies.DefaultBehaviour;

import hillbillies.exceptions.DefaultBehaviourException;
import hillbillies.model.MovementType;
import hillbillies.model.Unit;

/**
 * Class with the Sprinting functionality for a Unit in default behaviour
 * @version 1.0
 * @author Maxim Verbiest
 */
public class DefaultSprint extends DefaultAction {
    /**
     * Constructor
     *
     * @param newUnit The Unit for which this functionality will be created
     * @throws DefaultBehaviourException When newUnit is NULL
     * @effect setUnit
     */
    public DefaultSprint(Unit newUnit) throws DefaultBehaviourException {
        super(newUnit);
    }

    /**
     * Let the Unit Sprint
     */
    @Override
    public void executeIfChosen() {
        getUnit().Sprint();
    }

    /**
     * Check whether the Unit is able to start sprinting
     * @return true if the Unit is currently Walking
     */
    @Override
    protected boolean canBeExecuted() {

        return getUnit().isMoving();
    }

    @Override
    public String toString() {
        return "DB's Sprint";
    }
}
