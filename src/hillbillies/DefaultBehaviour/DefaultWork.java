package hillbillies.DefaultBehaviour;

import hillbillies.exceptions.DefaultBehaviourException;
import hillbillies.exceptions.UnitException;
import hillbillies.gameworld.Cube;
import hillbillies.gameworld.SceneryType;
import hillbillies.model.Unit;
import hillbillies.model.World;

import java.util.LinkedList;

/**
 * Class with the Work functionality of a Unit in default behaviour
 * @version 1.0
 * @author Maxim Verbiest
 */
public class DefaultWork extends DefaultAction {
    /**
     * Constructor
     *
     * @param newUnit The Unit for which this functionality will be created
     * @throws DefaultBehaviourException When newUnit is NULL
     * @effect setUnit
     */
    public DefaultWork(Unit newUnit) throws DefaultBehaviourException {
            super(newUnit);
    }

    /**
     * Let the Unit work on an adjacent cube when one of these cubes offers some interesting work, or
     * on its current cube when there is no interesting adjacent cube
     * @throws DefaultBehaviourException
     *          When a conflict occurs in Work() of Unit
     */
    @Override
    public void executeIfChosen() throws DefaultBehaviourException {
        try {
            //The coordinates of the cube where the Unit will work on
            Number[] workCoordinates = getWorkCube().getPosition().toVector();

            getUnit().Work(workCoordinates[0].intValue(),workCoordinates[1].intValue(),workCoordinates[2].intValue());

        } catch (UnitException e ){
            throw new DefaultBehaviourException(DefaultBehaviourException.CONFLICT_OF_EXECUTION_IN_DB,
                    "DB Work conflict: " + e.getMessage());
        }
    }

    /**
     * It will always be possible to work on the Unit's given Position because this functionality is total programmed
     * @return true
     */
    @Override
    protected boolean canBeExecuted() {
        return true;
    }

    /**
     * Get a cube to work on
     * @return a cube with interesting features to work on, or the Unit's current cube if there is no such interesting cube
     */
    public Cube getWorkCube(){

        //List containing all the cubes that are interesting to work on
        LinkedList<Cube> possibleCubes = new LinkedList<>();

        //Load this list
        possibleCubes = FillList();

        if (!possibleCubes.isEmpty()){
            try {
                return possibleCubes.get(genIndex(possibleCubes));
            } catch (DefaultBehaviourException e) {
                //Won't happen, list isn't empty
                return null;
            }
        } else {
            //There wasn't any interesting cube for this Unit, return it's current cube in stead
            return getUnit().getCurrentCube();
        }
    }

    /**
     * Returns a List with cubes where the Unit can work on
     * @return a linked list with cubes that are interesting and possible to work on
     */
    public LinkedList<Cube> FillList() {

        LinkedList<Cube> possibleCubes = new LinkedList<>();

        for (int y = -1 ; y < 2; y++){
            for (int x = -1 ; x < 2 ; x ++){

                //Get a potential cube to work on
                Cube potentialCube = World.getInstance().getCubeByKey(
                        getUnit().getCurrentCube().getPosition().getPosition_x() + x,
                        getUnit().getCurrentCube().getPosition().getPosition_y() + y,
                        getUnit().getCurrentCube().getPosition().getPosition_z());

                //Check whether the potential cube was not out of bound in the given World
                if (potentialCube != null){
                    //Check whether the Unit is already carrying an Object
                    if(getUnit().isCarryingObject()){
                        if(potentialCube.getSceneryType() == SceneryType.WORKSHOP || potentialCube.getSceneryType() == SceneryType.AIR)
                            possibleCubes.add(potentialCube);
                    } else {
                    //Check whether the potential cube has some interesting features to work on
                    if (potentialCube.getSceneryType().notAir() || potentialCube.containsBoulder() || potentialCube.containsLog())
                        possibleCubes.add(potentialCube);
                    }
                }

            }
        }

        return possibleCubes;
    }

    /**
     * Generate a random integer as an index for a given list
     * @param list  The given list
     * @return a random integer between 0 and the the last index of the list
     */
    private int genIndex(LinkedList list) throws DefaultBehaviourException {
        if (list.isEmpty()) throw new DefaultBehaviourException(DefaultBehaviourException.WRONG_PARAMETER,
                "Tried to generate a random integer for an empty list");

        return (int)( Math.floor(Math.random()*(list.size()-1)) );
    }

    @Override
    public String toString() {
        return "DB's Work";
    }
}
