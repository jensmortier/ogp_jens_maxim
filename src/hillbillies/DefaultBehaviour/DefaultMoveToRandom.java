package hillbillies.DefaultBehaviour;

import hillbillies.exceptions.DefaultBehaviourException;
import hillbillies.exceptions.PositionException;
import hillbillies.exceptions.UnitException;
import hillbillies.gameworld.Position;
import hillbillies.model.Unit;
import hillbillies.model.World;

/**
 * Class with the moveTo functionality of a Unit in default behaviour
 * @version 1.0
 * @author Maxim Verbiest
 */
public class DefaultMoveToRandom extends DefaultAction {
    /**
     * Constructor
     *
     * @param newUnit The Unit for which this functionality will be created
     * @throws DefaultBehaviourException When newUnit is NULL
     * @effect setUnit
     */
    public DefaultMoveToRandom(Unit newUnit) throws DefaultBehaviourException {
        super(newUnit);
    }


    /**
     * Let the Unit move to a random position
     * @throws DefaultBehaviourException
     *          When a conflict occurs in moveTo() of Unit
     */
    @Override
    public void executeIfChosen() throws DefaultBehaviourException {
        try{
            Position<Integer> targetPosition = genTargetPosition();
            getUnit().moveTo(targetPosition.getPosition_x(),targetPosition.getPosition_y(),targetPosition.getPosition_z());
        } catch (UnitException e){
            throw new DefaultBehaviourException(DefaultBehaviourException.CONFLICT_OF_EXECUTION_IN_DB,
                    " A conflict occurred during the executing of a random moveTo in DB.");
        }
    }

    /**
     * Move to a random position will always be possible
     * @return true
     */
    @Override
    protected boolean canBeExecuted() {
        return true;
    }

    @Override
    public String toString() {
        return "moveTo in DB invoked";
    }

    /**
     * Checks whether a given position's cube coordinates are the same as the Unit's current cube coordinates
     * @param newPosition   the given position
     * @return  true if the given position's cube coordinates are the same as the Unit's current cube coordinates
     */
    private boolean hasSameCube(Position newPosition){
        return getUnit().getCurrentCube().getPosition().equals(newPosition.toCubePosition());
    }

    /**
     * Checks whether a given coordinate is ouf of a given bound
     * @param coordinate    The given coordinate
     * @param bound         The given bound
     * @return  True if the coordinate is positive and less than the bound
     */
    private boolean outOfBounds(int coordinate, int bound){
        return(coordinate < 0 || coordinate > (bound - 1));
    }

    /**
     * Generate a random coordinate in a distance of 3 from a given coordinate withing a given bound
     * @param givenCoordinate
     * @param bound The bound to respect
     * @return  a random coordinate in a distance of 3 from a given coordinate within a given bound
     */
    private int genRandomCoordinate(int givenCoordinate,int bound){
        if (Math.random() < 0.5){
            return outOfBounds(givenCoordinate + (int)Math.floor(Math.random()*3),bound)? bound - 1: givenCoordinate + (int)Math.floor(Math.random()*3);
        } else return outOfBounds(givenCoordinate - (int)Math.floor(Math.random()*3),bound)? 0 :givenCoordinate - (int)Math.floor(Math.random()*3);
    }

    /**
     * Generate a random position
     * @return a randomly generated position
     */
    private Position<Integer> genRandomPosition()  {
        try {
            return new Position<Integer>(
                    genRandomCoordinate(getUnit().getCurrentCube().getPosition().getPosition_x(),World.GAME_WORLD_BOUND_X()),
                    genRandomCoordinate(getUnit().getCurrentCube().getPosition().getPosition_y(),World.GAME_WORLD_BOUND_Y()),
                    genRandomCoordinate(getUnit().getCurrentCube().getPosition().getPosition_z(),World.GAME_WORLD_BOUND_Z()) );
        } catch (PositionException e) {
            //The generated position was invalid, return a new one
            return genRandomPosition();
        }
    }

    /**
     * Checks whether a given position is a possible target position to move to
     * @param newPosition   a given position
     * @return  true if the given position is not solid and if the given position is a valid position considering this game world and if the given position is not the same
     *  position as the Unit's current position
     */
    private boolean isPossibleTarget(Position<Integer> newPosition){

                // May not be solid
        return !World.getInstance().getCubeByKey(
                newPosition.getPosition_x().intValue(),newPosition.getPosition_y().intValue(),newPosition.getPosition_z().intValue()).getSceneryType().isSolid()
                &&
                //Must be a valid position in the game world
                World.getInstance().isValidPosition(newPosition.getPosition_x().intValue(),newPosition.getPosition_y().intValue(),newPosition.getPosition_z().intValue())
                &&
                //Unit should walk to a different cube
                !hasSameCube(newPosition);
    }

    /**
     * Generates a valid random position
     * @return A valid random position
     */
    private Position genTargetPosition() {

        Position<Integer> possibleTarget = genRandomPosition();

        //Check if this position is valid, and different from the Unit's current position
        while(!isPossibleTarget(possibleTarget)){
            possibleTarget = genRandomPosition();
        }

        return possibleTarget;
    }

}
