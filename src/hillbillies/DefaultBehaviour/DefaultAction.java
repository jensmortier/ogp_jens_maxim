package hillbillies.DefaultBehaviour;

import be.kuleuven.cs.som.annotate.Basic;
import hillbillies.exceptions.DefaultBehaviourException;
import hillbillies.model.Unit;

/**
 * Abstract class for all possible kind of actions a Unit can perform in its default behaviour
 * @version 1.0
 * @author Maxim Verbiest
 */
public abstract class DefaultAction {

    /**
     * The Unit for which this actions will be created
     */
    private Unit unit;

    /**
     * Constructor
     * @param newUnit   The Unit for which this actions will be created
     * @effect setUnit
     * @throws DefaultBehaviourException
     *          When newUnit is NULL
     *          When this action cannot be created for this Unit
     */
    public DefaultAction(Unit newUnit) throws DefaultBehaviourException {
        if (newUnit == null) throw new DefaultBehaviourException(DefaultBehaviourException.NULL_VALUE_AS_PARAMETER,
                "The given Unit where Default behaviour must be executed for is a NULL value");

        //set the unit to the given unit
        setUnit(newUnit);

        //Abort if the action can't be executed
        if (!canBeExecuted()) throw new DefaultBehaviourException(DefaultBehaviourException.INSTANCE_COULD_NOT_BE_CREATED,
                toString() + " Could not be performed given the current circumstances of the game world");


    }

    /**
     * Set the Unit to a given Unit
     * @param newUnit   The given Unit
     * @throws DefaultBehaviourException
     *          When newUnit is NULL
     */
    private void setUnit(Unit newUnit) throws DefaultBehaviourException {
        if (newUnit != null){

            unit = newUnit;
        } else throw new DefaultBehaviourException(DefaultBehaviourException.NULL_VALUE_AS_PARAMETER,
                "Tried to set the Unit field to a NULL value");
    }

    @Basic
    public Unit getUnit(){
        return unit;
    }

    /**
     * Abstract method that will provide the functionality of the subclasses when they are chosen to be executed
     */
    public abstract void executeIfChosen() throws DefaultBehaviourException;

    /**
     * Check whether this action can be executed
     * @return true if the subclasses can be executed given the circumstances
     */
    protected abstract boolean canBeExecuted();

    /**
     * Returns the name of this action
     * @return the name of this action
     */
    @Override
    public abstract String toString();
}
