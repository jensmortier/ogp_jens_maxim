package hillbillies.DefaultBehaviour;

import hillbillies.exceptions.CubeException;
import hillbillies.exceptions.DefaultBehaviourException;
import hillbillies.exceptions.UnitException;
import hillbillies.gameworld.Cube;
import hillbillies.model.Unit;
import hillbillies.model.World;

/**
 * Class with the Attack functionality of a Unit in default behaviour
 * @version 1.0
 * @author Maxim Verbiest
 */
public class DefaultAttack extends DefaultAction {

    /**
     * Constructor
     *
     * @param newUnit The Unit for which this functionality will be created
     * @throws DefaultBehaviourException When newUnit is NULL
     * @effect setUnit
     */
    public DefaultAttack(Unit newUnit) throws DefaultBehaviourException {
        super(newUnit);
    }


    /**
     * Let the Unit attack a potential enemy
     * @throws DefaultBehaviourException
     *          When a conflict occurs during Attack() of Unit
     */
    @Override
    public void executeIfChosen() throws DefaultBehaviourException {

        try {
            if(getEnemy() == null) return ;
            getUnit().Attack(getEnemy());
        } catch (UnitException e) {
            throw new DefaultBehaviourException(DefaultBehaviourException.CONFLICT_OF_EXECUTION_IN_DB,
                    "A conflict occurred during the execution of Work in DB.");
        }

    }

    /**
     * Check whether this Unit has potential enemies given his current position
     * @return true when this Unit has potential enemies to fight with
     */
    @Override
    public boolean canBeExecuted() {

        try {
            for (Cube neighbour : getUnit().getCurrentCube().getNeighbourCubes_sameZ()) {

                if ( neighbour.hasPotentialEnemy(getUnit().getFaction()) )  return true;

            }

            //No enemy could be found
            return false;
        } catch (CubeException e) {
            //Won't happen
            return false;
        }
    }

    /**
     * Get an Enemy to fight with
     * @return An Enemy Unit to fight with
     */
    private Unit getEnemy(){
        try{
            for (Cube neighbour: getUnit().getCurrentCube().getNeighbourCubes_sameZ()) {

                if (neighbour.hasPotentialEnemy(getUnit().getFaction())) {

                    return neighbour.getRandomEnemy(getUnit().getFaction());
                }
            }

            return null;

        } catch(CubeException e){
            //No neighbouring cubes could be found, won't happen
            return null;
        }
    }

    @Override
    public String toString() {
        return "DB's Attack ";
    }


}
