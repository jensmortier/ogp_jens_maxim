package hillbillies.DefaultBehaviour;

import hillbillies.exceptions.DefaultBehaviourException;
import hillbillies.exceptions.UnitException;
import hillbillies.model.Unit;

/**
 * Class with the Rest functionality of a Unit in default behaviour
 * @version 1.0
 * @author Maxim Verbiest
 */
public class DefaultRest extends DefaultAction {
    /**
     * Constructor
     *
     * @param newUnit The Unit for which this functionality will be created
     * @throws DefaultBehaviourException When newUnit is NULL
     * @effect setUnit
     */
    public DefaultRest(Unit newUnit) throws DefaultBehaviourException {
        super(newUnit);
    }

    /**
     * Let the Unit rest
     * @throws DefaultBehaviourException
     *          when a conflict occurs in Rest of a Unit
     */
    @Override
    public void executeIfChosen() throws DefaultBehaviourException {
        try {
            getUnit().Rest();
        } catch (UnitException e) {
            throw new DefaultBehaviourException(DefaultBehaviourException.CONFLICT_OF_EXECUTION_IN_DB,
                    "A conflict occurred during the execution of Rest in default Behaviour");
        }
    }

    /**
     * Check whether the Unit can rest given the current circumstances
     * @return true if the Unit isn't fully recovered
     */
    @Override
    protected boolean canBeExecuted() {
        return !getUnit().isFullyRecovered();
    }

    @Override
    public String toString() {
        return "DB's rest";
    }
}
