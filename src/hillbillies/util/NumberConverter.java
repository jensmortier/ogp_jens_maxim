package hillbillies.util;

/**
 * Class with converter functionality
 *
 * @version 1.0
 * @author Maxim Verbiest
 */
abstract public class NumberConverter {
    public static Double[] vectorToDouble(Number[] vector) {
        Double[] result = new Double[vector.length];
        int c = 0;
        for(Number n : vector) {
            result[c] = n.doubleValue();
            c++;
        }
        return result;
    }

    public static Integer[] vectorToInteger(Number[] vector) {
        Integer[] result = new Integer[vector.length];
        int c = 0;
        for(Number n : vector) {
            result[c] = n.intValue();
            c++;
        }
        return result;
    }

    public static Integer[] basicVectorToInteger(int[] vector) {
        Integer[] result = new Integer[vector.length];
        int c = 0;
        for(int n : vector) {
            result[c] = new Integer(n);
            c++;
        }
        return result;
    }
}
