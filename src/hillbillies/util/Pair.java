package hillbillies.util;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;
import be.kuleuven.cs.som.annotate.Raw;

/**
 * Helper class that represents a pair of two values
 *
 * @version 1.0
 * @author Maxim Verbiest
 */
public class Pair<T1,T2> {

    private T1 firstValue;
    private T2 secondValue;

    public Pair(T1 first, T2 second) {
        firstValue = first;
        secondValue = second;
    }

    @Basic @Raw @Immutable
    public T1 getFirstValue() {
        return firstValue;
    }

    @Basic @Raw @Immutable
    public T2 getSecondValue() {
        return secondValue;
    }
}
