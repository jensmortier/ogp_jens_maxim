package hillbillies.util;

import java.util.LinkedList;

/**
 * Reverse stack that orders and pops elements in a FIFO manner
 * @param <T>
 *
 * @version 1.0
 * @author Jens Mortier
 */
public class ReverseStack<T> extends LinkedList<T> {
    /**
     * Constructs empty stack
     * @effect	Constructs empty list
     * 			| this.LinkedList()
     */
    public ReverseStack() {
        super();
    }

    /**
     * Adds action to the reversed stack
     * @param action The action to add
     * @post	First action added
     * 			| this.getLast() == action
     */
    public void addElement(T action) {
        this.add(action);
    }

    /**
     * Returns first action in this stack and removes this action
     * @return	First action of this stack
     * 			| result == this.getFirst()
     * @effect	Removes last action
     * 			| this.removeFirst()
     */
    public T popElement() {
        //First fetch it
        T action = this.getFirst();

        //Remove last action
        this.removeFirst();

        //And return
        return action;
    }
}
