package hillbillies.util;

/**
 * Class which represents a referenced integer
 */
public class ReferencedInteger {

    private int value;

    public ReferencedInteger(int value) {
        this.value = value;
    }

    public void raise() {
        value++;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public int intValue() {
        return value;
    }
}
