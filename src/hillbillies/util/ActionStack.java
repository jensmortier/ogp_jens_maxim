package hillbillies.util;

import java.util.LinkedList;

import hillbillies.model.MovementType;

/**
 * This 'action stack' represents a stack with certain objects of type E to performed in a LIFO way
 * @param <E>
 *
 *     @version 1.0
 *     @author Jens Mortier
 */
public class ActionStack<E> extends LinkedList<E> {
	
	/**
	 * Constructs empty stack
	 * @effect	Constructs empty list
	 * 			| this.LinkedList()
	 */
	public ActionStack() {
		super();
	}
	
	/**
	 * Adds action to the stack
	 * @param action The action to add
	 * @post	First action added
	 * 			| this.getFirst() == action
	 */
	public void addAction(E action) {
		this.add(action);
	}
	
	/**
	 * Returns last action in this stack and removes this action
	 * @return	Last action of this stack
	 * 			| result == this.getLast()
	 * @effect	Removes last action
	 * 			| this.removeLast()
	 */
	public E getNextAction() {
		//First fetch it
		E action = this.getLast();
		
		//Remove last action
		this.removeLast();
		//And return
		return action;
	}
}
