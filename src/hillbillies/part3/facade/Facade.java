package hillbillies.part3.facade;

import hillbillies.exceptions.CubeException;
import hillbillies.exceptions.SchedulerException;
import hillbillies.exceptions.UnitException;
import hillbillies.exceptions.WorldException;
import hillbillies.gameworld.SceneryType;
import hillbillies.model.*;
import hillbillies.part2.listener.TerrainChangeListener;
import hillbillies.part3.programs.ITaskFactory;
import hillbillies.scheduler.execution.checking.FormChecker;
import hillbillies.scheduler.execution.components.Scheduler;
import hillbillies.scheduler.execution.components.Task;
import hillbillies.scheduler.execution.factory.TaskFactory;
import ogp.framework.util.ModelException;

import java.util.Collection;
import java.util.Iterator;
import java.util.Objects;
import java.util.Set;

/**
 * Facade
 * @version 1.0
 * @author Maxim Verbiest
 *         Jens Mortier
 */
public class Facade implements IFacade {
    @Override
    public ITaskFactory<?, ?, Task> createTaskFactory() {
        return new TaskFactory();
    }

    @Override
    public boolean isWellFormed(Task task) throws ModelException {
        //Init checker engine
        FormChecker checker = new FormChecker(task);
        System.out.println("Checking wellformedness");
        return checker.isWellFormed();  
    }

    @Override
    public Scheduler getScheduler(Faction faction) throws ModelException {
        return faction.getScheduler();
    }

    @Override
    public void schedule(Scheduler scheduler, Task task) throws ModelException {
        System.out.println("Trying to schedule task " + task.getName());
        scheduler.addTask(task);

    }

    @Override
    public void replace(Scheduler scheduler, Task original, Task replacement) throws ModelException {
        try {
            scheduler.replaceTask(original,replacement);
        } catch (SchedulerException e) {
            throw new ModelException("Couldn't replace a Task",e);
        }
    }

    @Override
    public boolean areTasksPartOf(Scheduler scheduler, Collection<Task> tasks) throws ModelException {
        return scheduler.areTasksPartOf(tasks);
    }

    @Override
    public Iterator<Task> getAllTasksIterator(Scheduler scheduler) throws ModelException {
        return scheduler.iterator();
    }

    @Override
    public Set<Scheduler> getSchedulersForTask(Task task) throws ModelException {
        return task.schedulersToSet();
    }

    @Override
    public Unit getAssignedUnit(Task task) throws ModelException {
        return task.getAssignedUnit();
    }

    @Override
    public Task getAssignedTask(Unit unit) throws ModelException {
        return unit.getTask();
    }

    @Override
    public String getName(Task task) throws ModelException {
        return task.getName();
    }

    @Override
    public int getPriority(Task task) throws ModelException {
        return task.getPriority();
    }

    @Override
    public World createWorld(int[][][] terrainTypes, TerrainChangeListener modelListener) throws ModelException {
        try {
            World.getInstance().setTerrainChangeListener(modelListener);
            World.getInstance().setConfig(terrainTypes);
        } catch (WorldException e) {
            throw new ModelException("Couldn't instantiate world",e);
        }
        World.getInstance().setTerrainChangeListener(modelListener);
        return World.getInstance();
    }

    @Override
    public int getNbCubesX(World world) throws ModelException {
        return world.GAME_WORLD_BOUND_X();
    }

    @Override
    public int getNbCubesY(World world) throws ModelException {
        return world.GAME_WORLD_BOUND_Y();
    }

    @Override
    public int getNbCubesZ(World world) throws ModelException {
        return world.GAME_WORLD_BOUND_Z();
    }

    @Override
    public void advanceTime(World world, double dt) throws ModelException {
        try {
           world.advanceTime(dt);
        } catch (WorldException e) {
            throw new ModelException("Couldn't advance time",e);
        }
    }

    @Override
    public int getCubeType(World world, int x, int y, int z) throws ModelException {
        return world.getCubeByKey(x, y, z).getSceneryType().getNumber();
    }

    @Override
    public void setCubeType(World world, int x, int y, int z, int value) throws ModelException {
        try {
            world.getCubeByKey(x, y, z).setSceneryType(SceneryType.getSceneryType(value));
        } catch (CubeException e) {
            throw new ModelException("Couldn't modify cube",e);
        }
    }

    @Override
    public boolean isSolidConnectedToBorder(World world, int x, int y, int z) throws ModelException {
        return world.isCubeAttachedToBorder(world.getCubeByKey(x,y,z));
    }

    @Override
    public Unit spawnUnit(World world, boolean enableDefaultBehavior) throws ModelException {
        try {
            return world.addUnitToWorld(enableDefaultBehavior);
        } catch (WorldException e){
            throw new ModelException(e.getMessage(),e);
        }
    }

    @Override
    public void addUnit(Unit unit, World world) throws ModelException {
        try {
            world.addGameObject(unit);
        }catch(WorldException e) {
            throw new ModelException(e.getMessage(),e);
        }
    }

    @Override
    public Set<Unit> getUnits(World world) throws ModelException {
        return world.getActiveUnitsInWorld();
    }

    @Override
    public boolean isCarryingLog(Unit unit) throws ModelException {
        return unit.isCarryingLog();
    }

    @Override
    public boolean isCarryingBoulder(Unit unit) throws ModelException {
        return unit.isCarryingBoulder();
    }

    @Override
    public boolean isAlive(Unit unit) throws ModelException {
        return !unit.isTerminated();
    }

    @Override
    public int getExperiencePoints(Unit unit) throws ModelException {
        return unit.getExperiencePoints();
    }

    @Override
    public void workAt(Unit unit, int x, int y, int z) throws ModelException {
        try {
            unit.Work(x,y,z);
        } catch (UnitException e) {
            throw new ModelException("Couldn't let unit Work",e);
        }
    }

    @Override
    public Faction getFaction(Unit unit) throws ModelException {
        return unit.getFaction();
    }

    @Override
    public Set<Unit> getUnitsOfFaction(Faction faction) throws ModelException {
        return faction.unitsToSet();
    }

    @Override
    public Set<Faction> getActiveFactions(World world) throws ModelException {
        return world.factionsToSet();
    }

    @Override
    public double[] getPosition(Boulder boulder) throws ModelException {
        Number[] vector = boulder.getPosition().toVector();
        double[] converted = {vector[0].doubleValue(),vector[1].doubleValue(),vector[2].doubleValue()};
        return  converted;
    }

    @Override
    public Set<Boulder> getBoulders(World world) throws ModelException {
        return world.getActiveBouldersInWorld();
    }

    @Override
    public double[] getPosition(Log log) throws ModelException {
        Number[] vector = log.getPosition().toVector();
        double[] converted = {vector[0].doubleValue(),vector[1].doubleValue(),vector[2].doubleValue()};
        return  converted;
    }

    @Override
    public Set<Log> getLogs(World world) throws ModelException {
        return world.getActiveLogsInWorld();
    }

    @Override
    public Unit createUnit(String name, int[] initialPosition, int weight, int agility, int strength, int toughness, boolean enableDefaultBehavior) throws ModelException {
        try {
            return new Unit(name,initialPosition[0],initialPosition[1],initialPosition[2],weight,agility,strength,toughness,enableDefaultBehavior);
        } catch (UnitException e) {
            throw new ModelException(e.getMessage(),e);
        }
    }

    @Override
    public double[] getPosition(Unit unit) throws ModelException {
        double[] position = new double[3];
        position[0] = unit.getCurrentPosition().getPosition_x();
        position[1] = unit.getCurrentPosition().getPosition_y();
        position[2] = unit.getCurrentPosition().getPosition_z();
        return position;
    }

    @Override
    public int[] getCubeCoordinate(Unit unit) throws ModelException {
        Number[] vector = unit.getCurrentCube().getPosition().toVector();
        int[] converted = {(int)vector[0],(int)vector[1],(int)vector[2]};
        return  converted;
    }

    @Override
    public String getName(Unit unit) throws ModelException {
        return unit.getName();
    }

    @Override
    public void setName(Unit unit, String newName) throws ModelException {
        try {
            unit.setName(newName);
        }catch(UnitException e) {
            throw new ModelException(e.getMessage(),e);
        }
    }

    @Override
    public int getWeight(Unit unit) throws ModelException {
        return unit.getWeight();
    }

    @Override
    public void setWeight(Unit unit, int newValue) throws ModelException {
        unit.setWeight(newValue);
    }

    @Override
    public int getStrength(Unit unit) throws ModelException {
        return unit.getStrength();
    }

    @Override
    public void setStrength(Unit unit, int newValue) throws ModelException {
        unit.setStrength(newValue);
    }

    @Override
    public int getAgility(Unit unit) throws ModelException {
        return unit.getAgility();
    }

    @Override
    public void setAgility(Unit unit, int newValue) throws ModelException {
        unit.setAgility(newValue);
    }

    @Override
    public int getToughness(Unit unit) throws ModelException {
        return unit.getToughness();
    }

    @Override
    public void setToughness(Unit unit, int newValue) throws ModelException {
        unit.setToughness(newValue);
    }

    @Override
    public int getMaxHitPoints(Unit unit) throws ModelException {
        return unit.getMaxPoints();
    }

    @Override
    public int getCurrentHitPoints(Unit unit) throws ModelException {
        return unit.getHitPoints();
    }

    @Override
    public int getMaxStaminaPoints(Unit unit) throws ModelException {
        return unit.getMaxPoints();
    }

    @Override
    public int getCurrentStaminaPoints(Unit unit) throws ModelException {
        return unit.getStaminaPoints();
    }

    @Override
    public void moveToAdjacent(Unit unit, int dx, int dy, int dz) throws ModelException {
        try {
            unit.moveTo(
                    unit.getCurrentCube().getPosition().getPosition_x() + dx,
                    unit.getCurrentCube().getPosition().getPosition_y() + dy,
                    unit.getCurrentCube().getPosition().getPosition_z() + dz
            );
        }catch(UnitException e) {
            throw new ModelException(e.getMessage(),e);
        }
    }

    @Override
    public double getCurrentSpeed(Unit unit) throws ModelException {
        return unit.getCurrentSpeed();
    }

    @Override
    public boolean isMoving(Unit unit) throws ModelException {
        return unit.isMoving();
    }

    @Override
    public void startSprinting(Unit unit) throws ModelException {
        unit.Sprint();
    }

    @Override
    public void stopSprinting(Unit unit) throws ModelException {
        unit.StopSprint();
    }

    @Override
    public boolean isSprinting(Unit unit) throws ModelException {
        return unit.isSprinting();
    }

    @Override
    public double getOrientation(Unit unit) throws ModelException {
        return unit.getOrientation();
    }

    @Override
    public void moveTo(Unit unit, int[] cube) throws ModelException {
        try {
            unit.moveTo(cube[0],cube[1],cube[2]);
        }catch(UnitException e) {
            throw new ModelException(e.getMessage(),e);
        }
    }

    @Override
    public boolean isWorking(Unit unit) throws ModelException {
        return unit.getMovementStatus() == MovementType.WORKING;
    }

    @Override
    public void fight(Unit attacker, Unit defender) throws ModelException {
        try {
            attacker.Attack(defender);
        } catch (UnitException e) {
            throw new ModelException("Couldn't let unit fight",e);
        }
    }

    @Override
    public boolean isAttacking(Unit unit) throws ModelException {
        return unit.getMovementStatus() == MovementType.FIGHTING;
    }

    @Override
    public void rest(Unit unit) throws ModelException {
        try {
            unit.Rest();
        } catch (UnitException e) {
            throw new ModelException("Couldn't let unit rest",e);
        }
    }

    @Override
    public boolean isResting(Unit unit) throws ModelException {
        return unit.getMovementStatus() == MovementType.RESTING;
    }

    @Override
    public void setDefaultBehaviorEnabled(Unit unit, boolean value) throws ModelException {
        try {
            if (value) {
                unit.StartDefaultBehaviour();
            } else unit.StopDefaultBehaviour();
        } catch (UnitException e){
            throw new ModelException(e.getMessage(),e);
        }
    }

    @Override
    public boolean isDefaultBehaviorEnabled(Unit unit) throws ModelException {
        return unit.getDefaultBehaviourStatus();
    }
}
