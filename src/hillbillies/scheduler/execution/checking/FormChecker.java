package hillbillies.scheduler.execution.checking;

import hillbillies.scheduler.execution.components.Task;
import hillbillies.scheduler.memory.expression.Expression;
import hillbillies.scheduler.memory.expression.bool.interfaces.EBooleanDoubleOperation;
import hillbillies.scheduler.memory.expression.bool.interfaces.EBooleanSingleOperation;
import hillbillies.scheduler.memory.expression.model.position.PNextTo;
import hillbillies.scheduler.memory.expression.model.position.PPostionOf;
import hillbillies.scheduler.memory.expression.variable.ERead;
import hillbillies.scheduler.memory.statement.Statement;
import hillbillies.scheduler.memory.statement.generics.SFlowControl;
import hillbillies.scheduler.memory.statement.io.SPrint;
import hillbillies.scheduler.memory.statement.language.*;
import hillbillies.scheduler.memory.statement.model.SAction;

import java.util.HashSet;

/**
 * Class which represents a checker for well formedness
 *
 * @version 1.0
 * @author Jens Mortier
 */
public class FormChecker {

    private boolean while_opened;
    private HashSet<String> variables_initialized;
    private Task task;

    public FormChecker(Task t) {
        task = t;
    }

    public boolean isWellFormed() {
        //Initialize checker
        while_opened = false;
        Statement stat = task.getActivity();

        //Check for wellformedness considering breaks
        if(!checkBreak(stat)) return false;

        //Check for wellformedness considering assignments
        variables_initialized = new HashSet<>();
        if(!checkReadStatement(stat)) return false;

        //Everything seems okay
        return true;
    }

    private boolean checkBreak(Statement stat) {
        boolean is_ok = true;
        if(stat instanceof CompositeStatement) {
            boolean was_while_opened = while_opened;
            for(Statement s : ((CompositeStatement) stat).getStatements()) {
                is_ok = is_ok && checkBreak(s);
                if(!was_while_opened) while_opened = false;//Because if one of the upper statements isn't while, we have to reset that
            }
            //Set original status back
            while_opened = was_while_opened;
        }else if(stat instanceof SWhile) {
            while_opened = true;
            is_ok = checkBreak(((SWhile) stat).getStatement());
        }else if(stat instanceof SIf) {
            SIf if_stat = (SIf) stat;
            is_ok = checkBreak(if_stat.getStatement()) && checkBreak(if_stat.getAlternative_statement());
        }else if(stat instanceof SBreak) {
            if(!while_opened) is_ok = false;
        }

        return is_ok;
    }

    private boolean checkReadStatement(Statement stat) {
        if(stat instanceof SFlowControl) {
            //Check expression
            if(!checkReadExpression(((SFlowControl) stat).getExpression())) return false;

            //Check statement
            if(!checkReadStatement(((SFlowControl) stat).getStatement())) return false;

            //Check alternative if if statement
            if(stat instanceof SIf) return checkReadStatement(((SIf) stat).getAlternative_statement());

            return true;
        }else if(stat instanceof SAssignment) {
            //Record assignment
            variables_initialized.add(((SAssignment) stat).getVariable().getName());
            return true;
        }else if(stat instanceof SPrint) {
            return checkReadExpression(((SPrint) stat).getValue());
        }else if(stat instanceof CompositeStatement) {
            boolean is_ok = true;
            for(Statement s : ((CompositeStatement) stat).getStatements()) {
                is_ok = is_ok && checkReadStatement(s);
            }
            return is_ok;
        }else if(stat instanceof SAction) {
            return checkReadExpression(((SAction) stat).getOperand());
        }else {
            return true;
        }
    }

    private boolean checkReadExpression(Expression exp) {
        if(exp instanceof EBooleanSingleOperation) {
            if(((EBooleanSingleOperation) exp).getOperand() instanceof ERead) {
                //We have got a variable here! Check if defined
                return variableNameDefined(((ERead) ((EBooleanSingleOperation) exp).getOperand()).getVariableName());
            }else {
                return checkReadExpression(((EBooleanSingleOperation) ((EBooleanSingleOperation) exp).getOperand()).getOperand());
            }
        }else if(exp instanceof EBooleanDoubleOperation) {
            //Check first operand
            if(((EBooleanDoubleOperation) exp).getOperand1() instanceof ERead) {
                //Check if variable is defined
                if(!variableNameDefined(((ERead)((EBooleanDoubleOperation) exp).getOperand1()).getVariableName())) return false;
            }else {
                if(!checkReadExpression(((EBooleanDoubleOperation) exp).getOperand1())) return false;
            }

            //Check second operand
            if(((EBooleanDoubleOperation) exp).getOperand1() instanceof ERead) {
                //It's a variable
                return variableNameDefined(((ERead) ((EBooleanDoubleOperation) exp).getOperand1()).getVariableName());
            }else {
                return checkReadExpression(((EBooleanDoubleOperation) exp).getOperand1());
            }
        }else if(exp instanceof PPostionOf) {
            if(((PPostionOf) exp).getUnit() instanceof ERead) {
                return variableNameDefined(((ERead) ((PPostionOf) exp).getUnit()).getVariableName());
            }else {
                return checkReadExpression(((PPostionOf) exp).getUnit());
            }
        }else if(exp instanceof PNextTo) {
            if(((PNextTo) exp).getBench_position() instanceof ERead) {
                return variableNameDefined(((ERead) ((PNextTo) exp).getBench_position()).getVariableName());
            }else {
                return checkReadExpression(((PNextTo) exp).getBench_position());
            }
        }else if(exp instanceof ERead){
            return variableNameDefined(((ERead) exp).getVariableName());
        }else {
            return true;
        }
    }

    private boolean variableNameDefined(String variableName) {
        return variables_initialized.contains(variableName);
    }
}
