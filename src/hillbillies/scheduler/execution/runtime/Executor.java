package hillbillies.scheduler.execution.runtime;

import hillbillies.exceptions.SchedulerException;
import hillbillies.exceptions.TaskException;
import hillbillies.scheduler.execution.components.Task;
import hillbillies.scheduler.execution.components.TaskStatus;
import hillbillies.scheduler.memory.exceptions.DynamicExpressionException;
import hillbillies.scheduler.memory.statement.Statement;
import hillbillies.scheduler.memory.expression.exceptions.InterruptTrapException;
import hillbillies.scheduler.memory.statement.exceptions.StatementExecutionException;
import hillbillies.util.ReferencedInteger;

/**
 * Class which represents an executor, for the execution of statements
 *
 * @version 1.0
 * @author Jens Mortier
 */
abstract public class Executor {

    /**
     * Executes a call from a unit to execute a certain task and a number of statements
     * @param task              Task to be executed
     * @param nr_of_statements  Maximum nr of statements to be executed
     * @throws ExecutionException
     */
    public static void execute(Task task,int nr_of_statements ) throws ExecutionException {
        try {
            //Check if this task is was interrupted due to replacement in scheduler
            if(task.getStatus() == TaskStatus.INTERRUPTED){
                //Replace Task
                task.replaceTask();
            }

            //Launch!
            System.out.println("Execute task");
            task.getActivity().execute(task.getAssignedUnit(), new ReferencedInteger(nr_of_statements), new ReferencedInteger(0));
            System.out.println("Execution ended");

            //Check if task is finished
            if(task.getLatestStatementExecuted() == null) {//That's my boy!
                System.out.println("Finish task");
                task.setStatus(TaskStatus.FINISHED);
                task.terminate();
            }
        } catch (StatementExecutionException e) {
            e.printStackTrace();
            throw new ExecutionException("Failure during execution",e);
        } catch(DynamicExpressionException e) {
            if(e instanceof InterruptTrapException) {
                try {
                    System.out.println("INTERRUPT");
                    task.getSchedulers().getFirst().interruptTask(task);
                } catch (SchedulerException e1) {
                    throw new ExecutionException("Failed to reschedule task",e1);
                }
            }else {
                throw new ExecutionException("Failure during execution",e);
            }


        } catch (TaskException e) {
            throw new ExecutionException("Couldn't update and terminate task",e);
        }
    }
}
