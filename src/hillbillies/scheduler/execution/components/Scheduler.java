package hillbillies.scheduler.execution.components;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Raw;
import hillbillies.exceptions.*;
import hillbillies.model.Faction;
import hillbillies.model.Unit;

import java.util.*;

/**
 * Class representing a Scheduler that will manage a list of prioritised tasks to be executed by Units
 * @version 1.0
 * @author Maxim Verbiest
 */
public class Scheduler {

    /**
     * A scheduler will be associated with a faction
     */
    private Faction faction;

    /**
     * All the tasks managed by this Scheduler
     */
    private LinkedList<Task> tasks;

    /**
     * Constructor
     * @param faction   The faction associated with this Scheduler
     * @effect setFaction()
     *          |setFaction(faction)
     * @throws SchedulerException
     *          |setFaction(faction)
     */
    public Scheduler(Faction faction) throws SchedulerException {
        setFaction(faction);
        tasks = new LinkedList<>();
    }

    @Basic @Raw
    public Faction getFaction(){
        return faction;
    }

    /**
     * Assign a given faction to this Scheduler
     * @param   faction   The given faction
     * @post    The faction field will be set to the given faction
     *          |new.getFaction() == faction
     * @throws  SchedulerException
     *          |if(faction == null)
     */
    public void setFaction(Faction faction) throws SchedulerException{
        if (faction == null) throw new SchedulerException(SchedulerException.FACTION_ASSIGNMENT_ERROR,"Null value as faction to assign given");
        this.faction = faction;
    }

    @Basic @Raw
    public LinkedList<Task> getTasksList(){
        return tasks;
    }

    /**
     * Add a task to this Scheduler
     * @param newTask   The given task to add
     * @post    The task will be added to the list of tasks, if it is not already in this list and has no null reference
     *          |if(!getTaskList().contains(newTask) && newTask != null)
     *          |then getTaskList().add(newTask)
     * @effect  newTask.addScheduler()  To keep the relation up to date at both sides
     *          |if(!getTaskList().contains(newTask) && newTask != null)
     *          |then newTask.addScheduler(this)
     *
     */
    public void addTask(Task newTask){
        //To get out of the loop + null check
        if (getTasksList().contains(newTask) || newTask == null) return;

        getTasksList().add(newTask);
        System.out.println("TASK added");
        //Relation must be updated at two sides
        newTask.addScheduler(this);
    }

    /**
     * Remove a Task from the list of tasks
     * @param task  The task to remove
     * @post    The Task will be deleted from the list of tasks, if this list contains it
     *          |if(getTasksList.contains(task))
     *          |getTasksList().remove(task)
     * @effect  task.removeScheduler()  To keep the relationship up to dated at two sides
     *          |if(getTasksList.contains(task))
     *          |task.removeScheduler(this)
     */
    public void removeTask(Task task){
        if(!getTasksList().contains(task)) return;

        getTasksList().remove(task);
        //Relation must be updated at two sides
        task.removeScheduler(this);
    }

    /**
     * Check whether a given Task is part of the Tasks this scheduler manages
     * @param task  The given Task
     * @return  True if the given Task is part of this Scheduler
     *          |result == getTasksList().constains(task)
     */
    public boolean isTaskPartOf(Task task){
        return getTasksList().contains(task);
    }

    /**
     * Check whether a collection of Tasks are ALL part of this Scheduler
     * @param tasks The collection of Tasks to check
     * @return  True if all the Tasks of the collection are part of this Scheduler
     *          |for(Task tasToCheck: tasks)
     *          | if(!isTaskPartOf(taskToCheck)
     *          | then result == false
     */
    public boolean areTasksPartOf(Collection<Task> tasks){

        for(Task taskToCheck: tasks){
            if (!isTaskPartOf(taskToCheck)) return false;
        }

        //If reached here, all tasks of the collection were part of this Scheduler
        return true;
    }

    /**
     * Replace a Task managed by this Scheduler with a new Task
     * @param originalTask  The Task managed by this Scheduler
     * @param newTask   The new Task
     * @effect originalTask.replaceTask()
     *          |originalTask.replaceTask(newTask)
     * @throws SchedulerException
     *          |if(newTask == null)
     *          |originalTask.replaceTask(newTask)
     */
    public void replaceTask(Task originalTask, Task newTask) throws SchedulerException {

        if(originalTask == newTask) return; //Silencer

        if(newTask == null) throw new SchedulerException(SchedulerException.NULL_VALUE_PARAM,"New task has null reference");

        try {
            originalTask.replaceTask(newTask);
        } catch (TaskException e){
            throw new SchedulerException(SchedulerException.REPLACEMENT_CONFLICT, e.getMessage());
        }

    }

    /**
     * Get the task with the highest priority, which is not currently executed
     * @return  The task with the highest priority, which is not currently executed or null if there is no free Task
     */
    public Task getHighestFreeTask() {

        //Get a priorityIterator
        Iterator<Task> priorityIterator = iterator();

        //Iterate and check if free
        while (priorityIterator.hasNext()){
            Task taskToCheck = priorityIterator.next();
            if (taskToCheck.getStatus() == TaskStatus.NOT_ASSIGNED)
                return taskToCheck;
        }

        //No free Task could be found, return null
        return null;
    }

    /**
     * Get an Iterator which iterates over the list of Tasks managed by this Scheduler, in descending order of priority
     * @return Iterator over list of Tasks managed by this Scheduler, in descending order of priority
     */
    public Iterator<Task> iterator() {
        return new Iterator<Task>() {

            /**
             * Contains all tasks that are already visited
             */
            private HashSet<Task> tasks_visited = new HashSet<>();

            /**
             * Checks whether there are some unvisited elements
             * @return  True if some tasks are not visited
             */
            @Override
            public boolean hasNext() {
                return tasks_visited.size() < tasks.size();
            }

            /**
             * Returns next element of iterator, i.e: the Task with the highest priority
             * @return  Next element which is not visited yet
             * @throws NoSuchElementException
             *          | !hasNext()
             */
            @Override
            public Task next() {
                if(!hasNext()) throw new NoSuchElementException();
                int highestPriority = getModelPriority();
                Task selected = null;

                for(Task taskToCheck: tasks){
                    if(tasks_visited.contains(taskToCheck)) continue;
                    if (taskToCheck.getPriority() >= highestPriority) {
                        highestPriority = taskToCheck.getPriority();
                        selected = taskToCheck;
                    }

                }

                tasks_visited.add(selected);
                return selected;
            }

            public int getModelPriority() {
                for(Task t : tasks)
                   if(!tasks_visited.contains(t)) return t.getPriority();
                return -1;
            }
        };
    }

    /**
     * Check whether the Scheduler is able to schedule a given Task to a given Unit
     * @param unit  The given Unit
     * @param task  The given Task
     * @return  True if the given Unit and Task has no null references, the unit can have a new task assigned to it, and if this task is part of this scheduler
     *          |result == unit != null && task != null && unit.canAssignTaskTo() && isTaskPartOf(task) && task.isFree()
     */
    public boolean canSchedule(Unit unit, Task task) {
        return unit != null && task != null && unit.canAssignTaskTo() && isTaskPartOf(task) && task.isFree();
    }

    /**
     * Schedule a given Unit with the highest free Task
     * @param unit  The given Unit
     * @effect unit.assignTask()
     *          |unit.assignTask(getHighestFreeTask())
     * @throws SchedulerException
     *          If there is no free Task available
     *          |if(!hasFreeTask())
     *          If it is impossible to schedule the Unit with the highest free Task
     *          |if(!canSchedule(unit,getHighestFreeTask())
     *          If an error occurs during the assignment of this Task to the given Unit
     *          |unit.assignTask(getHighestFreeTask())
     *
     */
    public void schedule(Unit unit) throws SchedulerException {

        //Check whether there is a free Task to schedule this unit to
        if(!hasFreeTask()){
            throw new SchedulerException(SchedulerException.SCHEDULE_CONFLICT,"No free task could be found");
        }
            //A free Task could be found, get this task and assign it to the given unit
            Task freeTask = getHighestFreeTask();
            if (!canSchedule(unit,freeTask)) throw new SchedulerException(SchedulerException.SCHEDULE_CONFLICT,"Unable to schedule for the given unit and task");

            try {
                unit.assignTask(freeTask);
            } catch (UnitException e) {
                throw new SchedulerException(SchedulerException.SCHEDULE_CONFLICT,e.getMessage());
            }
    }

    /**
     * Reset the marking of a task scheduled for executing
     * @param task  The given task to unmark
     * @effect task.deAssignUnit()
     *
     * @throws SchedulerException
     *          |if(task == null  || task.getStatus() == TaskStatus.EXECUTING)
     * @throws SchedulerException   converted from TaskException
     *          |task.deAssignUnit()
     */
    public void resetScheduling(Task task) throws SchedulerException {
        if(task.getStatus() == TaskStatus.NOT_ASSIGNED) return;

        if(task.getStatus() == TaskStatus.EXECUTING) throw new SchedulerException(SchedulerException.UNMARK_CONFLICT,
                "Tried to unmark a scheduling, while the unit was already executing this task");
        if(task == null) throw new SchedulerException(SchedulerException.NULL_VALUE_PARAM,"Task to unmark has null reference");

        try{
            task.deAssignUnit();
        } catch (TaskException e){
            throw new SchedulerException(SchedulerException.UNMARK_CONFLICT,e.getMessage());
        }
    }

    /**
     * Get a Linked list of all the tasks managed by this scheduler that are currently being executed
     * @return  A Linked list of all the tasks that are currently being executed
     *
     */
    public LinkedList<Task> getCurrentlyExecuting(){

        LinkedList<Task> result = new LinkedList<>();

        for(Task taskToCheck : getTasksList()){
            if (taskToCheck.getStatus() == TaskStatus.EXECUTING) result.add(taskToCheck);
        }

        return  result;
    }

    /**
     * Get a Linked list of all the tasks managed by this scheduler that have a positive priority
     * @return  A Linked list of all the tasks with a positive priority, managed by this scheduler
     *          
     */
    public LinkedList<Task> getPositivePriorities(){

        LinkedList<Task> result = new LinkedList<>();

        for(Task taskToCheck : getTasksList()){
            if (taskToCheck.getPriority() >= 0) result.add(taskToCheck);
        }

        return  result;

    }

    /**
     * Check whether a Task can be interrupted
     * @param task  The Task to check
     * @return True if the task is assigned to a Unit, or if the Task is already Executing and if the Task is not null
     *          |result == (task.getStatus() == TaskStatus.EXECUTING ||  task.getStatus() == TaskStatus.ASSIGNED) && task != null
     */
    public boolean canBeInterrupted(Task task){
        return (task.getStatus() == TaskStatus.EXECUTING || task.getStatus() == TaskStatus.ASSIGNED)&& task != null;
    }

    /**
     * Interrupt a given Task
     * @param task  The given Task to interrupt
     * @effect task.deassingUnit()
     * @effect task.setPriority()
     *          |task.setPriority(task.getPriority() - 50)
     * @throws SchedulerException If the task cannot be interrupted, or if some error occurs during the interruption
     *          |if(!canBeInterrupted(task))
     *          |task.deAssignUnit()
     */
    public void interruptTask(Task task) throws SchedulerException{
        if (!canBeInterrupted(task)) throw new SchedulerException(SchedulerException.SCHEDULE_CONFLICT,"Task cannot be interrupted");

        try {
            task.deAssignUnit();

            task.setPriority(task.getPriority() - 50);

            if(task.getPriority() < -5000) {
                task.terminate();
            }
        } catch (TaskException e){
            throw new SchedulerException(SchedulerException.SCHEDULE_CONFLICT,"An error occurred during the interruption of the Task");
        }
    }

    /**
     * Check whether a free Task is available
     * @return True when a free Task could be found
     *          |result == getHighestFreeTask() != null
     */
    public boolean hasFreeTask(){
        return getHighestFreeTask() != null;
    }

}
