package hillbillies.scheduler.execution.components;

import be.kuleuven.cs.som.annotate.Value;

/**
 * All possible statuses a Tasks can be in
 * @version 1.0
 * @author Maxim Verbiest
 */
@Value
public enum TaskStatus {

    NOT_ASSIGNED,
    ASSIGNED,
    EXECUTING,
    INTERRUPTED,
    FINISHED;

}
