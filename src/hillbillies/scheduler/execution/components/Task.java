package hillbillies.scheduler.execution.components;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Raw;
import hillbillies.exceptions.TaskException;
import hillbillies.exceptions.UnitException;
import hillbillies.model.*;
import hillbillies.scheduler.memory.generics.VariableRegistry;
import hillbillies.scheduler.memory.statement.Statement;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

/**
 * A class of Tasks scheduled by a Scheduler, that can be executed by Units
 * @version 1.0
 * @author Maxim Verbiest
 */
public class Task {

    /**
     * The name of the task
     */
    private String name;

    /**
     * The priority of a task
     */
    private int priority;

    /**
     * Statement to execute when task is called
     */
    private Statement activity;

    /**
     * Cube which was selected when creating task
     */
    private int[] selectedCube;

    /**
     * The Unit that must execute this task
     */
    private Unit assignedUnit = null;

    /**
     * Linked list containing all the schedulers that currently contains this Task
     */
    private LinkedList<Scheduler> schedulers;

    /**
     * The status this Task has
     */
    private TaskStatus status;

    /**
     * Keeps track of liveliness of Task
     */
    private boolean isTerminated = false;

    /**
     * The statement of the task that was executed as last
     */
    private Statement latestExecutedStatement = null;

    private Task replacement = null;

    /**
     * Constructor
     * @param name  The name this Task will have
     * @param priority  The priority this Task will have
     * @param cube  The selected Cube coordinates this Task will have
     * @param activity  The activity this Task will have
     * @post list of Schedulers will be initialised
     *          |new.getSchedulers() == new LinkedList<Task>()
     * @effect setStatus()
     *          |setStatus(TaskStatus.NOT_ASSIGNED)
     * @effect setName()
     *          |setName(name)
     * @effect setPriority()
     *          |setPriority(priority)
     * @effect setActivity()
     *          |setActivity(activity)
     * @throws TaskException
     *          |setName()
     *          |setActivity()
     */
    public Task(String name, int priority,int[] cube,Statement activity) throws TaskException {
        setStatus(TaskStatus.NOT_ASSIGNED);
        setName(name);
        setPriority(priority);
        setActivity(activity);
        setSelectedCube(cube);
        schedulers = new LinkedList<>();
    }

    /**
     * Set the activity to a given activity
     * @param activity  The given activity
     * @post The activity will be set to the given activity
     *          |new.getActivity() == activity
     * @throws TaskException
     *          |if(!isValidActivity(activity))
     */
    public void setActivity(Statement activity) throws TaskException {
        if(!isValidActivity(activity)) throw new TaskException(TaskException.INVALID_ACTIVITY,"Activity given was invalid");
        this.activity = activity;
    }

    /**
     * Check whether the activity is a valid activity
     * @param activity  The activity to check
     * @return True when the activity is not null
     *          |result == activity != null
     */
    public static boolean isValidActivity(Statement activity) {
        return activity != null;
    }

    @Basic @Raw
    public Statement getActivity() {
        return activity;
    }

    /**
     * Set the selected cube to the cube that matches the given coordinates
     * @param selectedCube  The given coordinates
     * @post The selected cube will be set to the cube that matches the given coordinates
     *          |new.getSelectedCube() == World.getInstance().getCubeByKey(selectedCube[0],selectedCube[1],selectedCube[2])
     * @throws TaskException    When no cube matches with the given coordinates
     *          |if World.getInstance().getCubeByKey(selectedCube[0],selectedCube[1],selectedCube[2]) == null
     */
    public void setSelectedCube(int[] selectedCube) throws TaskException {
        if(selectedCube == null) return;

        if(World.getInstance().getCubeByKey(selectedCube[0],selectedCube[1],selectedCube[2]) == null)
            throw new TaskException(TaskException.NO_CUBE_AT_COORDINATE,"Selected coordinates matches no cube");

        this.selectedCube = selectedCube;
    }

    public int[] getSelectedCube() {
        return selectedCube;
    }

    @Basic @Raw
    public TaskStatus getStatus(){
        return status;
    }

    /**
     * Set the status of a task to a given status
     * @param newStatus The given status
     * @post The status will be changed to the given status
     *          |new.getStatus() == newStatus
     * @throws TaskException    When it is impossible to change to the given status from its current status
     *          |if(!canHaveAsStatus(newStatus))
     *
     */
    public void setStatus(TaskStatus newStatus) throws TaskException {

        if(!canHaveAsStatus(newStatus)) throw new TaskException(TaskException.INVALID_STATUS_MANIPULAITON,
                "Tried to change the status of a task to an invalid status given the its current status");
        status = newStatus;
    }

    /**
     * Check if valid status
     * @param newStatus Status to check
     * @return  newStatus != null
     */
    private boolean canHaveAsStatus(TaskStatus newStatus){
        return newStatus != null;
    }

    @Basic
    public String getName() {
        return name;
    }

    /**
     * Sets the name to a given name
     *
     * @param newName The given name
     * @post The name of this task will be set to the given name
     *          |new.getName() == newName
     * @throws TaskException When newName has null reference
     *          |if(newName == null)
     */
    public void setName(String newName) throws TaskException {
        if (newName == null) throw new TaskException(TaskException.NULL_VALUE_AS_PARAMETER, "A String with null reference was given as Name");

        name = newName;
    }

    @Basic
    @Raw
    public int getPriority() {
        return priority;
    }

    /**
     * Sets the priority to a given value
     *
     * @param newPriority The given value
     * @post The priority of will be updated to the given priority
     *          |new.getPriority() == newPriority
     */
    public void setPriority(int newPriority) {
       priority = newPriority;
    }

    @Basic
    @Raw
    public Unit getAssignedUnit() {
        return assignedUnit;
    }

    /**
     * Check whether this task can be assigned to a Unit
     * @param Assignee The Unit to check with
     * @return True when this task wasn't already assigned to another Unit and Assignee is no null reference
     * |result == (getStatus() == TaskStatus.NOT_ASSIGNED || Assignee == null)
     */
    public boolean canBeAssigned(Unit Assignee) {

        return getStatus() == TaskStatus.NOT_ASSIGNED && Assignee != null ;
    }

    /**
     * Assign this Task to a Unit that must perform it
     *
     * @param performingUnit The Unit that must perform it
     * @throws TaskException When the task is already assigned or performingUnit has a null reference
     *                       |if(!canBeAssigned())
     *                       When performingUnit has already another task assigned to it
     *                       |getAssigned().assignTask()
     *                       If it is impossible to change the status
     *                       |setStatus()
     * @post The task will be assigned to a Unit
     * |new.getAssignedUnit() == performingUnit
     * @effect getAssignedUnit().assignTask(this)
     * The relation must be updated at both sides
     */
    public void assignToUnit(Unit performingUnit) throws TaskException {
        if (!canBeAssigned(performingUnit))
            throw new TaskException(TaskException.INVALID_ASSIGNMENT,"Unable to assign this Task to the given Unit");

        //To avoid a loop, since getAssignedUnit().assignTask(this) will call this method again
        if (performingUnit == assignedUnit) return;



        //This Unit will execute the task
        assignedUnit = performingUnit;

        //Relation must be set in Unit's object
        try {
            getAssignedUnit().assignTask(this);

        } catch (UnitException e) {
            throw new TaskException(TaskException.INVALID_UNIT, "Unable to assign this Task to the given Unit");
        }

        //Change Task's status to assigned
        setStatus(TaskStatus.ASSIGNED);

    }

    /**
     * Remove the Unit which must perform this task
     *
     * @post The assigned Unit will be null again
     *          |new.getAssignedUnit() == null
     * @effect getAssignedUnit().removeTask()
     * @effect setStatus()
     * @effect setLatestStatementExecuted()
     *          |setLatestStatementExecuted(null)
     * @throws TaskException    When it is impossible to change task's status
     *          |setStatus(TaskStatus.NOT_ASSIGNED)
     *                          When the Task's current status differs from Assigned,Finished or Executing
     *          |if(!(getStatus() == TaskStatus.ASSIGNED || getStatus() == TaskStatus.EXECUTING || getStatus() == TaskStatus.FINISHED))
     *                          
     */
    public void deAssignUnit() throws TaskException {

        //To avoid a loop

        if (getAssignedUnit() == null) return;

        Unit assigned = getAssignedUnit();
        assignedUnit = null;

        //Relation must be deleted in Unit's object as well, to keep it consistent
        try {
            assigned.removeTask();
        } catch (UnitException e){
            //Do nothing, will be thrown anyway by setStatus
        }

        //set status to not_assigned or finished according to current status
        if (getStatus() == TaskStatus.ASSIGNED || getStatus() == TaskStatus.EXECUTING) {
            setStatus(TaskStatus.NOT_ASSIGNED);
            //Next time this task will be executed, start again from the beginning
            setLatestStatementExecuted(null);
        } else if (getStatus() == TaskStatus.FINISHED){
            setStatus(TaskStatus.FINISHED);
        } else throw new TaskException(TaskException.DEASSIGNMENT_ERROR,"Tried to de-assign a task from a Unit," +
                "but this task's status was different from what was expected");

    }

    @Basic @Raw
    public LinkedList<Scheduler> getSchedulers(){
        return schedulers;
    }

    /**
     * Converts the list of Schedulers to a Set
     * @return a Set containing all the schedulers
     */
    public Set<Scheduler> schedulersToSet(){
        HashSet<Scheduler> result = new HashSet<>();
        for(Scheduler scheduler: getSchedulers()){
            result.add(scheduler);
        }

        return result;

    }

    /**
     * Add a Scheduler to the list of schedulers that have scheduled this Task
     * @param newScheduler  The given Scheduler to add
     * @post    The Scheduler will be added to the list of Schedulers, if this Scheduler is not already in that list and has no null reference
     *          |if(!getSchedulers().contains(newScheduler) && newScheduler != null)
     *          |then getSchedulers().add(newScheduler)
     * @effect  newScheduler.addTask()  To keep the relationship up to date at both sides
     *          |if(!getSchedulers().contains(newScheduler) && newScheduler != null)
     *          |then newScheduler.addTask(this)
     */
    public void addScheduler(Scheduler newScheduler) {

        //To get out of the loop + null check
        if(schedulers.contains(newScheduler) || newScheduler == null) return;

        schedulers.add(newScheduler);
        //Relationship must be updated at two sides
        newScheduler.addTask(this);

    }

    /**
     * Remove a Scheduler from the list of Schedulers
     * @param scheduler The Scheduler to remove
     * @post    The Scheduler will be removed from the list, if the list of schedulers contains this Scheduler
     *          |if(getSchedulers().contains(scheduler)
     *          |then getSchedulers().remove(scheduler)
     * @effect  scheduler.removeTask()  To keep the relationship up to date at two sides
     *          |if(getSchedulers().contains(scheduler)
     *          |then scheduler.removeTask(this)
     */
    public void removeScheduler(Scheduler scheduler){
        if(!getSchedulers().contains(scheduler)) return;

        getSchedulers().remove(scheduler);
        //Relation must be updated at two sides
        scheduler.removeTask(this);

    }

    /**
     * Remove this task from all the Schedulers managing this tasks
     * @effect removeScheduler()
     *          |for(Scheduler sched: getSchedulers())
     *          | removeScheduler(sched)
     */
    public void removeSchedulers(){
        for(Scheduler sched: schedulers)
            removeScheduler(sched);
    }
    
    @Basic @Raw
    public Task getReplacement(){
        return replacement;
    }

    /**
     * Set the replacement of this Task
     * @param newTask   The given replacement
     * @post The replacement of this Task will be updated
     *          |new.getReplacement() == newTask
     */
    public void setReplacement(Task newTask){
        replacement = newTask;
    }

    /**
     * Destructor
     * @effect deAssignUnit()
     * @effect removeSchedulers()
     * @post The Task will be terminated
     *          |new.isTerminated() == true
     * @throws TaskException
     *          |deAssignUnit();
     */
    public void terminate() throws TaskException {
        //Unit has no longer this task assigned to it
        deAssignUnit();
        //Remove from Registry
        VariableRegistry.removeSpace(this);
        //Remove from schedulers
        removeSchedulers();
        //Finishing touch
        isTerminated = true;
    }

    @Basic @Raw
    public Statement  getLatestStatementExecuted(){
       return latestExecutedStatement;
    }

    /**
     * Set the latest executed statement to a given new statement
     * @param newStatement  The given new statement
     * @post The latest executed statement will be set to the new statement
     *          |new.getLatestStatementExecuted() == newStatement
     */
    public void setLatestStatementExecuted(Statement newStatement){
        latestExecutedStatement = newStatement;
    }

    /**
     * Check whether this Task is free. ie not assigned, executing or terminated
     * @return True when the Task is free
     *          |result == (getStatus() == TaskStatus.NOT_ASSIGNED)
     */
    public boolean isFree(){
        return getStatus() == TaskStatus.NOT_ASSIGNED;
    }

    /**
     * Replace the Task with its replacement
     * @effect setName()
     *          |if(isFree() || getStatus == TaskStatus.INTERRUPTED)
     *          | then setName(name)
     * @effect setActivity()
     *          |if(isFree() || getStatus == TaskStatus.INTERRUPTED)
     *          | then setActivity(activity)
     * @effect setLatestStatementExecuted()
     *          |if(isFree() || getStatus == TaskStatus.INTERRUPTED)
     *          | then setLatestStatementExecuted(null)
     * @effect setPriority()
     *          |if(isFree() || getStatus == TaskStatus.INTERRUPTED)
     *          | then setPriority(newPriority)
     * @effect setSelectedCube()
     *          |if(isFree() || getStatus == TaskStatus.INTERRUPTED)
     *          | then setSelectedCube(selectedCube)
     * @effect setReplacement()
     *          |if(isFree() || getStatus == TaskStatus.INTERRUPTED)
     *          | then setReplacement(null)
     *          |else setReplacement(replacement)
     * @effect setStatus()
     *          |if(!isFree() || getStatus == TaskStatus.INTERRUPTED)
     *          | then setStatus(TaskStatus.INTERRUPTED)
     * @post If this Task was first interrupted before the replacement, start executing after the replacement is finished
     *          |if(getStatus() == TaskStatus.INTERRUPTED)
     *          | then new.getStatus == TaskStatus.EXECUTING
     * @throws TaskException
     *          |if(getReplacement() == null)
     *          |setName(getReplacement().getName())
     *          |setActivity(getReplacement().getActivity())
     *          |setSelectedCube(getReplacement().getSelectedCube())
     */
    public void replaceTask(Task replacement) throws TaskException {

        if(replacement == null) throw new TaskException(TaskException.REPLACEMENT_CONFLICT,"No task to replace with found");

        if(isFree() || getStatus() == TaskStatus.INTERRUPTED){
            //Task must start from its beginning
            setLatestStatementExecuted(null);
            //Update all the fields of this Task
            setName(replacement.getName());
            setActivity(replacement.getActivity());
            setPriority(replacement.getPriority());
            setSelectedCube(replacement.getSelectedCube());
            //Check if this Task was first interrupted
            if(getStatus()==TaskStatus.INTERRUPTED) {
                //Replacement relation must be deleted now
                setReplacement(null);
                //Start Executing again
                status = TaskStatus.EXECUTING;
            }

        } else {
            //This Task is still executing at this time
            setStatus(TaskStatus.INTERRUPTED);
            //To keep track of this relation, will be used in the Executor to replace after gentle finish of activity
            setReplacement(replacement);
        }
    }

    /**
     * Wrapper for replacing a Task
     * @effect replaceTask()
     *          |if(getReplacement != null)
     *          | then replaceTask(getReplacement)
     * @throws TaskException
     *          |replaceTask(getReplacement())
     */
    public void replaceTask() throws TaskException {
        if(getReplacement() != null){
            replaceTask(getReplacement());
        }
    }

    @Basic @Raw
    public boolean isTerminated() {
        return isTerminated;
    }
}
