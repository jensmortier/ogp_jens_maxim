package hillbillies.scheduler.execution.factory;

import hillbillies.exceptions.TaskException;
import hillbillies.part3.programs.ITaskFactory;
import hillbillies.part3.programs.SourceLocation;
import hillbillies.scheduler.execution.components.Task;
import hillbillies.scheduler.memory.expression.Expression;
import hillbillies.scheduler.memory.expression.bool.EBoolean;
import hillbillies.scheduler.memory.expression.bool.operations.executors.base.EBooleanAND;
import hillbillies.scheduler.memory.expression.bool.operations.executors.base.EBooleanNOT;
import hillbillies.scheduler.memory.expression.bool.operations.executors.base.EBooleanOR;
import hillbillies.scheduler.memory.expression.bool.operations.executors.position.OIsPassable;
import hillbillies.scheduler.memory.expression.bool.operations.executors.position.OIsSolid;
import hillbillies.scheduler.memory.expression.bool.operations.executors.unit.OCarriesItem;
import hillbillies.scheduler.memory.expression.bool.operations.executors.unit.OIsAlive;
import hillbillies.scheduler.memory.expression.bool.operations.executors.unit.OIsEnemy;
import hillbillies.scheduler.memory.expression.bool.operations.executors.unit.OIsFriend;
import hillbillies.scheduler.memory.expression.model.interfaces.EPosition;
import hillbillies.scheduler.memory.expression.model.interfaces.EUnit;
import hillbillies.scheduler.memory.expression.model.position.*;
import hillbillies.scheduler.memory.expression.model.unit.UAny;
import hillbillies.scheduler.memory.expression.model.unit.UEnemy;
import hillbillies.scheduler.memory.expression.model.unit.UFriend;
import hillbillies.scheduler.memory.expression.model.unit.UThis;
import hillbillies.scheduler.memory.expression.variable.ERead;
import hillbillies.scheduler.memory.generics.Variable;
import hillbillies.scheduler.memory.statement.Statement;
import hillbillies.scheduler.memory.statement.io.SPrint;
import hillbillies.scheduler.memory.statement.language.*;
import hillbillies.scheduler.memory.statement.model.AAttack;
import hillbillies.scheduler.memory.statement.model.AFollow;
import hillbillies.scheduler.memory.statement.model.AMoveTo;
import hillbillies.scheduler.memory.statement.model.AWork;

import java.util.LinkedList;
import java.util.List;

/**
 * Class which represents a factory for task creation
 *
 * @version 1.0
 * @author Maxim Verbiest
 *          Jens Mortier
 */
public class TaskFactory implements ITaskFactory<Expression,Statement,Task> {
    /**
     * Create a list of tasks from the given arguments.
     *
     * @param name          The name of the task
     * @param priority      The initial priority of the task
     * @param activity      The activity of the task. Most likely this is a sequence
     *                      statement.
     * @param selectedCubes A list of cube coordinates (each represented as an array {x,
     *                      y, z}) that were selected by the player in the GUI.
     * @return A list of new task instances. One task instance should be created
     * for each selectedCube coordinate. If selectedCubes is empty and
     * the 'selected' expression does not occur in the activity, a list
     * with exactly one Task instance should be returned.
     */
    @Override
    public List<Task> createTasks(String name, int priority, Statement activity, List<int[]> selectedCubes) {
        List<Task> tasks = new LinkedList<>();
        System.out.println("Trying to create task");
        if(selectedCubes == null || selectedCubes.size() == 0) {
            try {
                tasks.add(new Task(name,priority, null,activity));
                return tasks;
            } catch (TaskException e) {
                return new LinkedList<>();
            }
        }
        for(int[] c : selectedCubes) {
            try {
                tasks.add(new Task(name,priority, c,activity));
            } catch (TaskException e) {
                return new LinkedList<>();
            }
        }
        return tasks;
    }

    /**
     * Create a statement that represents the assignment of a variable.
     *
     * @param variableName   The name of the assigned variable
     * @param value
     * @param sourceLocation
     */
    @Override
    public Statement createAssignment(String variableName, Expression value, SourceLocation sourceLocation) {
        return new SAssignment<Expression>(sourceLocation,new Variable<Expression>(variableName,null),value);
    }

    /**
     * Create a statement that represents a while loop.
     *
     * @param condition      The condition of the while loop
     * @param body           The body of the loop (most likely this is a sequence
     * @param sourceLocation
     */
    @Override
    public Statement createWhile(Expression condition, Statement body, SourceLocation sourceLocation) {
        return new SWhile((EBoolean)condition,body,sourceLocation);
    }

    /**
     * Create an if-then-else statement.
     *
     * @param condition      The condition of the if statement
     * @param ifBody         * The body of the if-part, which must be executed when the
     *                       condition evaluates to true
     * @param elseBody       The body of the else-part, which must be executed when the
     *                       condition evaluates to false. Can be null if no else clause is
     * @param sourceLocation
     */
    @Override
    public Statement createIf(Expression condition, Statement ifBody, Statement elseBody, SourceLocation sourceLocation) {
        return new SIf(ifBody,(EBoolean)condition,elseBody,sourceLocation);
    }

    /**
     * Create a break statement that immediately terminates the enclosing loop.
     *
     * @param sourceLocation
     * @note Students working alone may return null.
     */
    @Override
    public Statement createBreak(SourceLocation sourceLocation) {
        return new SBreak(sourceLocation);
    }

    /**
     * Create a print statement that prints the value obtained by evaluating the
     * given expression.
     *
     * @param value          The expression to evaluate and print
     * @param sourceLocation
     */
    @Override
    public Statement createPrint(Expression value, SourceLocation sourceLocation) {
        return new SPrint(sourceLocation,value);
    }

    /**
     * Create a sequence of statements.
     *
     * @param statements     The statements that must be executed in the given order.
     * @param sourceLocation
     */
    @Override
    public Statement createSequence(List<Statement> statements, SourceLocation sourceLocation) {
        return new CompositeStatement(sourceLocation,statements);
    }

    /**
     * Create a moveTo statement
     *
     * @param position       The position to which to move
     * @param sourceLocation
     */
    @Override
    public Statement createMoveTo(Expression position, SourceLocation sourceLocation) {
        if(position instanceof ERead) {
            return new AMoveTo((ERead<EPosition>) position,sourceLocation);
        }else {
            return new AMoveTo((EPosition)position,sourceLocation);
        }

    }

    /**
     * Create a work statement
     *
     * @param position       The position on which to work
     * @param sourceLocation
     */
    @Override
    public Statement createWork(Expression position, SourceLocation sourceLocation) {
        if(position instanceof ERead) {
            return new AWork((ERead<EPosition>) position,sourceLocation);
        }else {
            return new AWork((EPosition)position,sourceLocation);
        }
    }

    /**
     * Create a follow statement
     *
     * @param unit           The unit to follow
     * @param sourceLocation
     */
    @Override
    public Statement createFollow(Expression unit, SourceLocation sourceLocation) {
        if(unit instanceof ERead) {
            return new AFollow((ERead<EUnit>) unit,sourceLocation);
        }else {
            return new AFollow((EUnit)unit,sourceLocation);
        }
    }

    /**
     * Create an attack statement
     *
     * @param unit           The unit to attack
     * @param sourceLocation
     */
    @Override
    public Statement createAttack(Expression unit, SourceLocation sourceLocation) {
        if(unit instanceof ERead) {
            return new AAttack((ERead<EUnit>) unit,sourceLocation);
        }else {
            return new AAttack((EUnit)unit,sourceLocation);
        }
    }

    /**
     * Create an expression that evaluates to the current value of the given
     * variable.
     *
     * @param variableName   The name of the variable to read.
     * @param sourceLocation
     */
    @Override
    public Expression createReadVariable(String variableName, SourceLocation sourceLocation) {
        return new ERead<Expression>(variableName,sourceLocation);
    }

    /**
     * Create an expression that evaluates to true when the given position
     * evaluates to a solid position; false otherwise.
     *
     * @param position       The position expression
     * @param sourceLocation
     */
    @Override
    public Expression createIsSolid(Expression position, SourceLocation sourceLocation) {
        if(position instanceof EPosition) {
            return new OIsSolid((EPosition) position,sourceLocation);
        }else {
            return new OIsSolid((ERead<EPosition>) position,sourceLocation);
        }
    }

    /**
     * Create an expression that evaluates to true when the given position
     * evaluates to a passable position; false otherwise.
     *
     * @param position       The position expression
     * @param sourceLocation
     */
    @Override
    public Expression createIsPassable(Expression position, SourceLocation sourceLocation) {
        if(position instanceof EPosition) {
            return new OIsPassable((EPosition) position,sourceLocation);
        }else {
            return new OIsPassable((ERead<EPosition>) position,sourceLocation);
        }
    }

    /**
     * Create an expression that evaluates to true when the given unit evaluates
     * to a unit of the same faction; false otherwise.
     *
     * @param unit           The unit expression
     * @param sourceLocation
     */
    @Override
    public Expression createIsFriend(Expression unit, SourceLocation sourceLocation) {
        if(unit instanceof EUnit) {
            return new OIsFriend((EUnit) unit,sourceLocation);
        }else {
            return new OIsFriend((ERead<EUnit>) unit,sourceLocation);
        }

    }

    /**
     * Create an expression that evaluates to true when the given unit evaluates
     * to a unit of another faction; false otherwise.
     *
     * @param unit           The unit expression
     * @param sourceLocation
     */
    @Override
    public Expression createIsEnemy(Expression unit, SourceLocation sourceLocation) {
        if(unit instanceof EUnit) {
            return new OIsEnemy((EUnit) unit,sourceLocation);
        }else {
            return new OIsEnemy((ERead<EUnit>) unit,sourceLocation);
        }
    }

    /**
     * Create an expression that evaluates to true when the given unit evaluates
     * to a unit that is alive; false otherwise.
     *
     * @param unit           The unit expression
     * @param sourceLocation
     */
    @Override
    public Expression createIsAlive(Expression unit, SourceLocation sourceLocation) {
        if(unit instanceof EUnit) {
            return new OIsAlive((EUnit) unit,sourceLocation);
        }else {
            return new OIsAlive((ERead<EUnit>) unit,sourceLocation);
        }
    }

    /**
     * Create an expression that evaluates to true when the given unit evaluates
     * to a unit that carries an item; false otherwise.
     *
     * @param unit           The unit expression
     * @param sourceLocation
     */
    @Override
    public Expression createCarriesItem(Expression unit, SourceLocation sourceLocation) {
        if(unit instanceof EUnit) {
            return new OCarriesItem((EUnit) unit,sourceLocation);
        }else {
            return new OCarriesItem((ERead<EUnit>) unit,sourceLocation);
        }
    }

    /**
     * Create an expression that evaluates to true when the given expression
     * evaluates to false, and vice versa.
     *
     * @param expression
     * @param sourceLocation
     */
    @Override
    public Expression createNot(Expression expression, SourceLocation sourceLocation) {
        if(expression instanceof EBoolean) {
            return new EBooleanNOT((EBoolean) expression,sourceLocation);
        }else {
            return new EBooleanNOT((ERead<EBoolean>) expression,sourceLocation);
        }
    }

    /**
     * Create an expression that evaluates to true when both the left and right
     * expression evaluate to true; false otherwise.
     *
     * @param left
     * @param right
     * @param sourceLocation
     * @note short-circuit: the right expression does not need to be evaluated
     * when the left expression evaluates to false.
     */
    @Override
    public Expression createAnd(Expression left, Expression right, SourceLocation sourceLocation) {
        if(left.getClass() == right.getClass()) {
            if(left instanceof EBoolean) {
                return new EBooleanAND((EBoolean) left,(EBoolean) right,sourceLocation);
            }else {
                return new EBooleanAND((ERead<EBoolean>) left,(ERead<EBoolean>) right,sourceLocation);
            }
        }else {
            if(left instanceof EBoolean) {
                return new EBooleanAND((ERead<EBoolean>) right,(EBoolean) left,sourceLocation);
            }else {
                return new EBooleanAND((ERead<EBoolean>) left,(EBoolean) right,sourceLocation);
            }
        }

    }

    /**
     * Create an expression that evaluates to false only when the left and right
     * expression evaluate to false; true otherwise.
     *
     * @param left
     * @param right
     * @param sourceLocation
     * @note short-circuit: the right expression does not need to be evaluated
     * when the left expression evaluates to true.
     */
    @Override
    public Expression createOr(Expression left, Expression right, SourceLocation sourceLocation) {
        if(left.getClass() == right.getClass()) {
            if(left instanceof EBoolean) {
                return new EBooleanOR((EBoolean) left,(EBoolean) right,sourceLocation);
            }else {
                return new EBooleanOR((ERead<EBoolean>) left,(ERead<EBoolean>) right,sourceLocation);
            }
        }else {
            if(left instanceof EBoolean) {
                return new EBooleanOR((ERead<EBoolean>) right,(EBoolean) left,sourceLocation);
            }else {
                return new EBooleanOR((ERead<EBoolean>) left,(EBoolean) right,sourceLocation);
            }
        }
    }

    /**
     * Create an expression that evaluates to the current position of the unit
     * that is executing the task.
     *
     * @param sourceLocation
     */
    @Override
    public Expression createHerePosition(SourceLocation sourceLocation) {
        return new PHere(sourceLocation);
    }

    /**
     * Create an expression that evaluates to the position of a log.
     *
     * @param sourceLocation
     * @note for groups of two students, this needs to be the log closest to the
     * unit that is executing the task.
     */
    @Override
    public Expression createLogPosition(SourceLocation sourceLocation) {
        return new PLog(sourceLocation);
    }

    /**
     * Create an expression that evaluates to the position of a boulder.
     *
     * @param sourceLocation
     * @note for groups of two students, this needs to be the boulder closest to
     * the unit that is executing the task.
     */
    @Override
    public Expression createBoulderPosition(SourceLocation sourceLocation) {
        return new PBoulder(sourceLocation);
    }

    /**
     * Create an expression that evaluates to the position of a workshop.
     *
     * @param sourceLocation
     * @note for groups of two students, this needs to be the workshop closest
     * to the unit that is executing the task.
     */
    @Override
    public Expression createWorkshopPosition(SourceLocation sourceLocation) {
        return new PWorkshop(sourceLocation);
    }

    /**
     * Create an expression that evaluates to the position selected by the user
     * in the GUI.
     *
     * @param sourceLocation
     * @note Students working alone may return null.
     */
    @Override
    public Expression createSelectedPosition(SourceLocation sourceLocation) {
        return new PSelected(sourceLocation);
    }

    /**
     * Create an expression that evaluates to a position next to the given
     * position.
     *
     * @param position
     * @param sourceLocation
     */
    @Override
    public Expression createNextToPosition(Expression position, SourceLocation sourceLocation) {
        if(position instanceof EPosition) {
            return new PNextTo((EPosition) position,sourceLocation);
        }else {
            return new PNextTo((ERead<EPosition>) position,sourceLocation);
        }
    }

    /**
     * Create an expression that evaluates to the position of the given unit.
     *
     * @param unit
     * @param sourceLocation
     */
    @Override
    public Expression createPositionOf(Expression unit, SourceLocation sourceLocation) {
        if(unit instanceof EUnit) {
            return new PPostionOf((EUnit) unit,sourceLocation);
        }else {
            return new PPostionOf((ERead<EUnit>) unit,sourceLocation);
        }
    }

    /**
     * Create an expression that evaluates to a static position with a given
     * coordinate.
     *
     * @param x
     * @param y
     * @param z
     * @param sourceLocation
     */
    @Override
    public Expression createLiteralPosition(int x, int y, int z, SourceLocation sourceLocation) {
        return new PLiteral(x,y,z,sourceLocation);
    }

    /**
     * Create an expression that evaluates to the unit that is currently
     * executing the task.
     *
     * @param sourceLocation
     */
    @Override
    public Expression createThis(SourceLocation sourceLocation) {
        return new UThis(sourceLocation);
    }

    /**
     * Create an expression that evaluates to a unit that is part of the same
     * faction as the unit currently executing the task.
     *
     * @param sourceLocation
     */
    @Override
    public Expression createFriend(SourceLocation sourceLocation) {
        return new UFriend(sourceLocation);
    }

    /**
     * Create an expression that evaluates to a unit that is not part of the
     * same faction as the unit currently executing the task.
     *
     * @param sourceLocation
     */
    @Override
    public Expression createEnemy(SourceLocation sourceLocation) {
        return new UEnemy(sourceLocation);
    }

    /**
     * Create an expression that evaluates to any unit (other than this).
     *
     * @param sourceLocation
     */
    @Override
    public Expression createAny(SourceLocation sourceLocation) {
        return new UAny(sourceLocation);
    }

    /**
     * Create an expression that evaluates to true.
     *
     * @param sourceLocation
     */
    @Override
    public Expression createTrue(SourceLocation sourceLocation) {
        return new EBoolean(sourceLocation,true);
    }

    /**
     * Create an expression that evaluates to false.
     *
     * @param sourceLocation
     */
    @Override
    public Expression createFalse(SourceLocation sourceLocation) {
        return new EBoolean(sourceLocation,false);
    }
}
