package hillbillies.scheduler.memory.expression.variable;

import hillbillies.model.Unit;
import hillbillies.part3.programs.SourceLocation;
import hillbillies.scheduler.memory.exceptions.DynamicExpressionException;
import hillbillies.scheduler.memory.expression.Expression;
import hillbillies.scheduler.memory.generics.Variable;
import hillbillies.scheduler.memory.generics.VariableRegistry;
import hillbillies.scheduler.memory.interfaces.IDynamicExpression;
import hillbillies.scheduler.memory.statement.Statement;
import hillbillies.scheduler.memory.statement.exceptions.StatementExecutionException;

/**
 * Class which represents a SRead action
 *
 * @version 1.0
 * @author Jens Mortier
 */
public class ERead<T extends Expression> extends Expression implements IDynamicExpression<Unit> {

    private String variableName;
    private T value;

    public ERead(String variable, SourceLocation loc) {
        super(loc);
        this.variableName = variable;
    }

    @Override
    public String toString(Unit executor) {
        return value.toString(executor) + " @ " + variableName;
    }

    /**
     * Executor of statement
     *
     * @param unit Unit which is executing the statement
     */
    @Override
    public void execute(Unit unit) throws DynamicExpressionException {
        value = (T)VariableRegistry.getInstance(unit.getTask()).getVariable(variableName).getValue();
    }

    public T asExpression(Unit unit) throws DynamicExpressionException {
        if(value == null) execute(unit);
        return value;
    }

    public String getVariableName() {
        return variableName;
    }

    @Override
    public String toString() {
        return value == null ? "null" : value.toString();
    }
}
