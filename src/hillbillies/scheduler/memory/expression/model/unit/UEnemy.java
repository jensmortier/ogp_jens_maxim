package hillbillies.scheduler.memory.expression.model.unit;

import hillbillies.exceptions.WorldException;
import hillbillies.gameworld.GameObject;
import hillbillies.model.Type;
import hillbillies.model.Unit;
import hillbillies.model.World;
import hillbillies.part3.programs.SourceLocation;
import hillbillies.scheduler.memory.exceptions.DynamicExpressionException;
import hillbillies.scheduler.memory.expression.exceptions.InterruptTrapException;
import hillbillies.scheduler.memory.expression.model.interfaces.EUnit;

/**
 * Class which represents the enemy, closest the the executing unit
 *
 * @version 1.0
 * @author Jens Mortier
 */
public class UEnemy extends EUnit {
    public UEnemy(SourceLocation loc) {
        super(loc);
    }

    @Override
    public void execute(Unit executor) throws DynamicExpressionException {
        try {
            GameObject object = World.getInstance().filterByObject(Unit.class, Type.ENEMY,executor);
            if(object == null) throw new InterruptTrapException();
            unit = (Unit) object;
        } catch (WorldException e) {
            throw new DynamicExpressionException("Couldn't load unit",e);
        }
    }
}
