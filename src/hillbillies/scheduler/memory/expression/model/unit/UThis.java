package hillbillies.scheduler.memory.expression.model.unit;

import hillbillies.model.Unit;
import hillbillies.part3.programs.SourceLocation;
import hillbillies.scheduler.memory.expression.model.interfaces.EUnit;

/**
 * Class which represents expression representing the executing unit
 */
public class UThis extends EUnit {

    public UThis(SourceLocation loc) {
        super(loc);
    }

    @Override
    public void execute(Unit executor) {
        unit = executor;
    }
}
