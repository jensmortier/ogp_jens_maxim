package hillbillies.scheduler.memory.expression.model.position;

import hillbillies.exceptions.PositionException;
import hillbillies.gameworld.Position;
import hillbillies.model.Unit;
import hillbillies.part3.programs.SourceLocation;
import hillbillies.scheduler.memory.exceptions.DynamicExpressionException;
import hillbillies.scheduler.memory.expression.model.interfaces.EPosition;
import hillbillies.util.NumberConverter;

/**
 * Class which represents the position of the currently selected cube
 *
 * @version 1.0
 * @author Jens Mortier
 */
public class PSelected extends EPosition {

    public PSelected(SourceLocation loc) {
        super(loc);
    }

    @Override
    public void execute(Unit executor) throws DynamicExpressionException {
        try {
            Integer[] vector = NumberConverter.basicVectorToInteger(executor.getTask().getSelectedCube());
            position = new Position<Integer>(vector[0],vector[1],vector[2]);
        } catch (PositionException e) {
            throw new DynamicExpressionException("Couldn't load position",e);
        }
    }
}
