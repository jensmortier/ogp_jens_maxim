package hillbillies.scheduler.memory.expression.model.position;

import hillbillies.exceptions.WorldException;
import hillbillies.gameworld.SceneryType;
import hillbillies.model.Unit;
import hillbillies.model.World;
import hillbillies.part3.programs.SourceLocation;
import hillbillies.scheduler.memory.exceptions.DynamicExpressionException;
import hillbillies.scheduler.memory.expression.model.interfaces.EPosition;

/**
 * Class which represents the position of the nearest workshop
 *
 * @version 1.0
 * @author Jens Mortier
 */
public class PWorkshop extends EPosition {

    public PWorkshop(SourceLocation loc) {
        super(loc);
    }

    @Override
    public void execute(Unit executor) throws DynamicExpressionException {
        try {
            position = World.getInstance().filterByCube(SceneryType.WORKSHOP,false,executor).getPosition();
        } catch (WorldException e) {
            throw new DynamicExpressionException("Couldn't load position",e);
        }
    }
}
