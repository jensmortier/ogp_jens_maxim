package hillbillies.scheduler.memory.expression.model.position;

import hillbillies.exceptions.WorldException;
import hillbillies.gameworld.GameObject;
import hillbillies.model.Boulder;
import hillbillies.model.Unit;
import hillbillies.model.World;
import hillbillies.part3.programs.SourceLocation;
import hillbillies.scheduler.memory.exceptions.DynamicExpressionException;
import hillbillies.scheduler.memory.expression.exceptions.InterruptTrapException;
import hillbillies.scheduler.memory.expression.model.interfaces.EPosition;

/**
 * Class which represents the position of the nearest boulder
 *
 * @version 1.0
 * @author Jens Mortier
 */
public class PBoulder extends EPosition {

    public PBoulder(SourceLocation loc) {
        super(loc);
    }

    @Override
    public void execute(Unit executor) throws DynamicExpressionException {
        try {
            GameObject object = World.getInstance().filterByObject(Boulder.class,null,executor);
            if(object == null) throw new InterruptTrapException();
            position = object.getCurrentCube().getPosition();
        } catch (WorldException e) {
            throw new DynamicExpressionException("Couldn't load position",e);
        }
    }
}
