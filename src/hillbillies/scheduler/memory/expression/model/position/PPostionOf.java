package hillbillies.scheduler.memory.expression.model.position;

import hillbillies.model.Unit;
import hillbillies.part3.programs.SourceLocation;
import hillbillies.scheduler.memory.exceptions.DynamicExpressionException;
import hillbillies.scheduler.memory.expression.Expression;
import hillbillies.scheduler.memory.expression.model.interfaces.EPosition;
import hillbillies.scheduler.memory.expression.model.interfaces.EUnit;
import hillbillies.scheduler.memory.expression.variable.ERead;

/**
 * Class which represents the position of a unit given
 *
 * @version 1.0
 * @author Jens Mortier
 */
public class PPostionOf extends EPosition {

    private Expression unit = null;

    public PPostionOf(EUnit unit,SourceLocation loc) {
        super(loc);
        this.unit = unit;
    }

    public PPostionOf(ERead<EUnit> unit,SourceLocation loc) {
        super(loc);
        this.unit = unit;
    }

    @Override
    public void execute(Unit executor) throws DynamicExpressionException {
        position = (unit instanceof EUnit ? (EUnit)unit : ((ERead<EUnit>) unit).asExpression(executor)).asUnit(executor).getCurrentCube().getPosition();
    }

    public Expression getUnit() {
        return unit;
    }
}
