package hillbillies.scheduler.memory.expression.model.position;

import hillbillies.model.Unit;
import hillbillies.part3.programs.SourceLocation;
import hillbillies.scheduler.memory.expression.model.interfaces.EPosition;

/**
 * Class which represents the position of the cube executing
 *
 * @version 1.0
 * @author Jens Mortier
 */
public class PHere extends EPosition {

    public PHere(SourceLocation loc) {
        super(loc);
    }

    @Override
    public void execute(Unit executor) {
        position = executor.getCurrentCube().getPosition();
    }
}
