package hillbillies.scheduler.memory.expression.model.position;

import hillbillies.exceptions.PositionException;
import hillbillies.gameworld.Position;
import hillbillies.model.Unit;
import hillbillies.part3.programs.SourceLocation;
import hillbillies.scheduler.memory.exceptions.DynamicExpressionException;
import hillbillies.scheduler.memory.expression.model.interfaces.EPosition;

/**
 * Class which represents a literal position
 *
 * @version 1.0
 * @author Jens Mortier
 */
public class PLiteral extends EPosition {
    protected int x;
    protected int y;
    protected int z;

    public PLiteral(int x,int y, int z,SourceLocation loc) {
        super(loc);
        this.x = x;
        this.y = y;
        this.z = z;
    }

    @Override
    public void execute(Unit executor) throws DynamicExpressionException{
        try {
            position = new Position<Integer>(x,y,z);
        } catch (PositionException e) {
            throw new DynamicExpressionException(e.getMessage(),e);
        }
    }
}
