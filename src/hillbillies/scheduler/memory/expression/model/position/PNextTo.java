package hillbillies.scheduler.memory.expression.model.position;

import hillbillies.exceptions.WorldException;
import hillbillies.gameworld.SceneryType;
import hillbillies.model.Unit;
import hillbillies.model.World;
import hillbillies.part3.programs.SourceLocation;
import hillbillies.scheduler.memory.exceptions.DynamicExpressionException;
import hillbillies.scheduler.memory.expression.Expression;
import hillbillies.scheduler.memory.expression.model.interfaces.EPosition;
import hillbillies.scheduler.memory.expression.variable.ERead;

import java.util.HashSet;

/**
 * Class which represents the position next to another position
 *
 * @version 1.0
 * @author Jens Mortier
 */
public class PNextTo extends EPosition {
    private Expression bench_position;

    public PNextTo(EPosition p,SourceLocation loc) {
        super(loc);
        bench_position = p;
    }

    public PNextTo(ERead<EPosition> p,SourceLocation loc) {
        super(loc);
        bench_position = p;
    }

    @Override
    public void execute(Unit executor) throws DynamicExpressionException {
        HashSet<SceneryType> types_allowed = new HashSet<>();
        types_allowed.add(SceneryType.AIR);
        types_allowed.add(SceneryType.WORKSHOP);
        try {
            position = World.getInstance().filterByCube(types_allowed,true,(bench_position instanceof EPosition ? (EPosition)bench_position : ((ERead<EPosition>)bench_position).asExpression(executor)).asPosition(executor)).getPosition();
        } catch (WorldException e) {
            throw new DynamicExpressionException("Couldn't load position",e);
        }
    }

    public Expression getBench_position() {
        return bench_position;
    }
}
