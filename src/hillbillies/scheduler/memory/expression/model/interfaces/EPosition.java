package hillbillies.scheduler.memory.expression.model.interfaces;

import hillbillies.gameworld.Position;
import hillbillies.model.Unit;
import hillbillies.part3.programs.SourceLocation;
import hillbillies.scheduler.memory.exceptions.DynamicExpressionException;
import hillbillies.scheduler.memory.expression.Expression;
import hillbillies.scheduler.memory.interfaces.IDynamicExpression;

/**
 * Representing a position expression
 *
 * @author Jens Mortier
 * @version 1.0
 */
public abstract class EPosition extends Expression implements IDynamicExpression<Unit> {

    protected Position<Integer> position;

    public EPosition(SourceLocation loc) {
        super(loc);
    }

    public Position<Integer> asPosition(Unit executor) throws DynamicExpressionException {
        execute(executor);

        return position;
    }

    @Override
    public String toString(Unit executor) {
        try {
            execute(executor);
        } catch (DynamicExpressionException e) {
            //Do nothing
        }
        return position == null ? "null" : position.toString();
    }
}
