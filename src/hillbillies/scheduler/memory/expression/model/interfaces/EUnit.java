package hillbillies.scheduler.memory.expression.model.interfaces;

import hillbillies.model.Unit;
import hillbillies.part3.programs.SourceLocation;
import hillbillies.scheduler.memory.exceptions.DynamicExpressionException;
import hillbillies.scheduler.memory.expression.Expression;
import hillbillies.scheduler.memory.interfaces.IDynamicExpression;

/**
 * Created by Jens on 26/04/2016.
 */
abstract public class EUnit extends Expression implements IDynamicExpression<Unit> {
    protected Unit unit;

    public EUnit(SourceLocation loc) {
        super(loc);
    }

    @Override
    public String toString(Unit executor) {
        try {
            return "Unit " + asUnit(executor).getName();
        } catch (DynamicExpressionException e) {
            return "Unit null";
        }
    }

    public Unit asUnit(Unit executor) throws DynamicExpressionException {
        //First execute it
        execute(executor);
        return unit;
    }
}
