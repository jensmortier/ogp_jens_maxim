package hillbillies.scheduler.memory.expression;

import hillbillies.part3.programs.SourceLocation;
import hillbillies.scheduler.memory.interfaces.IPrintable;

/**
 * Super class for all expressions
 *
 * @version 1.0
 * @author Jens Mortier
 */
public abstract class Expression implements IPrintable {

    /**
     * Location of expression in source
     */
    private SourceLocation sourceLocation;

    public Expression(SourceLocation location) {
        sourceLocation = location;
    }
}
