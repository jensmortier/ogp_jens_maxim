package hillbillies.scheduler.memory.expression.bool;


import hillbillies.model.Unit;
import hillbillies.part3.programs.SourceLocation;
import hillbillies.scheduler.memory.exceptions.DynamicExpressionException;
import hillbillies.scheduler.memory.expression.Expression;

import java.io.OutputStream;
import java.io.PrintStream;

/**
 * Class which represents a boolean as an expression
 *
 * @author Jens Mortier
 * @version 1.0
 */
public class EBoolean extends Expression {

    protected boolean aBoolean;

    @Override
    public String toString(Unit executor) {
        return Boolean.toString(aBoolean);
    }

    public EBoolean(SourceLocation loc) {
        super(loc);
    }

    public EBoolean(SourceLocation loc,boolean init) {
        super(loc);
        aBoolean = init;
    }

    @Override
    public void print(Unit Executor,PrintStream stream) {
        stream.println(aBoolean ? "true" : false);
    }

    public boolean asBoolean(Unit unit) throws DynamicExpressionException {
        return aBoolean;
    }

    public void setBoolean(boolean newBoolean) {
        aBoolean = newBoolean;
    }
}
