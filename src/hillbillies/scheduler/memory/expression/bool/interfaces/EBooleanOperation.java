package hillbillies.scheduler.memory.expression.bool.interfaces;

import hillbillies.model.Unit;
import hillbillies.part3.programs.SourceLocation;
import hillbillies.scheduler.memory.exceptions.DynamicExpressionException;
import hillbillies.scheduler.memory.expression.Expression;
import hillbillies.scheduler.memory.expression.bool.EBoolean;
import hillbillies.scheduler.memory.interfaces.IDynamicExpression;

/**
 * Represents a boolean operation. T is the type of operand
 */
public abstract class EBooleanOperation<T> extends EBoolean implements IDynamicExpression<Unit> {

    public EBooleanOperation(SourceLocation loc) {
        super(loc);
    }

    @Override
    public boolean asBoolean(Unit unit) throws DynamicExpressionException {
        System.out.println("Recalc boolean operation");
        //Execute operation first
        execute(unit);

        //Return value
        return super.asBoolean(unit);
    }
}
