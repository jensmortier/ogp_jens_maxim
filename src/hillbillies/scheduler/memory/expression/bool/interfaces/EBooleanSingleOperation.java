package hillbillies.scheduler.memory.expression.bool.interfaces;

import hillbillies.part3.programs.SourceLocation;
import hillbillies.scheduler.memory.expression.Expression;
import hillbillies.scheduler.memory.expression.variable.ERead;

/**
 * Class which represents a boolean operation with only 1 operand
 *
 * @version 1.0
 * @author Jens Mortier
 */
abstract public class EBooleanSingleOperation<T extends Expression> extends EBooleanOperation<T> {

    protected Expression operand;

    public EBooleanSingleOperation(SourceLocation loc) {
        super(loc);
    }

    public abstract void setOperand(T operand);

    public abstract void setOperand(ERead<T> operand);

    public Expression getOperand() {
        return operand;
    }
}
