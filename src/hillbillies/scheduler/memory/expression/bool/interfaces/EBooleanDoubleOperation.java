package hillbillies.scheduler.memory.expression.bool.interfaces;

import hillbillies.part3.programs.SourceLocation;
import hillbillies.scheduler.memory.expression.Expression;
import hillbillies.scheduler.memory.expression.variable.ERead;

import java.util.ArrayList;

/**
 * Class which represents a double operation with two operands
 *
 * @version 1.0
 * @author Jens Mortier
 */
abstract public class EBooleanDoubleOperation<T extends Expression> extends EBooleanOperation<T> {

    protected Expression operand1;
    protected Expression operand2;

    public EBooleanDoubleOperation(SourceLocation loc){
        super(loc);
    }

    public abstract void setOperands(T operand1,T operand2);

    public abstract void setOperands(ERead<T> operand1,ERead<T> operand2);

    public abstract void setOperands(ERead<T> operand1, T operand2);

    public Expression getOperand1() {
        return operand1;
    }

    public Expression getOperand2() {
        return operand2;
    }
}
