package hillbillies.scheduler.memory.expression.bool.operations.executors.position;

import hillbillies.gameworld.Position;
import hillbillies.model.Unit;
import hillbillies.model.World;
import hillbillies.part3.programs.SourceLocation;
import hillbillies.scheduler.memory.exceptions.DynamicExpressionException;
import hillbillies.scheduler.memory.expression.bool.operations.interfaces.EPositionOperation;
import hillbillies.scheduler.memory.expression.model.interfaces.EPosition;
import hillbillies.scheduler.memory.expression.variable.ERead;

/**
 * Class which checks if the cube at a certain position is solid
 *
 * @version 1.0
 * @author Jens Mortier
 */
public class OIsSolid extends EPositionOperation {

    public OIsSolid(EPosition position,SourceLocation location) {
        super(location);
        setOperand(position);
    }


    public OIsSolid(ERead<EPosition> operand,SourceLocation loc) {
        super(loc);
        setOperand(operand);
    }

    @Override
    public void execute(Unit executor) throws DynamicExpressionException {
        Position<Integer> position = (operand instanceof EPosition ? (EPosition) operand : ((ERead<EPosition>) operand).asExpression(executor)).asPosition(executor);
        aBoolean = World.getInstance().getCubeByKey(position.getPosition_x(),position.getPosition_y(),position.getPosition_z()).getSceneryType().isSolid();
    }
}
