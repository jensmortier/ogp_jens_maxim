package hillbillies.scheduler.memory.expression.bool.operations.executors.unit;

import hillbillies.model.Unit;
import hillbillies.part3.programs.SourceLocation;
import hillbillies.scheduler.memory.exceptions.DynamicExpressionException;
import hillbillies.scheduler.memory.expression.bool.operations.interfaces.EUnitOperation;
import hillbillies.scheduler.memory.expression.model.interfaces.EUnit;
import hillbillies.scheduler.memory.expression.variable.ERead;

/**
 * Class which represents the operation which checks if the unit given carries an item
 *
 * @version 1.0
 * @author Jens Mortier
 */
public class OCarriesItem extends EUnitOperation {

    public OCarriesItem(EUnit operand,SourceLocation loc) {
        super(loc);
        setOperand(operand);
    }

    public OCarriesItem(ERead<EUnit> operand,SourceLocation loc) {
        super(loc);
        this.operand = operand;
    }


    @Override
    public void execute(Unit executor) throws DynamicExpressionException {
        aBoolean = (operand instanceof EUnit ? (EUnit) operand : ((ERead<EUnit>) operand).asExpression(executor)).asUnit(executor).isCarryingBoulder() || (operand instanceof EUnit ? (EUnit) operand : ((ERead<EUnit>) operand).asExpression(executor)).asUnit(executor).isCarryingLog();
    }
}
