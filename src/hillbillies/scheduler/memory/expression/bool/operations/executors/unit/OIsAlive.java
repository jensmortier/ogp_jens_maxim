package hillbillies.scheduler.memory.expression.bool.operations.executors.unit;

import hillbillies.model.Unit;
import hillbillies.part3.programs.SourceLocation;
import hillbillies.scheduler.memory.exceptions.DynamicExpressionException;
import hillbillies.scheduler.memory.expression.bool.operations.interfaces.EUnitOperation;
import hillbillies.scheduler.memory.expression.model.interfaces.EUnit;
import hillbillies.scheduler.memory.expression.variable.ERead;

/**
 * Class which represents the operation of checking whether the operand unit is alive
 *
 * @version 1.0
 * @author Jens Mortier
 */
public class OIsAlive extends EUnitOperation {

    public OIsAlive(EUnit operand,SourceLocation loc) {
        super(loc);
        setOperand(operand);
    }

    public OIsAlive(ERead<EUnit> operand,SourceLocation loc) {
        super(loc);
        this.operand = operand;
    }

    @Override
    public void execute(Unit executor) throws DynamicExpressionException {
        aBoolean = !(operand instanceof EUnit ? (EUnit) operand : ((ERead<EUnit>) operand).asExpression(executor)).asUnit(executor).isTerminated();
    }
}
