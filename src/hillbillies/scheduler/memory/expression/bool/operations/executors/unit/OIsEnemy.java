package hillbillies.scheduler.memory.expression.bool.operations.executors.unit;

import hillbillies.model.Unit;
import hillbillies.part3.programs.SourceLocation;
import hillbillies.scheduler.memory.exceptions.DynamicExpressionException;
import hillbillies.scheduler.memory.expression.bool.operations.interfaces.EUnitOperation;
import hillbillies.scheduler.memory.expression.model.interfaces.EUnit;
import hillbillies.scheduler.memory.expression.variable.ERead;

/**
 * Class which represents the operation of checking the enemy relation between two units
 *
 * @version 1.0
 * @author Jens Mortier
 */
public class OIsEnemy extends EUnitOperation {

    public OIsEnemy(EUnit operand,SourceLocation loc) {
        super(loc);
        setOperand(operand);
    }

    public OIsEnemy(ERead<EUnit> o,SourceLocation loc) {
        super(loc);
        operand = o;
    }

    @Override
    public void execute(Unit executor) throws DynamicExpressionException {
        aBoolean = executor.getFaction() != (operand instanceof EUnit ? (EUnit) operand : ((ERead<EUnit>) operand).asExpression(executor)).asUnit(executor).getFaction();
    }
}
