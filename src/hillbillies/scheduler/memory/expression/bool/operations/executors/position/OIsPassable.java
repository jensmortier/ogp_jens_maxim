package hillbillies.scheduler.memory.expression.bool.operations.executors.position;

import hillbillies.gameworld.Position;
import hillbillies.model.Unit;
import hillbillies.model.World;
import hillbillies.part3.programs.SourceLocation;
import hillbillies.scheduler.memory.exceptions.DynamicExpressionException;
import hillbillies.scheduler.memory.expression.bool.operations.interfaces.EPositionOperation;
import hillbillies.scheduler.memory.expression.model.interfaces.EPosition;
import hillbillies.scheduler.memory.expression.variable.ERead;

/**
 * Class which represents the operation of checking whether a certain cube is passable
 *
 * @version 1.0
 * @author Jens Mortier
 */
public class OIsPassable extends EPositionOperation {

    public OIsPassable(EPosition operand,SourceLocation loc) {
        super(loc);
        setOperand(operand);
    }

    public OIsPassable(ERead<EPosition> operand,SourceLocation loc) {
        super(loc);
        setOperand(operand);
    }

    @Override
    public void execute(Unit executor) throws DynamicExpressionException {
        Position<Integer> position = (operand instanceof EPosition ? (EPosition) operand : ((ERead<EPosition>) operand).asExpression(executor)).asPosition(executor);
        aBoolean = !World.getInstance().getCubeByKey(position.getPosition_x(),position.getPosition_y(),position.getPosition_z()).getSceneryType().isSolid();
    }
}
