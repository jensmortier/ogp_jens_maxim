package hillbillies.scheduler.memory.expression.bool.operations.executors.base;

import hillbillies.model.Unit;
import hillbillies.part3.programs.SourceLocation;
import hillbillies.scheduler.memory.exceptions.DynamicExpressionException;
import hillbillies.scheduler.memory.expression.Expression;
import hillbillies.scheduler.memory.expression.bool.EBoolean;
import hillbillies.scheduler.memory.expression.bool.interfaces.EBooleanDoubleOperation;
import hillbillies.scheduler.memory.expression.variable.ERead;

import java.util.ArrayList;

/**
 * Representing or operation on boolean
 *
 * @version 1.0
 * @author Jens Mortier
 */
public class EBooleanOR extends EBooleanDoubleOperation<EBoolean> {

    public EBooleanOR(EBoolean o1, EBoolean o2, SourceLocation loc) {

        super(loc);
        setOperands(o1,o2);
    }

    public EBooleanOR(ERead<EBoolean> o1,ERead<EBoolean> o2,SourceLocation loc) {
        super(loc);
        setOperands(o1,o2);
    }

    public EBooleanOR(ERead<EBoolean> o1, EBoolean o2, SourceLocation loc) {
        super(loc);
        setOperands(o1,o2);
    }

    @Override
    public void setOperands(EBoolean o1,EBoolean o2) {
        operand1 = o1;
        operand2 = o2;
    }

    @Override
    public void setOperands(ERead<EBoolean> o1, ERead<EBoolean> o2) {
        operand1 = o1;
        operand2 = o2;
    }

    @Override
    public void setOperands(ERead<EBoolean> o1, EBoolean o2) {
        operand1 = o1;
        operand2 = o2;
    }

    @Override
    public void execute(Unit unit) throws DynamicExpressionException {
        aBoolean = (operand1 instanceof EBoolean ? (EBoolean) operand1 : ((ERead<EBoolean>) operand1).asExpression(unit)).asBoolean(unit) || (operand2 instanceof EBoolean ? (EBoolean) operand2 : ((ERead<EBoolean>) operand2).asExpression(unit)).asBoolean(unit);
    }
}
