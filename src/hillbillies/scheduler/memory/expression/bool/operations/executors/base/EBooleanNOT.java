package hillbillies.scheduler.memory.expression.bool.operations.executors.base;

import be.kuleuven.cs.som.annotate.Basic;
import hillbillies.model.Unit;
import hillbillies.part3.programs.SourceLocation;
import hillbillies.scheduler.memory.exceptions.DynamicExpressionException;
import hillbillies.scheduler.memory.expression.Expression;
import hillbillies.scheduler.memory.expression.bool.EBoolean;
import hillbillies.scheduler.memory.expression.bool.interfaces.EBooleanSingleOperation;
import hillbillies.scheduler.memory.expression.variable.ERead;

/**
 * Representing expression which is NOT operator on boolean
 *
 * @author Jens Mortier
 * @version 1.0
 */
public class EBooleanNOT extends EBooleanSingleOperation<EBoolean> {


    public EBooleanNOT(EBoolean operand, SourceLocation loc) {
        super(loc);
        setOperand(operand);
    }

    public EBooleanNOT(ERead<EBoolean> operand, SourceLocation loc) {
        super(loc);
        setOperand(operand);
    }

    @Override
    public void setOperand(EBoolean o1) {
        operand = o1;
    }

    @Override
    public void setOperand(ERead<EBoolean> operand) {
        this.operand = operand;
    }

    @Override
    public void execute(Unit executor) throws DynamicExpressionException {
        aBoolean = !(operand instanceof EBoolean ? (EBoolean) operand : ((ERead<EBoolean>) operand).asExpression(executor)).asBoolean(executor);
    }
}
