package hillbillies.scheduler.memory.expression.bool.operations.interfaces;

import hillbillies.part3.programs.SourceLocation;
import hillbillies.scheduler.memory.expression.Expression;
import hillbillies.scheduler.memory.expression.bool.interfaces.EBooleanSingleOperation;
import hillbillies.scheduler.memory.expression.model.interfaces.EPosition;
import hillbillies.scheduler.memory.expression.variable.ERead;

/**
 * Super class for all position operation expression
 *
 * @version 1.0
 * @author Jens Mortier
 */
public abstract class EPositionOperation extends EBooleanSingleOperation<EPosition> {

    public EPositionOperation(SourceLocation loc) {
        super(loc);
    }

    @Override
    public void setOperand(EPosition o) {
        operand =  o;
    }

    @Override
    public void setOperand(ERead<EPosition> o) {
        operand = o;
    }
}
