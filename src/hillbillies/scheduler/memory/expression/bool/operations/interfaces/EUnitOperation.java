package hillbillies.scheduler.memory.expression.bool.operations.interfaces;

import hillbillies.part3.programs.SourceLocation;
import hillbillies.scheduler.memory.expression.Expression;
import hillbillies.scheduler.memory.expression.bool.interfaces.EBooleanSingleOperation;
import hillbillies.scheduler.memory.expression.model.interfaces.EUnit;
import hillbillies.scheduler.memory.expression.variable.ERead;

/**
 * Class which is a super class for all boolean operations on units
 *
 * @version 1.0
 * @author Jens Mortier
 */
abstract public class EUnitOperation extends EBooleanSingleOperation<EUnit>{

    public EUnitOperation(SourceLocation loc) {
        super(loc);
    }

    @Override
    public void setOperand(EUnit operand) {
        this.operand = operand;
    }

    @Override
    public void setOperand(ERead<EUnit> operand) {
        this.operand = operand;
    }
}
