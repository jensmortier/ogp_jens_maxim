package hillbillies.scheduler.memory.expression.exceptions;

import hillbillies.scheduler.memory.exceptions.DynamicExpressionException;

/**
 * Created by Jens on 15/05/2016.
 */
public class InterruptTrapException extends DynamicExpressionException {

    public InterruptTrapException() {
        super("Interruption call",null);
    }
}
