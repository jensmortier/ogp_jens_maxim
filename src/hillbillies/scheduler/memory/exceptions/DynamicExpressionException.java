package hillbillies.scheduler.memory.exceptions;

/**
 * Exception which will be thrown when an exception occurs when executing dynamic expression
 *
 * @version 1.0
 * @author Jens Mortier
 */
public class DynamicExpressionException extends Exception {

    public DynamicExpressionException(String message, Throwable cause) {
        super(message,cause);
    }
}
