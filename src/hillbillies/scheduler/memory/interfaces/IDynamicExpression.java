package hillbillies.scheduler.memory.interfaces;

import hillbillies.model.Unit;
import hillbillies.scheduler.memory.exceptions.DynamicExpressionException;

/**
 * Interface for declaring classes which are dynamic expressions. I.e. expressions which have to do some calculations before being meaningful
 *
 * @version 1.0
 * @author Jens Mortier
 */
public interface IDynamicExpression<T> {

    public void execute(T executor) throws DynamicExpressionException;
}
