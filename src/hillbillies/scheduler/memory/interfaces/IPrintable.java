package hillbillies.scheduler.memory.interfaces;

import hillbillies.model.Unit;

import java.io.PrintStream;

/**
 * Interfaces for classes which have to be printable
 *
 * @version 1.0
 * @author Jens Mortier
 */
public interface IPrintable {

    public String toString(Unit executor);

    public default void print(Unit executor,PrintStream stream) {
        stream.println(toString(executor));
    }

    public default void print(Unit executor){
        print(executor,System.out);
    }
}
