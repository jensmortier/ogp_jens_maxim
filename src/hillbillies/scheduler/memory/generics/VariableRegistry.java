package hillbillies.scheduler.memory.generics;

import hillbillies.scheduler.execution.components.Task;

import java.util.HashMap;

/**
 * Helper class to have a variable registry for every task
 *
 * @version 1.0
 * @author Jens Mortier
 *         Maxim Verbiest
 */
public class VariableRegistry {

    private static HashMap<Task,VariableRegistry> registryception = new HashMap<>();

    private HashMap<String,Variable> variables;

    private VariableRegistry() {
        //Initiate variable registry
        variables = new HashMap<>();
    }

    public void addVariable(Variable var) {
        variables.put(var.getName(),var);
    }

    public Variable getVariable(String var) {
        return variables.get(var);
    }

    public static VariableRegistry getInstance(Task task) {
        if(registryception.get(task) == null) {
            registryception.put(task,new VariableRegistry());
        }
        return registryception.get(task);
    }

    public static void removeSpace(Task task) {
        registryception.remove(task);
    }
}
