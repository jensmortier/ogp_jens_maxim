package hillbillies.scheduler.memory.generics;

import hillbillies.model.Unit;
import hillbillies.scheduler.memory.expression.Expression;
import hillbillies.scheduler.memory.interfaces.IPrintable;

/**
 * Class which represents a variable with type T
 *
 * @version 1.0
 * @author Jens Mortier
 */
public class Variable<T extends Expression> implements IPrintable {

    private String name;

    private T value;

    @Override
    public String toString(Unit executor) {
        return "Variable " + name + " [" + value.toString() + "]";
    }

    public String getName() {
        return name;
    }

    public T getValue() {
        return value;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setValue(T value) {
        this.value = value;
    }

    public Variable(String name, T value) {
        this.name = name;
        this.value = value;
    }

    public Variable() {
        //Default constructor
    }
}
