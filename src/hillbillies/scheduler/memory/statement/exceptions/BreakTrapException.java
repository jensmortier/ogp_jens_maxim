package hillbillies.scheduler.memory.statement.exceptions;

/**
 * Exception which makes a break occur
 *
 * @version 1.0
 * @author Jens Mortier
 */
public class BreakTrapException extends StatementExecutionException {

    public BreakTrapException() {
        super(null,null);
    }
}
