package hillbillies.scheduler.memory.statement.exceptions;

/**
 * Exception thrown in statement execution
 *
 * @version 1.0
 * @author Jens Mortier
 */
public class StatementExecutionException extends Exception {

    public StatementExecutionException(String message,Throwable cause) {
        super(message,cause);
    }
}
