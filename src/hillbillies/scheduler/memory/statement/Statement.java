package hillbillies.scheduler.memory.statement;

import hillbillies.model.Unit;
import hillbillies.part3.programs.SourceLocation;
import hillbillies.scheduler.memory.exceptions.DynamicExpressionException;
import hillbillies.scheduler.memory.statement.exceptions.StatementExecutionException;
import hillbillies.scheduler.memory.statement.generics.ActionStatus;
import hillbillies.util.ReferencedInteger;

/**
 * This class is a super class, defining the basics for statements
 *
 * @author Jens Mortier
 * @version 1.0
 */
abstract public class Statement {

    /**
     * Contains location in source
     */
    private SourceLocation sourceLocation;

    private ActionStatus status;

    /**
     * Executor of statement
     * @param unit  Unit which is executing the statement
     * @param total_number_of_statements    The total number of statements that can be executed in some period of time
     * @param statements_executed   How many statements already executed
     * @throws DynamicExpressionException
     * @throws StatementExecutionException
     */
    public abstract void execute(Unit unit, ReferencedInteger total_number_of_statements, ReferencedInteger statements_executed) throws DynamicExpressionException, StatementExecutionException;

    protected Statement(SourceLocation location) {
        sourceLocation = location;
        status = ActionStatus.BLOCKING;
    }


    public void recordStart() {
        status = ActionStatus.EXECUTING;
    }

    public void recordStop() {
        status = ActionStatus.FINISHED;
    }

    public void recordAgain() {
        status = ActionStatus.NOT_STARTED;
    }

    protected void nonBlocking() {
        status = ActionStatus.NOT_STARTED;
    }

    public ActionStatus getStatus() {
        return status;
    }

    public static boolean toBeExecuted(Statement instance, Unit unit) {
        boolean executed = unit.getTask().getLatestStatementExecuted() == null || unit.getTask().getLatestStatementExecuted() == instance;
        if(executed) unit.getTask().setLatestStatementExecuted(null);
        return executed;
    }
}
