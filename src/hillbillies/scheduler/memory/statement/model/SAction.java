package hillbillies.scheduler.memory.statement.model;

import hillbillies.part3.programs.SourceLocation;
import hillbillies.scheduler.memory.expression.Expression;
import hillbillies.scheduler.memory.statement.Statement;

/**
 * Class which represents an action of model objects
 *
 * @version 1.0
 * @author Jens Mortier
 */
abstract public class SAction extends Statement {

    protected Expression operand;

    public SAction(SourceLocation loc) {
        super(loc);
        nonBlocking();
    }

    public Expression getOperand() {
        return operand;
    }
}
