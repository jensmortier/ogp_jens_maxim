package hillbillies.scheduler.memory.statement.model;

import hillbillies.exceptions.UnitException;
import hillbillies.gameworld.Position;
import hillbillies.model.MovementType;
import hillbillies.model.Unit;
import hillbillies.part3.programs.SourceLocation;
import hillbillies.scheduler.memory.exceptions.DynamicExpressionException;
import hillbillies.scheduler.memory.expression.model.interfaces.EPosition;
import hillbillies.scheduler.memory.expression.variable.ERead;
import hillbillies.scheduler.memory.statement.exceptions.StatementExecutionException;
import hillbillies.util.ReferencedInteger;

/**
 * Class which represents a work action
 *
 * @version 1.0
 * @author Jens Mortier
 */
public class AWork extends SAction {

    public AWork(EPosition o,SourceLocation loc) {
        super(loc);
        operand = o;
    }

    public AWork(ERead<EPosition> o,SourceLocation loc) {
        super(loc);
        operand = o;
    }

    /**
     * Executor of statetement
     *
     * @param unit Unit which is executing the statement
     */
    @Override
    public void execute(Unit unit, ReferencedInteger total, ReferencedInteger done) throws DynamicExpressionException, StatementExecutionException {
        if(!toBeExecuted(this,unit)) return;
        if(total.intValue() == done.intValue()) {
            if(unit.getTask().getLatestStatementExecuted() == null) unit.getTask().setLatestStatementExecuted(this);
            return;
        }
        System.out.println("Evaluating work");
        switch(getStatus()) {
            case NOT_STARTED:
                recordStart();
                Position<Integer> pos = (operand instanceof EPosition ? (EPosition) operand : ((ERead<EPosition>) operand).asExpression(unit)).asPosition(unit);
                try {
                    unit.Work(pos.getPosition_x(),pos.getPosition_y(),pos.getPosition_z());
                } catch (UnitException e) {
                    recordStop();
                    throw new StatementExecutionException("Something went wrong while letting unit work",e);
                }
                unit.getTask().setLatestStatementExecuted(this);
                done.setValue(total.intValue());
                return;
            case EXECUTING:
                if(unit.getMovementStatus() != MovementType.WORKING) {
                    recordStop();
                    unit.getTask().setLatestStatementExecuted(null);
                }else {
                    unit.getTask().setLatestStatementExecuted(this);
                    done.setValue(total.intValue());
                    return;
                }
                break;
        }
        done.raise();
    }
}
