package hillbillies.scheduler.memory.statement.model;

import hillbillies.exceptions.UnitException;
import hillbillies.model.Unit;
import hillbillies.part3.programs.SourceLocation;
import hillbillies.scheduler.memory.exceptions.DynamicExpressionException;
import hillbillies.scheduler.memory.expression.model.interfaces.EUnit;
import hillbillies.scheduler.memory.expression.variable.ERead;
import hillbillies.scheduler.memory.statement.exceptions.StatementExecutionException;
import hillbillies.util.ReferencedInteger;

/**
 * Class which represents a follow action
 *
 * @version 1.0
 * @author Jens Mortier
 */
public class AFollow extends SAction {

    public AFollow(EUnit o,SourceLocation loc) {
        super(loc);
        operand = o;
    }

    public AFollow(ERead<EUnit> o,SourceLocation loc) {
        super(loc);
        operand = o;
    }

    /**
     * Executor of statetement
     *
     * @param unit Unit which is executing the statement
     */
    @Override
    public void execute(Unit unit, ReferencedInteger total, ReferencedInteger done) throws DynamicExpressionException, StatementExecutionException {
        if(!toBeExecuted(this,unit)) return;
        if(total.intValue() == done.intValue()) {
            if(unit.getTask().getLatestStatementExecuted() == null) unit.getTask().setLatestStatementExecuted(this);
            return;
        }
        switch(getStatus()) {
            case NOT_STARTED:
                recordStart();
                try {
                    unit.Follow((operand instanceof EUnit ? (EUnit) operand : ((ERead<EUnit>) operand).asExpression(unit)).asUnit(unit));
                } catch (UnitException e){
                    e.printStackTrace();
                    throw new StatementExecutionException("Error in executing Follow",e);
                }
                unit.getTask().setLatestStatementExecuted(this);
                done.setValue(total.intValue());
                return;
            case EXECUTING:
                if(!unit.isFollowing()) {
                    recordStop();
                    unit.getTask().setLatestStatementExecuted(null);
                }else {
                    unit.getTask().setLatestStatementExecuted(this);
                    done.setValue(total.intValue());
                    return;
                }
                break;
        }
        done.raise();
    }
}
