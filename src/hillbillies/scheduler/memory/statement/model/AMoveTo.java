package hillbillies.scheduler.memory.statement.model;

import hillbillies.exceptions.UnitException;
import hillbillies.gameworld.Position;
import hillbillies.model.Unit;
import hillbillies.part3.programs.SourceLocation;
import hillbillies.scheduler.memory.exceptions.DynamicExpressionException;
import hillbillies.scheduler.memory.expression.model.interfaces.EPosition;
import hillbillies.scheduler.memory.expression.variable.ERead;
import hillbillies.scheduler.memory.statement.exceptions.StatementExecutionException;
import hillbillies.util.ReferencedInteger;

/**
 * Class which represents move to action
 *
 * @version 1.0
 * @author Jens Mortier
 */
public class AMoveTo extends SAction {

    public AMoveTo(EPosition o, SourceLocation loc) {
        super(loc);
        operand = o;
    }

    public AMoveTo(ERead<EPosition> o ,SourceLocation loc) {
        super(loc);
        operand = o;
    }

    /**
     * Executor of statetement
     *
     * @param unit Unit which is executing the statement
     */
    @Override
    public void execute(Unit unit, ReferencedInteger total, ReferencedInteger done) throws DynamicExpressionException, StatementExecutionException {
        if(!toBeExecuted(this,unit)) return;
        if(total.intValue() == done.intValue()) {
            if(unit.getTask().getLatestStatementExecuted() == null) unit.getTask().setLatestStatementExecuted(this);
            return;
        }
        System.out.println("Evaluating move to statement");
        switch(getStatus()) {
            case NOT_STARTED:
                recordStart();
                Position<Integer> position = (operand instanceof EPosition ? (EPosition) operand :
                                        ((ERead<EPosition>) operand).asExpression(unit))
                                                                        .asPosition(unit);
                try {
                    System.out.println("Instructing unit to move to " + position.toString());
                    unit.moveTo(position.getPosition_x(), position.getPosition_y(), position.getPosition_z());
                } catch (UnitException e) {
                    recordStop();
                    throw new StatementExecutionException("Something went wrong while letting move unit",e);
                }
                unit.getTask().setLatestStatementExecuted(this);
                done.setValue(total.intValue());
                return;
            case EXECUTING:
                System.out.println("Currently executing");
                if(!unit.isMoving()) {
                    recordStop();
                    unit.getTask().setLatestStatementExecuted(null);
                }else {
                    System.out.println("Not ready yet");
                    unit.getTask().setLatestStatementExecuted(this);
                    done.setValue(total.intValue());
                    return;
                }

                break;
        }
        done.raise();
    }
}
