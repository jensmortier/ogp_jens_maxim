package hillbillies.scheduler.memory.statement.model;

import hillbillies.exceptions.UnitException;
import hillbillies.model.MovementType;
import hillbillies.model.Unit;
import hillbillies.part3.programs.SourceLocation;
import hillbillies.scheduler.memory.exceptions.DynamicExpressionException;
import hillbillies.scheduler.memory.expression.exceptions.InterruptTrapException;
import hillbillies.scheduler.memory.expression.model.interfaces.EUnit;
import hillbillies.scheduler.memory.expression.variable.ERead;
import hillbillies.scheduler.memory.statement.exceptions.StatementExecutionException;
import hillbillies.util.ReferencedInteger;

/**
 * Class which represents an attack action of a certain unit
 *
 * @version 1.0
 * @author Jens Mortier
 */
public class AAttack extends SAction {

    public AAttack(EUnit o,SourceLocation loc) {
        super(loc);
        operand = o;
    }

    public AAttack(ERead<EUnit> o,SourceLocation loc) {
        super(loc);
        operand = o;
    }

    /**
     * Executor of statetement
     *
     * @param unit Unit which is executing the statement
     */
    @Override
    public void execute(Unit unit, ReferencedInteger total, ReferencedInteger done) throws DynamicExpressionException, StatementExecutionException {
        if(!toBeExecuted(this,unit)) return;
        if(total.intValue() == done.intValue()) {
            if(unit.getTask().getLatestStatementExecuted() == null) unit.getTask().setLatestStatementExecuted(this);
            return;
        }
        try {
            switch(getStatus()) {
                case NOT_STARTED:
                    recordStart();
                    unit.Attack((operand instanceof EUnit ? (EUnit) operand : ((ERead<EUnit>)operand).asExpression(unit)).asUnit(unit));
                    unit.getTask().setLatestStatementExecuted(this);
                    done.setValue(total.intValue());
                    return;
                case EXECUTING:
                    if(unit.getMovementStatus() != MovementType.FIGHTING) {
                        recordStop();
                        unit.getTask().setLatestStatementExecuted(null);
                    }else {
                        unit.getTask().setLatestStatementExecuted(this);
                        done.setValue(total.intValue());
                        return;
                    }
                    break;
            }
        } catch (UnitException e) {
            if((e.getErrorCode() & UnitException.ATTACKING_CONFLICT) > 0) throw new InterruptTrapException();
            throw new StatementExecutionException("Something went wrong while attack unit",e);
        }
        done.raise();
    }
}
