package hillbillies.scheduler.memory.statement.io;

import hillbillies.model.Unit;
import hillbillies.part3.programs.SourceLocation;
import hillbillies.scheduler.memory.exceptions.DynamicExpressionException;
import hillbillies.scheduler.memory.expression.Expression;
import hillbillies.scheduler.memory.statement.Statement;
import hillbillies.scheduler.memory.statement.exceptions.StatementExecutionException;
import hillbillies.util.ReferencedInteger;

/**
 * Class which represents print to stdout
 *
 * @version 1.0
 * @author Jens Mortier
 */
public class SPrint extends Statement {

    private Expression value;

    public SPrint(SourceLocation loc, Expression v) {
        super(loc);
        value = v;
    }

    @Override
    public void execute(Unit unit, ReferencedInteger total_number, ReferencedInteger executed_number) throws DynamicExpressionException, StatementExecutionException {
        if(!toBeExecuted(this,unit)) return;
        if(executed_number.intValue() == total_number.intValue()) {
            //Save this statement at task
            if(unit.getTask().getLatestStatementExecuted() == null) unit.getTask().setLatestStatementExecuted(this);

            //Exit execution gracefully
            return;
        }
        value.print(unit);
        executed_number.raise();
        unit.getTask().setLatestStatementExecuted(null);
    }

    public Expression getValue() {
        return value;
    }
}
