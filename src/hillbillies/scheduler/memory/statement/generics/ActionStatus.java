package hillbillies.scheduler.memory.statement.generics;

import be.kuleuven.cs.som.annotate.Value;

/**
 * Enum which contains the different statuses an action can have
 *
 * @version 1.0
 * @author Jens Mortier
 */
@Value
public enum ActionStatus {
    NOT_STARTED,
    EXECUTING,
    FINISHED,
    BLOCKING
}
