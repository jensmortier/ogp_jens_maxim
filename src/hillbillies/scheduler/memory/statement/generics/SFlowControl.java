package hillbillies.scheduler.memory.statement.generics;

import hillbillies.part3.programs.SourceLocation;
import hillbillies.scheduler.memory.expression.bool.EBoolean;
import hillbillies.scheduler.memory.statement.Statement;

/**
 * Class which represents a flow control statement
 *
 * @version 1.0
 * @author Jens Mortier
 */
abstract public class SFlowControl extends Statement {
    protected EBoolean expression;
    protected Statement statement;

    protected SFlowControl(EBoolean b,Statement s,SourceLocation loc) {
        super(loc);
        expression = b;
        statement = s;
    }

    public Statement getStatement() {
        return statement;
    }

    public EBoolean getExpression() {
        return expression;
    }
}
