package hillbillies.scheduler.memory.statement.language;

import hillbillies.model.Unit;
import hillbillies.part3.programs.SourceLocation;
import hillbillies.scheduler.memory.exceptions.DynamicExpressionException;
import hillbillies.scheduler.memory.statement.Statement;
import hillbillies.scheduler.memory.statement.exceptions.BreakTrapException;
import hillbillies.scheduler.memory.statement.exceptions.StatementExecutionException;
import hillbillies.util.ReferencedInteger;

/**
 * Class which represents a break key word
 *
 * @version 1.0
 * @author Jens Mortier
 */
public class SBreak extends Statement {

    public SBreak(SourceLocation loc) {
        super(loc);
    }

    @Override
    public void execute(Unit unit, ReferencedInteger total, ReferencedInteger number) throws DynamicExpressionException,StatementExecutionException {
        if(!toBeExecuted(this,unit)) return;
        unit.getTask().setLatestStatementExecuted(null);

        throw new BreakTrapException();
    }
}
