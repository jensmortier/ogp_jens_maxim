package hillbillies.scheduler.memory.statement.language;

import hillbillies.model.Unit;
import hillbillies.part3.programs.SourceLocation;
import hillbillies.scheduler.memory.exceptions.DynamicExpressionException;
import hillbillies.scheduler.memory.statement.Statement;
import hillbillies.scheduler.memory.statement.exceptions.StatementExecutionException;
import hillbillies.util.ReferencedInteger;

import java.util.List;

/**
 * Class which represents composite statement
 *
 * @version 1.0
 * @author Jens Mortier
 */
public class CompositeStatement extends Statement {

    private List<Statement> statements;

    public CompositeStatement(SourceLocation loc,List<Statement> s) {
        super(loc);
        statements = s;
    }

    @Override
    public void execute(Unit unit, ReferencedInteger total, ReferencedInteger done) throws DynamicExpressionException, StatementExecutionException {
        if(unit.getTask().getLatestStatementExecuted() == this) unit.getTask().setLatestStatementExecuted(null);
        for(Statement s : statements) {
            System.out.println("Executing composite..[" + s.getClass() + "]");
            s.execute(unit,total,done);
        }
        //Don't add latest, because this isn't thought of as a statement
    }

    public List<Statement> getStatements() {
        return statements;
    }
}
