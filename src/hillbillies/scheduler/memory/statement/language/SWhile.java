package hillbillies.scheduler.memory.statement.language;

import hillbillies.model.Unit;
import hillbillies.part3.programs.SourceLocation;
import hillbillies.scheduler.memory.exceptions.DynamicExpressionException;
import hillbillies.scheduler.memory.expression.bool.EBoolean;
import hillbillies.scheduler.memory.statement.Statement;
import hillbillies.scheduler.memory.statement.exceptions.BreakTrapException;
import hillbillies.scheduler.memory.statement.exceptions.StatementExecutionException;
import hillbillies.scheduler.memory.statement.generics.SFlowControl;
import hillbillies.util.ReferencedInteger;

/**
 * Class which represents while loop
 *
 * @version 1.0
 * @author Jens Mortier
 */
public class SWhile extends SFlowControl {

    public SWhile(EBoolean b,Statement s,SourceLocation loc) {
        super(b,s,loc);
    }

    @Override
    public void execute(Unit unit, ReferencedInteger total, ReferencedInteger done) throws DynamicExpressionException, StatementExecutionException {
        //Eval
        System.out.println("Eval bool");
        boolean run = expression.asBoolean(unit);
        System.out.println("Bool set");
        System.out.println(unit.getTask().getLatestStatementExecuted() == null ? null : unit.getTask().getLatestStatementExecuted().toString());
        if(unit.getTask().getLatestStatementExecuted() == this) unit.getTask().setLatestStatementExecuted(null);
        while(run || unit.getTask().getLatestStatementExecuted() != null) {
            try {
                System.out.println("Running while");
                statement.recordAgain();
                statement.execute(unit, total, done);
                System.out.println(unit.getTask().getLatestStatementExecuted() == null ? null : unit.getTask().getLatestStatementExecuted().toString());
                if(total.intValue() == done.intValue()) {
                    run = false;
                    if(unit.getTask().getLatestStatementExecuted() == null) unit.getTask().setLatestStatementExecuted(statement);
                }
                if(unit.getTask().getLatestStatementExecuted() != null) break;
            } catch (StatementExecutionException e) {
                if(e instanceof BreakTrapException) {
                    System.out.println("BREAK TRAP");
                    unit.getTask().setLatestStatementExecuted(null);
                    run = false;
                }else {
                    throw e;
                }
            }
            run = run && expression.asBoolean(unit);
        }
    }


}
