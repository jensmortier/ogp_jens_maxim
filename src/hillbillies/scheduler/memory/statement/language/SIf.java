package hillbillies.scheduler.memory.statement.language;

import hillbillies.model.Unit;
import hillbillies.part3.programs.SourceLocation;
import hillbillies.scheduler.memory.exceptions.DynamicExpressionException;
import hillbillies.scheduler.memory.expression.bool.EBoolean;
import hillbillies.scheduler.memory.statement.Statement;
import hillbillies.scheduler.memory.statement.exceptions.StatementExecutionException;
import hillbillies.scheduler.memory.statement.generics.SFlowControl;
import hillbillies.util.ReferencedInteger;

/**
 * Class which represents if statement
 *
 * @version 1.0
 * @author Jens Mortier
 */
public class SIf extends SFlowControl {
    private Statement alternative_statement;

    public SIf(Statement s,EBoolean e,Statement alternative,SourceLocation loc) {
        super(e,s,loc);
        alternative_statement = alternative;
    }

    @Override
    public void execute(Unit unit, ReferencedInteger total, ReferencedInteger done) throws DynamicExpressionException, StatementExecutionException {
        System.out.println(unit.getTask().getLatestStatementExecuted() == null ? null : unit.getTask().getLatestStatementExecuted().toString());
        if(unit.getTask().getLatestStatementExecuted() == this) unit.getTask().setLatestStatementExecuted(null);
        if(expression.asBoolean(unit) || unit.getTask().getLatestStatementExecuted() != null) {
            statement.execute(unit,total,done);
        }else if(alternative_statement != null) {
            alternative_statement.execute(unit,total,done);
        }
        //Don't need to update counter, not a real statement
    }

    public Statement getAlternative_statement() {
        return alternative_statement;
    }
}
