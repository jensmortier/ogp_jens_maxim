package hillbillies.scheduler.memory.statement.language;

import hillbillies.model.Unit;
import hillbillies.part3.programs.SourceLocation;
import hillbillies.scheduler.memory.exceptions.DynamicExpressionException;
import hillbillies.scheduler.memory.expression.Expression;
import hillbillies.scheduler.memory.generics.Variable;
import hillbillies.scheduler.memory.generics.VariableRegistry;
import hillbillies.scheduler.memory.statement.Statement;
import hillbillies.util.ReferencedInteger;

/**
 * Class which represents an assignment
 *
 * @version 1.0
 * @author Jens Mortier
 */
public class SAssignment<T extends Expression> extends Statement {

    private Variable<T> variable;
    private T value;

    public SAssignment(SourceLocation loc,Variable<T> var,T val) {
        super(loc);
        variable = var;
        value = val;
    }

    @Override
    public void execute(Unit unit, ReferencedInteger total, ReferencedInteger done) throws DynamicExpressionException {
        if(!toBeExecuted(this,unit)) return;

        if(total.intValue() == done.intValue()) {
            //Check if to check latest
            if(unit.getTask().getLatestStatementExecuted() == null) unit.getTask().setLatestStatementExecuted(this);

            //Graceful end
            return;
        }

        //Set variable value
        variable.setValue(value);

        //Save in registry
        VariableRegistry.getInstance(unit.getTask()).addVariable(variable);

        //Update counter
        done.raise();
        unit.getTask().setLatestStatementExecuted(null);
    }

    public Variable<T> getVariable() {
        return variable;
    }
}
