package hillbillies.tests.custom;

import hillbillies.gameworld.Position;
import hillbillies.model.Unit;
import hillbillies.model.World;
import hillbillies.part2.listener.TerrainChangeListener;
import hillbillies.part3.programs.SourceLocation;
import hillbillies.scheduler.memory.expression.model.interfaces.EPosition;
import hillbillies.scheduler.memory.expression.model.position.PLiteral;
import hillbillies.scheduler.memory.expression.model.position.PNextTo;
import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;
import java.util.SortedMap;

import static org.junit.Assert.*;

/**
 * Test case for PNextTo
 * @version 1.0
 * @author Maxim Verbiest
 */
public class PNextToTest {

    private static Unit testUnit ;

    @Before
    public void setUp() throws Exception {
        //Init config
        int[][][] config = {//Global array
                {//x=0
                        {//y = 0
                                0,//z = 0
                                1,
                                1
                        },
                        {1,1,1},
                        {1,1,1}
                },
                {//x=1
                        {1,1,1},{0,1,1},{1,1,1} //At (1,1,0) an air cube will be present
                },
                {//x=2
                        {1,1,1},{1,1,1},{1,1,1}
                }
        };

        World.resetInstance();
        World.getInstance().setTerrainChangeListener(new TerrainChangeListener() {

            @Override
            public void notifyTerrainChanged(int x, int y, int z) {
                for (TerrainChangeListener listener : new HashSet<TerrainChangeListener>()) {
                    listener.notifyTerrainChanged(x, y, z);
                }
            }
        });
        World.getInstance().setConfig(config);

        testUnit = new Unit("Tester",0,0,0,75,75,75,75,false);
    }

    @Test
    public void testExecute() throws Exception {
        PLiteral litera = new PLiteral(0,0,0,new SourceLocation(11,1));
        PNextTo nextToPos = new PNextTo(litera,new SourceLocation(22,2));

        assertTrue(nextToPos.asPosition(testUnit).equals(new Position<Integer>(1,1,0)));
    }

}