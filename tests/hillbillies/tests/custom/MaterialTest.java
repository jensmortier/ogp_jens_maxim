package hillbillies.tests.custom;

import hillbillies.exceptions.MaterialException;
import hillbillies.gameworld.Cube;
import hillbillies.gameworld.Material;
import hillbillies.gameworld.Position;
import hillbillies.gameworld.StabilityType;
import hillbillies.model.Unit;
import hillbillies.model.World;
import hillbillies.part2.listener.TerrainChangeListener;
import org.junit.Before;
import org.junit.Test;
import java.util.HashSet;

import static org.junit.Assert.*;

/**
 * Test case for Material
 * @author Maxim Verbiest
 * @version 1.0
 */
public class MaterialTest {

    private static Material testMaterial;
    private static Material unstableMaterial;
    private static Unit carrier;

    @Before
    public void setUpMutableFixtures() throws Exception {
        //Init config
        int[][][] config = {//Global array
                {//x=0
                        {//y = 0
                                0,//z = 0
                                0,
                                0
                        },
                        {0,0,0},
                        {0,0,0}
                },
                {//x=1
                        {0,0,0},{0,0,0},{0,0,0}
                },
                {//x=2
                        {0,0,0},{0,0,0},{0,0,0}
                }
        };

        World.resetInstance();
        World.getInstance().setTerrainChangeListener(new TerrainChangeListener() {

            @Override
            public void notifyTerrainChanged(int x, int y, int z) {
                for (TerrainChangeListener listener : new HashSet<TerrainChangeListener>()) {
                    listener.notifyTerrainChanged(x, y, z);
                }
            }
        });
        World.getInstance().setConfig(config);

        testMaterial = new Material(new Position<Double>(0.0,0.0,0.0),World.getInstance().getCubeByKey(0,0,0)) {
            @Override
            protected void pickUpMaterial(Unit newCarrier) throws MaterialException {

            }

            @Override
            protected void dropMaterial(Unit carrier) throws MaterialException {

            }
        };

        unstableMaterial = new Material(new Position<Double>(2.0,2.0,2.0),World.getInstance().getCubeByKey(2,2,2)) {
            @Override
            protected void pickUpMaterial(Unit newCarrier) throws MaterialException {

            }

            @Override
            protected void dropMaterial(Unit carrier) throws MaterialException {

            }
        };

        carrier = new Unit("Carrier",0.0,0.0,0.0,50,50,50,50,false);
    }

    @Test
    public void testIsCurrentlyCarried() throws Exception {

        assertFalse("Not carried",testMaterial.isCurrentlyCarried());

        testMaterial.PickUp(carrier);
        assertTrue("Carried", testMaterial.isCurrentlyCarried());
    }

    @Test
    public void testGetCarrier() throws Exception {
        testMaterial.PickUp(carrier);
        assertEquals(testMaterial.getCarrier(),carrier);
    }

    @Test
    public void testGetPosition() throws Exception {
        assertTrue(testMaterial.getPosition().equals(new Position<Double>(0.0,0.0,0.0)));
    }

    @Test
    public void testGetStabilityStatus_STABLE() throws Exception {
        assertEquals(testMaterial.getStabilityStatus(), StabilityType.STABLE_RESTING);

    }

    @Test
    public void testGetStabilityStatus_UNSTABLE() throws MaterialException {
        unstableMaterial.advanceTime(0.2);
        assertEquals(unstableMaterial.getStabilityStatus(),StabilityType.UNSTABLE_FALLING);
    }

    @Test
    public void testGetWeight() throws Exception {
        boolean validWeight = false;
        if (testMaterial.getWeight() <= 50 && testMaterial.getWeight() >=10){
            validWeight = true;
        }
        assertTrue(validWeight);
    }

    @Test
    public void testFall() throws Exception {
        unstableMaterial.advanceTime(20);
        Position<Double> updatedPos = new Position<>(2.0,2.0,0.0);
        assertTrue(updatedPos.equals(unstableMaterial.getPosition()));
    }
}