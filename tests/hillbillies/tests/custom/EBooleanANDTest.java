package hillbillies.tests.custom;

import hillbillies.part3.programs.SourceLocation;
import hillbillies.scheduler.memory.exceptions.DynamicExpressionException;
import hillbillies.scheduler.memory.expression.bool.EBoolean;
import hillbillies.scheduler.memory.expression.bool.operations.executors.base.EBooleanAND;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Test Case for EBooleanAND
 * @version 1.0
 * @author Maxim Verbiest
 */
public class EBooleanANDTest {

    private static EBooleanAND andBool ;
    private static EBoolean leftOp;
    private static EBoolean rightOp;

    @Before
    public void setUp(){
        leftOp = new EBoolean(new SourceLocation(5,5));
        rightOp = new EBoolean(new SourceLocation(6,6));
        andBool = new EBooleanAND(leftOp,rightOp,new SourceLocation(7,7));
    }

    @Test
    public void testExecute_FT() throws Exception {
        leftOp.setBoolean(false);
        rightOp.setBoolean(true);
        assertFalse(andBool.asBoolean(null));
    }

    @Test
    public void testExecute_TT() throws Exception {

        leftOp.setBoolean(true);
        rightOp.setBoolean(true);

        assertTrue(andBool.asBoolean(null));
    }

    @Test
    public void testExecute_FF() throws Exception {
        leftOp.setBoolean(false);
        rightOp.setBoolean(false);

        assertFalse(andBool.asBoolean(null));
    }
}