package hillbillies.tests.custom;

import hillbillies.exceptions.WorldException;
import hillbillies.gameworld.Position;
import hillbillies.gameworld.SceneryType;
import hillbillies.model.*;
import hillbillies.part2.listener.TerrainChangeListener;
import ogp.framework.util.Util;
import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;

import static org.junit.Assert.*;

/**
 * Test case for World
 * @version 1.0
 * @author Jens Mortier
 */
public class WorldTest {
    private int[][][] config;
    private static Unit example_unit;
    private static Unit streamUnit;




    @Before
    public void setUp() throws Exception {
        //Init config
        int[][][] config = {//Global array
                {//x=0
                        {//y = 0
                                0,//z = 0
                                0,
                                0
                        },
                        {0,0,0},
                        {0,0,0}
                },
                {//x=1  //at (1,0,0) a cube of type Rock is placed, at (1,1,0) a cube of type Tree is placed
                        {1,0,0},{2,0,0},{0,0,0}
                },
                {//x=2  //at (1,0,0) a cube of type Rock is placed
                        {1,0,0},{0,0,0},{0,0,0}
                }
        };
        World.resetInstance();
        this.config = config;
        World.getInstance().setTerrainChangeListener(new TerrainChangeListener() {

            @Override
            public void notifyTerrainChanged(int x, int y, int z) {
                for (TerrainChangeListener listener : new HashSet<TerrainChangeListener>()) {
                    listener.notifyTerrainChanged(x, y, z);
                }
            }
        });
        World.getInstance().setConfig(config);

        example_unit = new Unit("Test",1,1,1,1,1,1,1,false);
        streamUnit = new Unit("McStreamy",0.0,0.0,0.0,75,75,75,75,false);
    }

    @Test
    public void testIsValidDoublePosition() throws Exception {
        assertTrue(World.getInstance().isValidDoublePosition(1, 1, 1));
    }

    @Test
    public void testIsValidDoublePosition_upper_bounds() throws Exception {
        assertFalse(World.getInstance().isValidDoublePosition(51,2,2));
        assertFalse(World.getInstance().isValidDoublePosition(2,51,2));
        assertFalse(World.getInstance().isValidDoublePosition(2, 2, 51));
    }

    @Test
    public void testIsValidDoublePosition_lower_bounds() throws Exception {
        assertFalse(World.getInstance().isValidDoublePosition(-1,2,2));
        assertFalse(World.getInstance().isValidDoublePosition(2,-1,2));
        assertFalse(World.getInstance().isValidDoublePosition(2, 2, -1));
    }

    @Test
    public void testIsValidIntegerPosition() throws Exception {
        assertTrue(World.getInstance().isValidDoublePosition(1, 1, 1));
    }

    @Test
    public void testIsValidIntegerPosition_upper_bounds() throws Exception {
        assertFalse(World.getInstance().isValidDoublePosition(51,2,2));
        assertFalse(World.getInstance().isValidDoublePosition(2,51,2));
        assertFalse(World.getInstance().isValidDoublePosition(2, 2, 51));
    }

    @Test
    public void testIsValidIntegerPosition_lower_bounds() throws Exception {
        assertFalse(World.getInstance().isValidDoublePosition(-1,2,2));
        assertFalse(World.getInstance().isValidDoublePosition(2,-1,2));
        assertFalse(World.getInstance().isValidDoublePosition(2, 2, -1));
    }

    @Test
    public void testSetConfig() throws Exception {
        World.getInstance().setConfig(config);
    }

    /*@Test*/ //TODO, i(maxim) changed world's structure, fix it
    public void testIsCubeAttachedToBorder_not() throws Exception {
        World.getInstance().setConfig(config);
        assertFalse(World.getInstance().isCubeAttachedToBorder(World.getInstance().getCubeByKey(1, 0, 0)));
    }

    @Test
    public void testAddUnitToWorld() throws Exception {
        World.getInstance().addUnitToWorld(false);
    }

    @Test
    public void testAddUnitToWorld_full() throws Exception {
        int counter = 0;
        Unit unit = null;
        while(counter <= World.MAX_UNITS * 2) {
            unit = World.getInstance().addUnitToWorld(false);
            counter++;
        }
        assertEquals(unit,null);
    }

    @Test
    public void testGetCubeByKey() throws Exception {
        assertTrue(Util.fuzzyEquals(World.getInstance().getCubeByKey(0, 0, 0).getPosition().getPosition_x(),0));
    }

    @Test
    public void testGetCubeByVector() throws Exception {
        Integer[] vector = {0,0,0};
        World.getInstance().setConfig(config);
        assertTrue(Util.fuzzyEquals(World.getInstance().getCubeByVector(vector).getPosition().getPosition_x(), 0));
    }

    @Test
    public void testAddFaction() throws Exception {
        World.getInstance().addFaction(new Faction());
    }

    @Test
    public void testAddFaction_null() throws Exception {
        World.getInstance().addFaction(null);
        assertEquals(World.getInstance().getFactions().size(),0);
    }

    @Test
    public void testAddFaction_terminated() throws Exception {
        Faction faction = new Faction();
        faction.terminate();
        World.getInstance().addFaction(faction);
        assertEquals(World.getInstance().getFactions().size(), 0);
    }

    @Test
    public void testAddFaction_overflow() throws Exception {
        int counter = 0;
        while(counter <= World.MAX_FACTIONS + 1) {
            World.getInstance().addFaction(new Faction());
            counter++;
        }
        assertEquals(World.getInstance().getFactions().size(),World.MAX_FACTIONS);
    }

    @Test
    public void testGetFactions() throws Exception {
        Faction faction = new Faction();
        World.getInstance().addFaction(faction);
        assertTrue(World.getInstance().getFactions().contains(faction));
    }

    @Test
    public void testRemoveFaction() throws Exception {
        Faction faction = new Faction();
        World.getInstance().addFaction(faction);
        World.getInstance().removeFaction(faction);
        assertFalse(World.getInstance().getFactions().contains(faction));
    }

    @Test
    public void testGetActiveBouldersInWorld() throws Exception {
        Boulder boulder = new Boulder(new Position<Double>(new Double(1.5),new Double(1.5),new Double(1.5)),World.getInstance().getCubeByKey(1,1,1));
        World.getInstance().addGameObject(boulder);
        assertEquals(World.getInstance().getActiveBouldersInWorld().size(), 1);
        assertTrue(World.getInstance().getActiveBouldersInWorld().contains(boulder));
    }

    @Test
    public void testFactionsToSet() throws Exception {
        World.getInstance().addFaction(new Faction());
        assertTrue(World.getInstance().getFactions().containsAll(World.getInstance().factionsToSet()));
    }

    @Test
    public void testRemoveGameObject() throws Exception {
        Log log = new Log(new Position<Double>(new Double(1.5),new Double(1.5),new Double(1.5)),World.getInstance().getCubeByKey(1,1,1));
        World.getInstance().addGameObject(log);
        World.getInstance().removeGameObject(log);
        assertFalse(World.getInstance().getActiveLogsInWorld().contains(log));
    }

    @Test
    public void testFilterByObject_onlyOneBoulderInMap() throws Exception {
        streamUnit.Work(1,0,0);
        World.getInstance().advanceTime(1);
        streamUnit.Work(20);
        World.getInstance().advanceTime(25);
        //To check if advance time is correctly used etc
        assertTrue(World.getInstance().getCubeByKey(1,0,0).containsBoulder());
        //Actual check
        assertTrue(World.getInstance().filterByObject(Boulder.class,null,streamUnit).getCurrentCube().getPosition().equals(new Position<Integer>(1,0,0)));
    }

    @Test
    public void testFilterByObject_MoreBouldersInMap() throws Exception{
        //Create boulder at (1,0,0)
        streamUnit.Work(1,0,0);
        World.getInstance().advanceTime(1);
        streamUnit.Work(20);
        World.getInstance().advanceTime(25);

        streamUnit.getCurrentPosition().setPosition(new Position(1.5,0.5,0.5));
        streamUnit.setCurrentCube(World.getInstance().getCubeByKey(1,0,0));
        World.getInstance().advanceTime(2);

        //Create boulder (2,0,0)
        streamUnit.Work(2,0,0);
        World.getInstance().advanceTime(1);
        streamUnit.Work(20);
        World.getInstance().advanceTime(25);

        assertTrue(World.getInstance().filterByObject(Boulder.class,null,streamUnit).getCurrentCube().getPosition().equals(new Position<Integer>(1,0,0)));

    }

    @Test
    public void testFilterByObject_BouldersAndLogsInMap() throws Exception{
        //Create boulder at (1,0,0)
        streamUnit.Work(1,0,0);
        World.getInstance().advanceTime(1);
        streamUnit.Work(20);
        World.getInstance().advanceTime(25);

        streamUnit.getCurrentPosition().setPosition(new Position(1.5,0.5,0.5));
        streamUnit.setCurrentCube(World.getInstance().getCubeByKey(1,0,0));
        World.getInstance().advanceTime(2);

        //Create boulder (2,0,0)
        streamUnit.Work(2,0,0);
        World.getInstance().advanceTime(1);
        streamUnit.Work(20);
        World.getInstance().advanceTime(25);

        //Create log at (1,1,0)
        streamUnit.Work(1,1,0);
        World.getInstance().advanceTime(1);
        streamUnit.Work(1,1,0);
        World.getInstance().advanceTime(25);

        assertTrue(World.getInstance().filterByObject(Log.class,null,streamUnit).getCurrentCube().getPosition().equals(new Position<Integer>(1,1,0)));

    }
    @Test(expected = WorldException.class)
    public void filterByObject_forUnit_ExceptionExpected() throws Exception{
        World.getInstance().filterByObject(Unit.class,null,streamUnit);
    }

    @Test
    public void filterByObject_forUnit() throws Exception {
        Unit secondUnit = new Unit("UpperLeft",0,2,0,75,75,75,75,false);
        Faction firstFaction = new Faction();
        firstFaction.addUnit(secondUnit);

        Unit thirdUnit = new Unit("Third",2,0,2,75,75,75,75,false);
        Faction secondFaction = new Faction();
        secondFaction.addUnit(thirdUnit);

        firstFaction.addUnit(streamUnit);

        example_unit.terminate();
        World.getInstance().advanceTime(55);

        Unit foundUnit = (Unit) World.getInstance().filterByObject(Unit.class,Type.ANY,streamUnit);
        assertEquals("UpperLeft",foundUnit.getName());

        Unit foundUnit2 = (Unit) World.getInstance().filterByObject(Unit.class,Type.ENEMY,streamUnit);
        assertEquals(thirdUnit,foundUnit2);
    }


    @Test
    public void filterByCube_OneTerrainTypeAsParam() throws Exception{
        assertTrue(World.getInstance().filterByCube(SceneryType.ROCK,false,streamUnit).getPosition().equals(new Position<Integer>(1,0,0)));
        assertTrue(World.getInstance().filterByCube(SceneryType.AIR,true,streamUnit).getPosition().isNeighbouringCube(new Position<Integer>(1,1,0)));
    }

    @Test
    public void filterByCube_moreTerrainTypesAsParam() throws Exception{

        //Init config
        int[][][] config = {//Global array
                {//x=0
                        {//y = 0
                                0,//z = 0
                                0,
                                3 //WorkShop placed at (0,0,2)
                        },
                        {0,0,0},
                        {0,0,0}
                },
                {//x=1  //at (1,0,0) a cube of type Rock is placed
                        {1,0,0},{0,0,0},{0,0,0}
                },
                {//x=2  //at (2,0,0) a cube of type Rock is placed , at (2,0,2) a Tree is placed
                        {1,0,2},{0,0,0},{0,0,0}
                }
        };
        World.resetInstance();
        this.config = config;
        World.getInstance().setTerrainChangeListener(new TerrainChangeListener() {

            @Override
            public void notifyTerrainChanged(int x, int y, int z) {
                for (TerrainChangeListener listener : new HashSet<TerrainChangeListener>()) {
                    listener.notifyTerrainChanged(x, y, z);
                }
            }
        });
        World.getInstance().setConfig(config);
        //Set of terrain types to query
        HashSet<SceneryType> typeSet = new HashSet<>(3);
        typeSet.add(SceneryType.ROCK);
        typeSet.add(SceneryType.TREE);
        typeSet.add(SceneryType.WORKSHOP);

        assertTrue(World.getInstance().filterByCube(typeSet,false,streamUnit.getCurrentPosition().toCubePosition()).getPosition().equals(new Position<Integer>(1,0,0)));
        assertTrue(World.getInstance().filterByCube(typeSet,false,new Position<Integer>(2,0,1)).getSceneryType() == SceneryType.TREE||
                World.getInstance().filterByCube(typeSet,false,new Position<Integer>(2,0,1)).getSceneryType() ==SceneryType.ROCK);
    }
}