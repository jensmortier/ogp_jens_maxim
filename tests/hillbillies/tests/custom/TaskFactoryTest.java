package hillbillies.tests.custom;

import hillbillies.common.internal.controller.CubeSelectionMode;
import hillbillies.gameworld.Position;
import hillbillies.model.Faction;
import hillbillies.model.Unit;
import hillbillies.model.World;
import hillbillies.part2.listener.TerrainChangeListener;
import hillbillies.part3.programs.SourceLocation;
import hillbillies.scheduler.execution.components.Task;
import hillbillies.scheduler.execution.factory.TaskFactory;
import hillbillies.scheduler.memory.expression.Expression;
import hillbillies.scheduler.memory.expression.bool.EBoolean;
import hillbillies.scheduler.memory.expression.bool.operations.executors.base.EBooleanAND;
import hillbillies.scheduler.memory.expression.bool.operations.executors.base.EBooleanNOT;
import hillbillies.scheduler.memory.expression.bool.operations.executors.base.EBooleanOR;
import hillbillies.scheduler.memory.expression.bool.operations.executors.position.OIsPassable;
import hillbillies.scheduler.memory.expression.bool.operations.executors.position.OIsSolid;
import hillbillies.scheduler.memory.expression.bool.operations.executors.unit.OCarriesItem;
import hillbillies.scheduler.memory.expression.bool.operations.executors.unit.OIsAlive;
import hillbillies.scheduler.memory.expression.bool.operations.executors.unit.OIsEnemy;
import hillbillies.scheduler.memory.expression.bool.operations.executors.unit.OIsFriend;
import hillbillies.scheduler.memory.expression.model.interfaces.EUnit;
import hillbillies.scheduler.memory.expression.model.position.*;
import hillbillies.scheduler.memory.expression.model.unit.UAny;
import hillbillies.scheduler.memory.expression.model.unit.UEnemy;
import hillbillies.scheduler.memory.expression.model.unit.UFriend;
import hillbillies.scheduler.memory.expression.model.unit.UThis;
import hillbillies.scheduler.memory.expression.variable.ERead;
import hillbillies.scheduler.memory.statement.io.SPrint;
import org.junit.Before;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.*;

/**
 * Test case for TaskFactory
 * @author Maxim Verbiest
 * @version 1.0
 */
public class TaskFactoryTest {

    private static TaskFactory factory;

    private static Faction friendlyFaction;
    private static Faction enemyFaction;

    private static Unit testUnit;
    private static Unit enemyUnit;

    @Before
    public void setUp() throws Exception {

        //Init config
        int[][][] config = {//Global array
                {//x=0
                        {//y = 0
                                0,//z = 0
                                0,
                                0
                        },
                        {0,0,0},
                        {0,0,0}
                },
                {//x=1
                        {0,0,0},{0,0,0},{0,0,0}
                },
                {//x=2
                        {0,0,0},{0,0,0},{0,0,0}
                }
        };

        World.resetInstance();
        World.getInstance().setTerrainChangeListener(new TerrainChangeListener() {

            @Override
            public void notifyTerrainChanged(int x, int y, int z) {
                for (TerrainChangeListener listener : new HashSet<TerrainChangeListener>()) {
                    listener.notifyTerrainChanged(x, y, z);
                }
            }
        });
        World.getInstance().setConfig(config);

        factory = new TaskFactory();

        friendlyFaction = new Faction();
        enemyFaction = new Faction();

        testUnit = new Unit("Tester",0,0,0,75,75,75,75,false);
        friendlyFaction.addUnit(testUnit);
        enemyUnit = new Unit("Enemy",0,0,0,75,75,75,75,false);
        enemyFaction.addUnit(enemyUnit);

    }

    @Test
    public void testCreateTasks() throws Exception {
        ERead<Expression> eRead = (ERead<Expression>) factory.createReadVariable("test", new SourceLocation(55,5));
        SPrint sPrint = (SPrint) factory.createPrint(eRead,new SourceLocation(22,2));

        List tasks = factory.createTasks("Task",50,sPrint,null);

        Task testTask = (Task) tasks.get(0);

        assertEquals("Task",testTask.getName());

    }

    @Test
    public void testCreateTasks_voidList() throws Exception {
        ERead<Expression> eRead = (ERead<Expression>) factory.createReadVariable("test", new SourceLocation(55,5));
        SPrint sPrint = (SPrint) factory.createPrint(eRead,new SourceLocation(22,2));

        LinkedList<int[]> cubes = new LinkedList<>();

        List tasks = factory.createTasks("Task",50,sPrint,cubes);

        Task testTask = (Task) tasks.get(0);

        assertEquals("Task",testTask.getName());

    }


    @Test
    public void testCreatePrint() throws Exception {
        ERead<Expression> eRead = (ERead<Expression>) factory.createReadVariable("test", new SourceLocation(55,5));
        SPrint sPrint = (SPrint) factory.createPrint(eRead,new SourceLocation(22,2));

        assertEquals(eRead,sPrint.getValue());
    }

    @Test
    public void testCreateReadVariable() throws Exception {
        ERead<Expression> eRead = (ERead) factory.createReadVariable("test", new SourceLocation(7,7));

        assertEquals(eRead.getVariableName(),"test");
    }

    @Test
    public void testCreateIsSolid() throws Exception {
        //Init config
        int[][][] config = {//Global array
                {//x=0
                        {//y = 0
                                0,//z = 0 // (0,0,1) is Rock
                                1,
                                0
                        },
                        {2,0,0}, // (0,1,0) is Tree
                        {0,0,0}
                },
                {//x=1
                        {0,0,0},{0,0,0},{0,0,0}
                },
                {//x=2
                        {0,0,0},{0,0,0},{0,0,0}
                }
        };

        World.resetInstance();
        World.getInstance().setTerrainChangeListener(new TerrainChangeListener() {

            @Override
            public void notifyTerrainChanged(int x, int y, int z) {
                for (TerrainChangeListener listener : new HashSet<TerrainChangeListener>()) {
                    listener.notifyTerrainChanged(x, y, z);
                }
            }
        });
        World.getInstance().setConfig(config);

        PLiteral pLiteral = (PLiteral) factory.createLiteralPosition(0,0,0,new SourceLocation(11,1));
        OIsSolid oIsSolid = (OIsSolid) factory.createIsSolid(pLiteral,new SourceLocation(44,4));

        assertFalse(oIsSolid.asBoolean(null));

        PLiteral pLiteral1 = (PLiteral) factory.createLiteralPosition(0,0,1, new SourceLocation(32,2));
        OIsSolid oIsSolid1 = (OIsSolid) factory.createIsSolid(pLiteral1, new SourceLocation(66,99));

        assertTrue(oIsSolid1.asBoolean(null));
    }

    @Test
    public void testCreateIsPassable() throws Exception {

        //Init config
        int[][][] config = {//Global array
                {//x=0
                        {//y = 0
                                0,//z = 0 // (0,0,1) is Rock
                                1,
                                0
                        },
                        {2,0,0}, // (0,1,0) is Tree
                        {0,0,0}
                },
                {//x=1
                        {0,0,0},{0,0,0},{0,0,0}
                },
                {//x=2
                        {0,0,0},{0,0,0},{0,0,0}
                }
        };

        World.resetInstance();
        World.getInstance().setTerrainChangeListener(new TerrainChangeListener() {

            @Override
            public void notifyTerrainChanged(int x, int y, int z) {
                for (TerrainChangeListener listener : new HashSet<TerrainChangeListener>()) {
                    listener.notifyTerrainChanged(x, y, z);
                }
            }
        });
        World.getInstance().setConfig(config);

        PLiteral pLiteral = (PLiteral) factory.createLiteralPosition(0,0,0,new SourceLocation(11,1));
        OIsPassable oIsPassable = (OIsPassable) factory.createIsPassable(pLiteral,new SourceLocation(22,2));
        assertTrue(oIsPassable.asBoolean(null));

        PLiteral pLiteral1 = (PLiteral) factory.createLiteralPosition(0,0,1, new SourceLocation(32,2));
        OIsPassable oIsPassable1 = (OIsPassable) factory.createIsPassable(pLiteral1,new SourceLocation(33,3));
        assertFalse(oIsPassable1.asBoolean(null));
    }

    @Test
    public void testCreateIsFriend() throws Exception {
        Unit friendlyUnit = new Unit("Friendo",1,0,0,75,75,75,75,false);
        friendlyFaction.addUnit(friendlyUnit);

        UFriend uFriend = (UFriend) factory.createFriend(new SourceLocation(11,1));

        OIsFriend oIsFriend = (OIsFriend) factory.createIsFriend(uFriend,new SourceLocation(22,2));
        assertTrue(oIsFriend.asBoolean(testUnit));

        UEnemy uEnemy = (UEnemy) factory.createEnemy(new SourceLocation(55,5));
        OIsFriend oIsFriend1 = (OIsFriend) factory.createIsFriend(uEnemy,new SourceLocation(66,6));
        assertFalse(oIsFriend1.asBoolean(testUnit));
    }

    @Test
    public void testCreateIsEnemy() throws Exception {
        UEnemy uEnemy= (UEnemy) factory.createEnemy(new SourceLocation(11,1));


        OIsEnemy oIsEnemy = (OIsEnemy) factory.createIsEnemy(uEnemy,new SourceLocation(22,2));

        assertTrue(oIsEnemy.asBoolean(testUnit));
    }

    @Test
    public void testCreateIsAlive() throws Exception {
        UThis uThis = (UThis) factory.createThis(new SourceLocation(11,1));
        OIsAlive oIsAlive = (OIsAlive) factory.createIsAlive(uThis,new SourceLocation(22,2));

        assertTrue(oIsAlive.asBoolean(testUnit));

        testUnit.terminate();
        assertFalse(oIsAlive.asBoolean(testUnit));
    }

    @Test
    public void testCreateCarriesItem() throws Exception {
        //Init config
        int[][][] config = {//Global array
                {//x=0
                        {//y = 0
                                0,//z = 0
                                0,
                                0
                        },
                        {2,0,0}, // (0,1,0) is Tree
                        {0,0,0}
                },
                {//x=1
                        {0,0,0},{0,0,0},{0,0,0}
                },
                {//x=2
                        {0,0,0},{0,0,0},{0,0,0}
                }
        };

        World.resetInstance();
        World.getInstance().setTerrainChangeListener(new TerrainChangeListener() {

            @Override
            public void notifyTerrainChanged(int x, int y, int z) {
                for (TerrainChangeListener listener : new HashSet<TerrainChangeListener>()) {
                    listener.notifyTerrainChanged(x, y, z);
                }
            }
        });
        World.getInstance().setConfig(config);

        testUnit.Work(0,1,0);
        testUnit.Work(20);
        World.getInstance().advanceTime(20);
        testUnit.Work(0,1,0);
        testUnit.Work(20);
        World.getInstance().advanceTime(20);

        UThis uThis = (UThis) factory.createThis(new SourceLocation(11,1));
        OCarriesItem oCarriesItem = (OCarriesItem) factory.createCarriesItem(uThis,new SourceLocation(22,2));
        assertTrue(oCarriesItem.asBoolean(testUnit));


        assertFalse(oCarriesItem.asBoolean(enemyUnit));
    }

    @Test
    public void testCreateNot() throws Exception {
        EBoolean eTrue = (EBoolean) factory.createTrue(new SourceLocation(11,1));
        EBoolean eFalse = (EBoolean) factory.createFalse(new SourceLocation(22,2));

        EBooleanNOT eBooleanNOT = (EBooleanNOT) factory.createNot(eTrue,new SourceLocation(33,3));
        EBooleanNOT eBooleanNOT1 = (EBooleanNOT) factory.createNot(eFalse,new SourceLocation(44,4));

        assertFalse(eBooleanNOT.asBoolean(null));
        assertTrue(eBooleanNOT1.asBoolean(null));
    }

    @Test
    public void testCreateAnd() throws Exception {
        EBoolean eTrue = (EBoolean) factory.createTrue(new SourceLocation(11,1));
        EBoolean eFalse = (EBoolean) factory.createFalse(new SourceLocation(22,2));
        EBoolean eFalse2 = (EBoolean) factory.createFalse(new SourceLocation( 44,4));
        EBoolean eTrue2 = (EBoolean) factory.createTrue(new SourceLocation(66,6));

        EBooleanAND eBooleanAND = (EBooleanAND) factory.createAnd(eTrue,eTrue2,new SourceLocation(33,3));
        assertTrue(eBooleanAND.asBoolean(null));

        EBooleanAND eBooleanAND1 = (EBooleanAND) factory.createAnd(eTrue,eFalse,new SourceLocation(55,5));
        assertFalse(eBooleanAND1.asBoolean(null));

        EBooleanAND eBooleanAND2 = (EBooleanAND) factory.createAnd(eFalse,eFalse,new SourceLocation( 77,7));
        assertFalse(eBooleanAND2.asBoolean(null));
    }

    @Test
    public void testCreateOr() throws Exception {
        EBoolean eTrue = (EBoolean) factory.createTrue(new SourceLocation(11,1));
        EBoolean eFalse = (EBoolean) factory.createFalse(new SourceLocation(22,2));

        EBooleanOR eBooleanOR = (EBooleanOR) factory.createOr(eTrue,eFalse,new SourceLocation(33,3));
        assertTrue(eBooleanOR.asBoolean(null));

        EBoolean eFalse2 = (EBoolean) factory.createFalse(new SourceLocation( 44,4));
        EBooleanOR eBooleanOR1 = (EBooleanOR) factory.createOr(eFalse,eFalse2,new SourceLocation(55,5));
        assertFalse(eBooleanOR1.asBoolean(null));

        EBoolean eTrue2 = (EBoolean) factory.createTrue(new SourceLocation(66,6));
        EBooleanOR eBooleanOR2 = (EBooleanOR )factory.createOr(eTrue,eTrue2, new SourceLocation(77,7));
        assertTrue(eBooleanOR2.asBoolean(null));

    }

    @Test
    public void testCreateHerePosition() throws Exception {
        PHere pHere = (PHere) factory.createHerePosition(new SourceLocation(11,1));
        assertTrue(pHere.asPosition(testUnit).equals(testUnit.getCurrentPosition()));
    }

    @Test
    public void testCreateLogPosition() throws Exception {
        //Init config
        int[][][] config = {//Global array
                {//x=0
                        {//y = 0
                                0,//z = 0
                                0,
                                0
                        },
                        {2,0,0}, // (0,1,0) is Tree
                        {0,0,0}
                },
                {//x=1
                        {0,0,0},{0,0,0},{0,0,0}
                },
                {//x=2
                        {0,0,0},{0,0,0},{0,0,0}
                }
        };

        World.resetInstance();
        World.getInstance().setTerrainChangeListener(new TerrainChangeListener() {

            @Override
            public void notifyTerrainChanged(int x, int y, int z) {
                for (TerrainChangeListener listener : new HashSet<TerrainChangeListener>()) {
                    listener.notifyTerrainChanged(x, y, z);
                }
            }
        });
        World.getInstance().setConfig(config);

        testUnit.Work(0,1,0);
        testUnit.Work(20);

        World.getInstance().advanceTime(20);

        PLog pLog = (PLog) factory.createLogPosition(new SourceLocation(66,6));

        assertTrue(pLog.asPosition(testUnit).equals(new Position<Integer>(0,1,0)));
    }

    @Test
    public void testCreateBoulderPosition() throws Exception {
        //Init config
        int[][][] config = {//Global array
                {//x=0
                        {//y = 0
                                0,//z = 0
                                0,
                                0
                        },
                        {1,0,0}, // (0,1,0) is Rock
                        {0,0,0}
                },
                {//x=1
                        {0,0,0},{0,0,0},{0,0,0}
                },
                {//x=2
                        {0,0,0},{0,0,0},{0,0,0}
                }
        };

        World.resetInstance();
        World.getInstance().setTerrainChangeListener(new TerrainChangeListener() {

            @Override
            public void notifyTerrainChanged(int x, int y, int z) {
                for (TerrainChangeListener listener : new HashSet<TerrainChangeListener>()) {
                    listener.notifyTerrainChanged(x, y, z);
                }
            }
        });
        World.getInstance().setConfig(config);

        testUnit.Work(0,1,0);
        testUnit.Work(20);

        World.getInstance().advanceTime(20);

        PBoulder pBoulder = new PBoulder(new SourceLocation(55,5));
        assertTrue(pBoulder.asPosition(testUnit).equals(new Position<Integer>(0,1,0)));
    }

    @Test
    public void testCreateWorkshopPosition() throws Exception {
        //Init config
        int[][][] config = {//Global array
                {//x=0
                        {//y = 0
                                0,//z = 0
                                0,
                                0
                        },
                        {3,0,0}, // (0,1,0) is Workshop
                        {0,0,0}
                },
                {//x=1
                        {0,0,0},{0,0,0},{0,0,0}
                },
                {//x=2
                        {0,0,0},{0,0,0},{0,0,0}
                }
        };

        World.resetInstance();
        World.getInstance().setTerrainChangeListener(new TerrainChangeListener() {

            @Override
            public void notifyTerrainChanged(int x, int y, int z) {
                for (TerrainChangeListener listener : new HashSet<TerrainChangeListener>()) {
                    listener.notifyTerrainChanged(x, y, z);
                }
            }
        });
        World.getInstance().setConfig(config);

        PWorkshop pWorkshop = (PWorkshop) factory.createWorkshopPosition(new SourceLocation(55,5));
        assertTrue(pWorkshop.asPosition(testUnit).equals(new Position<Integer>(0,1,0)));
    }

    @Test
    public void testCreateSelectedPosition() throws Exception {
        //To obtain a statement param
        EBoolean managedEBool = new EBoolean(new SourceLocation(11, 1), true);
        SPrint managedSPrint = new SPrint(new SourceLocation(33, 3), managedEBool);
        //To obtain the selected param
        int[] cube = {0,0,0};
        Task testTask = new Task("testTask",200,cube,managedSPrint);

        testTask.assignToUnit(testUnit);

        PSelected pSelected = (PSelected) factory.createSelectedPosition(new SourceLocation(44,4));
        assertTrue(pSelected.asPosition(testUnit).equals(new Position<Integer>(0,0,0)));
    }

    @Test
    public void testCreateNextToPosition() throws Exception {
        PLiteral pLiteral = (PLiteral) factory.createLiteralPosition(0,0,0,new SourceLocation(11,1));
        PNextTo pNextTo = (PNextTo) factory.createNextToPosition(pLiteral,new SourceLocation(22,2));
        assertTrue(pNextTo.asPosition(null).isNeighbouringCube(new Position<Integer>(0,0,0)));
    }

    @Test
    public void testCreatePositionOf() throws Exception {
        UThis uThis = new UThis(new SourceLocation(22,2));
        PPostionOf pPostionOf = (PPostionOf) factory.createPositionOf(uThis,new SourceLocation(11,1));
        assertTrue(testUnit.getCurrentPosition().equals(pPostionOf.asPosition(testUnit)));
    }

    @Test
    public void testCreateLiteralPosition() throws Exception {
        PLiteral pLiteral = (PLiteral) factory.createLiteralPosition(0,0,0,new SourceLocation(11,1));
        assertTrue(pLiteral.asPosition(null).equals(new Position<Integer>(0,0,0)));
    }

    @Test
    public void testCreateThis() throws Exception {
        UThis uThis = (UThis)factory.createThis(new SourceLocation(11,1));
        assertEquals(testUnit,uThis.asUnit(testUnit));
    }

    @Test
    public void testCreateFriend() throws Exception {
        Unit friendlyUnit = new Unit("Friend",1,0,0,75,75,75,75,false);
        friendlyFaction.addUnit(friendlyUnit);

        UFriend uFriend = (UFriend) factory.createFriend(new SourceLocation(11,1));
        assertEquals(friendlyUnit,uFriend.asUnit(testUnit));
    }

    @Test
    public void testCreateEnemy() throws Exception {
        UEnemy uEnemy = (UEnemy) factory.createEnemy(new SourceLocation(11,1));
        assertEquals(enemyUnit,uEnemy.asUnit(testUnit));
    }

    @Test
    public void testCreateAny() throws Exception {
        UAny anyTest = (UAny) factory.createAny(new SourceLocation(11,1));
        assertEquals(enemyUnit,anyTest.asUnit(testUnit));
    }

    @Test
    public void testCreateTrue() throws Exception {
        EBoolean trueTest = (EBoolean) factory.createTrue(new SourceLocation(22,2));
        assertTrue(trueTest.asBoolean(null));
    }

    @Test
    public void testCreateFalse() throws Exception {
        EBoolean falseTest = (EBoolean) factory.createFalse(new SourceLocation(11,1));
        assertFalse( falseTest.asBoolean( null ));
    }
}