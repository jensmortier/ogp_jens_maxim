package hillbillies.tests.custom;

import hillbillies.exceptions.TaskException;
import hillbillies.model.Unit;
import hillbillies.model.World;
import hillbillies.part2.listener.TerrainChangeListener;
import hillbillies.part3.programs.SourceLocation;
import hillbillies.scheduler.execution.components.Task;
import hillbillies.scheduler.execution.components.TaskStatus;
import hillbillies.scheduler.memory.expression.bool.EBoolean;
import hillbillies.scheduler.memory.statement.io.SPrint;
import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;

import static org.junit.Assert.*;

/**
 * Test case for Task
 * @version 1.0
 * @author Jens Mortier
 */
public class TaskTest {

    private Task testTask;

    @Before
    public void setUp() throws Exception {

        //Init config
        int[][][] config = {//Global array
                {//x=0
                        {//y = 0
                                0,//z = 0
                                0,
                                0
                        },
                        {1,0,0},
                        {0,0,0}
                },
                {//x=1
                        {0,0,0},{0,0,0},{0,0,0}
                },
                {//x=2
                        {0,0,0},{0,0,0},{0,0,0}
                }
        };

        World.resetInstance();
        World.getInstance().setTerrainChangeListener(new TerrainChangeListener() {

            @Override
            public void notifyTerrainChanged(int x, int y, int z) {
                for (TerrainChangeListener listener : new HashSet<TerrainChangeListener>()) {
                    listener.notifyTerrainChanged(x, y, z);
                }
            }
        });
        World.getInstance().setConfig(config);

        //To obtain a statement param
        EBoolean managedEBool = new EBoolean(new SourceLocation(11, 1), true);
        SPrint managedSPrint = new SPrint(new SourceLocation(33, 3), managedEBool);

        //To obtain the selected param
        int[] cube = {0,0,0};
        testTask = new Task("testTask",200,cube,managedSPrint);
    }

    @Test
    public void testSetSelectedCube() throws Exception {
        int[] selected = {0,0,0};
        testTask.setSelectedCube(selected);
        assertEquals(testTask.getSelectedCube(),selected);
    }

    @Test
    public void testSetStatus() throws Exception {
        testTask.setStatus(TaskStatus.ASSIGNED);
        assertEquals(testTask.getStatus(), TaskStatus.ASSIGNED);
    }

    @Test(expected = TaskException.class)
    public void testSetNullStatus() throws Exception{
        testTask.setStatus(null);
    }

    @Test
    public void testSetName() throws Exception {
        testTask.setName("Jens");
        assertEquals(testTask.getName(), "Jens");
    }

    @Test
    public void testSetPriority() throws Exception {
        testTask.setPriority(4);
        assertEquals(testTask.getPriority(), 4);
    }

    @Test
    public void testAssignToUnit() throws Exception {
        Unit testUnit = new Unit("Bla",1,1,1,1,1,1,1,false);
        testTask.assignToUnit(testUnit);
        assertEquals(testUnit.getTask(), testTask);
        assertEquals(testTask.getAssignedUnit(), testUnit);
    }

    @Test(expected = TaskException.class)
    public void testAssignNullUnit() throws Exception {
        testTask.assignToUnit(null);
    }

    @Test
    public void testTaskTerminate() throws Exception {
        Unit testUnit = new Unit("Bla",1,1,1,1,1,1,1,false);
        testTask.assignToUnit(testUnit);
        testTask.terminate();

        assertEquals(testUnit.getTask(), null);
        assertEquals(testTask.getAssignedUnit(),null);
    }

    @Test
    public void testDeassignUnit() throws Exception {
        Unit testUnit = new Unit("Bla",1,1,1,1,1,1,1,false);
        testTask.assignToUnit(testUnit);

        testTask.deAssignUnit();
        assertEquals(testTask.getAssignedUnit(), null);
        assertEquals(testUnit.getTask(), null);
    }

    //TODO replaced functionality to Scheduler class
    /*@Test
    public void testCurrentlyExecuted() throws Exception {
        testTask.setStatus(TaskStatus.EXECUTING);
        assertTrue(testTask.currentlyExecuted());
    }

    @Test
    public void testInterruptTask() throws Exception {
        Unit testUnit = new Unit("Bla",1,1,1,1,1,1,1,false);
        testTask.assignToUnit(testUnit);
        testTask.setStatus(TaskStatus.EXECUTING);
        testTask.setPriority(50);
        testTask.interruptTask();
        assertEquals(testTask.getPriority(),40);
        assertEquals(testTask.getStatus(), TaskStatus.NOT_ASSIGNED);
        assertEquals(testTask.getAssignedUnit(),null);

    }*/
}