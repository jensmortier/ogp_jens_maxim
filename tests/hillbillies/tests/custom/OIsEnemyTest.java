package hillbillies.tests.custom;

import hillbillies.model.Faction;
import hillbillies.model.Unit;
import hillbillies.model.World;
import hillbillies.part2.listener.TerrainChangeListener;
import hillbillies.part3.programs.SourceLocation;
import hillbillies.scheduler.memory.expression.bool.operations.executors.unit.OIsEnemy;
import hillbillies.scheduler.memory.expression.model.unit.UEnemy;
import hillbillies.scheduler.memory.expression.model.unit.UFriend;
import hillbillies.scheduler.memory.expression.model.unit.UThis;
import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;

import static org.junit.Assert.*;

/**
 * Test case for OIsEnemy
 * @version 1.0
 * @author Maxim Verbiest
 */
public class OIsEnemyTest {

    private static Unit unit1;
    private static Unit unit2;
    private static Unit friendOfUnit1;

    private static Faction faction1;
    private static Faction faction2;

    private static OIsEnemy oIsEnemy;
    private static OIsEnemy oisEnemyFalse;
    private static UEnemy uEnemy;
    private static UFriend uFriend;

    @Before
    public void setUp() throws Exception {
        //Init config
        int[][][] config = {//Global array
                {//x=0
                        {//y = 0
                                0,//z = 0
                                0,
                                0
                        },
                        {0,0,0},
                        {0,0,0}
                },
                {//x=1
                        {1,0,0},{0,0,0},{0,0,0}
                },
                {//x=2
                        {0,0,0},{0,0,0},{0,0,0}
                }
        };

        World.resetInstance();
        World.getInstance().setTerrainChangeListener(new TerrainChangeListener() {

            @Override
            public void notifyTerrainChanged(int x, int y, int z) {
                for (TerrainChangeListener listener : new HashSet<TerrainChangeListener>()) {
                    listener.notifyTerrainChanged(x, y, z);
                }
            }
        });
        World.getInstance().setConfig(config);

        faction1 = new Faction();
        faction2 = new Faction();

        unit1 = new Unit("Unit1",0,0,0,75,75,75,75,false);
        faction1.addUnit(unit1);

        unit2 = new Unit("Unit2",0,0,0,75,75,75,75,false);
        faction2.addUnit(unit2);

        friendOfUnit1 = new Unit("I'm Kind",0,0,0,75,75,75,75,false);
        faction1.addUnit(friendOfUnit1);

        uEnemy = new UEnemy(new SourceLocation(11,1));
        oIsEnemy = new OIsEnemy(uEnemy,new SourceLocation(22,2));

        uFriend = new UFriend(new SourceLocation(33,3));
        oisEnemyFalse = new OIsEnemy(uFriend,new SourceLocation(44,4));
    }

    @Test
    public void testExecute() throws Exception {

        assertTrue(oIsEnemy.asBoolean(unit2));
        assertFalse(oisEnemyFalse.asBoolean(unit1));

    }
}