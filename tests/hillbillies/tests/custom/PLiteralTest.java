package hillbillies.tests.custom;

import hillbillies.gameworld.Position;
import hillbillies.model.Unit;
import hillbillies.model.World;
import hillbillies.part2.listener.TerrainChangeListener;
import hillbillies.part3.programs.SourceLocation;
import hillbillies.scheduler.memory.exceptions.DynamicExpressionException;
import hillbillies.scheduler.memory.expression.model.position.PLiteral;
import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;

import static org.junit.Assert.*;

/**
 * Test case for PLiteral
 * @version 1.0
 * @author Maxim Verbiest
 */
public class PLiteralTest {

    private static Unit testUnit;
    private static PLiteral literalPost;

    @Before
    public void setUp() throws Exception {
        //Init config
        int[][][] config = {//Global array
                {//x=0
                        {//y = 0
                                0,//z = 0
                                0,
                                0
                        },
                        {0,0,0},
                        {0,0,0}
                },
                {//x=1
                        {0,0,0},{0,0,0},{0,0,0}
                },
                {//x=2
                        {0,0,0},{0,0,0},{0,0,0}
                }
        };

        World.resetInstance();
        World.getInstance().setTerrainChangeListener(new TerrainChangeListener() {

            @Override
            public void notifyTerrainChanged(int x, int y, int z) {
                for (TerrainChangeListener listener : new HashSet<TerrainChangeListener>()) {
                    listener.notifyTerrainChanged(x, y, z);
                }
            }
        });
        World.getInstance().setConfig(config);

        testUnit = new Unit("Jens",0,0,0,75,75,75,75,false);
        literalPost = new PLiteral(1,1,1,new SourceLocation(11,1));
    }

    @Test
    public void testExecute() throws Exception {

        assertTrue(literalPost.asPosition(testUnit).equals(new Position<Integer>(1,1,1)));

    }
    @Test(expected = DynamicExpressionException.class)
    public void testExecute_invalidPos() throws DynamicExpressionException {

        PLiteral invalidPos = new PLiteral(4,4,4,new SourceLocation(22,2));
        invalidPos.asPosition(testUnit);

    }
}