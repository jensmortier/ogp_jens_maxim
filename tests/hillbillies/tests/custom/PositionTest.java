package hillbillies.tests.custom;

import hillbillies.gameworld.Position;
import javafx.geometry.Pos;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Test case for Postition
 * @version 1.0
 * @author Jens Mortier
 */
public class PositionTest {
    Position<Integer> position;
    Position<Integer> scndPosition;

    @Before
    public void setUp() throws Exception {
        position = new Position<Integer>(1,2,3);
        scndPosition = new Position<Integer>(4,4,4);
    }

    @Test
    public void testGetPosition_x() throws Exception {
        assertEquals(position.getPosition_x().intValue(),1);
    }

    @Test
    public void testGetPosition_y() throws Exception {
        assertEquals(position.getPosition_y().intValue(),2);
    }

    @Test
    public void testGetPosition_z() throws Exception {
        assertEquals(position.getPosition_z().intValue(),3);
    }

    @Test
    public void testCalculateDistanceTo() throws Exception {
        assertEquals(position.calculateDistanceTo(scndPosition), Math.sqrt(14), 0.0005);
    }

    @Test
    public void testToVector() throws Exception {
        Integer[] vector = {1,2,3};
        assertArrayEquals(position.toVector(), vector);
    }

    @Test
    public void testEquals() throws Exception {
        assertTrue(position.equals(new Position(1, 2, 3)));
    }

    @Test
    public void testEquals_diff() throws Exception {
        assertFalse(position.equals(new Position(1,2,4)));
    }
}