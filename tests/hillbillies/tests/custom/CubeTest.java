package hillbillies.tests.custom;

import hillbillies.exceptions.CubeException;
import hillbillies.exceptions.PositionException;
import hillbillies.gameworld.Cube;
import hillbillies.gameworld.Position;
import hillbillies.gameworld.SceneryType;
import hillbillies.model.*;
import hillbillies.part2.listener.TerrainChangeListener;
import org.junit.*;
import java.util.HashSet;
import java.util.LinkedList;
import static org.junit.Assert.*;

/**
 * Test case for Cube
 * @version 2.0
 * @author  Maxim Verbiest
 *          Jens Mortier
 */
public class CubeTest {

    private static Cube testCube ;
    private static Cube neighbour;

    @Before
    public void setUpMutableFixture() throws Exception {
        //Init config
        int[][][] config = {//Global array
                {//x=0
                        {//y = 0
                                0,//z = 0
                                0,
                                0
                        },
                        {0,0,0},
                        {0,0,0}
                },
                {//x=1
                        {0,0,0},{0,0,0},{0,0,0}
                },
                {//x=2
                        {0,0,0},{0,0,0},{0,0,0}
                }
        };
        World.resetInstance();
        World.getInstance().setTerrainChangeListener(new TerrainChangeListener() {

            @Override
            public void notifyTerrainChanged(int x, int y, int z) {
                for (TerrainChangeListener listener : new HashSet<TerrainChangeListener>()) {
                    listener.notifyTerrainChanged(x, y, z);
                }
            }
        });
        World.getInstance().setConfig(config);
        testCube = new Cube(SceneryType.AIR,new Position<Integer>(1,1,1));
        neighbour = new Cube(SceneryType.AIR,new Position<Integer>(2,1,1));
    }

    @Test
    public void isNeighbourCube_neighbouringCube(){
     Assert.assertTrue(testCube.isNeighbourCube(neighbour));
    }

    @Test
    public void testCubeTypeAfterAdvance_workshop() {
        try {
            Cube test = new Cube(SceneryType.WORKSHOP,new Position<Integer>(1,1,1));
            test.advanceTime(2);
            Assert.assertEquals("Testing of scenery type stays the same",test.getSceneryType(),SceneryType.WORKSHOP);
        } catch (PositionException e) {
            e.printStackTrace();
        } catch (CubeException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testAddGameObject() throws Exception {
        testCube.addGameObject(new Log(new Position(1,1,1),World.getInstance().getCubeByKey(1,1,1)));
    }

    @Test(expected = CubeException.class)
    public void testAddGameObject_null() throws Exception {
        testCube.addGameObject(null);
    }

    @Test(expected = CubeException.class)
    public void testAddGameObject_terminated() throws Exception {
        Log log = new Log(new Position(1,1,1),World.getInstance().getCubeByKey(1,1,1));
        log.terminate();
        testCube.addGameObject(log);
    }

    @Test(expected = CubeException.class)
    public void testAddGameObject_contains() throws Exception {
        Log log = new Log(new Position(1,1,1),World.getInstance().getCubeByKey(1,1,1));
        testCube.addGameObject(log);
        testCube.addGameObject(log);
    }

    @Test
    public void testRemoveGameObject() throws Exception {
        Log log = new Log(new Position(1,1,1),World.getInstance().getCubeByKey(1,1,1));
        testCube.addGameObject(log);
        testCube.removeGameObject(log);
        Assert.assertEquals(testCube.getLog(),null);
    }

    @Test
    public void testGetLog() throws Exception {
        Log log = new Log(new Position(1,1,1),World.getInstance().getCubeByKey(1,1,1));
        testCube.addGameObject(log);
        Assert.assertEquals(testCube.getLog(),log);
    }

    @Test
    public void testGetBoulder() throws Exception {
        Boulder boulder = new Boulder(new Position(1,1,1),World.getInstance().getCubeByKey(1,1,1));
        testCube.addGameObject(boulder);
        Assert.assertEquals(testCube.getBoulder(), boulder);
    }

    @Test
    public void testTerminate() throws Exception {
        testCube.terminate();
        Assert.assertTrue(testCube.isTerminated());
    }

    @Test
    public void testContainsLog() throws Exception {
        Log log = new Log(new Position(1,1,1),World.getInstance().getCubeByKey(1,1,1));
        testCube.addGameObject(log);
        Assert.assertTrue(testCube.containsLog());
    }

    @Test
    public void testContainsBoulder() throws Exception {
        Boulder boulder = new Boulder(new Position(1,1,1),World.getInstance().getCubeByKey(1,1,1));
        testCube.addGameObject(boulder);
        Assert.assertTrue(testCube.containsBoulder());
    }

    @Test
    public void testGetNeighbourCubes_sameZAtCorner() throws CubeException {
        //Check for z-level 0
        LinkedList<Cube> groundZeros = World.getInstance().getCubeByKey(0,0,0).getNeighbourCubes_sameZ();

        boolean otherZFound = false;

        for(Cube cubes : groundZeros){
            if (cubes.getPosition().getPosition_z() != 0) otherZFound = true;
        }
        assertFalse(otherZFound);
        assertEquals(3,groundZeros.size());
    }

    @Test
    public void testGetNeighbourCubes_sameZInTheMiddle() throws Exception {
        //Check for z-level 0
        LinkedList<Cube> groundZeros = World.getInstance().getCubeByKey(1,1,0).getNeighbourCubes_sameZ();

        boolean otherZFound = false;

        for(Cube cubes : groundZeros){
            if (cubes.getPosition().getPosition_z() != 0) otherZFound = true;
        }
        assertFalse(otherZFound);
        assertEquals(8,groundZeros.size());
    }

    @Test
    public void testGetStableNeighbour() throws Exception{
        // in a 3x3x3 world it is always possible to find a stable neighbour
        // at z = 0 , or a boundary cube is stated as stable..
        assertTrue(World.getInstance().getCubeByKey(0,0,0).getStableNeighbour().hasSolidNeighbour());
    }

    @Test
    public void testHasPotentialEnemy() throws Exception{
        Faction friendlyFaction = new Faction();
        Faction enemyFaction = new Faction();
        Unit enemy = new Unit("Enemy",1.0,1.0,1.0,75,75,75,75,false);
        enemyFaction.addUnit(enemy);

        testCube.addGameObject(enemy);

        assertTrue(testCube.hasPotentialEnemy(friendlyFaction));
    }

    @Test
    public void testGetRandomEnemy() throws Exception{
        Faction friendlyFaction = new Faction();
        Faction enemyFaction = new Faction();
        Unit enemy = new Unit("Enemy",1.0,1.0,1.0,75,75,75,75,false);
        enemyFaction.addUnit(enemy);

        testCube.addGameObject(enemy);

        assertEquals(enemy,testCube.getRandomEnemy(friendlyFaction));

    }

    @Test
    public void testGetEnemies() throws Exception{
        Faction friendlyFaction = new Faction();
        Faction enemyFaction = new Faction();
        Unit enemy = new Unit("Enemy",1.0,1.0,1.0,75,75,75,75,false);
        enemyFaction.addUnit(enemy);

        testCube.addGameObject(enemy);

        assertTrue(testCube.getEnemies(friendlyFaction).size() == 1);
        assertTrue(testCube.getEnemies(friendlyFaction).contains(enemy));
    }

    @Test
    public void testHasPotentialEnemies_inCorner() throws Exception{
        //Init config
        int[][][] config = {//Global array
                {//x=0
                        {//y = 0
                                0,//z = 0
                                0,
                                0
                        },
                        {0,0,0},
                        {0,0,0}
                },
                {//x=1
                        {0,0,0},{0,0,0},{0,0,0}
                },
                {//x=2
                        {0,0,0},{0,0,0},{0,0,0}
                }
        };
        World.resetInstance();
        World.getInstance().setTerrainChangeListener(new TerrainChangeListener() {

            @Override
            public void notifyTerrainChanged(int x, int y, int z) {
                for (TerrainChangeListener listener : new HashSet<TerrainChangeListener>()) {
                    listener.notifyTerrainChanged(x, y, z);
                }
            }
        });
        World.getInstance().setConfig(config);

        Unit friendlyUnit = new Unit("Friend",1,0,0,75,75,75,75,false);
        Unit enemyUnit = new Unit("Enemy",0,0,0,75,75,75,75,false);

        Faction friendlyFaction = new Faction();
        friendlyFaction.addUnit(friendlyUnit);

        Faction enemyFaction = new Faction();
        enemyFaction.addUnit(enemyUnit);

        assertTrue(World.getInstance().getCubeByKey(0,0,0).hasPotentialEnemy(friendlyFaction));


    }

}
