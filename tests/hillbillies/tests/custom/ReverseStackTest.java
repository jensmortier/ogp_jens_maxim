package hillbillies.tests.custom;

import hillbillies.util.ReverseStack;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Test case for ReverseStack
 * @version 1.0
 * @author Jens Mortier
 */
public class ReverseStackTest {

    private ReverseStack<String> stackTest;

    @Before
    public void setupObject() {
        stackTest = new ReverseStack<>();
    }

    @Test
    public void testAddElement() throws Exception {
        stackTest.addElement("Element 1");
        assertEquals("Get first element check",stackTest.getFirst(),"Element 1");
        assertEquals("Pop element check",stackTest.popElement(),"Element 1");
        assertEquals("Check empty",stackTest.isEmpty(),true);
    }
}