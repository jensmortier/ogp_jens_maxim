package hillbillies.tests.custom;

import hillbillies.exceptions.CubeException;
import hillbillies.exceptions.PositionException;
import hillbillies.gameworld.Cube;
import hillbillies.gameworld.Position;
import hillbillies.gameworld.SceneryType;

/**
 * A type test
 * @version 1.0
 * @author Jens Mortier
 */
public class TypeTest {

    public static void main(String[] args) throws PositionException, CubeException {
        Position<Double> test = new Position<Double>(0.1,0.1,0.1);
        System.out.println(test.getClass());
    }
}
