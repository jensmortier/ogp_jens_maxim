package hillbillies.tests.custom;

import hillbillies.gameworld.Position;
import hillbillies.model.Unit;
import hillbillies.model.World;
import hillbillies.part2.listener.TerrainChangeListener;
import hillbillies.part3.programs.SourceLocation;
import hillbillies.scheduler.memory.expression.model.position.PWorkshop;
import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;

import static org.junit.Assert.*;

/**
 * Test case for PWorkshop
 * @version 1.0
 * @author Maxim Verbiest
 */
public class PWorkshopTest {

    private static Unit testUnit;
    private static PWorkshop workshop;

    @Before
    public void setUp() throws Exception {
        //Init config
        int[][][] config = {//Global array
                {//x=0
                        {//y = 0
                                0,//z = 0
                                0,
                                0
                        },
                        {0,0,0},
                        {0,0,0}
                },
                {//x=1
                        {0,0,0},{0,0,0},{0,0,0}
                },
                {//x=2
                        {0,0,0},{3,0,0},{0,0,0} // at (2,1,0) a workshop is placed
                }
        };

        World.resetInstance();
        World.getInstance().setTerrainChangeListener(new TerrainChangeListener() {

            @Override
            public void notifyTerrainChanged(int x, int y, int z) {
                for (TerrainChangeListener listener : new HashSet<TerrainChangeListener>()) {
                    listener.notifyTerrainChanged(x, y, z);
                }
            }
        });
        World.getInstance().setConfig(config);
        testUnit = new Unit("Workshopper",0,0,0,75,75,75,75,false);
        workshop = new PWorkshop(new SourceLocation(11,1));
    }

    @Test
    public void testExecute() throws Exception {

        assertTrue(workshop.asPosition(testUnit).equals(new Position<Integer>(2,1,0)));

    }
}