package hillbillies.tests.custom;

import hillbillies.DefaultBehaviour.DefaultAttack;
import hillbillies.model.Faction;
import hillbillies.model.Unit;
import hillbillies.model.World;
import hillbillies.part2.listener.TerrainChangeListener;
import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;

import static org.junit.Assert.*;

/**
 * Test case for Default Attack
 * @version 1.0
 * @author Maxim Verbiest
 */
public class DefaultAttackTest {

    DefaultAttack attackObject;
    Unit testUnit;

    @Before
    public void setUp() throws Exception {

        //Init config
        int[][][] config = {//Global array
                {//x=0
                        {//y = 0
                                0,//z = 0
                                0,
                                0
                        },
                        {0,0,0},
                        {0,0,0}
                },
                {//x=1
                        {0,0,0},{0,0,0},{0,0,0}
                },
                {//x=2
                        {0,0,0},{0,0,0},{0,0,0}
                }
        };
        World.resetInstance();
        World.getInstance().setTerrainChangeListener(new TerrainChangeListener() {

            @Override
            public void notifyTerrainChanged(int x, int y, int z) {
                for (TerrainChangeListener listener : new HashSet<TerrainChangeListener>()) {
                    listener.notifyTerrainChanged(x, y, z);
                }
            }
        });
        World.getInstance().setConfig(config);


    }

    @Test
    public void testCanBeExecuted() throws Exception {

        Faction friendlyFaction = new Faction();
        Faction enemyFaction = new Faction();

        Unit enemyUnit= new Unit("Enemy",0,0,0,75,75,75,75,false);
        enemyFaction.addUnit(enemyUnit);

        testUnit = new Unit("Tester",1,0,0,75,75,75,75,false);
        friendlyFaction.addUnit(testUnit);

        World.getInstance().advanceTime(2);

        attackObject = new DefaultAttack(testUnit);

        assertTrue(attackObject.canBeExecuted());
    }
}