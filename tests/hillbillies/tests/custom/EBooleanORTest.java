package hillbillies.tests.custom;

import hillbillies.part3.programs.SourceLocation;
import hillbillies.scheduler.memory.exceptions.DynamicExpressionException;
import hillbillies.scheduler.memory.expression.bool.EBoolean;
import hillbillies.scheduler.memory.expression.bool.operations.executors.base.EBooleanOR;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Test case for EBooleanOR
 * @version 1.0
 * @author Maxim Verbiest
 */
public class EBooleanORTest {

    private static EBooleanOR orBool;

    private static EBoolean leftOperand;

    private static EBoolean rightOperand;

    @Before
    public void setUp() throws Exception {

        leftOperand = new EBoolean(new SourceLocation(7,8));
        rightOperand = new EBoolean(new SourceLocation(10,0));
        orBool = new EBooleanOR(leftOperand,rightOperand,new SourceLocation(55,5));

    }

    @Test
    public void testExecute_FT() throws Exception {

        leftOperand.setBoolean(false);
        rightOperand.setBoolean(true);

        assertTrue(orBool.asBoolean(null));
    }

    @Test
    public void testExecute_FF() throws DynamicExpressionException {

        leftOperand.setBoolean(false);
        rightOperand.setBoolean(false);

        assertFalse(orBool.asBoolean(null));
    }

    @Test
    public void testExecute_TT()throws Exception{
        leftOperand.setBoolean(true);
        rightOperand.setBoolean(true);
        assertTrue(orBool.asBoolean(null));
    }
}