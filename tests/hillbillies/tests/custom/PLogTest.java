package hillbillies.tests.custom;

import hillbillies.gameworld.Position;
import hillbillies.model.Unit;
import hillbillies.model.World;
import hillbillies.part2.listener.TerrainChangeListener;
import hillbillies.part3.programs.SourceLocation;
import hillbillies.scheduler.memory.expression.model.position.PLog;
import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;

import static org.junit.Assert.*;

/**
 * Test case for PLog
 * @version 1.0
 * @author Maxim Verbiest
 */
public class PLogTest {

    private static Unit testUnit;

    @Before
    public void setUp() throws Exception {
        //Init config
        int[][][] config = {//Global array
                {//x=0
                        {//y = 0
                                0,//z = 0
                                0,
                                0
                        },
                        {2,0,0}, //At (0,1,0) a Tree is planted, this will be chopped to a log
                        {0,0,0}
                },
                {//x=1
                        {0,0,0},{0,0,0},{0,0,0}
                },
                {//x=2
                        {0,0,0},{0,0,0},{0,0,0}
                }
        };

        World.resetInstance();
        World.getInstance().setTerrainChangeListener(new TerrainChangeListener() {

            @Override
            public void notifyTerrainChanged(int x, int y, int z) {
                for (TerrainChangeListener listener : new HashSet<TerrainChangeListener>()) {
                    listener.notifyTerrainChanged(x, y, z);
                }
            }
        });
        World.getInstance().setConfig(config);

        testUnit = new Unit("Chopper",0,0,0,75,75,75,75,false);
    }

    @Test
    public void testExecute() throws Exception {

        PLog logPos = new PLog(new SourceLocation(11,1));
        testUnit.Work(0,1,0);
        World.getInstance().advanceTime(25);
        assertTrue(logPos.asPosition(testUnit).equals(new Position<Integer>(0,1,0)));

    }
}