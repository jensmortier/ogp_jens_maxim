package hillbillies.tests.custom;

import hillbillies.model.Unit;
import hillbillies.model.World;
import hillbillies.part2.listener.TerrainChangeListener;
import hillbillies.part3.programs.SourceLocation;
import hillbillies.scheduler.memory.exceptions.DynamicExpressionException;
import hillbillies.scheduler.memory.expression.bool.operations.executors.unit.OCarriesItem;
import hillbillies.scheduler.memory.expression.model.interfaces.EUnit;
import hillbillies.scheduler.memory.expression.model.unit.UThis;
import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;

import static org.junit.Assert.*;

/**
 * Test case for OCarriesItem
 * @version 1.0
 * @author Maxim Verbiest
 */
public class OCarriesItemTest {

    private static UThis uThis;
    private static Unit testUnit;
    private static OCarriesItem oCarriesItem;

    @Before
    public void setUp() throws Exception {

        //Init config
        int[][][] config = {//Global array
                {//x=0
                        {//y = 0
                                0,//z = 0
                                0,
                                0
                        },
                        {0,0,0},
                        {0,0,0}
                },
                {//x=1
                        {1,0,0},{0,0,0},{0,0,0}// at (1,0,0) a Rock Cube is placed, here will be worked on ot obtain a boulder
                },
                {//x=2
                        {0,0,0},{0,0,0},{0,0,0}
                }
        };

        World.resetInstance();
        World.getInstance().setTerrainChangeListener(new TerrainChangeListener() {

            @Override
            public void notifyTerrainChanged(int x, int y, int z) {
                for (TerrainChangeListener listener : new HashSet<TerrainChangeListener>()) {
                    listener.notifyTerrainChanged(x, y, z);
                }
            }
        });
        World.getInstance().setConfig(config);

        uThis = new UThis(new SourceLocation(11,1));
        testUnit = new Unit("Tester",0,0,0,75,75,75,75,false);
        oCarriesItem = new OCarriesItem(uThis,new SourceLocation(22,2));

    }

    @Test
    public void testExecute() throws Exception {
        //Unit isn't carrying anything at the start
        assertFalse(oCarriesItem.asBoolean(testUnit));

        //let the unit work to create a boulder
        testUnit.Work(1,0,0);
        World.getInstance().advanceTime(30);

        //let the unit pick up this boulder
        testUnit.Work(1,0,0);
        World.getInstance().advanceTime(30);

        //Normally the unit must now hold a boulder
        assertTrue(oCarriesItem.asBoolean(testUnit));
    }
}