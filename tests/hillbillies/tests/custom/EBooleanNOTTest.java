package hillbillies.tests.custom;

import hillbillies.part3.programs.SourceLocation;
import hillbillies.scheduler.memory.expression.bool.EBoolean;
import hillbillies.scheduler.memory.expression.bool.operations.executors.base.EBooleanNOT;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Test case for EBooleanNOT
 * @version 1.0
 * @author Maxim Verbiest
 */
public class EBooleanNOTTest {

    private static EBooleanNOT notBool;
    private static EBoolean operand;

    @Before
    public void setUp() throws Exception {
        operand = new EBoolean(new SourceLocation(5,5));
        notBool = new EBooleanNOT(operand,new SourceLocation(6,6));
    }

    @Test
    public void testExecute_OisFalse() throws Exception {
        operand.setBoolean(false);
        assertTrue(notBool.asBoolean(null));
    }

    @Test
    public void testExecute_OisTrue() throws Exception {
        operand.setBoolean(true);
        assertFalse(notBool.asBoolean(null));
    }
}