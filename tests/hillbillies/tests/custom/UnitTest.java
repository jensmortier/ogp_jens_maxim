package hillbillies.tests.custom;

import static org.junit.Assert.*;

import hillbillies.exceptions.*;
import hillbillies.gameworld.Position;
import hillbillies.model.*;
import hillbillies.part2.listener.TerrainChangeListener;
import org.junit.*;

import java.util.HashSet;

/**
 * A class collecting tests for the class of Unit
 * @version 1.0
 * @author Maxim Verbiest
 * 			Jens Mortier
 *
 */
public class UnitTest {
	
	
	private static Unit firstUnit;
	private static Unit lowWeightUnit;
	private static Unit heavyUnit;
	private static Unit neighbourFirstUnit;
	private static Unit noNeighbours;
	private static Unit	hulkUnit;
	
	/**
	 * Set up a mutable test fixture
	 * @throws UnitException
	 */
	@Before
	public void setUpMutableFixture() throws UnitException, WorldException {

		//Init config
		int[][][] config = {//Global array
				{//x=0
						{//y = 0
								0,//z = 0
								0,
								0
						},
						{1,0,0},
						{0,0,0}
				},
				{//x=1
						{0,0,0},{0,0,0},{0,0,0}
				},
				{//x=2
						{0,0,0},{0,0,0},{0,0,0}
				}
		};

		World.resetInstance();
		World.getInstance().setTerrainChangeListener(new TerrainChangeListener() {

			@Override
			public void notifyTerrainChanged(int x, int y, int z) {
				for (TerrainChangeListener listener : new HashSet<TerrainChangeListener>()) {
					listener.notifyTerrainChanged(x, y, z);
				}
			}
		});
		World.getInstance().setConfig(config);


		firstUnit = new Unit("Maxim Verbiest",0.0,0.0,0.0,75,50,50,50,false);
		lowWeightUnit = new Unit("Jaghen N'har",0.0,0.0,0.0,0,0,0,105,false);
		heavyUnit = new Unit("Maxim Verbiest",0.0,0.0,0.0,150,50,50,50,false);
		neighbourFirstUnit = new Unit("I'm a good neighbour",1.0,0.0,0.0,75,50,50,50,false);
		noNeighbours = new Unit("I want to be alone",2.5,2.5,2.5,75,50,50,50,false);
		hulkUnit = new Unit("Hulk",0.0,0.0,0.0,75,50,50,50,false);
	}

	@Test
	public void isValidPosition_ValidPosition(){
		assertTrue(World.isValidPosition(1, 1, 1));
	}
	
	@Test
	public void isValidPosition_OutOfBounds(){
		assertFalse(World.isValidPosition(51, 4, 9));
	}
	
	@Test
	public void isValidPosition_Negatives(){
		assertFalse(World.isValidPosition(-5, 5, 9));
	}
	
	@Test
	public void isValidName_ValidName(){
		assertTrue(Unit.isValidName("Maxim verbiest'007"));
	}
	
	@Test
	public void getCubePosition_MatchesUnitPosition(){
		double Unit_posX = firstUnit.getCurrentPosition().getPosition_x();
		int Cube_posX = firstUnit.getCurrentCube().getPosition().getPosition_x();
		assertEquals((int) Math.floor(Unit_posX),Cube_posX);
		
	}

	@Test
	public void isValidName_NoUpperCase(){
		assertFalse(Unit.isValidName("maxim verbiest007"));
	}
	
	@Test
	public void isValidName_NameTooShort(){
		assertFalse(Unit.isValidName("A"));
	}
	
	@Test
	public void setInitialStrength_Valid(){
		assertEquals(50,firstUnit.getStrength());
	}
	
	@Test
	public void setInititalStrength_TooLow(){
		assertEquals(lowWeightUnit.getMinInitialValue(),lowWeightUnit.getStrength());
		
	}
	
	@Test
	public void setInitialToughness_TooHigh(){
		assertEquals(lowWeightUnit.getMaxInitialValue(),lowWeightUnit.getToughness());
	}

	@Test
	public void setInitialWeight_Valid(){
		assertEquals(75,firstUnit.getWeight());
	}

	@Test
	public void setInitialWeight_TooLow(){
		assertEquals(25,lowWeightUnit.getWeight());
	}
	
	@Test
	public void setInitialWeight_TooHigh(){
		assertEquals(100, heavyUnit.getWeight());
	}

	@Test
	public void isValidOrientation_Valid(){
		assertTrue(Unit.isValidOrientation(3));
	}
	
	@Test
	public void isValidOrientation_Negative(){
		assertFalse(Unit.isValidOrientation(-3));
	}
	
	@Test
	public void isValidOrientation_TooHigh(){
		assertFalse(Unit.isValidOrientation(8));
	}
	
	@Test
	public void setOrientation_LegalCase() throws UnitException{
		
			firstUnit.setOrientation(3);
			//Delta parameter must be given while comparing floats
			assertEquals(3,firstUnit.getOrientation(),0.01);
			
	}

	@Test
	public void getMaxPoints_firstUnit(){
		assertEquals(75,firstUnit.getMaxPoints(),0.01);
	}
	
	@Test
	public void canHaveAsPoints_ValidPoints(){
		assertTrue(firstUnit.canHaveAsPoints(33));
	}
	
	@Test
	public void canHaveAsPoints_Negative(){
		assertFalse(firstUnit.canHaveAsPoints(-55));
	}
	
	@Test
	public void canHaveAsPoints_TooHigh(){
		// firstUnit maxPoints = 200*(75/100)*(1/2) = 75
		assertFalse(firstUnit.canHaveAsPoints(76));
	}
	
	@Test
	public void getMaxPoits_firstUnit(){
		assertEquals(75,firstUnit.getMaxPoints(),0.01);
	}
	
	@Test
	public void isNeighbouringCube_LegalCase(){
		assertTrue(firstUnit.isNeighbouringCube(1, 1, 0));
	}
	

	@Test
	public void isNeighbouringCube_SameCube(){
		assertFalse(firstUnit.isNeighbouringCube(0, 0, 0));
	}

	
	@Test
	public void isNeighbouringCube_invalidCube(){
		assertFalse(firstUnit.isNeighbouringCube(2, 0, 0));
	}
	
	@Test
	public void isNeigbouringCube_UnitOnNeighBouringCube(){
		assertTrue(firstUnit.isNeighbouringCube(neighbourFirstUnit));
	}
	
	@Test
	public void isNeighbouringCube_UnitOnFurtherCube(){
		assertFalse(firstUnit.isNeighbouringCube(noNeighbours));
	
	}
	
	@Test
	public void Walk_CheckMovement(){
		firstUnit.Walk();
		Assert.assertEquals(MovementType.WALKING, firstUnit.getMovementStatus());
	}

	@Test
	public void getBaseSpeed_firstUnit(){
		assertEquals(1,firstUnit.getBaseSpeed(),0.01);
	}

	@Test
	public void getMovementStatus_After1secondFighting() throws UnitException, FactionException {
		Faction first = new Faction();
		Faction second = new Faction();

		first.addUnit(firstUnit);
		second.addUnit(heavyUnit);

		firstUnit.Attack(heavyUnit);
		firstUnit.Attack(1.001);
		assertEquals(MovementType.DOING_NOTHING,firstUnit.getMovementStatus());
				
	}
	@Test
	public void getMovementStatus_AfterLessThan1secondFighting() throws UnitException, FactionException {
		Faction first = new Faction();
		Faction second = new Faction();

		first.addUnit(firstUnit);
		second.addUnit(heavyUnit);

		firstUnit.Attack(heavyUnit);
		firstUnit.Attack(0.5);
		assertEquals(MovementType.FIGHTING, firstUnit.getMovementStatus());
	}
	
	@Test
	public void Attack_FacingEachother() throws UnitException, FactionException {
		Faction first = new Faction();
		Faction second = new Faction();

		first.addUnit(firstUnit);
		second.addUnit(heavyUnit);

		firstUnit.Attack(heavyUnit);
		assertEquals(firstUnit.getOrientation(), firstUnit.getOrientation(),0.001);
		
		
	}

	@Test
	public void Defend_MovementStatusWhenNeighbouringCube() throws UnitException, FactionException {

		Faction first = new Faction();
		Faction second = new Faction();

		first.addUnit(firstUnit);
		second.addUnit(neighbourFirstUnit);
		firstUnit.Attack(neighbourFirstUnit);
		firstUnit.advanceTime(0.2);
		neighbourFirstUnit.advanceTime(0.2);

		assertEquals(MovementType.DEFENDING, neighbourFirstUnit.getMovementStatus());
	}


	@Test
	public void Defend_mustDodge() throws UnitException {
		firstUnit.setAgility(100);
		lowWeightUnit.setAgility(20);
		//Now if the second unit attacks the first, the probability of a successful dodge is 1
		assertTrue(firstUnit.isSuccessfulDodge(lowWeightUnit));
	}

	@Test
	public void Rest_checkHP() throws UnitException{
		firstUnit.setStaminaPoints(50);
		firstUnit.setHitPoints(50);
		firstUnit.Rest();
		firstUnit.Rest(0.8);
		assertEquals(51,firstUnit.getHitPoints());
	}
	
	@Test
	public void Rest_checkSPwhileHPnotFull() throws UnitException{
		firstUnit.setStaminaPoints(50);
		firstUnit.setHitPoints(50);
		firstUnit.Rest();
		firstUnit.Rest(0.8);
		//SP shouldn't be updated because HP are nut fully recovered
		assertEquals(50,firstUnit.getStaminaPoints());
	}
	
	@Test
	public void Rest_checkHPwhenTransitionFromHPtoSP() throws UnitException{
		firstUnit.setHitPoints(firstUnit.getMaxPoints()-1);
		firstUnit.setStaminaPoints(50);
		firstUnit.Rest();
		firstUnit.Rest(1.2);
		assertEquals(firstUnit.getMaxPoints(),firstUnit.getHitPoints(),0.01);
	}
	
	@Test
	public void Rest_checkSPwhenTransitionFromHPtoSP() throws UnitException{
		firstUnit.setHitPoints(firstUnit.getMaxPoints()-1);
		firstUnit.setStaminaPoints(50);
		firstUnit.Rest();
		firstUnit.Rest(0.8);
		firstUnit.Rest(0.4);
		assertEquals(51,firstUnit.getStaminaPoints());
	}
	
	@Test
	public void Rest_CheckSPwhileHPFull() throws UnitException{
		
		firstUnit.setStaminaPoints(50);
		firstUnit.setHitPoints(firstUnit.getMaxPoints());
		firstUnit.Rest();
		firstUnit.Rest(0.4);
		assertEquals(51,firstUnit.getStaminaPoints());		
	}

	@Test
	public void getHitPoints_AfterWeightUpdateMaxValueAffected(){
		
		firstUnit.setHitPoints(firstUnit.getMaxPoints());
		firstUnit.setStaminaPoints(firstUnit.getMaxPoints());
		firstUnit.setWeight(51);
		assertEquals(51,firstUnit.getHitPoints());

	}
	
	@Test
	public void getHitPoints_AfterWeightUpdateMaxValueNotAffected(){
		
		firstUnit.setHitPoints(74);
		firstUnit.setStaminaPoints(74);
		firstUnit.setWeight(99);
		assertEquals(74,firstUnit.getHitPoints());
	}
	
	@Test
	public void getStaminaPoints_AfterWeightUpdateMaxValueAffected(){
		
		firstUnit.setHitPoints(firstUnit.getMaxPoints());
		firstUnit.setStaminaPoints(firstUnit.getMaxPoints());
		firstUnit.setWeight(51);
		assertEquals(51,firstUnit.getStaminaPoints());
		
	}
	
	@Test
	public void getStaminaPoints_AfterWeightUpdateMaxValueNotAffected(){
		
		firstUnit.setHitPoints(74);
		firstUnit.setStaminaPoints(74);
		firstUnit.setWeight(99);
		assertEquals(74,firstUnit.getHitPoints());
	}
	
	@Test
	public void getStaminaPoints_AfterToughnessUpdateMaxValueAffected(){
		firstUnit.setHitPoints(firstUnit.getMaxPoints());
		firstUnit.setStaminaPoints(firstUnit.getMaxPoints());
		firstUnit.setToughness(25);
		assertEquals(37,firstUnit.getStaminaPoints(),0.01);
	}
	
	@Test
	public void getStaminaPoints_AfterToughnessUpdateMaxValueNotAffected(){
		firstUnit.setHitPoints(firstUnit.getMaxPoints());
		double x = firstUnit.getMaxPoints();
		firstUnit.setStaminaPoints(firstUnit.getMaxPoints());
		firstUnit.setToughness(75);
		assertEquals(x,firstUnit.getStaminaPoints(),0.01);
	}
	
	@Test
	public void getWeight_AfterStrengthUpdateMinValueNotAffected(){
		
		firstUnit.setStrength(150);
		assertEquals(100,firstUnit.getWeight());
		
	}
	
	@Test
	public void getWeight_AfterStrengthUpdateDoubleMinValue(){
		firstUnit.setStrength(199);
		assertEquals(124,firstUnit.getWeight());
	}
	
	@Test
	public void getWeight_afterAgilityUpdateDoubleMinValue(){
		firstUnit.setAgility(199);
		assertEquals(124,firstUnit.getWeight());
	}
	
	@Test
	public void stopDefaultBehaviour_checkMovementStatus() throws UnitException{
		firstUnit.StopDefaultBehaviour();
		assertEquals(MovementType.DOING_NOTHING,firstUnit.getMovementStatus());
		
	}
	
	@Test
	public void stopDefaultBehaviour_checkDefaultBehaviourStatus() throws UnitException{
		firstUnit.StopDefaultBehaviour();
		assertFalse(firstUnit.getDefaultBehaviourStatus());
	}

	@Test
	public void setLog() throws MaterialException, PositionException {
		firstUnit.setLog(new Log(new Position<Double>(0.0,0.0,0.0),	World.getInstance().getCubeByKey(0,0,0)));
		assertTrue(firstUnit.isCarryingLog());
	}

	@Test
	public void setBoulder() throws PositionException, MaterialException {
		firstUnit.setBoulder(new Boulder(new Position<Double>(0.0,0.0,0.0),	World.getInstance().getCubeByKey(0,0,0)));
		assertTrue(firstUnit.isCarryingBoulder());
	}

	@Test
	public void getWorkToBeDone_AIR() throws UnitException {
		firstUnit.Work(0,0,0);
		firstUnit.advanceTime(0.2);
		assertEquals(firstUnit.getMovementStatus(),MovementType.WORKING);
		assertEquals(firstUnit.getWorkToBeDone(),WorkType.NOTHING);
	}

	@Test
	public void getWorkToBeDone_ROCK() throws UnitException {
		firstUnit.Work(0,1,0);
		firstUnit.advanceTime(0.2);
		assertEquals(firstUnit.getWorkToBeDone(),WorkType.COLLAPSE_ROCK_CUBE);
		assertFalse(firstUnit.getWorkToBeDone() == WorkType.CONSUME_BOULDER_AND_LOG);
	}

	@Test
	public void increaseXP_Positive() throws UnitException {
		firstUnit.increaseXP(2);
		assertEquals(firstUnit.getNewXPEarned(),2);
	}

	@Test(expected = UnitException.class)
	public void increaseXP_Negative() throws UnitException{
		firstUnit.increaseXP(-4);
	}

	@Test
	public void canRaise_Yes() throws UnitException {
		firstUnit.increaseXP(20);
		assertTrue(firstUnit.canRaise());
	}

	@Test
	public void canRaise_No() throws UnitException {
		firstUnit.increaseXP(5);
		assertFalse(firstUnit.canRaise());
	}

	@Test
	public void hasMaxStrenght_No(){
		assertFalse(firstUnit.hasMaxStrength());
	}

	@Test
	public void hasMaxStrenght_Yes(){
		hulkUnit.setStrength(200);
		assertTrue(hulkUnit.hasMaxStrength());
	}

	@Test
	public void testFollow() throws Exception{
		Unit follower = new Unit("I follow", 0.5,0.5,0.5,75,75,75,75,false);
		Unit leader = new Unit("Leader",2.5,2.5,0.5,75,75,75,75,false);

		follower.Follow(leader);
		World.getInstance().advanceTime(0.2);
		assertTrue(follower.isFollowing());

		World.getInstance().advanceTime(0.2);
		assertTrue(leader.getCurrentCube().getPosition().equals(follower.getFollowPosition()));

	}
}
