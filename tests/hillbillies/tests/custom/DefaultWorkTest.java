package hillbillies.tests.custom;

import hillbillies.DefaultBehaviour.DefaultWork;
import hillbillies.gameworld.Cube;
import hillbillies.gameworld.Position;
import hillbillies.model.Unit;
import hillbillies.model.World;
import hillbillies.part2.listener.TerrainChangeListener;
import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;
import java.util.LinkedList;

import static org.junit.Assert.*;

/**
 * Test class for DefaultWork
 * @version 1.0
 * @author Maxim Verbiest
 */
public class DefaultWorkTest {


    private static Unit workUnit;

    private static DefaultWork workClass;

    @Before
    public void setUp() throws Exception {

        //Init config
        int[][][] config = {//Global array
                {//x=0
                        {//y = 0
                                1,//z = 0
                                0,
                                0
                        },
                        {3,0,0},
                        {1,0,0}
                },
                {//x=1
                        {2,0,0},{0,0,0},{3,0,0}
                },
                {//x=2
                        {1,0,0},{3,0,0},{1,0,0}
                }
        };

        World.resetInstance();
        World.getInstance().setTerrainChangeListener(new TerrainChangeListener() {

            @Override
            public void notifyTerrainChanged(int x, int y, int z) {
                for (TerrainChangeListener listener : new HashSet<TerrainChangeListener>()) {
                    listener.notifyTerrainChanged(x, y, z);
                }
            }
        });
        World.getInstance().setConfig(config);

        //Unit standing in the middle of the world at z = 0
        workUnit = new Unit("Worker007",1.0,1.0,0.0,75,75,75,75,false);

        workClass = new DefaultWork(workUnit);


    }


    @Test
    public void testFillList_EightPossible() throws Exception {

        LinkedList<Cube> possibleCubes = workClass.FillList();

        assertTrue(possibleCubes.size() == 8);

    }

    @Test
    public void testFillList_atCorner() throws Exception{
        //Create a World with 3 rocks around the lower left edge
        int[][][] config = {//Global array
                {//x=0
                        {//y = 0
                                0,//z = 0
                                0,
                                0
                        },
                        {1,0,0},
                        {0,0,0}
                },
                {//x=1
                        {1,0,0},{1,0,0},{0,0,0}
                },
                {//x=2
                        {0,0,0},{0,0,0},{0,0,0}
                }
        };

        World.resetInstance();
        World.getInstance().setTerrainChangeListener(new TerrainChangeListener() {

            @Override
            public void notifyTerrainChanged(int x, int y, int z) {
                for (TerrainChangeListener listener : new HashSet<TerrainChangeListener>()) {
                    listener.notifyTerrainChanged(x, y, z);
                }
            }
        });
        World.getInstance().setConfig(config);

        //Get a Unit at the edge
        workUnit.getCurrentPosition().setPosition(new Position(0.5,0.5,0));

        assertTrue(3 == workClass.FillList().size());
    }

    @Test
    public void testGetWorkCube_interestingCubeOK() throws Exception{

        //Create a game world with a Rock cube at (0,0,0)
        int[][][] config = {//Global array
                {//x=0
                        {//y = 0
                                1,//z = 0
                                0,
                                0
                        },
                        {0,0,0},
                        {0,0,0}
                },
                {//x=1
                        {0,0,0},{0,0,0},{0,0,0}
                },
                {//x=2
                        {0,0,0},{0,0,0},{0,0,0}
                }
        };

        World.resetInstance();
        World.getInstance().setTerrainChangeListener(new TerrainChangeListener() {

            @Override
            public void notifyTerrainChanged(int x, int y, int z) {
                for (TerrainChangeListener listener : new HashSet<TerrainChangeListener>()) {
                    listener.notifyTerrainChanged(x, y, z);
                }
            }
        });
        World.getInstance().setConfig(config);

        assertEquals(World.getInstance().getCubeByKey(0,0,0),workClass.getWorkCube());

    }

    @Test
    public void testGetWorkCube_noInterestingCubes() throws Exception{
        //Create an empty World, only air cubes present
        int[][][] config = {//Global array
                {//x=0
                        {//y = 0
                                0,//z = 0
                                0,
                                0
                        },
                        {0,0,0},
                        {0,0,0}
                },
                {//x=1
                        {0,0,0},{0,0,0},{0,0,0}
                },
                {//x=2
                        {0,0,0},{0,0,0},{0,0,0}
                }
        };

        World.resetInstance();
        World.getInstance().setTerrainChangeListener(new TerrainChangeListener() {

            @Override
            public void notifyTerrainChanged(int x, int y, int z) {
                for (TerrainChangeListener listener : new HashSet<TerrainChangeListener>()) {
                    listener.notifyTerrainChanged(x, y, z);
                }
            }
        });
        World.getInstance().setConfig(config);

        assertEquals(workUnit.getCurrentCube(),workClass.getWorkCube());

    }
}