package hillbillies.tests.custom;

import hillbillies.model.Unit;
import hillbillies.model.World;
import hillbillies.part2.listener.TerrainChangeListener;
import hillbillies.part3.programs.SourceLocation;
import hillbillies.scheduler.memory.exceptions.DynamicExpressionException;
import hillbillies.scheduler.memory.expression.model.interfaces.EUnit;
import hillbillies.scheduler.memory.expression.model.position.PPostionOf;
import hillbillies.scheduler.memory.expression.model.unit.UAny;
import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;

import static org.junit.Assert.*;

/**
 * Test case for PPositionOf
 * @version 1.0
 * @author Maxim Verbiest
 */
public class PPostionOfTest {

    private static Unit testUnit;
    private static Unit anyUnit;
    private static UAny uAnyUnit;
    private static PPostionOf testPos;

    @Before
    public void setUp() throws Exception {
        //Init config
        int[][][] config = {//Global array
                {//x=0
                        {//y = 0
                                0,//z = 0
                                0,
                                0
                        },
                        {2,0,0}, //At (0,1,0) a Tree is planted, this will be chopped to a log
                        {0,0,0}
                },
                {//x=1
                        {0,0,0},{0,0,0},{0,0,0}
                },
                {//x=2
                        {0,0,0},{0,0,0},{0,0,0}
                }
        };

        World.resetInstance();
        World.getInstance().setTerrainChangeListener(new TerrainChangeListener() {

            @Override
            public void notifyTerrainChanged(int x, int y, int z) {
                for (TerrainChangeListener listener : new HashSet<TerrainChangeListener>()) {
                    listener.notifyTerrainChanged(x, y, z);
                }
            }
        });
        World.getInstance().setConfig(config);

        testUnit = new Unit("Tester",2,2,0,75,75,75,75,false);
        anyUnit = new Unit("Any",1,1,0,75,75,75,75,false);

        uAnyUnit = new UAny(new SourceLocation(11,1));
        testPos = new PPostionOf(uAnyUnit,new SourceLocation(22,2));


    }

    @Test
    public void testExecute() throws Exception {

        assertTrue(testPos.asPosition(testUnit).equals(anyUnit.getCurrentPosition().toCubePosition()));

    }
}