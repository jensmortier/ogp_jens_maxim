package hillbillies.tests.custom;

import hillbillies.model.Unit;
import hillbillies.model.World;
import hillbillies.part2.listener.TerrainChangeListener;
import hillbillies.part3.programs.SourceLocation;
import hillbillies.scheduler.memory.expression.bool.operations.executors.position.OIsSolid;
import hillbillies.scheduler.memory.expression.model.position.PLiteral;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;

import static org.junit.Assert.*;

/**
 * Test case for OIsSolid
 * @version 1.0
 * @author Maxim Verbiest
 */
public class OIsSolidTest {

    private static Unit testUnit;

    @Before
    public void setUp() throws Exception {
        //Init config
        int[][][] config = {//Global array
                {//x=0
                        {//y = 0
                                0,//z = 0
                                0,
                                0
                        },
                        {3,0,0},
                        {1,0,0}
                },
                {//x=1
                        {2,0,0},{0,0,0},{3,0,0}
                },
                {//x=2
                        {1,0,0},{3,0,0},{1,0,0}
                }
        };

        World.resetInstance();
        World.getInstance().setTerrainChangeListener(new TerrainChangeListener() {

            @Override
            public void notifyTerrainChanged(int x, int y, int z) {
                for (TerrainChangeListener listener : new HashSet<TerrainChangeListener>()) {
                    listener.notifyTerrainChanged(x, y, z);
                }
            }
        });
        World.getInstance().setConfig(config);

        testUnit = new Unit("Maxim",0.0,0.0,0.0,75,75,75,60,false);
    }

    @After
    public void tearDown() throws Exception {
        testUnit.terminate();
    }

    @Test
    public void testExecute_AIR() throws Exception {

        PLiteral airPos = new PLiteral(0,0,0,new SourceLocation(55,5));
        OIsSolid solidBool = new OIsSolid(airPos, new SourceLocation(66,6));

        assertFalse(solidBool.asBoolean(testUnit));
    }

    @Test
    public void testExecute_ROCK() throws Exception {
        PLiteral rockPos = new PLiteral(0,2,0, new SourceLocation(77,7));
        OIsSolid solidBool = new OIsSolid(rockPos,new SourceLocation(88,8));

        assertTrue(solidBool.asBoolean(testUnit));
    }
}