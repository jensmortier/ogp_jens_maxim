package hillbillies.tests.custom;

import hillbillies.model.Unit;
import hillbillies.model.World;
import hillbillies.part2.listener.TerrainChangeListener;
import hillbillies.part3.programs.SourceLocation;
import hillbillies.scheduler.memory.expression.bool.operations.executors.position.OIsPassable;
import hillbillies.scheduler.memory.expression.model.position.PHere;
import hillbillies.scheduler.memory.expression.model.position.PLiteral;
import hillbillies.scheduler.memory.expression.model.position.PLog;
import hillbillies.scheduler.memory.expression.model.position.PNextTo;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;

import static org.junit.Assert.*;

/**
 * Test class for OIsPassable
 * @version 1.0
 * @author Maxim Verbiest
 */
public class OIsPassableTest {

    private static Unit testUnit;

    private static PLog testLog;

    private static PHere testHere;

    @Before
    public void setUp() throws Exception {

        //Init config
        int[][][] config = {//Global array
                {//x=0
                        {//y = 0
                                1,//z = 0
                                0,
                                0
                        },
                        {3,0,0},
                        {1,0,0}
                },
                {//x=1
                        {2,0,0},{0,0,0},{3,0,0}
                },
                {//x=2
                        {1,0,0},{3,0,0},{1,0,0}
                }
        };

        World.resetInstance();
        World.getInstance().setTerrainChangeListener(new TerrainChangeListener() {

            @Override
            public void notifyTerrainChanged(int x, int y, int z) {
                for (TerrainChangeListener listener : new HashSet<TerrainChangeListener>()) {
                    listener.notifyTerrainChanged(x, y, z);
                }
            }
        });
        World.getInstance().setConfig(config);

        testLog = new PLog(new SourceLocation(5,5));

        testUnit = new Unit("Maxim",0.0,0.0,1.5,75,75,75,75,false);

        testHere = new PHere(new SourceLocation(66,6));

    }

    @After
    public void tearDown() throws Exception {
        testUnit.terminate();
        testLog = null;
        testHere = null;
    }

    @Test
    public void testExecute_AIR() throws Exception {
        OIsPassable passableBool = new OIsPassable(testLog,new SourceLocation(7,99));
        passableBool.setOperand(testHere);

        assertTrue(passableBool.asBoolean(testUnit));

    }

    @Test
    public void testExecute_ROCK() throws Exception{

        PLiteral rock = new PLiteral(0,2,0,new SourceLocation(44,5));
        OIsPassable passableBool = new OIsPassable(testLog,new SourceLocation(7,99));
        passableBool.setOperand(rock);
        assertFalse(passableBool.asBoolean(testUnit));

    }
}