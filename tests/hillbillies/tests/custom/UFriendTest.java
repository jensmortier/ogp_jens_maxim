package hillbillies.tests.custom;

import hillbillies.model.Faction;
import hillbillies.model.Unit;
import hillbillies.model.World;
import hillbillies.part2.listener.TerrainChangeListener;
import hillbillies.part3.programs.SourceLocation;
import hillbillies.scheduler.memory.expression.model.unit.UEnemy;
import hillbillies.scheduler.memory.expression.model.unit.UFriend;
import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;

import static org.junit.Assert.*;

/**
 * Test case for UFriend
 * @version 1.0
 * @author Maxim Verbiest
 */
public class UFriendTest {

    private static Unit referenceUnit;
    private static Unit friendlyUnit;
    private static Unit enemyUnit;
    private static Unit Unit2;
    private static Faction faction1;
    private static Faction faction2;
    private static Faction faction3;

    @Before
    public void setUp() throws Exception {
        //Init config
        int[][][] config = {//Global array
                {//x=0
                        {//y = 0
                                0,//z = 0
                                0,
                                0
                        },
                        {0,0,0},
                        {0,0,0}
                },
                {//x=1
                        {0,0,0},{0,0,0},{0,0,0}
                },
                {//x=2
                        {0,0,0},{0,0,0},{0,0,0}
                }
        };

        World.resetInstance();
        World.getInstance().setTerrainChangeListener(new TerrainChangeListener() {

            @Override
            public void notifyTerrainChanged(int x, int y, int z) {
                for (TerrainChangeListener listener : new HashSet<TerrainChangeListener>()) {
                    listener.notifyTerrainChanged(x, y, z);
                }
            }
        });
        World.getInstance().setConfig(config);

        referenceUnit = new Unit("Reference",0,0,0,75,75,75,75,false);
        friendlyUnit  = new Unit("Friend",1,0,0,75,75,75,75,false);
        enemyUnit = new Unit("Enemy1",0,1,0,75,75,75,75,false);
        Unit2 = new Unit("Friend2",1,1,0,75,75,75,75,false);

        faction1 = new Faction();
        faction1.addUnit(referenceUnit);
        faction1.addUnit(friendlyUnit);
        faction1.addUnit(Unit2);

        faction2 = new Faction();
        faction2.addUnit(enemyUnit);
    }

    @Test
    public void testExecute() throws Exception {
        UFriend ufriend = new UFriend(new SourceLocation(11,1));
        HashSet<Unit> unitSet = new HashSet<>(3);
        unitSet.add(friendlyUnit);
        unitSet.add(enemyUnit);
        unitSet.add(Unit2);

        referenceUnit.setCurrentCube(World.getInstance().getCubeByKey(0,0,0));
        friendlyUnit.setCurrentCube(World.getInstance().getCubeByVector(friendlyUnit.getCurrentPosition().toCubePosition().toVector()));
        enemyUnit.setCurrentCube(World.getInstance().getCubeByVector(enemyUnit.getCurrentPosition().toCubePosition().toVector()));
        Unit2.setCurrentCube(World.getInstance().getCubeByVector(Unit2.getCurrentPosition().toCubePosition().toVector()));

        assertEquals(ufriend.asUnit(referenceUnit),friendlyUnit);
    }
}