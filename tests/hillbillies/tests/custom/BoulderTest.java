package hillbillies.tests.custom;

import hillbillies.gameworld.Position;
import hillbillies.model.Boulder;
import hillbillies.model.Unit;
import hillbillies.model.World;
import hillbillies.part2.listener.TerrainChangeListener;
import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;

import static org.junit.Assert.*;

/**
 * Test case for Boulder
 * @author Jens Mortier
 * @version 1.0
 */
public class BoulderTest {

    Boulder testBoulder;
    Unit interactingUnit;

    @Before
    public void setUp() throws Exception {
        //Init config
        int[][][] config = {//Global array
                {//x=0
                        {//y = 0
                                0,//z = 0
                                0,
                                0
                        },
                        {0,0,0},
                        {0,0,0}
                },
                {//x=1
                        {0,0,0},{0,0,0},{0,0,0}
                },
                {//x=2
                        {0,0,0},{0,0,0},{0,0,0}
                }
        };

        World.resetInstance();
        World.getInstance().setTerrainChangeListener(new TerrainChangeListener() {

            @Override
            public void notifyTerrainChanged(int x, int y, int z) {
                for (TerrainChangeListener listener : new HashSet<TerrainChangeListener>()) {
                    listener.notifyTerrainChanged(x, y, z);
                }
            }
        });
        World.getInstance().setConfig(config);

        testBoulder = new Boulder(new Position<Double>(0.0,0.0,0.0), World.getInstance().getCubeByKey(0,0,0));
        interactingUnit = new Unit("Carrier",0.0,0.0,0.0,50,50,50,50,false);
    }

    @Test
    public void testPickUpMaterial() throws Exception {

        int weight = interactingUnit.getWeight();
        testBoulder.pickUpMaterial(interactingUnit);

        assertTrue(interactingUnit.isCarryingBoulder());

        assertEquals(interactingUnit.getWeight() , weight + testBoulder.getWeight());

        assertEquals(interactingUnit.getBoulder(),testBoulder);

    }

    @Test
    public void testDropMaterial() throws Exception {
        int weight = interactingUnit.getWeight();
        testBoulder.pickUpMaterial(interactingUnit);
        testBoulder.dropMaterial(interactingUnit);

        assertEquals(weight,interactingUnit.getWeight());

        assertEquals(testBoulder.isCurrentlyCarried(),false);

        assertEquals(interactingUnit.isCarryingBoulder(),false);
    }
}