package hillbillies.tests.custom;

import hillbillies.exceptions.FactionException;
import hillbillies.model.Faction;
import hillbillies.model.Unit;
import hillbillies.model.World;
import hillbillies.part2.listener.TerrainChangeListener;
import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;

import static org.junit.Assert.*;

/**
 * Test case for Faction
 * @author Jens Mortier
 * @version 1.0
 */
public class FactionTest {

    private Faction faction;
    private Unit example_unit;

    @Before @Test
    public void setUp() throws Exception {
        //Init world
        int[][][] config = {//Global array
                {//x=0
                        {//y = 0
                                0,//z = 0
                                0,
                                0
                        },
                        {0,0,0},
                        {0,0,0}
                },
                {//x=1
                        {0,0,0},{0,0,0},{0,0,0}
                },
                {//x=2
                        {0,0,0},{0,0,0},{0,0,0}
                }
        };


        World.getInstance().setTerrainChangeListener(new TerrainChangeListener() {

            @Override
            public void notifyTerrainChanged(int x, int y, int z) {
                for (TerrainChangeListener listener : new HashSet<TerrainChangeListener>()) {
                    listener.notifyTerrainChanged(x, y, z);
                }
            }
        });
        World.getInstance().setConfig(config);
        //Init variables
        faction = new Faction();
        World.getInstance().getFactions().contains(faction);
        example_unit = new Unit("Test",1,1,1,1,1,1,1,false);
    }

    @Test
    public void testTerminate() throws Exception {
        faction.terminate();
        assertTrue(faction.isTerminated());
        assertFalse(World.getInstance().getFactions().contains(faction));
    }

    @Test
    public void testTerminate_with_units() throws Exception {
        faction.addUnit(example_unit);
        faction.terminate();
        assertTrue(example_unit.isTerminated());
    }

    @Test
    public void testGetUnits() throws Exception {
        faction.addUnit(example_unit);
        assertEquals(faction.getUnits().size(), 1);
        assertTrue(faction.getUnits().contains(example_unit));
    }

    @Test
    public void testUnitsToSet() throws Exception {
        faction.addUnit(example_unit);
        assertTrue(faction.unitsToSet().containsAll(faction.getUnits()));
    }

    @Test
    public void testGetNbOfUnits() throws Exception {
        faction.addUnit(example_unit);
        assertEquals(faction.getNbOfUnits(), 1);
    }

    @Test
    public void testAddUnit_regular() throws Exception {
        faction.addUnit(example_unit);
        assertEquals("Number of units raised", faction.getNbOfUnits(), 1);
        assertTrue("Faction contains unit", faction.getUnits().contains(example_unit));
        assertTrue("Unit is set to faction", example_unit.getFaction() == faction);
    }

    @Test(expected = FactionException.class)
    public void testAddUnit_terminated() throws Exception {
        example_unit.terminate();
        faction.addUnit(example_unit);
    }

    @Test(expected = FactionException.class)
    public void testAddUnit_null() throws Exception {
        faction.addUnit(null);
    }

    @Test(expected = FactionException.class)
    public void testAddUnit_terminated_faction() throws Exception {
        faction.terminate();
        faction.addUnit(example_unit);
    }

    @Test(expected = FactionException.class)
    public void testAddUnit_nb_overflow() throws Exception {
        int counter = 0;
        while(counter <= Faction.MAX_NUMBER_OF_UNITS + 1) {
            Unit cu = new Unit("Te",1,1,1,1,1,1,1,false);
            faction.addUnit(cu);
            counter++;
        }
    }

    @Test
    public void testGetUnitAt() throws Exception {
        faction.addUnit(example_unit);
        assertEquals(faction.getUnitAt(0),example_unit);
    }

    @Test
    public void testRemoveUnit() throws Exception {
        faction.addUnit(example_unit);
        example_unit.transferToFaction(new Faction());
    }

    @Test(expected = FactionException.class)
    public void testRemoveUnit_not_transferred() throws Exception {
        faction.addUnit(example_unit);
        faction.removeUnit(example_unit);
        assertFalse(faction.getUnits().contains(example_unit));
    }
}