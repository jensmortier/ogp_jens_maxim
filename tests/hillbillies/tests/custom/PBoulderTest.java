package hillbillies.tests.custom;

import hillbillies.gameworld.Position;
import hillbillies.model.Boulder;
import hillbillies.model.Unit;
import hillbillies.model.World;
import hillbillies.part2.listener.TerrainChangeListener;
import hillbillies.part3.programs.SourceLocation;
import hillbillies.scheduler.memory.expression.model.position.PBoulder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;

import static org.junit.Assert.*;

/**
 * Test case ofr PBoulder
 * @version 1.0
 * @author Maxim Verbiest
 */
public class PBoulderTest {

    private static Unit testUnit ;

    private static Boulder testBoulder;

    @Before
    public void setUp() throws Exception {
        //Init config
        int[][][] config = {//Global array
                {//x=0
                        {//y = 0
                                0,//z = 0
                                0,
                                0
                        },
                        {3,0,0},
                        {1,0,0}
                },
                {//x=1
                        {0,0,0},{0,0,0},{3,0,0}
                },
                {//x=2
                        {1,0,0},{3,0,0},{1,0,0}
                }
        };

        //Cube at (2,0,0) is air, a boulder will be added here

        World.resetInstance();
        World.getInstance().setTerrainChangeListener(new TerrainChangeListener() {

            @Override
            public void notifyTerrainChanged(int x, int y, int z) {
                for (TerrainChangeListener listener : new HashSet<TerrainChangeListener>()) {
                    listener.notifyTerrainChanged(x, y, z);
                }
            }
        });
        World.getInstance().setConfig(config);

        testUnit = new Unit("Maxim",0.0,0.0,0.0,75,75,75,75,false);

        testBoulder = new Boulder(new Position(2.0,0.0,0.0),World.getInstance().getCubeByKey(2,0,0));

        World.getInstance().advanceTime(22);
    }

    @After
    public void tearDown() throws Exception {
        testUnit.terminate();
        testBoulder.terminate();
    }

    @Test
    public void testExecute() throws Exception {
        PBoulder pboulder = new PBoulder(new SourceLocation(11,1));
        assertTrue(testBoulder.getPosition().equals(pboulder.asPosition(testUnit)));
    }
}