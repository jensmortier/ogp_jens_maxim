package hillbillies.tests.custom;

import hillbillies.gameworld.Position;
import hillbillies.model.Unit;
import hillbillies.model.World;
import hillbillies.part2.listener.TerrainChangeListener;
import hillbillies.part3.programs.SourceLocation;
import hillbillies.scheduler.memory.expression.model.position.PHere;
import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;

import static org.junit.Assert.*;

/**
 * Test case for PHere
 * @version 1.0
 * @author Maxim Verbiest
 */
public class PHereTest {

    private static Unit testUnit ;
    private static PHere herePos;

    @Before
    public void setUp() throws Exception {

        //Init config
        int[][][] config = {//Global array
                {//x=0
                        {//y = 0
                                0,//z = 0
                                0,
                                0
                        },
                        {0,0,0},
                        {0,0,0}
                },
                {//x=1
                        {0,0,0},{0,0,0},{0,0,0}
                },
                {//x=2
                        {0,0,0},{0,0,0},{0,0,0}
                }
        };

        World.resetInstance();
        World.getInstance().setTerrainChangeListener(new TerrainChangeListener() {

            @Override
            public void notifyTerrainChanged(int x, int y, int z) {
                for (TerrainChangeListener listener : new HashSet<TerrainChangeListener>()) {
                    listener.notifyTerrainChanged(x, y, z);
                }
            }
        });
        World.getInstance().setConfig(config);

        testUnit = new Unit("I'll test",0,0,0,75,75,75,75,false);
        herePos = new PHere(new SourceLocation(11,1));
    }

    @Test
    public void testExecute() throws Exception {
        assertTrue(herePos.asPosition(testUnit).equals(testUnit.getCurrentPosition()));
        assertFalse(herePos.asPosition(testUnit).equals(new Position<Integer>(0,2,1)));
    }
}