package hillbillies.tests.custom;

import hillbillies.model.Unit;
import hillbillies.model.World;
import hillbillies.part2.listener.TerrainChangeListener;
import hillbillies.part3.programs.SourceLocation;
import hillbillies.scheduler.memory.expression.bool.operations.executors.unit.OIsAlive;
import hillbillies.scheduler.memory.expression.model.unit.UThis;
import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;

import static org.junit.Assert.*;

/**
 * Test case for OIsAlive
 * @version 1.0
 * @author Maxim Verbiest
 */
public class OIsAliveTest {

    private static UThis uThis;
    private static Unit testUnit;
    private static OIsAlive oIsAlive;

    @Before
    public void setUp() throws Exception {

        //Init config
        int[][][] config = {//Global array
                {//x=0
                        {//y = 0
                                0,//z = 0
                                0,
                                0
                        },
                        {0,0,0},
                        {0,0,0}
                },
                {//x=1
                        {1,0,0},{0,0,0},{0,0,0}
                },
                {//x=2
                        {0,0,0},{0,0,0},{0,0,0}
                }
        };

        World.resetInstance();
        World.getInstance().setTerrainChangeListener(new TerrainChangeListener() {

            @Override
            public void notifyTerrainChanged(int x, int y, int z) {
                for (TerrainChangeListener listener : new HashSet<TerrainChangeListener>()) {
                    listener.notifyTerrainChanged(x, y, z);
                }
            }
        });
        World.getInstance().setConfig(config);

        uThis = new UThis(new SourceLocation(11,1));
        oIsAlive = new OIsAlive(uThis,new SourceLocation(22,2));
        testUnit = new Unit("Tester",0,0,0,75,75,75,75,false);

    }

    @Test
    public void testExecute() throws Exception {
        assertTrue(oIsAlive.asBoolean(testUnit));

        //Let the unit die
        testUnit.terminate();
        assertFalse(oIsAlive.asBoolean(testUnit));
    }
}