package hillbillies.tests.custom;

import hillbillies.gameworld.Position;
import hillbillies.model.Log;
import hillbillies.model.Unit;
import hillbillies.model.World;
import hillbillies.part2.listener.TerrainChangeListener;
import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;

import static org.junit.Assert.*;

/**
 * Test case for Log
 * @author Maxim Verbiest
 * @version 1.0
 */
public class LogTest {

    Log testLog;
    Unit interactingUnit;

    @Before
    public void setUp() throws Exception {

        //Init config
        int[][][] config = {//Global array
                {//x=0
                        {//y = 0
                                0,//z = 0
                                0,
                                0
                        },
                        {0,0,0},
                        {0,0,0}
                },
                {//x=1
                        {0,0,0},{0,0,0},{0,0,0}
                },
                {//x=2
                        {0,0,0},{0,0,0},{0,0,0}
                }
        };

        World.resetInstance();
        World.getInstance().setTerrainChangeListener(new TerrainChangeListener() {

            @Override
            public void notifyTerrainChanged(int x, int y, int z) {
                for (TerrainChangeListener listener : new HashSet<TerrainChangeListener>()) {
                    listener.notifyTerrainChanged(x, y, z);
                }
            }
        });
        World.getInstance().setConfig(config);

        testLog = new Log(new Position<Double>(0.0,0.0,0.0), World.getInstance().getCubeByKey(0,0,0));
        interactingUnit = new Unit("Carrier",0.0,0.0,0.0,50,50,50,50,false);

    }

    @Test
    public void testPickUpMaterial() throws Exception {
        int weight = interactingUnit.getWeight();
        testLog.pickUpMaterial(interactingUnit);

        assertTrue(interactingUnit.isCarryingLog());

        assertEquals(interactingUnit.getWeight() , weight + testLog.getWeight());

        assertEquals(interactingUnit.getLog(),testLog);
    }

    @Test
    public void testDropMaterial() throws Exception {
        int weight = interactingUnit.getWeight();
        testLog.pickUpMaterial(interactingUnit);
        testLog.dropMaterial(interactingUnit);

        assertEquals(weight,interactingUnit.getWeight());

        assertEquals(testLog.isCurrentlyCarried(),false);

        assertEquals(interactingUnit.isCarryingLog(),false);
    }
}