package hillbillies.tests.custom;

import hillbillies.exceptions.GameObjectException;
import hillbillies.gameworld.Cube;
import hillbillies.gameworld.GameObject;
import hillbillies.gameworld.Position;
import hillbillies.gameworld.SceneryType;
import hillbillies.model.World;
import hillbillies.part2.listener.TerrainChangeListener;
import org.junit.Before;
import org.junit.Test;

import java.util.DoubleSummaryStatistics;
import java.util.HashSet;

import static org.junit.Assert.*;

/**
 * Test case for gameObject
 * @author Jens Mortier
 * @version 1.O
 */
public class GameObjectTest {

    private GameObject object;

    @Before
    public void setUp() throws Exception {
        //Init config
        int[][][] config = {//Global array
                {//x=0
                        {//y = 0
                                0,//z = 0
                                0,
                                0
                        },
                        {0,0,0},
                        {0,0,0}
                },
                {//x=1
                        {0,0,0},{0,0,0},{0,0,0}
                },
                {//x=2
                        {0,0,0},{0,0,0},{0,0,0}
                }
        };
        World.resetInstance();
        World.getInstance().setTerrainChangeListener(new TerrainChangeListener() {

            @Override
            public void notifyTerrainChanged(int x, int y, int z) {
                for (TerrainChangeListener listener : new HashSet<TerrainChangeListener>()) {
                    listener.notifyTerrainChanged(x, y, z);
                }
            }
        });
        World.getInstance().setConfig(config);
        object = new GameObject(new Cube(SceneryType.AIR,new Position<Integer>(1,1,1))) {
            @Override
            public void advanceTime(double gameTime) throws Exception {

            }
        };
    }

    @Test
    public void testSetCurrentCube() throws Exception {
        object.setCurrentCube(new Cube(SceneryType.AIR, new Position<Integer>(0, 0, 1)));
        assertEquals(new Integer(1),object.getCurrentCube().getPosition().getPosition_z());
    }

    @Test(expected = GameObjectException.class)
    public void testSetCurrentCube_null() throws Exception {
        object.setCurrentCube(null);
    }

    @Test(expected = GameObjectException.class)
    public void testSetCurrentCube_terminated() throws Exception {
        Cube cube = new Cube(SceneryType.AIR, new Position<Integer>(0, 0, 1));
        cube.terminate();
        object.setCurrentCube(cube);
    }

    @Test
    public void testTransferToCube() throws Exception {
        object.transferToCube(new Cube(SceneryType.AIR,new Position<Integer>(0,0,1)));
        assertEquals(new Integer(1), object.getCurrentCube().getPosition().getPosition_z());
    }

    @Test
    public void testTerminate() throws Exception {
        object.terminate();
        assertTrue(object.isTerminated());
    }
}