package hillbillies.tests.custom;

import hillbillies.exceptions.SchedulerException;
import hillbillies.model.Faction;
import hillbillies.model.Unit;
import hillbillies.model.World;
import hillbillies.part2.listener.TerrainChangeListener;
import hillbillies.part3.programs.SourceLocation;
import hillbillies.scheduler.execution.components.Scheduler;
import hillbillies.scheduler.execution.components.Task;
import hillbillies.scheduler.memory.expression.bool.EBoolean;
import hillbillies.scheduler.memory.expression.bool.operations.executors.base.EBooleanNOT;
import hillbillies.scheduler.memory.statement.io.SPrint;
import hillbillies.scheduler.memory.statement.language.SIf;
import org.junit.Before;
import org.junit.Test;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;

import static org.junit.Assert.*;

/**
 * Test class for Scheduler
 * @version 1.0
 * @author Maxim Verbiest
 */
public class SchedulerTest {

    private static Faction faction;
    private static Scheduler scheduler;

    //Components to create a Task
    private static int[] selectedCube = {1,1,1};
    private static Task managedTask;

    private static Unit scheduleUnit;

    @Before
    public void setUp() throws Exception {

        //Init config
        int[][][] config = {//Global array
                {//x=0
                        {//y = 0
                                0,//z = 0
                                0,
                                0
                        },
                        {1,0,0},
                        {0,0,0}
                },
                {//x=1
                        {0,0,0},{0,0,0},{0,0,0}
                },
                {//x=2
                        {0,0,0},{0,0,0},{0,0,0}
                }
        };

        World.resetInstance();
        World.getInstance().setTerrainChangeListener(new TerrainChangeListener() {

            @Override
            public void notifyTerrainChanged(int x, int y, int z) {
                for (TerrainChangeListener listener : new HashSet<TerrainChangeListener>()) {
                    listener.notifyTerrainChanged(x, y, z);
                }
            }
        });
        World.getInstance().setConfig(config);

        faction = new Faction();
        scheduler = new Scheduler(faction);

        //all the work we have to do to create a Task
        EBoolean managedEBool = new EBoolean(new SourceLocation(11, 1), true);
        SPrint managedSPrint = new SPrint(new SourceLocation(33, 3), managedEBool);
        managedTask = new Task("managedByScheduler",2222,selectedCube, managedSPrint);

        //Add the task to the scheduler
        scheduler.addTask(managedTask);

        //Init Unit
        scheduleUnit = new Unit("Schedule Unit",0,0,0,75,75,75,75,false);
        faction.addUnit(scheduleUnit);
    }

    @Test
    public void testSetFaction_noNull() throws Exception {
        scheduler.setFaction(faction);
        assertEquals(faction,scheduler.getFaction());
    }

    @Test(expected = SchedulerException.class)
    public void setFaction_Null() throws Exception{
        scheduler.setFaction(null);
    }

    @Test
    public void testAddTask() throws Exception {
        //Init
        int[] selected = {0,0,0};
        EBoolean eBoolTest = new EBoolean(new SourceLocation(11,1),false);
        EBooleanNOT eBoolNotTest= new EBooleanNOT(eBoolTest,new SourceLocation(22,2));
        SPrint testPrint = new SPrint(new SourceLocation(11,1),eBoolNotTest);
        Task taskToAdd = new Task("testTask",50,selected,testPrint);

        scheduler.addTask(taskToAdd);
        assertTrue(scheduler.getTasksList().contains(taskToAdd));
    }

    @Test
    public void addTask_nothingDone() throws Exception{
        //Init
        int[] selected = {0,0,0};
        EBoolean eBoolTest = new EBoolean(new SourceLocation(11,1),false);
        EBooleanNOT eBoolNotTest= new EBooleanNOT(eBoolTest,new SourceLocation(22,2));
        SPrint testPrint = new SPrint(new SourceLocation(11,1),eBoolNotTest);
        Task taskToAdd = new Task("testTask",50,selected,testPrint);

        //Add this task to the scheduler
        scheduler.addTask(taskToAdd);

        //add the same task again, this should have no effect
        scheduler.addTask(taskToAdd);
        assertEquals(2,scheduler.getTasksList().size());

        //add a task with null references
        scheduler.addTask(null);
        assertEquals(2, scheduler.getTasksList().size());

    }

    @Test
    public void testRemoveTask() throws Exception {
        //Init
        int[] selected = {0,0,0};
        EBoolean eBoolTest = new EBoolean(new SourceLocation(11,1),false);
        EBooleanNOT eBoolNotTest= new EBooleanNOT(eBoolTest,new SourceLocation(22,2));
        SPrint testPrint = new SPrint(new SourceLocation(11,1),eBoolNotTest);
        Task taskToAdd = new Task("testTask",50,selected,testPrint);

        //Add this task to the scheduler
        scheduler.addTask(taskToAdd);

        scheduler.removeTask(taskToAdd);
        assertFalse(scheduler.getTasksList().contains(taskToAdd));
    }

    @Test
    public void removeTask_TaskNotManagedByScheduler() throws Exception{
        //Init
        int[] selected = {0,0,0};
        EBoolean eBoolTest = new EBoolean(new SourceLocation(11,1),false);
        EBooleanNOT eBoolNotTest= new EBooleanNOT(eBoolTest,new SourceLocation(22,2));
        SPrint testPrint = new SPrint(new SourceLocation(11,1),eBoolNotTest);
        Task taskToAdd = new Task("testTask",50,selected,testPrint);

        //Add this task to the scheduler
        scheduler.addTask(taskToAdd);

        //create another task
        int[] selected2 = {1,1,1};
        EBoolean eBoolTest2 = new EBoolean(new SourceLocation(33,3),true);
        EBooleanNOT eBooleanNOTTest2 = new EBooleanNOT(eBoolTest2, new SourceLocation(44,4));
        SPrint testPrint2 = new SPrint(new SourceLocation(55,5),eBooleanNOTTest2);
        Task notManaged = new Task("notManaged",44,selected2,testPrint2);

        scheduler.removeTask(notManaged);
        assertEquals(2, scheduler.getTasksList().size());
    }

    @Test
    public void testIsTaskPartOf() throws Exception {
        //Init
        int[] selected = {0,0,0};
        EBoolean eBoolTest = new EBoolean(new SourceLocation(11,1),false);
        EBooleanNOT eBoolNotTest= new EBooleanNOT(eBoolTest,new SourceLocation(22,2));
        SPrint testPrint = new SPrint(new SourceLocation(11,1),eBoolNotTest);
        Task taskToAdd = new Task("testTask",50,selected,testPrint);

        //Add this task to the scheduler
        scheduler.addTask(taskToAdd);

        //create another task
        int[] selected2 = {1,1,1};
        EBoolean eBoolTest2 = new EBoolean(new SourceLocation(33,3),true);
        EBooleanNOT eBooleanNOTTest2 = new EBooleanNOT(eBoolTest2, new SourceLocation(44,4));
        SPrint testPrint2 = new SPrint(new SourceLocation(55,5),eBooleanNOTTest2);
        Task notManaged = new Task("notManaged",44,selected2,testPrint2);

        assertTrue(scheduler.isTaskPartOf(taskToAdd));
        assertFalse(scheduler.isTaskPartOf(notManaged));
    }

    @Test
    public void testAreTasksPartOf() throws Exception {
        //Init
        int[] selected = {1,1,1};
        EBoolean eBoolTest = new EBoolean(new SourceLocation(11,1),false);
        EBooleanNOT eBoolNotTest= new EBooleanNOT(eBoolTest,new SourceLocation(22,2));
        SPrint testPrint = new SPrint(new SourceLocation(11,1),eBoolNotTest);
        Task taskToAdd = new Task("testTask",50,selected,testPrint);

        //Add this task to the scheduler
        scheduler.addTask(taskToAdd);

        //Create a Collection
        LinkedList<Task> taskList = new LinkedList<>();
        taskList.add(taskToAdd);
        taskList.add(managedTask);

        assertTrue(scheduler.areTasksPartOf(taskList));

        taskList.add(null);
        assertFalse(scheduler.areTasksPartOf(taskList));
    }

    @Test
    public void testReplaceTask_correctInput() throws Exception {
        //Init
        int[] selected = {0,0,0};
        EBoolean eBoolTest = new EBoolean(new SourceLocation(11,1),false);
        EBooleanNOT eBoolNotTest= new EBooleanNOT(eBoolTest,new SourceLocation(22,2));
        SPrint testPrint = new SPrint(new SourceLocation(11,1),eBoolNotTest);
        Task replacement = new Task("testTask",50,selected,testPrint);

        scheduler.replaceTask(managedTask, replacement);

        //Check relations
        assertTrue(managedTask.getSchedulers().contains(scheduler));

        //Check if the replaced task is no part anymore
        assertTrue(scheduler.isTaskPartOf(managedTask));
    }

    @Test(expected = SchedulerException.class)
    public void replaceTask_nullValue() throws Exception{
        scheduler.replaceTask(managedTask,null);
    }

    @Test
    public void testHighestFreeTask() throws Exception {
        assertEquals(managedTask, scheduler.getHighestFreeTask());
    }

    @Test
    public void getHighestFreeTask_noFreeTasksAvailable() throws Exception {
        scheduler.removeTask(managedTask);
        assertEquals(null,scheduler.getHighestFreeTask());

    }

    @Test
    public void testCanSchedule() throws Exception {
        assertTrue(scheduler.canSchedule(scheduleUnit,managedTask));

        //Check null checkers
        assertFalse(scheduler.canSchedule(null,null));
        assertFalse(scheduler.canSchedule(null,managedTask));
        assertFalse(scheduler.canSchedule(scheduleUnit,null));

        //Check when unit has already Task assigned to it
        int[] selected = {0,0,0};
        EBoolean eBoolTest = new EBoolean(new SourceLocation(11,1),false);
        EBooleanNOT eBoolNotTest= new EBooleanNOT(eBoolTest,new SourceLocation(22,2));
        SPrint testPrint = new SPrint(new SourceLocation(11,1),eBoolNotTest);
        Task secondTask = new Task("testTask",50,selected,testPrint);

        scheduleUnit.assignTask(secondTask);
        assertFalse(scheduler.canSchedule(scheduleUnit,managedTask));

    }

    @Test
    public void testSchedule() throws Exception {
        //TODO after executor implemented
    }

    @Test
    public void testResetScheduling() throws Exception {
        //TODO after executor implemented
    }

    @Test
    public void testGetCurrentlyExecuting() throws Exception {
        //TODO after executor implemented
    }

    //TODO execute this test after PR, now every negative task will get priority 0
    public void testGetPositivePriorities() throws Exception {

        int[] selected = {0,0,0};
        EBoolean eBoolTest = new EBoolean(new SourceLocation(11,1),false);
        EBooleanNOT eBoolNotTest= new EBooleanNOT(eBoolTest,new SourceLocation(22,2));
        SPrint testPrint = new SPrint(new SourceLocation(11,1),eBoolNotTest);
        Task negativeTask = new Task("testTask",-50,selected,testPrint);

        scheduler.addTask(negativeTask);

        assertEquals(1,scheduler.getPositivePriorities().size());
        assertFalse(scheduler.getPositivePriorities().contains(negativeTask));
        assertTrue(scheduler.getPositivePriorities().contains(managedTask));

    }

    @Test
    public void testCanBeInterrupted() throws Exception {
        //TODO after executor
    }

    @Test
    public void testInterruptTask() throws Exception {
        //TODO after executor
    }

    @Test
    public void testIterator() throws Exception {
        scheduler.removeTask(managedTask);
        for(int i = 1;i <= 10;i ++) {
            scheduler.addTask(new Task("test " + Integer.toString(i),1,new int[]{1,1,1},new SPrint(null,new EBoolean(null,false))));
        }

        //Let's test it
        HashSet<Task> tasks = new HashSet<>();
        Iterator<Task> taskIterator = scheduler.iterator();
        int counter = 0;
        while(taskIterator.hasNext()) {
            Task task = taskIterator.next();
            if(tasks.contains(task)) throw new Exception("Task already returned");
            assertNotEquals(null,task);
            tasks.add(task);
            System.out.println(task.getName());
            counter++;
        }

        //Make an assertion
        assertEquals(10,counter);
    }
}