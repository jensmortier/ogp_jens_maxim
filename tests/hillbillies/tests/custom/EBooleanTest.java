package hillbillies.tests.custom;

import hillbillies.part3.programs.SourceLocation;
import hillbillies.scheduler.memory.expression.bool.EBoolean;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Test case for EBoolean
 * @version 1.0
 * @author Maxim Verbiest
 */
public class EBooleanTest {

    private static EBoolean ebool;

    @Before
    public void setUp() throws Exception {
        ebool = new EBoolean(new SourceLocation(11,1),true);
    }

    @Test
    public void testAsBoolean() throws Exception {

        assertTrue(ebool.asBoolean(null));
    }

    @Test
    public void testSetBoolean() throws Exception {
        ebool.setBoolean(false);
        assertFalse(ebool.asBoolean(null));
    }
}