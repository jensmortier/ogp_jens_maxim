package hillbillies.tests.util;

import hillbillies.util.Pair;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Jens on 10/04/2016.
 */
public class PairTest {
    Pair<String,String> testPair;

    @Before
    public void setUp() throws Exception {
        testPair = new Pair<>("T1","T2");
    }

    @Test
    public void testGetFirstValue() throws Exception {
        assertEquals(testPair.getFirstValue(),"T1");
    }

    @Test
    public void testGetSecondValue() throws Exception {
        assertEquals(testPair.getSecondValue(),"T2");
    }
}